<?php
​
namespace App\Actions;
​
use TCG\Voyager\Actions\AbstractAction;
​
class OrderSupplierAction extends AbstractAction
{
    public function getTitle()
    {
        return 'My Action';
    }
​
    public function getIcon()
    {
        return 'voyager-eye';
    }
​
    public function getPolicy()
    {
        return 'read';
    }
​
    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-right',
        ];
    }
​
    public function getDefaultRoute()
    {
        return route('my.route');
    }

    public function shouldActionDisplayOnDataType()
    {
    return $this->dataType->slug == 'pos-suppliers';
    }
}
