<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TbAccountingCategory extends Model
{
    protected $table = 'tb_accounting_categories';

    public function bookkeeping_categories()
    {
        return $this->hasMany('App\TbBookkeepingAccountingCategory','accounting_category_id','id');
    }

    public function subcategories()
    {
        return $this->hasMany('App\TbAccountingCategory','parent_id','id');
    }

    public function parent_category()
    {
        return $this->belongsTo('App\TbAccountingCategory','parent_id','id');
    }

    public function scopeTopLevel($query)
    {
        return $query->where('level',1);
    }

}
