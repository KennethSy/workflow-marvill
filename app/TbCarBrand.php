<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TbCarBrand extends Model
{
    public function models()
    {
        return $this->hasMany('App\TbCarModel', 'brand_id');
    }

    public function scopeEnabled($query)
    {
        return $query->where('status', '=', 1);
    }
}
