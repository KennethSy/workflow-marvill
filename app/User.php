<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'provider', 'provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeActive($query)
    {
        return $query->where('users.is_active', 1);
    }

    public function scopeSuperAdmin($query)
    {
        return $query->where('users.role_id', 3);
    }

    public function scopeAdmin($query)
    {
        return $query->where('users.role_id', 1);
    }

    public function scopeEmployee($query)
    {
        return $query->where('users.role_id', 4)->orWhere('users.role_id', 19);
    }

    public function scopeGetAllActiveEmployeesAndAdmins()
    {
        return $this->active()
            ->where(function ($query) {
                $query->where(function ($q) {
                    $q->superAdmin();
                })->orWhere(function ($q) {
                    $q->admin();
                })->orWhere(function ($q) {
                    $q->employee();
                });
            })
            ->get();
    }

    public function scopeGetAllActiveEmployees()
    {
        return $this->active()
            ->employee()
            ->get();
    }
}
