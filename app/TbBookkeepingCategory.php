<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TbBookkeepingCategory extends Model
{
    protected $table = 'tb_bookkeeping_categories';
}
