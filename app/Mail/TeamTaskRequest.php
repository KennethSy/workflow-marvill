<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TeamTaskRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name;
    public $company;
    public $project;
    public $details;
    public $task_link;

    public function __construct($request_task)
    {
        $this->name = $request_task->user;
        $this->company = $request_task->company;
        $this->project = $request_task->project;
        $this->details = nl2br($request_task->description);
        $this->task_link = route('voyager.request-tasks.show',['id'=>$request_task->id]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('maileclipse::templates.teamTaskRequest')
            ->from('notifications@marvill.com')
            ->subject('New Request Task Notification');
    }
}
