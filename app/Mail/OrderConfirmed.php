<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderConfirmed extends Mailable
{
    use Queueable, SerializesModels;

    public $date;
    public $name;
    public $street;
    public $barangay;
    public $city;
    public $number;
    public $order;
    public $qty;




    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order_confirmation, $userinfo, $useraddress)
    {
        $this->date = $order_confirmation->created_at->toFormattedDateString();
        $this->name = $userinfo->name;
        $this->street = $useraddress->user_address;
        $this->barangay = $useraddress->user_barangay;
        $this->city = $useraddress->user_city;
        $this->number = $useraddress->user_number;
        $this->order = json_decode($order_confirmation->order);
        $this->qty = json_decode($order_confirmation->qty);


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('maileclipse::templates.orderConfirmed')
        ->from('notifications@marvill.com')
        ->subject('Order Confirmation');






    }
}
