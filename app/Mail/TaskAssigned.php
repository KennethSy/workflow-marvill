<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TaskAssigned extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $company;
    public $project;
    public $details;
    public $task_link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request_task)
    {
        $this->name = $request_task->user;
        $this->company = $request_task->company;
        $this->project = $request_task->project;
        $this->details = nl2br($request_task->description);
        $this->task_link = route('voyager.dailytask.show',['id'=>$request_task->id]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('maileclipse::templates.taskAssigned')
            ->from('notifications@marvill.com')
            ->subject('A Task has been assigned to you');
    }
}
