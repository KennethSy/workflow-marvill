<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Inquiry extends Mailable
{
    use Queueable, SerializesModels;

    public $first_name;
    public $last_name;
    public $email;
    public $contact_number;
    public $details;
    public $inquiry_link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inquiry)
    {
        $this->first_name = $inquiry->first_name;
        $this->last_name = $inquiry->last_name;
        $this->email = $inquiry->email;
        $this->contact_number = $inquiry->contact_number;
        $this->details = nl2br(e($inquiry->message));
        $this->inquiry_link = 'https://v2workflow.marvill.com/admin/inquiries/'.$inquiry->id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('maileclipse::templates.inquiry')
            ->from('notifications@marvill.com')
            ->subject('An Inquiry has been submitted.');
    }
}
