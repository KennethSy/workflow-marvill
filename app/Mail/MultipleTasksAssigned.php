<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MultipleTasksAssigned extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $name;
    public $tasks;

    public function __construct($request_tasks, $name)
    {
        $this->name = $name;
        $this->tasks = $request_tasks;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('maileclipse::templates.multipleTasksAssigned')
            ->from('notifications@marvill.com')
            ->subject('Tasks overdue for 3 days');
    }
}
