<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TbCarAutoLoan extends Model
{
    const civil_status = ['Single','Married','Widowed','Separated'];
    const home_ownership = ['Owned Mortgaged','Owned Not Mortgaged','Living with Family','Company Provided','Rented'];
    const loan_term = [
        2 => '2 Years',
        3 => '3 Years',
        4 => '4 Years',
        5 => '5 Years',
    ];

    function getLoanTerm() {
        return self::loan_term;
    }

    function getCivilStatus() {
        return self::civil_status;
    }

    function getHomeOwnership() {
        return self::home_ownership;
    }
}
