<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TbInvoiceFormType extends Model
{
    protected $table = 'tb_invoice_form_types';
}
