<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TbBookkeepingAccountingCategory extends Model
{
    protected $table = 'tb_bookkeeping_accounting_categories';

    public function accounting_category()
    {
        return $this->belongsTo('App\TbAccountingCategory','accounting_category_id','id');
    }
}
