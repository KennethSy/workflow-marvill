<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use Validator, Redirect, Response, File;
use Socialite;
use App\User;
use Log,DB,Mail;
use App\Mail\Inquiry;

class PageController
{
    public function index()
    {
        return view('frontend.pages.home');
    }

    public function about()
    {
        return view('frontend.pages.about');
    }

    public function services()
    {
        return view('frontend.pages.services');
    }

    public function careers()
    {
        return view('frontend.pages.career');
    }

    public function contactus()
    {
        return view('frontend.pages.contactus');
    }

    public function submitInquiry(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'g-recaptcha-response' => 'recaptcha',
        ]);

        if($validator->fails()) {
            $errors = $validator->errors();
            abort(422 );
            return response()->json(['message' => 'error']);
        }

        $inquiry_id = DB::table('tb_inquiries')->insertGetId([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'contact_number' => $request->contact_number,
            'message' => $request->message,
        ]);

        $inquiry = DB::table('tb_inquiries')->where('id',$inquiry_id)->first();

        Mail::to('rjvillanueva@marvill.com')->send(new Inquiry($inquiry));

        return response()->json(['message' => 'success']);
    }
}
