<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;
use Validator, Redirect, Response, File;
use Socialite;
use App\User;
use Carbon\Carbon;
use Image, Storage, PDF, DataTables, Auth;
use App\TbDomainAndHosing;
use App\Http\Queries\TaskSummaryItems;

class CompanyController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function __construct(TaskSummaryItems $taskSummaryItemsQuery)
    {
        $this->taskSummaryItemsQuery = $taskSummaryItemsQuery;
    }

    public function create(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'add', $isModelTranslatable);

        $domain_and_hosting = new TbDomainAndHosing();

        $view = 'admin.company.edit-add';

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'domain_and_hosting'));
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

        event(new BreadDataAdded($dataType, $data));

        TbDomainAndHosing::insert([
            'company_id' => $data->id,
            'domain_name' => $request->domain_name,
            'domain_under' => $request->domain_under,
            'domain_start_billing_date' => $request->domain_billing_plan!=null?Carbon::createFromFormat('m/d/Y',$request->domain_start_billing_date):null,
            'domain_billing_plan' => $request->domain_billing_plan,
            'hosting_under' => $request->hosting_under,
            'hosting_type' => $request->hosting_type,
            'hosting_start_billing_date' => $request->hosting_start_billing_date!=null?Carbon::createFromFormat('m/d/Y',$request->hosting_start_billing_date):null,
            'hosting_billing_plan' => $request->hosting_billing_plan,
            'email_provider_under' => $request->email_provider_under,
            'email_provider_start_billing_date' => $request->email_provider_start_billing_date!=null?Carbon::createFromFormat('m/d/Y',$request->email_provider_start_billing_date):null,
            'email_provider_billing_plan' => $request->email_provider_billing_plan,
            'status' => 0,
        ]);

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', $data)) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'edit', $isModelTranslatable);

        $domain_and_hosting = TbDomainAndHosing::where('company_id',$id)->firstOrNew([]);

        $view = 'admin.company.edit-add';

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'domain_and_hosting'));
    }

    public function update(Request $request, $id)
    {

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
            $data = $model->withTrashed()->findOrFail($id);
        } else {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
        }

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id)->validate();
        $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

        event(new BreadDataUpdated($dataType, $data));

        $update_data = [
            'domain_name' => $request->domain_name,
            'domain_under' => $request->domain_under,
            'hosting_under' => $request->hosting_under,
            'hosting_type' => $request->hosting_type,
            'email_provider_under' => $request->email_provider_under,
        ];
        if($request->domain_under == 'Marvill') {
            $update_data['domain_start_billing_date'] = $request->domain_billing_plan!=null?Carbon::createFromFormat('m/d/Y',$request->domain_start_billing_date):null;
            $update_data['domain_billing_plan'] = $request->domain_billing_plan;
        }
        if($request->hosting_under == 'Marvill') {
            $update_data['hosting_start_billing_date'] = $request->hosting_start_billing_date!=null?Carbon::createFromFormat('m/d/Y',$request->hosting_start_billing_date):null;
            $update_data['hosting_billing_plan'] = $request->hosting_billing_plan;
        }
        if($request->email_provider_under == 'Gmail Marvill') {
            $update_data['email_provider_start_billing_date'] = $request->email_provider_start_billing_date!=null?Carbon::createFromFormat('m/d/Y',$request->email_provider_start_billing_date):null;
            $update_data['email_provider_billing_plan'] = $request->email_provider_billing_plan;
        }

        TbDomainAndHosing::updateOrCreate(
            ['company_id' => $data->id],
            $update_data
        );

        if (auth()->user()->can('browse', $model)) {
            $redirect = redirect()->route("voyager.{$dataType->slug}.index");
        } else {
            $redirect = redirect()->back();
        }

        return $redirect->with([
            'message'    => __('voyager::generic.successfully_updated')." {$dataType->getTranslatedAttribute('display_name_singular')}",
            'alert-type' => 'success',
        ]);
    }

    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $isSoftDeleted = false;

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'read', $isModelTranslatable);

        $domain_and_hosting = TbDomainAndHosing::where('company_id',$id)->firstOrNew([]);
        $tasks_and_notes = $this->taskSummaryItemsQuery->execute($id);
        $attachments = DB::table('tb_daily_task_attachments')
            ->whereIn('tb_daily_task_attachments.note_id',$tasks_and_notes->pluck('note_id')->all())
            ->get();

        $view = 'admin.company.read';

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted', 'domain_and_hosting','tasks_and_notes','attachments'));
    }
}
