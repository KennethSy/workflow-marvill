<?php

namespace App\Http\Controllers\Admin;

use App\TbDailyLogSubTask;
use App\TbTaskStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use Validator, Redirect, Response, File;
use Socialite;
use App\User;
use Carbon\Carbon;
use App\TbDailyLog;
use App\TbCompany;
use App\TbProject;
use Image, Storage, PDF, DataTables, Auth;
use App\TbRequestTask;
use App\Http\Queries\TaskSummaryItems;
use App\Http\Queries\ConsolidatedTaskSummaryItems;
use Mail;
use App\Mail\TaskAssigned;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Events\BreadDataDeleted;

class TaskController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $filter_params = array();

        if($request->has('description')) {
            $filter_params['description'] = $request->get('description');
        }
        if($request->has('status')) {
            $filter_params['status'] = $request->get('status');
        }
        if($request->has('company')) {
            $filter_params['company'] = $request->get('company');
        }
        if($request->has('project')) {
            $filter_params['project'] = $request->get('project');
        }
        if($request->has('created-at')) {
            $filter_params['created-at'] = $request->get('created-at');
        }
        if($request->has('completed-at')) {
            $filter_params['completed-at'] = $request->get('completed-at');
        }
        if($request->has('confirmed-at')) {
            $filter_params['confirmed-at'] = $request->get('confirmed-at');
        }

        $task_status = TbTaskStatus::get();
        $companies = TbCompany::get();
        $projects = TbProject::get();
        $users = User::employee()->active();
        $all_users = User::getAllActiveEmployeesAndAdmins();
        if($request->has('assigned-to')) {
            $users = $users->where('id',$request->get('assigned-to'));
            $filter_params['assigned-to'] = $request->get('assigned-to');
        }
        $users = $users->get();
        $count = $users->count();
        $counter = 0;
        $employees = array();
        if(Auth::user()->role_id != 1 && Auth::user()->role_id != 3) {
            $users = $users->where('id','=',Auth::user()->id)->all();
        }

        if(Carbon::now() >= Carbon::now()->startOfMonth()->addDays(14)) {
            $cutoff_start = Carbon::now()->startOfMonth()->addDays(15)->subDay();
            $cutoff_end = Carbon::now()->endOfMonth()->subDay();
        } else {
            $cutoff_start = Carbon::now()->startOfMonth()->subDay();
            $cutoff_end = Carbon::now()->startOfMonth()->addDays(14)->endOfDay()->subDay();
        }

        foreach ($users as $user)
        {
            $employees[$counter]['id'] = $user->id;
            $employees[$counter]['name'] = $user->name;
            $employees[$counter]['basic'] = $user->basic_salary;
            $tasks_value = TbDailyLogSubTask::where('assigned_to',$user->id)->whereDate('created_at',Carbon::today())->sum('value');
            $employees[$counter]['assigned']=$tasks_value;
            $confirmed_tasks_value = TbDailyLogSubTask::where('assigned_to',$user->id)->whereDate('confirmed_at',Carbon::today())->sum('value');
            $employees[$counter]['confirmed']=$confirmed_tasks_value;
            $employees[$counter]['total_confirmed'] = TbDailyLogSubTask::where('assigned_to',$user->id)->where('confirmed_at','>=',$cutoff_start)->where('confirmed_at','<=',$cutoff_end)->sum('value');
            $employees[$counter]['total_confirmed_and_unconfirmed'] = TbDailyLogSubTask::where('assigned_to',$user->id)
                ->where(function($query) use($cutoff_start,$cutoff_end) {
                    $query->where(function($query2) use($cutoff_start,$cutoff_end) {
                        $query2->where('confirmed_at','>=',$cutoff_start)
                            ->where('confirmed_at','<=',$cutoff_end);
                    })
                        ->orWhere(function($query3) use($cutoff_start,$cutoff_end) {
                            $query3->where('created_at','>=',$cutoff_start)
                                ->where('created_at','<=',$cutoff_end);
                        });
                })
                ->sum('value');
            $employees[$counter]['salary_cutoff'] = ($user->basic_salary * $user->working_days) / 2;
            $counter++;
        }
        $project_data = array();
        foreach($companies as $company) {
            $project_data[$company->id] = array();
            foreach($projects->where('company_id','=',$company->id)->all() as $project) {
                $project_data[$company->id][] = [
                    'id' => $project->id,
                    'text' => $project->title,
                ];
            }
        }
        return view('admin.dailytask.browse')->with([
            'dataType' => $dataType,
            'employees' => $employees,
            'filter_params' => $filter_params,
            'all_users' => $all_users,
            'task_status' => $task_status,
            'companies' => $companies,
            'projects' => $projects,
            'project_data' => $project_data,
        ]);
    }

    public function data(Request $request)
    {
        $canView = Auth::user()->can('read', app('App\TbDailyLogSubTask'));
        $canEdit = Auth::user()->can('edit', app('App\TbDailyLogSubTask'));
        $canDelete = Auth::user()->can('delete', app('App\TbDailyLogSubTask'));

        $tasks = TbDailyLogSubTask::leftjoin('users','users.id','=','tb_daily_log_sub_tasks.assigned_to')
            ->leftjoin('tb_projects','tb_projects.id','=','tb_daily_log_sub_tasks.project_id')
            ->leftjoin('tb_companies','tb_companies.id','=','tb_projects.company_id')
            ->leftjoin('tb_task_status','tb_task_status.id','=','tb_daily_log_sub_tasks.status')
            ->leftjoin('tb_daily_task_notes','tb_daily_task_notes.id','=',DB::raw('(SELECT n.id FROM tb_daily_task_notes AS n WHERE n.task_id = tb_daily_log_sub_tasks.id ORDER BY n.created_at DESC LIMIT 1)'));

        if(Auth::user()->role_id != 1 && Auth::user()->role_id != 3) {
            $tasks = $tasks->where('tb_daily_log_sub_tasks.assigned_to',Auth::user()->id);
        }

        if($request->has('description')) {
            $tasks = $tasks->where('tb_daily_log_sub_tasks.description','LIKE','%'.$request->get('description').'%');
        }

        if($request->has('assigned-to')) {
            $tasks = $tasks->where('tb_daily_log_sub_tasks.assigned_to',$request->get('assigned-to'));
        }

        if($request->has('status')) {
            $tasks = $tasks->where('tb_daily_log_sub_tasks.status',$request->get('status'));
        }

        if($request->has('company')) {
            $tasks = $tasks->where('tb_companies.id',$request->get('company'));
        }

        if($request->has('project')) {
            $tasks = $tasks->where('tb_projects.id',$request->get('project'));
        }

        if($request->has('created-at')) {
            $tasks = $tasks->whereDate('tb_daily_log_sub_tasks.created_at',Carbon::createFromFormat('m/d/Y',$request->get('created-at'))->toDateString());
        }

        if($request->has('completed-at')) {
            $tasks = $tasks->whereDate('tb_daily_log_sub_tasks.completed_at',Carbon::createFromFormat('m/d/Y',$request->get('completed-at'))->toDateString());
        }

        if($request->has('confirmed-at')) {
            $tasks = $tasks->whereDate('tb_daily_log_sub_tasks.confirmed_at',Carbon::createFromFormat('m/d/Y',$request->get('confirmed-at'))->toDateString());
        }

        if($request->has('old-task')) {
            $tasks = $tasks->where('tb_daily_log_sub_tasks.created_at','<',Carbon::now()->startOfDay());
        }

        $tasks = $tasks->select([
                'users.name as assigned_user',
                'tb_task_status.status as task_status',
                'tb_companies.company_name as company',
                'tb_projects.title as project',
                'tb_daily_log_sub_tasks.*',
                'tb_daily_task_notes.note as latest_note',
        ]);

        return DataTables::of($tasks)
            ->addColumn('actions', function ($task) use($canView, $canDelete, $canEdit,$request) {
                $return = null;
                if($request->has('assigned-to')) {
                    $return = $request->get('assigned-to');
                }
                return '<span id="button-'.$task->id.'">'.view('admin.dailytask.action-buttons')->with([
                    'task' => $task,
                    'canView' => $canView,
                    'canEdit' => $canEdit,
                    'canDelete' => $canDelete,
                    'return' => $return,
                ])->render().'</span>';
            })
            ->editColumn('assigned_user', function ($task) {
                return (!empty($task->latest_note)?'<i style="margin-left:-15px;float:left;" class="fas fa-thumbtack tips" data-toggle="tooltip" data-placement="bottom" title="'.htmlspecialchars($task->latest_note, ENT_QUOTES, 'UTF-8').'" data-fa-transform="rotate--45"></i>':'').$task->assigned_user;
            })
            ->editColumn('description', function ($task) {
                return nl2br($task->description);
            })
            ->editColumn('value', function ($task) {
                $string = '';
                if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3) {
                    $string .= '<form style="display:none;" data-id='.$task->id.' method="post" class="edit-value-form" id="edit-value-form-'.$task->id.'" action="'.route('dailytask.changevalue',['id' => $task->id]).'">';
                    $string .= '<input name="value" type="number" class="form-control" id="edit-value-'.$task->id.'" value="'.$task->value.'"/>';
                    $string .= '<button type="submit" class="btn-edit-save btn btn-primary" data-id='.$task->id.'><i class="fa fa-check"></i></button>';
                    $string .= '<button class="btn-edit-cancel btn btn-danger" data-id='.$task->id.'><i class="fa fa-times"></i></button>';
                    $string .= '</form>';
                }
                $string .= '<span ';
                if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3) {
                    $string .= 'class="value-cell" data-id='.$task->id.' style="text-decoration: underline;cursor: pointer" ';
                }
                $string .= ' id="value-'.$task->id.'">'.$task->value.'</span>';
                return $string;
            })
            ->editColumn('completed_at', function ($task) {
                return '<span id="completed-at-'.$task->id.'">'.$task->completed_at.'</span>';
            })
            ->editColumn('confirmed_at', function ($task) {
                return '<span id="confirmed-at-'.$task->id.'">'.$task->confirmed_at.'</span>';
            })
            ->editColumn('task_status', function ($task) {
                return '<span id="status-'.$task->id.'">'.$task->task_status.'</span>';
            })
            ->rawColumns(['actions','description','assigned_user','completed_at','confirmed_at','task_status','value'])
            ->setRowId(function ($task) {
                return'row-'.$task->id;
            })
            ->setRowClass(function ($task) {
                $class = '';
                if($task->status == 0) {
                    $created = Carbon::parse($task->created_at)->startOfDay();
                    $now = Carbon::now()->startOfDay();
                    $difference = $created->diff($now)->days;
                    if($created->dayOfWeek == 5 && $difference <= 2) {

                    }elseif($created->dayOfWeek == 6 && $difference <= 1) {

                    }elseif($task->status == 0 && Carbon::now()->startOfDay() > Carbon::parse($task->created_at)->startOfDay()) {
                        if($task->carryover_at != null) {
                            $class .= ' tr-orangebg';
                        } else {
                            $class .= ' tr-redbg';
                        }
                    }
                }
                return $class;
            })
            ->make(true);
    }

    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $this->authorize('read', app($dataType->model_name));

        $task = TbDailyLogSubTask::leftjoin('tb_projects','tb_projects.id','=','tb_daily_log_sub_tasks.project_id')
            ->leftjoin('tb_companies','tb_companies.id','=','tb_projects.company_id')
            ->select([
                'tb_daily_log_sub_tasks.*',
                'tb_projects.title as project',
                'tb_companies.company_name as company',
            ])
            ->findOrFail($id);
        $status = TbTaskStatus::find($task->status);
        $user = User::find($task->assigned_to);
        $notes = DB::table('tb_daily_task_notes')
            ->where('tb_daily_task_notes.task_id',$id)
            ->orderBy('tb_daily_task_notes.created_at','asc')
            ->select([
                'tb_daily_task_notes.*',
            ])
            ->get();

        $task_attachments = DB::table('tb_daily_task_attachments')
            ->where('tb_daily_task_attachments.file_type','image')
            ->where('tb_daily_task_attachments.task_id',$id)
            ->where('tb_daily_task_attachments.note_id',null)
            ->get();

        $task_attachment_docs = DB::table('tb_daily_task_attachments')
            ->where('tb_daily_task_attachments.file_type','docs')
            ->where('tb_daily_task_attachments.task_id',$id)
            ->where('tb_daily_task_attachments.note_id',null)
            ->get();

        $attachments = DB::table('tb_daily_task_attachments')
            ->whereIn('tb_daily_task_attachments.note_id',$notes->pluck('id')->all())
            ->get();

        $users = User::getAllActiveEmployeesAndAdmins();
        $companies = TbCompany::get();
        $projects = TbProject::get();
        $project_data = array();
        foreach($companies as $company) {
            $project_data[$company->id] = array();
            foreach($projects->where('company_id','=',$company->id)->all() as $project) {
                $project_data[$company->id][] = [
                    'id' => $project->id,
                    'text' => $project->title,
                ];
            }
        }

        $data = [
            'task' => $task,
            'user' => $user,
            'status' => $status,
            'notes' => $notes,
            'attachments' => $attachments,
            'task_attachment_docs' => $task_attachment_docs,
            'task_attachments' => $task_attachments,
            'users' => $users,
            'companies' => $companies,
            'projects' => $projects,
            'project_data' => $project_data,
        ];

        return view('admin.dailytask.read', $data);
    }

    public function create(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $task = new TbDailyLogSubTask();
        $users = User::getAllActiveEmployeesAndAdmins();
        $companies = TbCompany::get();
        $projects = TbProject::get();
        $task->company_id = null;
        $attachments = null;
        $attachment_docs = null;
        $request_assigned_to = null;

        if($request->has('request_id')){
            $request_task = TbRequestTask::where('id',$request->request_id)->first();
            $task->description = $request_task->description;
            $task->project_id = $request_task->project_id;
            $task->company_id = TbProject::where('id',$request_task->project_id)->first()->company_id;
            $task->created_at = $request_task->requested_date;
            $request_assigned_to = DB::table('tb_request_task_assigned_users')->where('request_id',$request->request_id)->get();
            $attachments = DB::table('tb_request_task_attachments')->where('request_id',$request->request_id)->get();
        }

        $project_data = array();
        foreach($companies as $company) {
            $project_data[$company->id] = array();
            foreach($projects->where('company_id','=',$company->id)->all() as $project) {
                $project_data[$company->id][] = [
                    'id' => $project->id,
                    'text' => $project->title,
                ];
            }
        }

        $users_employees = User::getAllActiveEmployees();
        $employees = array();
        $counter = 0;
        
        if(Carbon::now() >= Carbon::now()->startOfMonth()->addDays(14)) {
            $cutoff_start = Carbon::now()->startOfMonth()->addDays(15)->subDay();
            $cutoff_end = Carbon::now()->endOfMonth()->subDay();
        } else {
            $cutoff_start = Carbon::now()->startOfMonth()->subDay();
            $cutoff_end = Carbon::now()->startOfMonth()->addDays(14)->endOfDay()->subDay();
        }

        foreach ($users_employees as $users_employee)
        {
            $employees[$counter]['id'] = $users_employee->id;
            $employees[$counter]['name'] = $users_employee->name;
            $employees[$counter]['basic'] = $users_employee->basic_salary;
            $tasks_value = TbDailyLogSubTask::where('assigned_to',$users_employee->id)->whereDate('created_at',Carbon::today())->sum('value');
            $employees[$counter]['assigned']=$tasks_value;
            $confirmed_tasks_value = TbDailyLogSubTask::where('assigned_to',$users_employee->id)->whereDate('confirmed_at',Carbon::today())->sum('value');
            $employees[$counter]['confirmed']=$confirmed_tasks_value;
            $employees[$counter]['total_confirmed'] = TbDailyLogSubTask::where('assigned_to',$users_employee->id)->where('confirmed_at','>=',$cutoff_start)->where('confirmed_at','<=',$cutoff_end)->sum('value');
            $employees[$counter]['total_confirmed_and_unconfirmed'] = TbDailyLogSubTask::where('assigned_to',$users_employee->id)
                ->where(function($query) use($cutoff_start,$cutoff_end) {
                    $query->where(function($query2) use($cutoff_start,$cutoff_end) {
                        $query2->where('confirmed_at','>=',$cutoff_start)
                            ->where('confirmed_at','<=',$cutoff_end);
                    })
                        ->orWhere(function($query3) use($cutoff_start,$cutoff_end) {
                            $query3->where('created_at','>=',$cutoff_start)
                                ->where('created_at','<=',$cutoff_end);
                        });
                })
                ->sum('value');
            $employees[$counter]['salary_cutoff'] = ($users_employee->basic_salary * $users_employee->working_days) / 2;
            $counter++;
        }

        return view('admin.dailytask.edit-add')->with([
            'dataType' => $dataType,
            'edit' => false,
            'task' => $task,
            'users' => $users,
            'companies' => $companies,
            'projects' => $projects,
            'project_data' => $project_data,
            'employees' => $employees,
            'attachments' => $attachments,
            'attachment_docs' => $attachment_docs,
            'request_assigned_to' => $request_assigned_to,
        ]);
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $rules = [
            'description.*' => 'required|string',
            'company.*' => 'required|exists:tb_companies,id',
            'value.*' => 'required|numeric',
        ];
        foreach($request->assigned_to as $key => $val){
            $rules['assigned_to.'.$key.'.*'] = 'required|exists:users,id';
        }
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        foreach($request->company as $request_index => $request_company) {
            if(Carbon::createFromFormat('m/d/Y',$request->date[$request_index])->startOfDay() > Carbon::now()->startOfDay()) {
                $date = Carbon::createFromFormat('m/d/Y',$request->date[$request_index])->startOfDay();
            } else {
                $date = Carbon::now();
            }

            $task_id_array = array();

            if(TbProject::find($request->project[$request_index])){
                $project_id = $request->project[$request_index];
            } else {
                $project_id = TbProject::insertGetId([
                    'title' => $request->project[$request_index],
                    'company_id' => $request->company[$request_index],
                ]);
            }

            $main_id = DB::table('tb_daily_main_tasks')->insertGetId([
                'description' => $request->description[$request_index],
                'value' => $request->value[$request_index],
                'created_at' => $date,
                'project_id' => $project_id,
            ]);

            foreach($request->assigned_to[$request_index] as $assigned_to) {
                $user = User::where('id',$assigned_to)->first();

                $latest = TbDailyLog::where('assigned_to',$assigned_to)
                    ->where('created_at','>=',Carbon::createFromFormat('m/d/Y',$request->date[$request_index])->startOfDay())
                    ->where('created_at','<=',Carbon::createFromFormat('m/d/Y',$request->date[$request_index])->endOfDay())
                    ->where('status',0)
                    ->orderBy('id', 'desc')->first();
                if ($latest) {
                    $log_id = $latest->id;
                }
                else {
                    $target_daily_rate = $user->basic_salary;


                    $log_id = TbDailyLog::insertGetId(
                        [
                            'assigned_to' => $assigned_to,
                            'location' => 'Pending',
                            'created_at' => $date,
                            'target_daily_rate' => $target_daily_rate
                        ]
                    );
                }

                $inserted_task_id = TbDailyLogSubTask::insertGetId([
                    'log_id' => $log_id,
                    'description' => $request->description[$request_index],
                    'value' => $request->value[$request_index],
                    'assigned_to' => $assigned_to,
                    'created_at' => $date,
                    'project_id' => $project_id,
                    'department_id' => $user->department_id,
                    'main_task_id' => $main_id,
                ]);

                $task_id_array[] = $inserted_task_id;

                if(Carbon::createFromFormat('m/d/Y',$request->date[$request_index])->startOfDay() <= Carbon::now()->startOfDay()) {
                    $request_task = TbDailyLogSubTask::leftjoin('tb_projects','tb_projects.id','=','tb_daily_log_sub_tasks.project_id')
                        ->leftjoin('tb_companies','tb_companies.id','=','tb_projects.company_id')
                        ->leftjoin('users','users.id','=','tb_daily_log_sub_tasks.assigned_to')
                        ->where('tb_daily_log_sub_tasks.id', $inserted_task_id)
                        ->select([
                            'tb_daily_log_sub_tasks.*',
                            'tb_projects.title as project',
                            'users.name as user',
                            'tb_companies.company_name as company',
                        ])
                        ->first();

                    Mail::to($user->email)->send(new TaskAssigned($request_task));
                }


            }

            if(isset($request->attachment[$request_index])) {
                foreach($request->file('attachment')[$request_index] as $file){
                    //get filename with extension
                    //$filenamewithextension = $file->getClientOriginalName();
                    //get filename without extension
                    //$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                    //get file extension
                    $extension = $file->getClientOriginalExtension();
                    $orig_name = $file->getClientOriginalName();

                    $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.'.$extension;
                    while(DB::table('tb_daily_task_attachments')->where('attachment',$filename)->first()) {
                        $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.'.$extension;
                    }
                    Image::make($file)->save(public_path().'/uploads/notes/'.$filename,50);

                    foreach($task_id_array as $task_id){
                        DB::table('tb_daily_task_attachments')
                            ->insert(
                                [
                                    'note_id' => null,
                                    'task_id' => $task_id,
                                    'attachment' => $filename,
                                    'created_by' => Auth::user()->id,
                                    'file_type' => 'image',
                                    'file_name' => $orig_name,
                                    'file_extension' => $extension,
                                ]
                            );
                    }
                }
            }

            if(isset($request->attachment_docs[$request_index])) {
                foreach($request->file('attachment_docs')[$request_index] as $file){
                    $extension = $file->getClientOriginalExtension();
                    $orig_name = $file->getClientOriginalName();

                    $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.'.$extension;
                    while(DB::table('tb_daily_task_attachments')->where('attachment',$filename)->first()) {
                        $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.'.$extension;
                    }
                    $file->move(public_path().'/uploads/notes/',$filename);

                    foreach($task_id_array as $task_id){
                        DB::table('tb_daily_task_attachments')
                            ->insert(
                                [
                                    'note_id' => null,
                                    'task_id' => $task_id,
                                    'attachment' => $filename,
                                    'created_by' => Auth::user()->id,
                                    'file_type' => 'docs',
                                    'file_name' => $orig_name,
                                    'file_extension' => $extension,
                                ]
                            );
                    }
                }
            }

            if(isset($request->attachment_file_name[$request_index])) {
                foreach($request->attachment_file_name[$request_index] as $name){
                    foreach($task_id_array as $task_id) {
                        DB::table('tb_daily_task_attachments')
                            ->insert(
                                [
                                    'note_id' => null,
                                    'task_id' => $task_id,
                                    'attachment' => $name,
                                    'created_by' => Auth::user()->id,
                                    'file_type' => 'image',
                                ]
                            );
                    }
                }
            }
        }

        if (!$request->has('_tagging')) {
            if($request->has('request_id')){
                $redirect = redirect()->route('voyager.request-tasks.index');
            }
            elseif (auth()->user()->can('browse', app($dataType->model_name))) {
                $redirect = redirect()->back();
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_added_new')." Task",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true]);
        }
    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $task = TbDailyLogSubTask::findOrFail($id);
        $users = User::getAllActiveEmployeesAndAdmins();
        $companies = TbCompany::get();
        $projects = TbProject::get();
        if($task->project_id == null) {
            $task->company_id = null;
        } else {
            $task->company_id = $projects->where('id','=',$task->project_id)->first()->company_id;
        }

        $project_data = array();
        foreach($companies as $company) {
            $project_data[$company->id] = array();
            foreach($projects->where('company_id','=',$company->id)->all() as $project) {
                $project_data[$company->id][] = [
                    'id' => $project->id,
                    'text' => $project->title,
                ];
            }
        }

        $users_employees = User::getAllActiveEmployees();
        $counter = 0;
        $employees = array();

        if(Carbon::now() >= Carbon::now()->startOfMonth()->addDays(14)) {
            $cutoff_start = Carbon::now()->startOfMonth()->addDays(15)->subDay();
            $cutoff_end = Carbon::now()->endOfMonth()->subDay();
        } else {
            $cutoff_start = Carbon::now()->startOfMonth()->subDay();
            $cutoff_end = Carbon::now()->startOfMonth()->addDays(14)->endOfDay()->subDay();
        }

        foreach ($users_employees as $users_employee)
        {
            $employees[$counter]['id'] = $users_employee->id;
            $employees[$counter]['name'] = $users_employee->name;
            $employees[$counter]['basic'] = $users_employee->basic_salary;
            $tasks_value = TbDailyLogSubTask::where('assigned_to',$users_employee->id)->whereDate('created_at',Carbon::today())->sum('value');
            $employees[$counter]['assigned']=$tasks_value;
            $confirmed_tasks_value = TbDailyLogSubTask::where('assigned_to',$users_employee->id)->whereDate('confirmed_at',Carbon::today())->sum('value');
            $employees[$counter]['confirmed']=$confirmed_tasks_value;
            $employees[$counter]['total_confirmed'] = TbDailyLogSubTask::where('assigned_to',$users_employee->id)->where('confirmed_at','>=',$cutoff_start)->where('confirmed_at','<=',$cutoff_end)->sum('value');
            $employees[$counter]['total_confirmed_and_unconfirmed'] = TbDailyLogSubTask::where('assigned_to',$users_employee->id)
                ->where(function($query) use($cutoff_start,$cutoff_end) {
                    $query->where(function($query2) use($cutoff_start,$cutoff_end) {
                        $query2->where('confirmed_at','>=',$cutoff_start)
                            ->where('confirmed_at','<=',$cutoff_end);
                    })
                        ->orWhere(function($query3) use($cutoff_start,$cutoff_end) {
                            $query3->where('created_at','>=',$cutoff_start)
                                ->where('created_at','<=',$cutoff_end);
                        });
                })
                ->sum('value');
            $employees[$counter]['salary_cutoff'] = ($users_employee->basic_salary * $users_employee->working_days) / 2;
            $counter++;
        }

        $attachments = DB::table('tb_daily_task_attachments')
            ->where('task_id',$id)
            ->where('note_id',null)
            ->where('file_type','image')
            ->get();

        $attachment_docs = DB::table('tb_daily_task_attachments')
            ->where('task_id',$id)
            ->where('note_id',null)
            ->where('file_type','docs')
            ->get();

        return view('admin.dailytask.edit-add')->with([
            'dataType' => $dataType,
            'edit' => true,
            'task' => $task,
            'users' => $users,
            'companies' => $companies,
            'projects' => $projects,
            'project_data' => $project_data,
            'employees' => $employees,
            'attachments' => $attachments,
            'attachment_docs' => $attachment_docs,
        ]);

    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        // Validate fields with ajax
        $validator = Validator::make($request->all(), [
            'description' => 'required|string',
            'company' => 'required|exists:tb_companies,id',
            'value' => 'required|numeric',
            'assigned_to' => 'required|exists:users,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::where('id',$request->assigned_to)->first();


        if(TbProject::find($request->project)){
            $project_id = $request->project;
        } else {
            $project_id = TbProject::insertGetId([
                'title' => $request->project,
                'company_id' => $request->company,
            ]);
        }

        if(Carbon::createFromFormat('m/d/Y',$request->date)->startOfDay() > Carbon::now()->startOfDay()) {
            $date = Carbon::createFromFormat('m/d/Y',$request->date)->startOfDay();
        } else {
            $date = Carbon::now();
        }

        TbDailyLogSubTask::where('id',$id)->update([
            'description' => $request->description,
            'value' => $request->value,
            'assigned_to' => $request->assigned_to,
            'project_id' => $project_id,
            'department_id' => $user->department_id,
            'created_at' => $date,
        ]);

        if($request->has('attachment_file_name')) {
            DB::table('tb_daily_task_attachments')
                ->where([
                    'note_id' => null,
                    'task_id' => $id,
                    'file_type' => 'image',
                ])
                ->whereNotIn('attachment',$request->attachment_file_name)
                ->delete();
        } else {
            DB::table('tb_daily_task_attachments')
                ->where('note_id' ,'=', null)
                ->where('task_id' ,'=', $id)
                ->where('file_type' ,'=', 'image')
                ->delete();
        }

        if($request->has('attachment_doc_file_name')) {
            DB::table('tb_daily_task_attachments')
                ->where([
                    'note_id' => null,
                    'task_id' => $id,
                    'file_type' => 'docs',
                ])
                ->whereNotIn('attachment',$request->attachment_doc_file_name)
                ->delete();
        } else {
            DB::table('tb_daily_task_attachments')
                ->where('note_id' ,'=', null)
                ->where('task_id' ,'=', $id)
                ->where('file_type' ,'=', 'docs')
                ->delete();
        }

        if($request->hasFile('attachment')) {
            foreach($request->file('attachment') as $file){
                //get filename with extension
                //$filenamewithextension = $file->getClientOriginalName();
                //get filename without extension
                //$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                //get file extension
                $extension = $file->getClientOriginalExtension();
                $orig_name = $file->getClientOriginalName();

                $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.'.$extension;
                while(DB::table('tb_daily_task_attachments')->where('attachment',$filename)->first()) {
                    $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.'.$extension;
                }
                Image::make($file)->save(public_path().'/uploads/notes/'.$filename,50);

                DB::table('tb_daily_task_attachments')
                    ->insert(
                        [
                            'note_id' => null,
                            'task_id' => $id,
                            'attachment' => $filename,
                            'created_by' => Auth::user()->id,
                            'file_type' => 'image',
                            'file_name' => $orig_name,
                            'file_extension' => $extension,
                        ]
                    );
            }
        }

        if($request->hasFile('attachment_docs')) {
            foreach($request->file('attachment_docs') as $file){
                $extension = $file->getClientOriginalExtension();
                $orig_name = $file->getClientOriginalName();

                $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.'.$extension;
                while(DB::table('tb_daily_task_attachments')->where('attachment',$filename)->first()) {
                    $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.'.$extension;
                }
                $file->move(public_path().'/uploads/notes/',$filename);

                DB::table('tb_daily_task_attachments')
                    ->insert(
                        [
                            'note_id' => null,
                            'task_id' => $id,
                            'attachment' => $filename,
                            'created_by' => Auth::user()->id,
                            'file_type' => 'docs',
                            'file_name' => $orig_name,
                            'file_extension' => $extension,
                        ]
                    );
            }
        }

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', app($dataType->model_name))) {
                $redirect = redirect()->route('voyager.dailytask.index');
                if($request->has('return')) {
                    $redirect = redirect()->route('voyager.dailytask.index',['assigned-to' => $request->return]);
                }
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => "Successfully Updated Task",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true]);
        }
    }

    public function changeValue (Request $request, $id)
    {
        TbDailyLogSubTask::where('id',$id)
            ->update(['value' => $request->value]);

        $task = TbDailyLogSubTask::where('id',$id)->first();
        $status = TbTaskStatus::where('id',$task->status)->first();

        $canView = Auth::user()->can('read', app('App\TbDailyLogSubTask'));
        $canEdit = Auth::user()->can('edit', app('App\TbDailyLogSubTask'));
        $canDelete = Auth::user()->can('delete', app('App\TbDailyLogSubTask'));

        if($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => 'Value of Task has been changed to '.$task->value,
                'completed_at' => $task->completed_at,
                'confirmed_at' => $task->confirmed_at,
                'status' => $status->status,
                'value' => $task->value,
                'button' => view('admin.dailytask.action-buttons')->with([
                    'task' => $task,
                    'canView' => $canView,
                    'canEdit' => $canEdit,
                    'canDelete' => $canDelete,
                ])->render(),
            ]);
        }
        return redirect()->back()->with([
            'message'    => 'Value of Task has been changed to '.$task->value,
            'alert-type' => 'success',
        ]);
    }

    public function complete (Request $request, $id)
    {
        if(isset($request->note)) {
            DB::table('tb_daily_task_notes')
                ->whereBetween('created_at', [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])
                ->updateOrInsert(
                    [
                        'task_id' => $id,
                        'created_by' => Auth::user()->id,],
                    [
                        'task_id' => $id,
                        'note' => $request->note,
                        'created_by' => Auth::user()->id,
                    ]
                );
        }

        $notes = DB::table('tb_daily_task_notes')
            ->where('task_id',$id)
            ->where('created_at','>=',Carbon::now()->startOfDay())
            ->first();

        if(!$notes) {
            if($request->ajax()) {
                return response()->json([
                    'success' => false,
                    'message' => 'Task needs a note before marked as complete',
                ]);
            }
            return redirect()->back()->with([
                'message'    => 'Task needs a note before marked as complete',
                'alert-type' => 'error',
            ]);
        }

        TbDailyLogSubTask::where('id',$id)
            ->update(['status' => 1, 'completed_at' => Carbon::now()]);


        $task = TbDailyLogSubTask::where('id',$id)->first();
        $status = TbTaskStatus::where('id',$task->status)->first();

        $canView = Auth::user()->can('read', app('App\TbDailyLogSubTask'));
        $canEdit = Auth::user()->can('edit', app('App\TbDailyLogSubTask'));
        $canDelete = Auth::user()->can('delete', app('App\TbDailyLogSubTask'));

        if($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => 'Task Marked as Complete',
                'completed_at' => $task->completed_at,
                'confirmed_at' => $task->confirmed_at,
                'status' => $status->status,
                'value' => $task->value,
                'button' => view('admin.dailytask.action-buttons')->with([
                    'task' => $task,
                    'canView' => $canView,
                    'canEdit' => $canEdit,
                    'canDelete' => $canDelete,
                ])->render(),
            ]);
        }
        return redirect()->back()->with([
            'message'    => 'Task Marked as Complete',
            'alert-type' => 'success',
        ]);
    }

    public function open (Request $request, $id)
    {
        TbDailyLogSubTask::where('id',$id)
            ->update(['status' => 0, 'completed_at' => null]);

        $task = TbDailyLogSubTask::where('id',$id)->first();
        $status = TbTaskStatus::where('id',$task->status)->first();

        $canView = Auth::user()->can('read', app('App\TbDailyLogSubTask'));
        $canEdit = Auth::user()->can('edit', app('App\TbDailyLogSubTask'));
        $canDelete = Auth::user()->can('delete', app('App\TbDailyLogSubTask'));

        if($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => 'Task Marked as Open',
                'completed_at' => $task->completed_at,
                'confirmed_at' => $task->confirmed_at,
                'status' => $status->status,
                'value' => $task->value,
                'button' => view('admin.dailytask.action-buttons')->with([
                    'task' => $task,
                    'canView' => $canView,
                    'canEdit' => $canEdit,
                    'canDelete' => $canDelete,
                ])->render(),
            ]);
        }
        return redirect()->back()->with([
            'message'    => 'Task Marked as Open',
            'alert-type' => 'success',
        ]);
    }

    public function confirm (Request $request, $id)
    {
        $task = TbDailyLogSubTask::where('id',$id)->first();
        $log = TbDailyLog::where('id',$task->log_id)->first();

        $sum = $log->achieved_rate + $task->value;

        TbDailyLog::where('id',$task->log_id)
            ->update(['achieved_rate' => $sum]);

        TbDailyLogSubTask::where('id',$id)
            ->update(['status' => 2, 'confirmed_at' => Carbon::now()]);

        $task = TbDailyLogSubTask::where('id',$id)->first();
        $status = TbTaskStatus::where('id',$task->status)->first();

        $canView = Auth::user()->can('read', app('App\TbDailyLogSubTask'));
        $canEdit = Auth::user()->can('edit', app('App\TbDailyLogSubTask'));
        $canDelete = Auth::user()->can('delete', app('App\TbDailyLogSubTask'));

        if($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => 'Task Confirmed as Complete',
                'completed_at' => $task->completed_at,
                'confirmed_at' => $task->confirmed_at,
                'status' => $status->status,
                'value' => $task->value,
                'button' => view('admin.dailytask.action-buttons')->with([
                    'task' => $task,
                    'canView' => $canView,
                    'canEdit' => $canEdit,
                    'canDelete' => $canDelete,
                ])->render(),
            ]);
        }
        return redirect()->back()->with([
            'message'    => 'Task Confirmed as Complete',
            'alert-type' => 'success',
        ]);
    }

    public function reject (Request $request, $id)
    {
        $task = TbDailyLogSubTask::where('id',$id)->first();
        if(Carbon::parse($task->completed_at) >= Carbon::now()->startOfDay()) {
            $new_value = $task->value;
        } else {
            $new_value = $task->value/2;
        }

        TbDailyLogSubTask::where('id',$id)
            ->update(['status' => 0, 'completed_at' => null, 'value' => $new_value, 'rejected_at' => Carbon::now()]);

        $task = TbDailyLogSubTask::where('id',$id)->first();
        $status = TbTaskStatus::where('id',$task->status)->first();

        $canView = Auth::user()->can('read', app('App\TbDailyLogSubTask'));
        $canEdit = Auth::user()->can('edit', app('App\TbDailyLogSubTask'));
        $canDelete = Auth::user()->can('delete', app('App\TbDailyLogSubTask'));

        if($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => 'Task Completion Rejected',
                'completed_at' => $task->completed_at,
                'confirmed_at' => $task->confirmed_at,
                'status' => $status->status,
                'value' => $task->value,
                'button' => view('admin.dailytask.action-buttons')->with([
                    'task' => $task,
                    'canView' => $canView,
                    'canEdit' => $canEdit,
                    'canDelete' => $canDelete,
                ])->render(),
            ]);
        }
        return redirect()->back()->with([
            'message'    => 'Task Completion Rejected',
            'alert-type' => 'success',
        ]);
    }

    public function carryover (Request $request, $id)
    {
        TbDailyLogSubTask::where('id',$id)
            ->update(['status' => 0, 'completed_at' => null, 'carryover_at' => Carbon::now()]);

        $task = TbDailyLogSubTask::where('id',$id)->first();
        $status = TbTaskStatus::where('id',$task->status)->first();

        $canView = Auth::user()->can('read', app('App\TbDailyLogSubTask'));
        $canEdit = Auth::user()->can('edit', app('App\TbDailyLogSubTask'));
        $canDelete = Auth::user()->can('delete', app('App\TbDailyLogSubTask'));

        if($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => 'Task Carried Over',
                'completed_at' => $task->completed_at,
                'confirmed_at' => $task->confirmed_at,
                'status' => $status->status,
                'value' => $task->value,
                'button' => view('admin.dailytask.action-buttons')->with([
                    'task' => $task,
                    'canView' => $canView,
                    'canEdit' => $canEdit,
                    'canDelete' => $canDelete,
                ])->render(),
            ]);
        }
        return redirect()->back()->with([
            'message'    => 'Task Carried Over',
            'alert-type' => 'success',
        ]);
    }

    public function editnote(Request $request, $id)
    {
        if($request->note != null || !empty($request->note)) {
            DB::table('tb_daily_task_notes')
                ->whereBetween('created_at', [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])
                ->updateOrInsert(
                    [
                        'task_id' => $id,
                        'created_by' => Auth::user()->id,],
                    [
                        'task_id' => $id,
                        'note' => $request->note,
                        'created_by' => Auth::user()->id,
                    ]
                );
        }
        $updatedOrInsertedRecord = DB::table('tb_daily_task_notes')
            ->whereBetween('created_at', [Carbon::now()->startOfDay(), Carbon::now()->endOfDay()])
            ->where('task_id', $id)
            ->where('created_by' , Auth::user()->id)
            ->first();

        if(isset($request->complete)) {
            TbDailyLogSubTask::where('id',$id)
                ->where('status',0)
                ->update(['status' => 1, 'completed_at' => Carbon::now()]);
        }

        DB::table('tb_daily_task_attachments')
            ->where('note_id' ,'=', $updatedOrInsertedRecord->id)
            ->where('task_id' ,'=', $id)
            ->where('created_by','=', Auth::user()->id)
            ->delete();

        if($request->hasFile('attachment')) {
            foreach($request->file('attachment') as $file){
                //get filename with extension
                //$filenamewithextension = $file->getClientOriginalName();
                //get filename without extension
                //$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                //get file extension
                $extension = $file->getClientOriginalExtension();

                $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.'.$extension;
                while(DB::table('tb_daily_task_attachments')->where('attachment',$filename)->first()) {
                    $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.'.$extension;
                }
                Image::make($file)->save(public_path().'/uploads/notes/'.$filename,50);

                DB::table('tb_daily_task_attachments')
                    ->insert(
                        [
                            'note_id' => $updatedOrInsertedRecord->id,
                            'task_id' => $id,
                            'attachment' => $filename,
                            'created_by' => Auth::user()->id,
                        ]
                    );
            }
        }

        if($request->has('attachment_file_name')) {
            foreach($request->attachment_file_name as $name){
                DB::table('tb_daily_task_attachments')
                    ->insert(
                        [
                            'note_id' => $updatedOrInsertedRecord->id,
                            'task_id' => $id,
                            'attachment' => $name,
                            'created_by' => Auth::user()->id,
                        ]
                    );
            }
        }

        if (!$request->has('_tagging')) {
            $redirect = redirect()->back();

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_added_new')." Note",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true]);
        }
    }

    public function summary(Request $request, ConsolidatedTaskSummaryItems $taskSummaryItemsQuery, $company_id = null) {
        $client_companies = array();
        if(Auth::user()->role_id == 7) {
            $directories = DB::table('tb_directories')->where('contact_email',Auth::user()->email)->get();
            foreach($directories as $directory) {
                $client_companies[] = $directory->contact_company_id;
            }
        }

        //Fetch list of companies with projects that you have tasks
        $companies = TbCompany::leftjoin('tb_projects','tb_projects.company_id','=','tb_companies.id')
            ->where('tb_projects.id','!=',null);

        if(Auth::user()->role_id == 7) {
            $companies = $companies->whereIn('tb_companies.id',$client_companies);
        }

        $companies = $companies->orderBy('tb_companies.company_name','asc')
            ->select('tb_companies.*')
            ->groupBy('tb_companies.id')
            ->get();

        $tasks_and_notes = collect();
        $projects = null;
        $attachments = null;
        $selected_project = null;
        $notes_without_main = null;
        $notes_with_main = null;
        $users_without_main = null;
        $users_with_main = null;
        $note_attachments_without_main = null;
        $note_attachments_with_main = null;

        if($request->has('project')){
            $selected_project = $request->project;
        }

        if($company_id != null) {

            $tasks_and_notes = $taskSummaryItemsQuery->execute($company_id, $selected_project);

            $projects = TbProject::where('company_id',$company_id)
                ->orderBy('title','asc')
                ->get();

            $unique_task_ids = $tasks_and_notes->pluck('unique_task_id')->all();
            $sub_task_id_array = array();
            $main_task_id_array = array();
            foreach($unique_task_ids as $unique_task_id) {
                if($unique_task_id < 0 ) {
                    $sub_task_id_array[] = abs($unique_task_id);
                } else {
                    $main_task_id_array[] = $unique_task_id;
                }
            }

            $notes_without_main = DB::table('tb_daily_task_notes')
                ->leftjoin('users','users.id','=','tb_daily_task_notes.created_by')
                ->whereIn('tb_daily_task_notes.task_id',$sub_task_id_array)
                ->select(['tb_daily_task_notes.*','users.name as user'])
                ->orderBy('tb_daily_task_notes.created_at','desc')
                ->get();

            $notes_with_main = DB::table('tb_daily_task_notes')
                ->leftjoin('tb_daily_log_sub_tasks','tb_daily_task_notes.task_id','=','tb_daily_log_sub_tasks.id')
                ->leftjoin('users','users.id','=','tb_daily_task_notes.created_by')
                ->whereIn('tb_daily_log_sub_tasks.main_task_id',$main_task_id_array)
                ->select(['tb_daily_task_notes.*','tb_daily_log_sub_tasks.main_task_id','users.name as user'])
                ->orderBy('tb_daily_task_notes.created_at','desc')
                ->get();

            $users_without_main = DB::table('tb_daily_log_sub_tasks')
                ->leftjoin('users','users.id','=','tb_daily_log_sub_tasks.assigned_to')
                ->whereIn('tb_daily_log_sub_tasks.id',$sub_task_id_array)
                ->select(['tb_daily_log_sub_tasks.id','users.name'])
                ->get();

            $users_with_main = DB::table('tb_daily_log_sub_tasks')
                ->leftjoin('users','users.id','=','tb_daily_log_sub_tasks.assigned_to')
                ->whereIn('tb_daily_log_sub_tasks.main_task_id',$main_task_id_array)
                ->select(['tb_daily_log_sub_tasks.main_task_id','users.name'])
                ->get();

            $note_attachments_without_main = DB::table('tb_daily_task_attachments')
                ->leftjoin('tb_daily_log_sub_tasks','tb_daily_task_attachments.task_id','=','tb_daily_log_sub_tasks.id')
                ->where('tb_daily_task_attachments.note_id','!=',null)
                ->whereIn('tb_daily_log_sub_tasks.id',$sub_task_id_array)
                ->select(['tb_daily_task_attachments.*'])
                ->get();

            $note_attachments_with_main = DB::table('tb_daily_task_attachments')
                ->leftjoin('tb_daily_log_sub_tasks','tb_daily_task_attachments.task_id','=','tb_daily_log_sub_tasks.id')
                ->where('tb_daily_task_attachments.note_id','!=',null)
                ->whereIn('tb_daily_log_sub_tasks.main_task_id',$main_task_id_array)
                ->select(['tb_daily_log_sub_tasks.main_task_id','tb_daily_task_attachments.*'])
                ->get();
        }

        $data = [
            'companies' => $companies,
            'selected_company' => $company_id,
            'selected_project' => $selected_project,
            'tasks_and_notes' => $tasks_and_notes,
            'projects' => $projects,
            'note_attachments_without_main' => $note_attachments_without_main,
            'note_attachments_with_main' => $note_attachments_with_main,
            'notes_without_main' => $notes_without_main,
            'notes_with_main' => $notes_with_main,
            'users_without_main' => $users_without_main,
            'users_with_main' => $users_with_main,
        ];

        if($request->ajax()) {
            return response()->json([
                'html' => view('admin.dailytask.summary-task-items', [
                    'tasks_and_notes' => $tasks_and_notes,
                    'note_attachments_without_main' => $note_attachments_without_main,
                    'note_attachments_with_main' => $note_attachments_with_main,
                    'notes_without_main' => $notes_without_main,
                    'notes_with_main' => $notes_with_main,
                    'users_without_main' => $users_without_main,
                    'users_with_main' => $users_with_main,
                ])->render(),
                'hasPages' => $tasks_and_notes->hasMorePages(),
            ]);
        }
        return view('admin.dailytask.summary', $data);
    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            // Check permission
            $this->authorize('delete', $data);

            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses_recursive($model)))) {
                $this->cleanup($dataType, $data);
            }
        }

        $displayName = count($ids) > 1 ? $dataType->getTranslatedAttribute('display_name_plural') : $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->destroy($ids);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect()->back()->with($data);
    }

    public function downloadAttachment(Request $request, $id, $file_id) {
        $file = DB::table('tb_daily_task_attachments')
            ->where('id',$file_id)
            ->first();
        return response()->download(public_path().'/uploads/notes/'.$file->attachment, $file->file_name);
    }

    public function calendarData(Request $request) {
        $events = TbDailyLogSubTask::leftjoin('users','users.id','=','tb_daily_log_sub_tasks.assigned_to');
        if($request->has('assigned-to')) {
            $events = $events->where('users.id','=',$request->get('assigned-to'));
        }
        if($request->has('start')) {
            $events = $events->where('tb_daily_log_sub_tasks.created_at','>=',Carbon::parse($request->get('start'))->startOfDay());
        }
        if($request->has('end')) {
            $events = $events->where('tb_daily_log_sub_tasks.created_at','<=',Carbon::parse($request->get('end'))->endOfDay());
        }
        $events = $events->groupBy([
            'tb_daily_log_sub_tasks.assigned_to',
            DB::raw('YEAR(tb_daily_log_sub_tasks.created_at)'),
            DB::raw('MONTH(tb_daily_log_sub_tasks.created_at)'),
            DB::raw('DAY(tb_daily_log_sub_tasks.created_at)'),
        ])
            ->select([
                DB::raw("CONCAT(users.name,' - ',SUM(tb_daily_log_sub_tasks.value)) as title"),
                DB::raw("DATE_FORMAT(tb_daily_log_sub_tasks.created_at,'%Y-%m-%d') as start"),
            ])
            ->get();

        return response()->json($events) ;
    }
}
