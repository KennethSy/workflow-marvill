<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use Validator, Redirect, Response, File;
use Socialite;
use App\TbBookkeeping;
use App\TbCompany;
use Carbon\Carbon;
use Image, Storage, PDF, Auth;

class HealthSurveyController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function survey(Request $request)
    {
        if(DB::table('tb_health_survey_employees')
            ->where('user_id',Auth::user()->id)
            ->where('created_at','>=',Carbon::now()->startOfDay())
            ->where('created_at','<=',Carbon::now()->endofDay())
            ->first()) {
            return redirect()->route('voyager.dashboard');
        }
        $questions = DB::table('tb_health_survey_questions')->get();
        return view('admin.healthsurvey.survey')->with([
            'questions' => $questions
        ]);
    }

    public function surveySubmit(Request $request)
    {
        $survey_id = DB::table('tb_health_survey_employees')
            ->insertGetId([
                'status' => 'Pending',
                'user_id' => Auth::user()->id,
            ]);

        $yes = 0;
        foreach($request->question as $id => $question) {
            DB::table('tb_health_survey_employee_answers')
                ->insert([
                    'survey_id' => $survey_id,
                    'question_id' => $id,
                    'answer' => $question,
                ]);
            if($question == 'Yes') {
                $yes++;
            }
        }

        if($yes > 0) {
            DB::table('tb_health_survey_employees')
                ->where('id',$survey_id)
                ->update([
                    'status' => 'Deny'
                ]);
        } else {
            DB::table('tb_health_survey_employees')
                ->where('id',$survey_id)
                ->update([
                    'status' => 'Allow'
                ]);
        }

        return redirect()->route('voyager.dashboard')->with([
            'message'    => "Survey answers have been submitted",
            'alert-type' => 'success',
        ]);
    }

    public function index(Request $request)
    {
        if(isset($request->date)) {
            $date = Carbon::now()->format('Y-m-d');
        } else {
            $date = Carbon::now()->format('Y-m-d');
        }


        $survey_results = DB::table('users')
            ->leftjoin('tb_health_survey_employees','tb_health_survey_employees.user_id','=','users.id')
            ->leftjoin('tb_health_survey_employee_answers','tb_health_survey_employee_answers.survey_id','=','tb_health_survey_employees.id')
            ->where('users.role_id',4)
            ->where('users.is_active',1)
            ->where( function($q) use($date) {
                $q->whereDate('tb_health_survey_employees.created_at',$date)
                    ->orWhere('tb_health_survey_employees.id',null);
            })
            ->select([
                'users.name',
                'tb_health_survey_employees.*',
                DB::raw("SUM(case tb_health_survey_employee_answers.answer
             when 'Yes' then 1 else 0 end) as yes_answers"),
                DB::raw("SUM(case tb_health_survey_employee_answers.answer
             when 'No' then 1 else 0 end) as no_answers"),
            ])
            ->groupBy('tb_health_survey_employees.id','users.id')
            ->orderBy('users.name','asc')
            ->get();

        $survey_answers = DB::table('tb_health_survey_employees')
        ->leftjoin('tb_health_survey_employee_answers','tb_health_survey_employee_answers.survey_id','=','tb_health_survey_employees.id')
        ->whereDate('tb_health_survey_employees.created_at',$date)
        ->select([
            'tb_health_survey_employee_answers.*',
        ])
        ->get();
        $survey_answers = $survey_answers->groupBy('survey_id')->toArray();

        $survey_questions = DB::table('tb_health_survey_questions')
            ->get();

        if(isset($request->action) && $request->action == 'print') {
            return PDF::loadView('admin.healthsurvey.pdf', [
                'survey_results' => $survey_results,
                'date' => $date,
            ])->inline('health-survey-'.$date.'.pdf');
        }

        return view('admin.healthsurvey.index')->with([
            'survey_results' => $survey_results,
            'date' => $date,
            'survey_answers' => $survey_answers,
            'survey_questions' => $survey_questions,
        ]);
    }
}
