<?php

namespace App\Http\Controllers\Admin;

use App\TbDailyLogSubTask;
use App\TbTaskStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use Validator, Redirect, Response, File;
use Socialite;
use App\User;
use Carbon\Carbon;
use App\TbDailyLog;
use App\TbCompany;
use App\TbProject;
use Image, Storage, PDF, DataTables, Auth;
use App\TbRequestTask;
use App\Http\Queries\TaskSummaryItems;
use App\Http\Queries\ConsolidatedTaskSummaryItems;
use Mail;
use App\Mail\TaskAssigned;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Events\BreadDataDeleted;
use App\TbCarAutoLoan;
use App\TbCarBrand;
use App\TbCarModel;

class CarLoanController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function apply(Request $request) {
        $loan = new TbCarAutoLoan();
        $brands = TbCarBrand::enabled()->get();
        $models = TbCarModel::enabled()->get();
        return view('frontend.pages.autoloan-apply', [
            'loan' => $loan,
            'brands' => $brands,
            'models' => $models,
        ]);
    }

    public function applyPost(Request $request) {
        $loan = new TbCarAutoLoan();
        $loan->first_name = $request->first_name;
        $loan->middle_name = $request->middle_name;
        $loan->last_name = $request->last_name;
        $loan->birthday = $request->birthday;
        $loan->mobile_number = $request->mobile_number;
        $loan->email_address = $request->email_address;
        $loan->home_address = $request->home_address;
        $loan->facebook_account = $request->facebook_account;
        $loan->home_ownership = $request->home_ownership;
        $loan->length_of_stay = $request->length_of_stay;
        $loan->civil_status = $request->civil_status;
        $loan->employer_business_name = $request->employer_business_name;
        $loan->tenure = $request->tenure;
        $loan->office_telephone = $request->office_telephone;
        $loan->office_address = $request->office_address;
        $loan->monthly_income = $request->monthly_income;
        $loan->tin_number = $request->tin_number;
        $loan->sss_number = $request->sss_number;
        $loan->brand_id = $request->car_brand;
        $loan->model_id = $request->car_model;
        $loan->loan_term = $request->loan_term;
        $loan->has_co_borrower = $request->co_borrower;
        if($request->co_borrower == 1) {
            $loan->co_first_name = $request->co_borrower_first_name;
            $loan->co_middle_name = $request->co_borrower_middle_name;
            $loan->co_last_name = $request->co_borrower_last_name;
            $loan->co_home_address = $request->co_borrower_home_address;
            $loan->co_birthday = $request->co_borrower_birthday;
            $loan->co_business_name = $request->co_borrower_employer_business_name;
            $loan->co_tenure = $request->co_borrower_tenure;
            $loan->co_office_telephone = $request->co_borrower_tenure;
        }
        $loan->save();
        return response()->json(['message' => 'success']);
    }

    public function applySuccess(Request $request) {
        return view('frontend.pages.autoloan-success', [
        ]);
    }

    public function getModels(Request $request) {
        $brand = TbCarBrand::where('id',$request->id)->enabled()->first();
        if($brand) {
            $models = TbCarModel::where('brand_id',$brand->id)->enabled()->select(['id','name'])->get();
            return response()->json($models);
        }

        return response()->json(null);
    }
}
