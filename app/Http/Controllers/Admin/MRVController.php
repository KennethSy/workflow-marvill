<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use Validator, Redirect, Response, File;
use Socialite;
use App\User;
use Carbon\Carbon;
use Image, Storage, PDF, DataTables, Auth;
use Mail;
use App\Mail\MultipleTasksAssigned;
use App\Libraries\gps;

class MRVController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function index(Request $request)
    {
        $this->authorize('browse_mrv_tracking');

        return view('admin.mrv.index')->with([

        ]);
    }
	
	public function login(Request $request)
	{
		//dd($request);
		//dd($request);
		
		$username = $request->username;
		$password = $request->password;

		$login = gps::login($username, $password);
		
		//dd($login);
		
		if ($login->responseCode==200)
		{
			//Login Success
			$sessionId = gps::$cookie;
			session(['sessionId' => $sessionId]);

			$response = $login->response;
			$responseCode = $login->responseCode;

			$temp = json_decode($login->response);
			session(['userId' => $temp->id]);
			
			$b = gps::devices($sessionId);
			
			//$c = gps::device($sessionId,1);
			
			$d = json_decode($b->response);
			
			return view('admin.mrv.map')->with([
			   'data' => $d
			]);
		}
		else
		{
			//Login Fail
			return redirect()->back()->with([
			'message' => "Login Failed", 'alert-type' => 'error'
			]);
		}	
		
		
	}
	

}
