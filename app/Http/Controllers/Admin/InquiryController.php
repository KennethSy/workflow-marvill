<?php

namespace App\Http\Controllers\Admin;
use App\TbInquiry;
use \TCG\Voyager\Models\Post;
use Auth;

class InquiryController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function reply($id){
        $userId = Auth::id();

        $user =  \DB::table('users')->where('id',$userId)->first();

      \DB::table('tb_inquiries')->where('id',$id)->update([
            'status' => 'Replied',
            'responded_by' => $user->name,

        ]);
        return redirect()->back();
    }

}
