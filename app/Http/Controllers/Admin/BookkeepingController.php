<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use Validator, Redirect, Response, File;
use Socialite;
use App\TbBookkeeping;
use App\TbBookkeepingAccountingCategory;
use App\TbAccountingCategory;
use App\TbJournals;
use App\TbCompany;
use Carbon\Carbon;
use Image, Storage, PDF, Auth;

class BookkeepingController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        if(!isset($request->date)) {
            $start_of_month = Carbon::now()->startOfMonth()->format('Y-m-d');
            $end_of_month = Carbon::now()->endOfMonth()->format('Y-m-d');
            $date = Carbon::now()->startOfMonth()->format('M-Y');
        } else {
            $start_of_month = Carbon::createFromFormat('M-Y',$request->date)->startOfMonth()->format('Y-m-d');
            $end_of_month = Carbon::createFromFormat('M-Y',$request->date)->endOfMonth()->format('Y-m-d');
            $date = $request->date;
        }

        if(!isset($request->company)) {
            $selected_company = 'all';
        } else {
            $selected_company = $request->company;
        }

        $records = TbBookkeeping::leftJoin('tb_companies','tb_companies.id','=','tb_bookkeeping.company_id')
            ->leftJoin('tb_bookkeeping_accounting_categories','tb_bookkeeping_accounting_categories.id','=','tb_bookkeeping.accounting_category_id')
            ->where('date','>=',$start_of_month)
            ->where('date','<=',$end_of_month);
        if($selected_company != 'all') {
            $records = $records->where('company_id','=',$selected_company);
        }
        $records = $records
            ->orderBy('tb_bookkeeping.id','desc')
            ->select([
                'tb_bookkeeping.*',
                'tb_companies.company_name as company',
                'tb_bookkeeping_accounting_categories.account_code',
                'tb_bookkeeping_accounting_categories.account_name',
            ])
            ->get();

        $summary = TbBookkeeping::where('date','>=',$start_of_month)
            ->where('date','<=',$end_of_month);
        if($selected_company != 'all') {
            $summary = $summary->where('company_id','=',$selected_company);
        }
        $summary = $summary->select([
                DB::raw('SUM(income) as total_income'),
                DB::raw('SUM(expense) as total_expense'),
                DB::raw('SUM(income) - SUM(expense) as total_balance'),
            ])
            ->first();

        $companies = TbCompany::where('is_main',1)->orWhere('is_sub',1)
            ->orderBy('company_name','asc')
            ->get();

        return view('admin.bookkeeping.browse')->with([
            'records' => $records,
            'summary' => $summary,
            'current_balance' => $summary->total_balance,
            'dataType' => $dataType,
            'date' => $date,
            'selected_company' => $selected_company,
            'companies' => $companies,
        ]);
    }

    public function create(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $record = new TbBookkeeping();
        $companies = TbCompany::where('is_main',1)->orWhere('is_sub',1)
            ->orderBy('company_name','asc')
            ->get();
        $record->date = Carbon::now()->format('d-M-Y');
        $employees = User::where('role_id',4)
            ->get();

        $top_level_accounting_categories = TbAccountingCategory::topLevel()->get();
        $categories = array();
        foreach($top_level_accounting_categories as $accounting_category ) {
            $subcategories = $accounting_category->subcategories;
            foreach($subcategories as $subcategory) {
                $bookkeeping_categories = TbBookkeepingAccountingCategory::where('accounting_category_id',$subcategory->id)->get();
                foreach($bookkeeping_categories as $bookkeeping_category) {
                    $categories[$accounting_category->category_name][] = $bookkeeping_category;
                }
            }
        }

        return view('admin.bookkeeping.edit-add')->with([
            'dataType' => $dataType,
            'edit' => false,
            'record' => $record,
            'companies' => $companies,
            'employees' => $employees ,
            'categories' => $categories ,
        ]);
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $validator = Validator::make($request->all(), [
            'date' => 'required|date_format:d-M-Y',
            'company' => 'required|exists:tb_companies,id',
            'income_or_expense' => 'required|in:Income,Expense,Cash Advance',
            'income' => 'nullable|numeric',
            'expense' => 'nullable|numeric',
            'tin_no' => 'nullable|string',
            'description' => 'required|string',
            'cheque_no' => 'nullable|string',
            'attachment' => 'nullable|image',
            'category' => 'required|exists:tb_bookkeeping_accounting_categories,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if(!$request->file('attachment')) {
            $attachment = null;
        } else {
            $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.jpg';
            while(TbBookkeeping::where('attachment',$filename)->first()) {
                $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.jpg';
            }
            Image::make($request->file('attachment'))->save(public_path().'/uploads/bookkeeping/'.$filename,75);
            $attachment = $filename;
        }

        $date = Carbon::createFromFormat('d-M-Y',$request->date)->format('Y-m-d');
        $data_id = TbBookkeeping::insertGetId([
            'description' => $request->description.($request->income_or_expense == 'Cash Advance'?' - '.User::where('id',$request->employee)->first()->name:''),
            'company_id' => $request->company,
            'income' => $request->income_or_expense == 'Income'?$request->income:0,
            'expense' => ($request->income_or_expense == 'Expense'||$request->income_or_expense == 'Cash Advance')?$request->expense:0,
            'tin_no' => $request->tin_no,
            'date' => $date,
            'cheque_no' => $request->cheque_no,
            'attachment' => $attachment,
            'type' => $request->income_or_expense == 'Cash Advance'?'Cash Advance':null,
            'accounting_category_id' => $request->category,
        ]);

        if($request->income_or_expense == 'Cash Advance') {
            $payroll_employee = DB::table('tb_payroll_employee')
                ->leftjoin('tb_payroll','tb_payroll.id','=','tb_payroll_employee.payroll_id')
                ->where('tb_payroll.start_date','<=',$date)
                ->where('tb_payroll.end_date','>=',$date)
                ->where('tb_payroll_employee.user_id','=',$request->employee)
                ->select('tb_payroll_employee.*')
                ->first();

            $payroll_employee_details_id = DB::table('tb_payroll_employee_details')->insertGetId([
                'payroll_employee_id' => $payroll_employee->id,
                'name' => 'Cash Advance',
                'value' => $request->expense,
                'type' => 'Deduction',
            ]);

            DB::table('tb_cash_advances')->insert([
                'user_id' => $request->employee,
                'amount' => $request->expense,
                'disbursement_date' => Carbon::createFromFormat('d-M-Y',$request->date)->format('Y-m-d'),
                'reason' => $request->description,
                'status' => 0,
                'bookkeeping_id' => $data_id,
                'payroll_employee_details_id' => $payroll_employee_details_id,
                'created_by' => Auth::user()->id,
            ]);
        }

        $journal_counter = DB::table('tb_journal_counter')->first();
        if(Carbon::now()->year != $journal_counter->year) {
            DB::table('tb_journal_counter')->where('id',1)
            ->update([
                'year' => Carbon::now()->year,
                'count' => 1,
            ]);
            $journal_counter->year = Carbon::now()->year;
            $journal_counter->count = 1;
        } else {
            DB::table('tb_journal_counter')->where('id',1)
                ->update([
                    'count' => $journal_counter->count+1,
                ]);
            $journal_counter->count += 1;
        }

        TbJournals::insert([
            'bookkeeping_id' =>  $data_id,
            'journal_entry_no' => 'JV'.$journal_counter->year.'-'.str_pad($journal_counter->count, 4, '0', STR_PAD_LEFT),
            'status' => 'Pending',
        ]);

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', app($dataType->model_name))) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_added_new')." Bookkeeping Record",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $record = TbBookkeeping::findOrFail($id);
        $companies = TbCompany::where('is_main',1)->orWhere('is_sub',1)
            ->orderBy('company_name','asc')
            ->get();
        $record->company = TbCompany::where('id',$record->company_id)->first()->id;
        $record->date = Carbon::parse($record->date)->format('d-M-Y');
        $employees = User::where('role_id',4)
            ->get();

        $top_level_accounting_categories = TbAccountingCategory::topLevel()->get();
        $categories = array();
        foreach($top_level_accounting_categories as $accounting_category ) {
            $subcategories = $accounting_category->subcategories;
            foreach($subcategories as $subcategory) {
                $bookkeeping_categories = TbBookkeepingAccountingCategory::where('accounting_category_id',$subcategory->id)->get();
                foreach($bookkeeping_categories as $bookkeeping_category) {
                    $categories[$accounting_category->category_name][] = $bookkeeping_category;
                }
            }
        }

        return view('admin.bookkeeping.edit-add')->with([
            'dataType' => $dataType,
            'edit' => true,
            'record' => $record,
            'companies' => $companies,
            'employees' => $employees,
            'categories' => $categories ,
        ]);

    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $record = TbBookkeeping::findOrFail($id);

        // Validate fields with ajax
        $validator = Validator::make($request->all(), [
            'date' => 'required|date_format:d-M-Y',
            'company' => 'required|exists:tb_companies,id',
            'income_or_expense' => 'required|in:Income,Expense,Cash Advance',
            'income' => 'nullable|numeric',
            'expense' => 'nullable|numeric',
            'tin_no' => 'nullable|string',
            'description' => 'required|string',
            'cheque_no' => 'nullable|string',
            'attachment' => 'nullable|image',
            'category' => 'required|exists:tb_bookkeeping_accounting_categories,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if($request->attachment_file_name != null && $record->attachment != null) {
            $attachment = $record->attachment;
        } elseif(!$request->file('attachment')) {
            if($request->attachment_file_name == null && $record->attachment != null) {
                File::delete(public_path().'/uploads/bookkeeping/'.$record->attachment);
            }
            $attachment = null;
        } else {
            $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.jpg';
            while(TbBookkeeping::where('attachment',$filename)->first()) {
                $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.jpg';
            }
            Image::make($request->file('attachment'))->save(public_path().'/uploads/bookkeeping/'.$filename,75);
            $attachment = $filename;
        }

        $data = TbBookkeeping::where('id',$id)
        ->update([
            'description' => $request->description,
            'company_id' => $request->company,
            'income' => $request->income_or_expense == 'Income'?$request->income:0,
            'expense' => ($request->income_or_expense == 'Expense'||$request->income_or_expense == 'Cash Advance')?$request->expense:0,
            'tin_no' => $request->tin_no,
            'date' => Carbon::createFromFormat('d-M-Y',$request->date)->format('Y-m-d'),
            'cheque_no' => $request->cheque_no,
            'attachment' => $attachment,
            'accounting_category_id' => $request->category,
        ]);

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', app($dataType->model_name))) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_updated')." Bookkeeping Record",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    public function export(Request $request)
    {
        $slug = $this->getSlug($request);

        if(!isset($request->date)) {
            $start_of_month = Carbon::now()->startOfMonth()->format('Y-m-d');
            $end_of_month = Carbon::now()->endOfMonth()->format('Y-m-d');
            $date = Carbon::now()->startOfMonth()->format('M-Y');
        } else {
            $start_of_month = Carbon::createFromFormat('M-Y',$request->date)->startOfMonth()->format('Y-m-d');
            $end_of_month = Carbon::createFromFormat('M-Y',$request->date)->endOfMonth()->format('Y-m-d');
            $date = $request->date;
        }

        if(!isset($request->company)) {
            $selected_company = 'all';
        } else {
            $selected_company = $request->company;
        }

        $records = TbBookkeeping::leftJoin('tb_companies','tb_companies.id','=','tb_bookkeeping.company_id')
            ->where('date','>=',$start_of_month)
            ->where('date','<=',$end_of_month);
        if($selected_company != 'all') {
            $records = $records->where('company_id','=',$selected_company);
        }
        $records = $records
            ->orderBy('tb_bookkeeping.id','desc')
            ->select([
                'tb_bookkeeping.*',
                'tb_companies.company_name as company'
            ])
            ->get();

        $summary = TbBookkeeping::where('date','>=',$start_of_month)
            ->where('date','<=',$end_of_month);
        if($selected_company != 'all') {
            $summary = $summary->where('company_id','=',$selected_company);
        }
        $summary = $summary->select([
            DB::raw('SUM(income) as total_income'),
            DB::raw('SUM(expense) as total_expense'),
            DB::raw('SUM(income) - SUM(expense) as total_balance'),
        ])
            ->first();

        $companies = TbCompany::where('is_main',1)->orWhere('is_sub',1)
            ->orderBy('company_name','asc')
            ->get();

        $company_title='';
        $date_title='';
        if($selected_company != 'all') {
            $company_title = '_'.TbCompany::where('id',$selected_company)->first()->company_name;
        }
        if(isset($request->date)) {
            $date_title = '_'.$request->date;
        }

        $title = 'Bookkeeping'.$company_title.$date_title;
        return view('admin.bookkeeping.excel')
            ->with(compact('records','title'));
    }
}
