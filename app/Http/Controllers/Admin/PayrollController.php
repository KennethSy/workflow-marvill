<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use Validator, Redirect, Response, File;
use Socialite;
use App\User;
use App\TbDailyLogSubTask;
use App\TbBookkeeping;
use Carbon\Carbon;
use Image, Storage, PDF, DataTables, Auth;

class PayrollController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function test(Request $request)
    {
        //$this->authorize('browse_socials');
        $employees = User::getAllActiveEmployees();

        if(Carbon::now() >= Carbon::now()->startOfMonth()->addDays(14)) {
            $payroll_start = Carbon::now()->startOfMonth()->addDays(15)->subDay();
            $payroll_end = Carbon::now()->endOfMonth()->subDay();
        } else {
            $payroll_start = Carbon::now()->startOfMonth()->subDay();
            $payroll_end = Carbon::now()->startOfMonth()->addDays(14)->endOfDay()->subDay();
        }


        $payroll = DB::table('tb_payroll')->where('start_date',$payroll_start->format('Y-m-d'))->where('end_date',$payroll_end->format('Y-m-d'))->first();
        if(isset($request->id)) {
            $payroll = DB::table('tb_payroll')->where('id',$request->id)->first();
            $payroll_start = Carbon::parse($payroll->start_date)->startOfDay();
            $payroll_end = Carbon::parse($payroll->end_date)->endOfDay();
        }
        $payroll_settings = DB::table('tb_payroll_settings')->get();

        $payroll_earnings = $payroll_settings->where('name','!=','Basic Pay')->where('type','Earning')->all();
        $payroll_deductions = $payroll_settings->where('type','Deduction')->all();

        $previous_payroll = DB::table('tb_payroll')->where('end_date','<=',$payroll_start)->orderBy('end_date','desc')->first();
        $next_payroll = DB::table('tb_payroll')->where('start_date','>=',$payroll_end)->orderBy('start_date','asc')->first();

        $employee_payroll = array();
        $total_unreleased_net_pay = 0;
        $total_net_pay = 0;

        foreach($employees as $employee) {
            $payroll_per_employee = DB::table('tb_payroll_employee')->where('user_id',$employee->id)->where('payroll_id',$payroll->id)->first();
            if($payroll_per_employee) {
                $employee_payroll[$employee->id]['payroll_employee_id'] = $payroll_per_employee->id;
                $employee_payroll[$employee->id]['is_paid'] = $payroll_per_employee->is_paid;
                $employee_payroll[$employee->id]['basic_pay'] = TbDailyLogSubTask::where('assigned_to',$employee->id)->where('confirmed_at','>=',$payroll_start)->where('confirmed_at','<=',$payroll_end)->sum('value');
                $employee_payroll[$employee->id]['allowance'] = 0;
                $employee_payroll[$employee->id]['total'] = 0;
                $employee_payroll[$employee->id]['net_pay'] = 0;
                $employee_payroll[$employee->id]['expected_salary'] = ($employee->basic_salary * $employee->working_days) / 2;
                $employee_payroll[$employee->id]['other_earnings'] = array();
                $employee_payroll[$employee->id]['other_deductions'] = array();

                if($employee_payroll[$employee->id]['basic_pay'] > $payroll_settings->where('name','Basic Pay')->first()->default_value) {
                    $employee_payroll[$employee->id]['allowance'] = $employee_payroll[$employee->id]['basic_pay'] - $payroll_settings->where('name','Basic Pay')->first()->default_value;
                    $employee_payroll[$employee->id]['basic_pay'] = $payroll_settings->where('name','Basic Pay')->first()->default_value;
                }

                $employee_payroll[$employee->id]['total'] += ($employee_payroll[$employee->id]['allowance'] + $employee_payroll[$employee->id]['basic_pay']);
                $employee_payroll[$employee->id]['net_pay'] += ($employee_payroll[$employee->id]['allowance'] + $employee_payroll[$employee->id]['basic_pay']);

                $employee_payroll_earnings = DB::table('tb_payroll_employee_details')->where('payroll_employee_id',$payroll_per_employee->id)->where('name','!=','Basic Pay')->where('type','Earning')->get();
                $employee_payroll_deductions = DB::table('tb_payroll_employee_details')->where('payroll_employee_id',$payroll_per_employee->id)->where('type','Deduction')->get();
                foreach($employee_payroll_earnings as $earning) {
                    if($payroll_settings->where('name',$earning->name)->first()) {
                        $employee_payroll[$employee->id][$earning->name] = $earning->value;
                    } else {
                        $employee_payroll[$employee->id]['other_earnings'][$earning->name] = $earning->value;
                    }
                    $employee_payroll[$employee->id]['total'] += $earning->value;
                    if($earning->name != 'Food Allowance' && $earning->name != 'Electricity' && $earning->name != 'Water' && $earning->name != 'Rent' && $earning->name != 'SSS Employer Contribution' && $earning->name != 'Communication') {
                        $employee_payroll[$employee->id]['net_pay'] += $earning->value;
                    }
                }
                foreach($employee_payroll_deductions as $deduction) {
                    if($payroll_settings->where('name',$deduction->name)->first()) {
                        if(isset($employee_payroll[$employee->id][$deduction->name])) {
                            $employee_payroll[$employee->id][$deduction->name] += $deduction->value;
                        } else {
                            $employee_payroll[$employee->id][$deduction->name] = $deduction->value;
                        }
                    } else {
                        $employee_payroll[$employee->id]['other_deductions'][$deduction->name] = $deduction->value;
                    }
                    //$employee_payroll[$employee->id]['total'] -= $deduction->value;
                    $employee_payroll[$employee->id]['net_pay'] -= $deduction->value;
                }

                if($payroll_per_employee->is_paid == 0) {
                    $total_unreleased_net_pay += $employee_payroll[$employee->id]['net_pay'];
                }
                $total_net_pay += $employee_payroll[$employee->id]['net_pay'];
            }
        }

        return view('admin.payroll.index')->with([
            'employees' => $employees,
            'employee_payroll' => $employee_payroll,
            'payroll_earnings' => $payroll_earnings,
            'payroll_deductions' => $payroll_deductions,
            'payroll_start' => Carbon::parse($payroll_start)->addDay()->format('F j, Y'),
            'payroll_end' => Carbon::parse($payroll_end)->addDay()->format('F j, Y'),
            'payroll' => $payroll,
            'total_unreleased_net_pay' => $total_unreleased_net_pay ,
            'total_net_pay' => $total_net_pay ,
            'previous_payroll' => $previous_payroll ,
            'next_payroll' => $next_payroll ,
        ]);
    }

    public function markPaid(Request $request)
    {
        //$this->authorize('browse_socials');
        $payroll_settings = DB::table('tb_payroll_settings')->get();

        $payroll_earnings = $payroll_settings->where('name','!=','Basic Pay')->where('type','Earning')->all();
        $payroll_deductions = $payroll_settings->where('type','Deduction')->all();

        $payroll_employee_ids = explode(',',$request->ids);
        foreach($payroll_employee_ids as $payroll_employee_id) {
            $payroll_employee = DB::table('tb_payroll_employee')->where('id',$payroll_employee_id)->first();
            $payroll = DB::table('tb_payroll')->where('id',$payroll_employee->payroll_id)->first();

            DB::table('tb_payroll_employee')->where([
                'id' => $payroll_employee_id,
            ])->update([
                'is_paid' => 1
            ]);

            $employee_id = DB::table('tb_payroll_employee')->where([
                'id' => $payroll_employee_id,
            ])->first()->user_id;
            $user = User::where('id',$employee_id)->first();

            $earnings['Basic Pay'] = TbDailyLogSubTask::where('assigned_to',$user->id)
                ->where('confirmed_at','>=',Carbon::parse($payroll->start_date)->startOfDay())
                ->where('confirmed_at','<=',Carbon::parse($payroll->end_date)->endOfDay())
                ->sum('value');

            $allowance = 0;
            $net_pay = 0;

            $basic_pay = TbDailyLogSubTask::where('assigned_to',$user->id)
                ->where('confirmed_at','>=',Carbon::parse($payroll->start_date)->startOfDay())
                ->where('confirmed_at','<=',Carbon::parse($payroll->end_date)->endOfDay())
                ->sum('value');

            if($basic_pay > $payroll_settings->where('name','Basic Pay')->first()->default_value) {
                $allowance = $basic_pay - $payroll_settings->where('name','Basic Pay')->first()->default_value;
                $basic_pay = $payroll_settings->where('name','Basic Pay')->first()->default_value;
            }

            $net_pay += ($allowance + $basic_pay);

            $employee_payroll_earnings = DB::table('tb_payroll_employee_details')->where('payroll_employee_id',$payroll_employee_id)->where('name','!=','Basic Pay')->where('type','Earning')->get();
            $employee_payroll_deductions = DB::table('tb_payroll_employee_details')->where('payroll_employee_id',$payroll_employee_id)->where('type','Deduction')->get();

            foreach($employee_payroll_earnings as $earning) {
                if($earning->name != 'Food Allowance' && $earning->name != 'Electricity' && $earning->name != 'Water' && $earning->name != 'Rent' && $earning->name != 'SSS Employer Contribution'  && $earning->name != 'Communication') {
                    $net_pay += $earning->value;
                }
            }
            foreach($employee_payroll_deductions as $deduction) {
                $net_pay -= $deduction->value;
            }

            TbBookkeeping::insert([
                'description' => $user->name.' Payroll',
                'company_id' => 1,
                'income' => 0,
                'expense' => $net_pay,
                'date' => Carbon::now()->format('Y-m-d'),
            ]);
        }

        $redirect = redirect()->route("payroll.index");
        return $redirect->with([
            'message'    => "Successfully Marked as Released",
            'alert-type' => 'success',
        ]);
    }

    public function view(Request $request, $id)
    {
        //$this->authorize('browse_socials');
        $payroll_per_employee = DB::table('tb_payroll_employee')->where('id',$id)->first();
        $payroll = DB::table('tb_payroll')->where('id',$payroll_per_employee->payroll_id)->first();
        $payroll_start = Carbon::parse($payroll->start_date)->startOfDay();
        $payroll_end = Carbon::parse($payroll->end_date)->endOfDay();
        $payroll_settings = DB::table('tb_payroll_settings')->get();
        $employee = User::where('id',$payroll_per_employee->user_id)->first();

        $employee_payroll = array();
        $earnings = array();
        $deductions = array();
        $total_net_pay = 0;

        $payroll_earnings = DB::table('tb_payroll_employee_details')->where('payroll_employee_id',$payroll_per_employee->id)->where('name','!=','Basic Pay')->where('type','Earning')->get();
        $payroll_deductions =  DB::table('tb_payroll_employee_details')->where('payroll_employee_id',$payroll_per_employee->id)->where('type','Deduction')->get();

        $earnings['Basic Pay'] = TbDailyLogSubTask::where('assigned_to',$employee->id)->where('confirmed_at','>=',$payroll_start)->where('confirmed_at','<=',$payroll_end)->sum('value');
        $earnings['Allowance'] = 0;

        if($earnings['Basic Pay'] > $payroll_settings->where('name','Basic Pay')->first()->default_value) {
            $earnings['Allowance'] = $earnings['Basic Pay'] - $payroll_settings->where('name','Basic Pay')->first()->default_value;
            $earnings['Basic Pay'] = $payroll_settings->where('name','Basic Pay')->first()->default_value;
        }

        $total_net_pay += ($earnings['Allowance'] + $earnings['Basic Pay']);

        foreach($payroll_earnings as $earning) {
                $earnings[$earning->name] = $earning->value;
                if($earning->name != 'Food Allowance' && $earning->name != 'Electricity' && $earning->name != 'Water' && $earning->name != 'Rent' && $earning->name != 'SSS Employer Contribution' && $earning->name != 'Communication') {
                    $total_net_pay += $earning->value;
                }
        }
        foreach($payroll_deductions as $deduction) {

                    if(isset($deductions[$deduction->name])) {
                        $deductions[$deduction->name] += $deduction->value;
                    } else {
                        $deductions[$deduction->name] = $deduction->value;
                    }
                    $total_net_pay -= $deduction->value;
        }

        $earnings = (object) $earnings;
        $deductions = (object) $deductions;

        return view('admin.payroll.read')->with([
            'employee' => $employee,
            'earnings' => $earnings,
            'deductions' => $deductions,
            'payroll_start' => Carbon::parse($payroll_start)->format('F j, Y'),
            'payroll_end' => Carbon::parse($payroll_end)->format('F j, Y'),
            'total_net_pay' => $total_net_pay,
        ]);
    }

    public function adjustment(Request $request, $id)
    {
        //$this->authorize('browse_socials');
        $employees = User::where('role_id',4)->get();
        $payroll = DB::table('tb_payroll')->where('id',$id)->first();
        $payroll_start = $payroll->start_date;
        $payroll_end = $payroll->end_date;

        return view('admin.payroll.adjustment')->with([
            'employees' => $employees,
            'edit' => false,
            'payroll_id' => $id,
            'payroll_settings' => DB::table('tb_payroll_settings')->where('name','!=','Basic Pay')->get(),
            'payroll_start' => Carbon::parse($payroll_start)->format('F j, Y'),
            'payroll_end' => Carbon::parse($payroll_end)->format('F j, Y'),
        ]);
    }

    public function saveAjustment(Request $request, $id)
    {
        //$this->authorize('browse_socials');

        foreach($request->employees as $employee) {
            $payroll_employee_id = DB::table('tb_payroll_employee')->where([
                'user_id' => $employee,
                'payroll_id' => $id,
            ])->first()->id;

            DB::table('tb_payroll_employee_details')->updateOrInsert(
                [
                    'payroll_employee_id' => $payroll_employee_id,
                    'name' => $request->name,
                ],
                [
                    'payroll_employee_id' => $payroll_employee_id,
                    'name' => $request->name,
                    'value' => $request->value,
                    'type' => $request->type,
                ]
            );
        }

        $redirect = redirect()->route("payroll.index");
        return $redirect->with([
            'message'    => __('voyager::generic.successfully_added_new')." Adjustment",
            'alert-type' => 'success',
        ]);
    }
}
