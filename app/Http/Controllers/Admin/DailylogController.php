<?php

namespace App\Http\Controllers\Admin;

use App\TbDailyLogSubTask;
use App\TbLogStatus;
use App\TbTaskStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use Validator, Redirect, Response, File;
use Socialite;
use App\User;
use App\TbDailyLog;
use Carbon\Carbon;
use Image, Storage, PDF, DataTables, Auth;
use App\Mail\MultipleTasksAssigned;
use Mail;
class DailylogController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        return view('admin.dailylog.browse')->with([
            'dataType' => $dataType,
        ]);
    }

    public function data(Request $request)
    {
        $canView = Auth::user()->can('read', app('App\TbDailyLog'));

        $logs = TbDailyLog::leftjoin('users','users.id','=','tb_daily_log.assigned_to')
            ->leftjoin('tb_log_status','tb_log_status.id','=','tb_daily_log.status');

        if(Auth::user()->role_id != 1 && Auth::user()->role_id != 3) {
            $logs = $logs->where('tb_daily_log.assigned_to',Auth::user()->id);
        }

        $logs = $logs->select([
                'users.name as assigned_user',
                'tb_log_status.status as log_status',
                'tb_daily_log.*',
            ]);

        return DataTables::of($logs)
            ->addColumn('actions', function ($log) use($canView) {
                $button = '<div class="dropdown">
<button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown"> Action</button>
<ul class="dropdown-menu">';
                if($log->location != 'Home') {
                    $button .= '<li><a href="'.route('dailylog.switchloc',['id' => $log->id, 'work' => 'Home']).'" class="tips" title="Switch Location to Home"> Switch Location to Home </a></li>';
                }
                if($log->location != 'Office') {
                    $button .= '<li><a href="'.route('dailylog.switchloc',['id' => $log->id, 'work' => 'Office']).'" class="tips" title="Switch Location to Office"> Switch Location to Office </a></li>';
                }
                if($canView) {
                    $button .= '<li><a href="'.route('voyager.dailylog.show',['id' => $log->id ]).'" title="View" class="tips">View</li>';
                }
                $button .= '</ul></div>';
                return $button;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $this->authorize('read', app($dataType->model_name));

        $log = TbDailyLog::findOrFail($id);
        $tasks = TbDailyLogSubTask::where('tb_daily_log_sub_tasks.log_id',$log->id)
            ->leftjoin('tb_task_status','tb_task_status.id','=','tb_daily_log_sub_tasks.status')
            ->select([
                'tb_daily_log_sub_tasks.*',
                'tb_task_status.status as task_status'
            ])
            ->get();
        $status = TbLogStatus::find($log->status);
        $user = User::find($log->assigned_to);

        $data = [
            'log' => $log,
            'tasks' => $tasks,
            'user' => $user,
            'status' => $status,
        ];

        return view('admin.dailylog.read', $data);
    }

    public function create(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $log = new TbDailyLog();
        $users = User::get();

        return view('admin.dailylog.edit-add')->with([
            'dataType' => $dataType,
            'edit' => false,
            'log' => $log,
            'users' => $users,
        ]);
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $validator = Validator::make($request->all(), [
            'assigned_to' => 'required|exists:users,id',
            'working_from' => 'required|in:Home,Office',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::where('id',$request->assigned_to)->first();

        $id = TbDailyLog::insertGetId([
            'assigned_to' => $user->id,
            'location' => $request->working_from,
            'target_daily_rate' => $user->basic_salary,
            'achieved_rate' => 0,
            'created_at' => Carbon::now(),
        ]);

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', app($dataType->model_name))) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_added_new')." Log",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true]);
        }
    }

    public function edit(Request $request, $id)
    {
        if(isset($request->type) && $request->type == 'html') {
            return view('invoice2');
        }

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $invoice = new TbInvoice();
        $invoice_items = new TbInvoiceItem();
        $companies = TbCompany::get();
        $form_types = TbInvoiceFormType::get();

        $invoice->date = Carbon::now()->format('m/d/Y');

        return view('admin.invoices.edit-add')->with([
            'dataType' => $dataType,
            'edit' => false,
            'invoice' => $invoice,
            'invoice_items' => $invoice_items,
            'companies' => $companies,
            'form_types' => $form_types,
        ]);

    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        // Validate fields with ajax
        $validator = Validator::make($request->all(), [
            'date' => 'required|date_format:d-M-Y',
            'company' => 'required|exists:tb_companies,id',
            'income_or_expense' => 'required|in:Income,Expense',
            'income' => 'nullable|numeric',
            'expense' => 'nullable|numeric',
            'tin_no' => 'nullable|string',
            'description' => 'required|string',
            'cheque_no' => 'nullable|string',
            'attachment' => 'nullable|image',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if(!$request->file('attachment')) {
            $attachment = null;
        } else {
            $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.jpg';
            while(TbBookkeeping::where('attachment',$filename)->first()) {
                $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.jpg';
            }
            Image::make($request->file('attachment'))->save(public_path().'/uploads/bookkeeping/'.$filename,75);
            $attachment = $filename;
        }

        $data = TbBookkeeping::insert([
            'description' => $request->description,
            'company_id' => $request->company,
            'income' => $request->income_or_expense == 'Income'?$request->income:0,
            'expense' => $request->income_or_expense == 'Expense'?$request->expense:0,
            'tin_no' => $request->tin_no,
            'date' => Carbon::createFromFormat('d-M-Y',$request->date)->format('Y-m-d'),
            'cheque_no' => $request->cheque_no,
            'attachment' => $attachment,
        ]);

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', app($dataType->model_name))) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_added_new')." Bookkeeping Record",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    public function switchloc(Request $request, $id, $work)
    {
        TbDailyLog::where('id',$id)
            ->update(['location' => $work]);

        return redirect()->back()->with('message', 'Location Switched')->with('alert-type','success');
    }

    public function rollover(Request $request)
    {
        $employees = User::where('role_id',4)->get();

        foreach ($employees as $employee)
        {
            //Close all open 'Logs' for current employee
            TbDailyLog::where('assigned_to', $employee->id)
                ->where('status', 0)
                ->where('created_at', '<', Carbon::now()->startOfDay())
                ->update(['updated_at' => Carbon::now(), 'status' => 1]);

            //Check if it is Saturday
            if(Carbon::now()->dayOfWeek == Carbon::SATURDAY)
            {
                $created_at = Carbon::now()->addDay(2);
            }
            elseif(Carbon::now()->dayOfWeek == Carbon::SUNDAY)
            {
                $created_at = Carbon::now()->addDay(1);
            }
            else
            {
                $created_at = Carbon::now();
            }

            //Get open tasks for current employee
            $tasks = TbDailyLogSubTask::where('assigned_to',$employee->id)->where('created_at', '<', Carbon::now()->startOfDay())->where('status',0)->get();

            //Action if there are pending tasks
            if(!$tasks->isEmpty())
            {
                //Check if Log instance exists for $created_at date
                $log_exists = TbDailyLog::where('assigned_to',$employee->id)->whereDate('created_at',$created_at)->first();

                if($log_exists)
                {
                    $log_id = $log_exists->id;
                }
                else
                {
                    //Create new Log instance
                    $log_id = TbDailyLog::insertGetId(
                        [
                            'assigned_to' => $employee->id,
                            'location' => 'Pending',
                            'created_at' => $created_at,
                            'target_daily_rate' => $employee->basic_salary,
                        ]
                    );
                }


                //Update each task to the new Log's ID and half the value
                foreach ($tasks as $task)
                {
                    if($task->log_id==$log_id)
                    {
                    //skip
                    }
                    elseif(Carbon::now()->dayOfWeek == Carbon::SUNDAY || Carbon::now()->dayOfWeek == Carbon::SATURDAY )
                    {
                        //skip
                    }
                    elseif(Carbon::now()->dayOfWeek == Carbon::MONDAY && (Carbon::parse($task->created_at)->dayOfWeek == Carbon::SUNDAY || Carbon::parse($task->created_at)->dayOfWeek == Carbon::SATURDAY ))
                    {
                        //skip
                    }
                    else
                    {
                    $new_value = $task->value/2;
                    TbDailyLogSubTask::where('id', $task->id)
                        ->update(['log_id' => $log_id, 'value' => $new_value]);
                    }
                }

            }
        }
    }

    public function overdue() {
        $start = Carbon::now()->subDays(3)->startOfDay();
        $end = Carbon::now()->subDays(3)->endOfDay();
        $request_tasks = DB::table('tb_daily_log_sub_tasks')->leftjoin('tb_projects','tb_projects.id','=','tb_daily_log_sub_tasks.project_id')
            ->leftjoin('tb_companies','tb_companies.id','=','tb_projects.company_id')
            ->leftjoin('users','users.id','=','tb_daily_log_sub_tasks.assigned_to')
            ->whereBetween('tb_daily_log_sub_tasks.created_at', [$start, $end])
            ->where('tb_daily_log_sub_tasks.status', '=',0)
            //->where('tb_daily_log_sub_tasks.id', '<=',90)
            ->select([
                'tb_daily_log_sub_tasks.*',
                'tb_projects.title as project',
                'users.name as user',
                'tb_companies.company_name as company',
            ])
            ->get();

        foreach($request_tasks as $task) {
            DB::table('tb_daily_task_overdue_notif')->insert([
                'task_id' => $task->id,
            ]);
        }

        //Mail::to('rjvillanueva@marvill.com')->bcc('kenneth.sy@marvill.com')->send(new MultipleTasksAssigned($request_tasks,''));
    }

}
