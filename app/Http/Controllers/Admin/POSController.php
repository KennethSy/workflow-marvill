<?php

namespace App\Http\Controllers\Admin;

use App\TbBookkeeping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use Validator, Redirect, Response, File;
use Socialite;
use App\TbPosProduct;
use App\TbPosSupplier;
use App\TbPosUom;
use App\User;
use Carbon\Carbon;
use Image, Storage, PDF, DataTables, Auth;

class POSController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
  public function index(Request $request)
  {
      // GET THE SLUG, ex. 'posts', 'pages', etc.
      $slug = $this->getSlug($request);

      // GET THE DataType based on the slug
      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('browse', app($dataType->model_name));

      return view('admin.pos.suppliers')->with([
          'dataType' => $dataType,
      ]);
  }


}
