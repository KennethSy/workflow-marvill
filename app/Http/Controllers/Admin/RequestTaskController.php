<?php

namespace App\Http\Controllers\Admin;

use App\TbDailyLogSubTask;
use App\TbTaskStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Facades\Voyager;
use Validator, Redirect, Response, File;
use Socialite;
use App\User;
use Carbon\Carbon;
use App\TbDailyLog;
use App\TbCompany;
use App\TbProject;
use Image, Storage, PDF, DataTables, Auth, Mail;
use App\TbRequestTask;
use App\Mail\ClientTaskRequest;
use App\Mail\TeamTaskRequest;


class RequestTaskController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $filter_params = array();

        return view('admin.requesttask.browse')->with([
            'dataType' => $dataType,
        ]);
    }

    public function data(Request $request)
    {
        $canView = Auth::user()->can('read', app('App\TbRequestTask'));
        $canEdit = Auth::user()->can('edit', app('App\TbRequestTask'));
        $canDelete = Auth::user()->can('delete', app('App\TbRequestTask'));

        $requests = TbRequestTask::leftjoin('users','users.id','=','tb_request_tasks.created_by')
            ->leftjoin('tb_projects','tb_projects.id','=','tb_request_tasks.project_id')
            ->leftjoin('tb_companies','tb_companies.id','=','tb_projects.company_id')
            ->leftjoin('tb_request_task_assigned_users','tb_request_task_assigned_users.request_id','=','tb_request_tasks.id')
            ->leftjoin('users as assigned_users','tb_request_task_assigned_users.user_id','=','assigned_users.id')
            ->groupBy('tb_request_tasks.id');

        if(Auth::user()->role_id != 1 && Auth::user()->role_id != 3) {
            $requests->where('tb_request_tasks.created_by',Auth::user()->id);
        }

        $requests = $requests->select([
            'users.name as request_user',
            'tb_request_tasks.*',
            'tb_companies.company_name as company',
            'tb_projects.title as project',
            DB::raw("GROUP_CONCAT(assigned_users.name SEPARATOR ', ') as assigned_employees"),
        ]);

        return DataTables::of($requests)
            ->addColumn('actions', function ($request) use($canView, $canDelete, $canEdit) {
                return '<span id="button-'.$request->id.'">'.view('admin.requesttask.action-buttons')->with([
                        'request' => $request,
                        'canView' => $canView,
                        'canEdit' => $canEdit,
                        'canDelete' => $canDelete,
                    ])->render().'</span>';
            })
            ->editColumn('description', function ($request) {
                return nl2br($request->description);
            })
            ->editColumn('status', function ($request) {
                if($request->status == 0) {
                    return 'Pending';
                }
                if($request->status == 1) {
                    return 'Approved';
                }
                if($request->status == 2) {
                    return 'Rejected';
                }
            })
            ->rawColumns(['actions','description',])
            ->setRowId(function ($request) {
                return'row-'.$request->id;
            })
            ->make(true);
    }

    public function create(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'add');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'add', $isModelTranslatable);

        $view = 'admin.requesttask.edit-add';

        $request = new TbRequestTask();
        $users = User::getAllActiveEmployeesAndAdmins();

        if(Auth::user()->role_id == 7) {
            $client_companies = array();

            $directories = DB::table('tb_directories')->where('contact_email',Auth::user()->email)->get();
            foreach($directories as $directory) {
                $client_companies[] = $directory->contact_company_id;
            }

            $companies = TbCompany::whereIn('id',$client_companies)->get();
        } else {
            $companies = TbCompany::get();
        }

        $projects = TbProject::get();

        $request->company_id = null;

        $project_data = array();
        foreach($companies as $company) {
            $project_data[$company->id] = array();
            foreach($projects->where('company_id','=',$company->id)->all() as $project) {
                $project_data[$company->id][] = [
                    'id' => $project->id,
                    'text' => $project->title,
                ];
            }
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'))->with([
            'edit' => false,
            'request' => $request,
            'companies' => $companies,
            'projects' => $projects,
            'project_data' => $project_data,
            'users' => $users,
        ]);
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $id = DB::table('tb_request_tasks')
            ->insertGetId([
                'description' => $request->description,
                'created_by' => Auth::user()->id,
                'project_id' => $request->project,
                'requested_date' => Carbon::createFromFormat('m/d/Y',$request->requested_date)->startOfDay(),
            ]);

        foreach($request->assigned_to as $assigned_to) {
            DB::table('tb_request_task_assigned_users')
                ->insert([
                    'request_id' => $id,
                    'user_id' => $assigned_to,
                ]);
        }

        if($request->hasFile('attachment')) {
            foreach($request->file('attachment') as $file){
                //get filename with extension
                //$filenamewithextension = $file->getClientOriginalName();
                //get filename without extension
                //$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                //get file extension
                $extension = $file->getClientOriginalExtension();

                $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.'.$extension;
                while(DB::table('tb_daily_task_attachments')->where('attachment',$filename)->first()) {
                    $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.'.$extension;
                }
                Image::make($file)->save(public_path().'/uploads/notes/'.$filename,50);

                DB::table('tb_request_task_attachments')
                    ->insert(
                        [
                            'request_id' => $id,
                            'attachment' => $filename,
                            'created_by' => Auth::user()->id,
                        ]
                    );
            }
        }

        //TODO: Should be from database
        $request_task = TbRequestTask::leftjoin('tb_projects','tb_projects.id','=','tb_request_tasks.project_id')
            ->leftjoin('tb_companies','tb_companies.id','=','tb_projects.company_id')
            ->leftjoin('users','users.id','=','tb_request_tasks.created_by')
            ->where('tb_request_tasks.id',$id)
            ->select([
                'tb_request_tasks.*',
                'tb_projects.title as project',
                'users.name as user',
                'tb_companies.company_name as company',
            ])
            ->first();

        if(Auth::user()->role_id == 7) {
            Mail::to('rjvillanueva@marvill.com')->send(new ClientTaskRequest($request_task));
        } else {
            Mail::to('rjvillanueva@marvill.com')->send(new TeamTaskRequest($request_task));
        }

        $redirect = redirect()->route("voyager.{$dataType->slug}.index");

        return $redirect->with([
            'message'    => __('voyager::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
            'alert-type' => 'success',
        ]);
    }

    public function approve (Request $request, $id)
    {
        TbRequestTask::where('id',$id)->update([
            'status' => 1,
            'approved_at' => Carbon::now(),
        ]);

        return redirect()->route('voyager.dailytask.create',['request_id' => $id])->with([
            'message'    => 'Request Approved',
            'alert-type' => 'success',
        ]);
    }


    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $this->authorize('read', app($dataType->model_name));

        $request = TbRequestTask::leftjoin('tb_projects','tb_projects.id','=','tb_request_tasks.project_id')
            ->leftjoin('tb_companies','tb_companies.id','=','tb_projects.company_id')
            ->leftjoin('tb_request_task_assigned_users','tb_request_task_assigned_users.request_id','=','tb_request_tasks.id')
            ->leftjoin('users as assigned_users','tb_request_task_assigned_users.user_id','=','assigned_users.id')
            ->groupBy('tb_request_tasks.id')
            ->select([
                'tb_request_tasks.*',
                'tb_projects.title as project',
                'tb_companies.company_name as company',
                DB::raw("GROUP_CONCAT(assigned_users.name SEPARATOR ', ') as assigned_employees"),
            ])
            ->findOrFail($id);

        if($request->status == 0) {
            $status = 'Pending';
        } elseif($request->status == 1) {
            $status = 'Approved';
        } elseif($request->status == 2) {
            $status = 'Rejected';
        }

        $user = User::find($request->created_by);

        $attachments = DB::table('tb_request_task_attachments')
            ->where('tb_request_task_attachments.request_id',$id)
            ->get();

        $data = [
            'request' => $request,
            'user' => $user,
            'attachments' => $attachments,
            'status' => $status,
        ];

        return view('admin.requesttask.read', $data);
    }

    public function testMail(Request $request) {
        $request_task = TbRequestTask::leftjoin('tb_projects','tb_projects.id','=','tb_request_tasks.project_id')
            ->leftjoin('tb_companies','tb_companies.id','=','tb_projects.company_id')
            ->leftjoin('users','users.id','=','tb_request_tasks.created_by')
            ->where('tb_request_tasks.id',26)
            ->select([
                'tb_request_tasks.*',
                'tb_projects.title as project',
                'users.name as user',
                'tb_companies.company_name as company',
            ])
            ->first();

        Mail::to('kenneth.sy@marvill.com')->send(new NewRequestTaskNotification($request_task));
    }
}
