<?php

namespace App\Http\Controllers\Admin;

use App\TbBookkeeping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use Validator, Redirect, Response, File;
use Socialite;
use App\TbInvoice;
use App\TbInvoiceItem;
use App\TbInvoiceFormType;
use App\TbInvoiceStatus;
use App\TbCompany;
use App\User;
use Carbon\Carbon;
use Image, Storage, PDF, DataTables, Auth;

class InvoiceController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        return view('admin.invoices.browse')->with([
            'dataType' => $dataType,
        ]);
    }

    public function data(Request $request)
    {
        $canView = Auth::user()->can('read', app('App\TbInvoice'));
        $canEdit = Auth::user()->can('edit', app('App\TbInvoice'));
        $canDelete = Auth::user()->can('delete', app('App\TbInvoice'));

        $invoices = TbInvoice::leftjoin('tb_invoice_items','tb_invoice_items.invoice_id','=','tb_invoices.id')
            ->leftjoin('users as create_user','create_user.id','=','tb_invoices.created_by')
            ->leftjoin('users as update_user','update_user.id','=','tb_invoices.created_by')
            ->leftjoin('tb_invoice_status','tb_invoice_status.id','=','tb_invoices.status')
            ->groupBy('tb_invoices.id')
            ->select([
                'tb_invoices.id',
                'tb_invoices.client',
                'tb_invoices.invoice_no',
                'tb_invoices.date',
                'tb_invoices.cust_ref',
                'tb_invoices.form_type',
                'create_user.name as created_by',
                'update_user.name as updated_by',
                'tb_invoices.timestamp',
                'tb_invoice_status.value as status',
                DB::raw("CASE WHEN tb_invoices.form_type = 'SOT' THEN SUM(COALESCE(tb_invoice_items.cost,0)-COALESCE(tb_invoice_items.discount,0)) ELSE SUM(tb_invoice_items.cost) END as total"),
            ]);

        return DataTables::of($invoices)
            ->addColumn('actions', function ($invoice) use($canView, $canDelete, $canEdit) {
                return '<span id="button-'.$invoice->id.'">'.view('admin.invoices.action-buttons')->with([
                        'invoice' => $invoice,
                        'canView' => $canView,
                        'canEdit' => $canEdit,
                        'canDelete' => $canDelete,
                    ])->render().'</span>';
            })
            ->editColumn('total', function ($invoice) {
                return number_format($invoice->total,2);
            })
            ->editColumn('status', function ($invoice) {
                if($invoice->form_type == 'INVOICE' || $invoice->form_type == 'AR') {
                    return '<span id="status-'.$invoice->id.'">'.$invoice->status.'</span>';
                }
                return '<span id="status-'.$invoice->id.'">'.$invoice->form_type.'</span>';
            })
            ->rawColumns(['actions','status'])
            ->make(true);
    }

    public function show(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $this->authorize('read', app($dataType->model_name));

        $invoice = TbInvoice::findOrFail($id);
        $invoice_status = TbInvoiceStatus::find($invoice->status);
        $invoice_items = TbInvoiceItem::where('invoice_id',$id)->get();
        $invoice_form_type = TbInvoiceFormType::where('value',$invoice->form_type)->first();
        $created_by = $invoice->created_by == null?null:User::find($invoice->created_by);

        $pages = array();
        foreach($invoice_items as $invoice_item) {
            if(!isset($pages[$invoice_item->line_page])) {
                $pages[$invoice_item->line_page]['invoice_items'][] = $invoice_item;
            } else {
                $pages[$invoice_item->line_page]['invoice_items'][] = $invoice_item;
            }
        }
        $data = [
            'invoice' => $invoice,
            'invoice_items' => $invoice_items,
            'invoice_status' => $invoice_status,
            'invoice_form_type' => $invoice_form_type,
            'pages' => $pages,
            'created_by' => $created_by == null?null:$created_by->name,
        ];

        $company = TbCompany::where('company_name',$invoice->client)->first();
        if($company) {
            $company_code = $company->company_code;
        } else {
            $company_code = str_slug($invoice->client);
        }

        if(isset($request->type) && $request->type == 'pdf') {
            return PDF::loadView('admin.invoices.pdf', $data)->setOption('margin-bottom', 0)->inline($company_code.'-'.$invoice_form_type->value.'-'.$invoice->cust_ref.'-'.$invoice->date.'.pdf');
        }
        if(isset($request->type) && $request->type == 'download') {
            return PDF::loadView('admin.invoices.pdf', $data)->setOption('margin-bottom', 0)->download($company_code.'-'.$invoice_form_type->value.'-'.$invoice->cust_ref.'-'.$invoice->date.'.pdf');
        }

        return view('admin.invoices.read', $data);
    }

    public function create(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $invoice = new TbInvoice();
        $invoice_items = array();
        $companies = TbCompany::get();
        $form_types = TbInvoiceFormType::get();
        $invoice_status = TbInvoiceStatus::get();

        $invoice->date = Carbon::now()->format('m/d/Y');

        return view('admin.invoices.edit-add')->with([
            'dataType' => $dataType,
            'edit' => false,
            'invoice' => $invoice,
            'invoice_items' => $invoice_items,
            'companies' => $companies,
            'form_types' => $form_types,
            'invoice_status' => $invoice_status,
        ]);
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $validator = Validator::make($request->all(), [
            'form_type' => 'required|string|exists:tb_invoice_form_types,value',
            'company' => 'nullable|exists:tb_companies,id',
            'invoice_no' => 'required|string',
            'date' => 'required|date_format:m/d/Y',
            'cust_ref' => 'required|string',
            'first_name' => 'nullable|string',
            'middle_name' => 'nullable|string',
            'last_name' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if(!empty($request->company)) {
            $company = TbCompany::find($request->company);
        }

        $id = TbInvoice::insertGetId([
            'form_type' => $request->form_type,
            'client' => $company?$company->company_name:'N/A',
            'client_address' => $company?$company->company_address:'N/A',
            'invoice_no' => $request->invoice_no,
            'date' => Carbon::createFromFormat('m/d/Y',$request->date)->format('Y-m-d'),
            'cust_ref' => $request->cust_ref,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'created_by' => Auth::user()->id,
            'status' => 0,
        ]);

        $items_data = array();
        foreach($request->provider as $tkey => $provider) {
            $items_data['invoice_id'] = $id;
            $items_data['provider'] = strtoupper($provider);
            $items_data['line_page'] = $request->line_page[$tkey];
            foreach($request->service[$tkey] as $skey => $svc) {
                $items_data['service'] = strtoupper($svc);
                $items_data['cost'] = $request->cost[$tkey][$skey];
                $items_data['discount'] = $request->discount[$tkey][$skey];
                $items_data['currency'] = $request->currency[$tkey][$skey];
                TbInvoiceItem::insert($items_data);
            }
        }


        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', app($dataType->model_name))) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.show",['id'=>$id]);
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_added_new')." Invoice",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true]);
        }
    }

    public function edit(Request $request, $id)
    {
        if(isset($request->type) && $request->type == 'html') {
            return view('invoice2');
        }

        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $invoice = TbInvoice::findOrFail($id);
        $invoice_items = TbInvoiceItem::where('invoice_id',$invoice->id)->get();
        $companies = TbCompany::get();
        $form_types = TbInvoiceFormType::get();
        $invoice_status = TbInvoiceStatus::get();

        $invoice->date = Carbon::parse($invoice->date)->format('m/d/Y');

        return view('admin.invoices.edit-add')->with([
            'dataType' => $dataType,
            'edit' => true,
            'invoice' => $invoice,
            'invoice_items' => $invoice_items,
            'companies' => $companies,
            'form_types' => $form_types,
            'invoice_status' => $invoice_status,
        ]);

    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $validator = Validator::make($request->all(), [
            'form_type' => 'required|string|exists:tb_invoice_form_types,value',
            'company' => 'nullable|exists:tb_companies,id',
            'invoice_no' => 'required|string',
            'date' => 'required|date_format:m/d/Y',
            'cust_ref' => 'required|string',
            'first_name' => 'nullable|string',
            'middle_name' => 'nullable|string',
            'last_name' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        if(!empty($request->company)) {
            $company = TbCompany::find($request->company);
        }

        TbInvoice::where('id',$id)->update([
            'form_type' => $request->form_type,
            'client' => $company?$company->company_name:'N/A',
            'client_address' => $company?$company->company_address:'N/A',
            'invoice_no' => $request->invoice_no,
            'date' => Carbon::createFromFormat('m/d/Y',$request->date)->format('Y-m-d'),
            'cust_ref' => $request->cust_ref,
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'updated_by' => Auth::user()->id,
        ]);

        TbInvoiceItem::where('invoice_id',$id)->delete();
        $php_total = 0;
        $last_discount = 0;
        $last_provider = '';
        $last_service = '';
        $items_data = array();
        foreach($request->provider as $tkey => $provider) {
            $items_data['invoice_id'] = $id;
            $items_data['provider'] = strtoupper($provider);
            $items_data['line_page'] = $request->line_page[$tkey];
            foreach($request->service[$tkey] as $skey => $svc) {
                $items_data['service'] = strtoupper($svc);
                $items_data['cost'] = $request->cost[$tkey][$skey];
                $items_data['discount'] = $request->discount[$tkey][$skey];
                $items_data['currency'] = $request->currency[$tkey][$skey];
                TbInvoiceItem::insert($items_data);
                if(!empty($items_data['discount'])) {
                    $last_discount = $items_data['discount'];
                    $last_provider = strtoupper($provider);
                    $last_service = strtoupper($svc);
                    $php_total -= $items_data['discount'];
                }
                if(!empty($items_data['cost'])) {
                    $php_total += $items_data['cost'];
                }
            }
        }

        if($request->form_type=='SOT') {
            if($php_total<=0) {
                TbInvoice::where('id',$id)->update([
                    'status' => 1
                ]);
            }
            $description = $last_provider.' - '.$last_service;
            $exists = TbBookkeeping::where('description','=', $description)
                ->where('income','=', $last_discount)
                ->where('date','<', '2020-05-26')
                ->first();
            $exists2 = TbBookkeeping::where('description','=', $description)
                ->where('income','=', $last_discount)
                ->where('invoice_id','=', $id)
                ->first();

            if(!$exists2 && !$exists) {
                TbBookkeeping::insert([
                    'description' => $description,
                    'company_id' => 1,
                    'income' => $last_discount,
                    'date' => Carbon::now(),
                    'invoice_id' => $id,
                ]);
            }
        }

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', app($dataType->model_name))) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.show",['id'=>$id]);
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_updated')." Invoice",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true]);
        }
    }

    public function markpaid (Request $request, $id)
    {
        $invoice = TbInvoice::where('id', $id)->first();//
        TbInvoice::where('id', $id)->update([
            'status' => 1,
            'form_type' => 'AR',
        ]);
        $items_query = TbInvoiceItem::where('invoice_id', $id)->get();
        $amount = 0;
        $description = $invoice->client.'</br>';//
        foreach($items_query as $items_row) {
            $amount += $items_row->cost;
            $amount -= $items_row->discount;
            $description .= '*'.$items_row->provider.'</br>';
        }
        TbBookkeeping::insert([
            'description' => $description,
            'company_id' => 1,
            'income' => $amount,
            'date' => Carbon::now(),
        ]);

        $canView = Auth::user()->can('read', app('App\TbInvoice'));
        $canEdit = Auth::user()->can('edit', app('App\TbInvoice'));
        $canDelete = Auth::user()->can('delete', app('App\TbInvoice'));

        if($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => 'Invoice Mark as Paid',
                'status' => 'Paid',
                'button' => view('admin.invoices.action-buttons')->with([
                    'invoice' => $invoice,
                    'canView' => $canView,
                    'canEdit' => $canEdit,
                    'canDelete' => $canDelete,
                ])->render(),
            ]);
        }
    }

    public function copyAndEdit (Request $request, $id) {

        $this->authorize('edit', app('App\TbInvoice'));

        $invoice = TbInvoice::where('id',$id)->first();
        $invoice_items = TbInvoiceItem::where('invoice_id',$invoice->id)->get();

        $id = TbInvoice::insertGetId([
            'form_type' => $invoice->form_type,
            'client' => $invoice->client,
            'client_address' => $invoice->client_address,
            'invoice_no' => '',
            'date' => $invoice->date,
            'cust_ref' => $invoice->cust_ref,
            'first_name' => $invoice->first_name,
            'middle_name' => $invoice->middle_name,
            'last_name' => $invoice->last_name,
            'created_by' => Auth::user()->id,
            'status' => 0,
        ]);

        $items_data = array();
        foreach($invoice_items as $tkey => $invoice_item) {
            $items_data['invoice_id'] = $id;
            $items_data['provider'] = $invoice_item->provider;
            $items_data['line_page'] = $invoice_item->line_page;
            $items_data['service'] = $invoice_item->service;
            $items_data['cost'] = $invoice_item->cost;
            $items_data['discount'] = $invoice_item->discount;
            $items_data['currency'] = $invoice_item->currency;
            TbInvoiceItem::insert($items_data);
        }

        return redirect()->route("voyager.invoices.edit",['id'=>$id])->with([
            'message'    => "Successfully Copied Invoice",
            'alert-type' => 'success',
        ]);
    }
}
