<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use Validator, Redirect, Response, File;
use Socialite;
use App\TbPnbwing;
use Carbon\Carbon;
use Image, Storage, PDF, Auth;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\CapabilityProfile;
use Mail;
use App\Mail\OrderConfirmed;


class PnbController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        if(!isset($request->date)) {
            $start_of_month = Carbon::now()->startOfMonth()->format('Y-m-d');
            $end_of_month = Carbon::now()->endOfMonth()->format('Y-m-d');
            $date = Carbon::now()->startOfMonth()->format('M-Y');
        } else {
            $start_of_month = Carbon::createFromFormat('M-Y',$request->date)->startOfMonth()->format('Y-m-d');
            $end_of_month = Carbon::createFromFormat('M-Y',$request->date)->endOfMonth()->format('Y-m-d');
            $date = $request->date;
        }
        $role = Auth::user()->role_id;

        if($role == '16'){

          $orders = TbPnbwing::orderBy('id','DESC')->where('location','pampanga')->whereDate('created_at', Carbon::today())->take(20)->get();

              return view('admin.pnb.browse')->with([
                  'dataType' => $dataType,
                  'date' => $date,
                  'orders' => $orders,


              ]);
        }

        else{
          $ordersmak = TbPnbwing::orderBy('id','DESC')->where('location','makati')->whereDate('created_at', Carbon::today())->take(20)->get();

              return view('admin.pnb.browse')->with([
                  'dataType' => $dataType,
                  'date' => $date,
                  'orders' => $ordersmak,


              ]);
        }

    }





    public function all_orders(Request $request)
    {

        // GET THE DataType based on the slug
      $dataType = Voyager::model('DataType')->where('slug', '=', 'tb-pnbwings')->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];

        $searchNames = [];
        if ($dataType->server_side) {
            $searchable = SchemaManager::describeTable(app($dataType->model_name)->getTable())->pluck('name')->toArray();
            $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->get();
            foreach ($searchable as $key => $value) {
                $displayName = $dataRow->where('field', $value)->first() ? $dataRow->where('field', $value)->first()->getTranslatedAttribute('display_name') : null;
                $searchNames[$value] = $displayName ?: ucwords(str_replace('_', ' ', $value));
            }
        }

        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $usesSoftDeletes = false;
        $showSoftDeleted = false;

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $query = $model->{$dataType->scope}();
            } else {
                $query = $model::select('*');
            }

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model)) && Auth::user()->can('delete', app($dataType->model_name))) {
                $usesSoftDeletes = true;

                if ($request->get('showSoftDeleted')) {
                    $showSoftDeleted = true;
                    $query = $query->withTrashed();
                }
            }

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value != '' && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
                $dataTypeContent = call_user_func([
                    $query->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($model);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'browse', $isModelTranslatable);

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        // Check if a default search key is set
        $defaultSearchKey = $dataType->default_search_key ?? null;

        // Actions
        $actions = [];
        if (!empty($dataTypeContent->first())) {
            foreach (Voyager::actions() as $action) {
                $action = new $action($dataType, $dataTypeContent->first());

                if ($action->shouldActionDisplayOnDataType()) {
                    $actions[] = $action;
                }
            }
        }

        // Define showCheckboxColumn
        $showCheckboxColumn = false;
        if (Auth::user()->can('delete', app($dataType->model_name))) {
            $showCheckboxColumn = true;
        } else {
            foreach ($actions as $action) {
                if (method_exists($action, 'massAction')) {
                    $showCheckboxColumn = true;
                }
            }
        }

        // Define orderColumn
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first() + ($showCheckboxColumn ? 1 : 0);
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::tb-pnbwings.browse")) {
            $view = "voyager::tb-pnbwings.browse";
        }

        return Voyager::view($view, compact(
            'actions',
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'orderColumn',
            'sortOrder',
            'searchNames',
            'isServerSide',
            'defaultSearchKey',
            'usesSoftDeletes',
            'showSoftDeleted',
            'showCheckboxColumn'
        ));
    }















    public function create(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $order = new TbPnbwing();



        return view('admin.pnb.new-order')->with([
            'dataType' => $dataType,
            'edit' => false,
            'order' => $order,

        ]);
    }

    public function custorder(Request $request)
    {
        $slug = "tb-pnbwings";

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $order = new TbPnbwing();



        return view('admin.pnb.order')->with([
            'dataType' => $dataType,
            'edit' => false,
            'order' => $order,

        ]);
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        if ($request->input('order') != ""){

          $orderobj = $request->input('order');

          $fororderobj = array_filter( $orderobj);
          $fororderobj = array_values($fororderobj);

          $fororder = json_encode($fororderobj);



       }



       if ($request->input('qty') != ""){

         $orderqty= json_encode($request->input('qty'));


      }


        // Validate fields with ajax
        if($request->input('orderedby') == "inhouse"){
            $role = Auth::user()->role_id;
              if($role == '16'){
            DB::table('tb_pnbwings')->insert([
                'order_type' => $request->input('order_type'),
                'order_number' => $request->input('order_number'),
                'order' => $fororder,
                'qty' => $orderqty,
                'status' => 'released',
                'location' => 'pampanga',
                'price' => $request->input('price'),
                'created_at' => Carbon::today(),
                'order_status' => 'confirmed',
                'customer_name' => $request->input('customer_name'),
                'notes'=> $request->input('order_note'),

            ]);
            }

            else{
              DB::table('tb_pnbwings')->insert([
                  'order_type' => $request->input('order_type'),
                  'order_number' => $request->input('order_number'),
                  'order' => $fororder,
                  'qty' => $orderqty,
                  'status' => 'released',
                  'location' => 'makati',
                  'price' => $request->input('price'),
                  'created_at' => Carbon::today(),
                  'order_status' => 'confirmed',
                  'customer_name' => $request->input('customer_name'),
                  'notes'=> $request->input('order_note'),

              ]);
            }



            $idcode = DB::getPdo()->lastInsertId();

            if($request->input('order_type') == 'delivery'){

              DB::table('tb_pnbcodes')->insert([
                'order_id' => $idcode,
                'code' => 'PNB-'.strtoupper(str_random(4)),
                'status' => '0',


              ]);

            }


            if (!$request->has('_tagging')) {
                if (auth()->user()->can('browse', app($dataType->model_name))) {
                    $redirect = redirect()->route("voyager.{$dataType->slug}.index");
                } else {
                    $redirect = redirect()->back();
                }

                return $redirect->with([
                    'message'    => __('voyager::generic.successfully_added_new')." Ang Paborito ni Boss Order",
                    'alert-type' => 'success',
                ]);
            } else {
                return response()->json(['success' => true, 'data' => $data]);
            }



        }

        elseif($request->input('orderedby') == "customer")
        {

          $userId = Auth::id();
          $custname = DB::table('users')->where('id',$userId)->first();

             $orderqty= json_encode($request->input('qty'));

          DB::table('tb_pnbwings')->insert([
              'order_type' => $request->input('order_type'),
              'order_number' => $request->input('order_number'),
              'order' => $fororder,
              'qty' => $orderqty,
              'status' => 'released',
              'location' => 'makati',
              'price' => $request->input('price'),
              'created_at' => Carbon::today(),
              'ordered_by' => $userId,
              'order_status' => 'unconfirmed',
              'order_time' => $request->input('order_time'),
              'customer_name' => $custname->name,


          ]);



            $idcode = DB::getPdo()->lastInsertId();





          return redirect("/pnbwings/checkout/$idcode");




    }

  }




    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $order = TbPnbwing::findOrFail($id);


        return view('admin.pnb.edit-add')->with([
            'dataType' => $dataType,
            'edit' => true,
            'order' => $order,

        ]);

    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $order = TbPnbwing::findOrFail($id);

        // Validate fields with ajax


        $data = TbPnbwing::where('id',$id)
        ->update([
            'order_type' => $request->order_type,
            'order_number' => $request->order_number,

        ]);

        if (!$request->has('_tagging')) {
            if (auth()->user()->can('browse', app($dataType->model_name))) {
                $redirect = redirect()->route("voyager.{$dataType->slug}.index");
            } else {
                $redirect = redirect()->back();
            }

            return $redirect->with([
                'message'    => __('voyager::generic.successfully_updated')." Sales Record",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }


    public function cooking(Request $request, $id)
    {
        $slug = $this->getSlug($request);


        $order = TbPnbwing::findOrFail($id);

        // Validate fields with ajax


        $data = TbPnbwing::where('id',$id)
        ->update([
            'status' => "cooking",


        ]);

        if (!$request->has('_tagging')) {

                $redirect = redirect()->route("voyager.tb-pnbwings.index");


            return $redirect->with([
                'message'    => __('voyager::generic.successfully_updated')." Sales Record",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    public function preparing(Request $request, $id)
    {
      $slug = $this->getSlug($request);


      $order = TbPnbwing::findOrFail($id);

      // Validate fields with ajax


      $data = TbPnbwing::where('id',$id)
      ->update([
          'status' => "preparing",


      ]);

      if (!$request->has('_tagging')) {

              $redirect = redirect()->route("voyager.tb-pnbwings.index");


          return $redirect->with([
              'message'    => __('voyager::generic.successfully_updated')." Sales Record",
              'alert-type' => 'success',
          ]);
      } else {
          return response()->json(['success' => true, 'data' => $data]);
      }
    }









    public function checkout (Request $request, $id)
    {


        $id = $request->route('id');
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = "tb-pnbwings";;

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        $userid = Auth::user()->id;
        $order = TbPnbwing::where('id',$id)->first();

            return view('admin.pnb.checkout')->with([
              'dataType' => $dataType,
                'order' => $order,
                'id' => $id,
                'userid' => $userid,


            ]);
        // Validate fields with ajax





    }


    public function checkedout (Request $request, $id)
    {


        $order = TbPnbwing::findOrFail($id);

        // Validate fields with ajax


        $data = TbPnbwing::where('id',$id)
        ->update([
            'order_status' => "confirmed",
        ]);




                    $email = Auth::user()->email;

                    $order_confirmation = TbPnbwing::where('id',$id)->first();
                    $userinfo = DB::table('users')->where('email',$email)->first();
                    $useraddress = DB::table('tb_pnbdetails')->where('user_id',$userinfo->id)->first();

                    //Mail::to($email)->send(new OrderConfirmed($order_confirmation, $userinfo, $useraddress));




        if (!$request->has('_tagging')) {


          $dataType = Voyager::model('DataType')->where('slug', '=', 'tb-pnbwings')->first();

          // Check permission
          $this->authorize('add', app($dataType->model_name));

          $order = new TbPnbwing();





           return redirect()->route('pnbwings.order')->with([
                'message'    => 'Successfully placed your order',
                'alert-type' => 'success',
                'dataType' => $dataType,
                'edit' => false,
                'order' => $order,
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }


    public function released (Request $request, $id)
    {


        $order = TbPnbwing::findOrFail($id);

        // Validate fields with ajax


        $data = TbPnbwing::where('id',$id)
        ->update([
            'status' => "released",


        ]);

        if (!$request->has('_tagging')) {

                $redirect = redirect()->route("voyager.tb-pnbwings.index");


            return $redirect->with([
                'message'    => __('voyager::generic.successfully_updated')." Sales Record",
                'alert-type' => 'success',
            ]);
        } else {
            return response()->json(['success' => true, 'data' => $data]);
        }
    }

    public function receipt(Request $request, $id)
    {
        $datatest = DB::table("tb_pnbwings")->where('id',$id)->first();


          $data = [
            'id' => $id,
            'order_type' => $datatest->order_type,
            'order_number' => $datatest->order_number,
            'order' => $datatest->order,
            'price' => $datatest->price,

          ];


        return PDF::loadView('admin.pnb.receipt', $data)->inline($datatest->id.'-'.$datatest->order_type.'.pdf');
    }

    public function test()
    {

      if (Auth::user()) {   // Check is user logged in
        $slug = "tb-pnbwings";

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $order = new TbPnbwing();



        return view('admin.pnb.order')->with([
            'dataType' => $dataType,
            'edit' => false,
            'order' => $order,

        ]);

      }  else {
        return view('admin.pnb.pnblogin');
      }


    }


    public function test2(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', 'tb-pnbwings')->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $order = new TbPnbwing();

        $action = new \App\Http\Actions\CalculatePNBNetSales();

        $date = \Carbon\Carbon::today();
        $action->run($date);

        return view('admin.pnb.test')->with([
            'dataType' => $dataType,
            'edit' => false,
            'order' => $order,

        ]);
    }
}
