<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBreadController;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use Validator, Redirect, Response, File;
use Socialite;
use App\TbDocument;
use App\TbCompany;
use App\TbDocumentsSetting;
use Carbon\Carbon;
use Image, Storage, PDF, Auth;

class DocumentController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{


  //***************************************
  //               ____
  //              |  _ \
  //              | |_) |
  //              |  _ <
  //              | |_) |
  //              |____/
  //
  //      Browse our Data Type (B)READ
  //
  //****************************************

  public function index(Request $request)
  {
      // GET THE SLUG, ex. 'posts', 'pages', etc.
      $slug = $this->getSlug($request);

      // GET THE DataType based on the slug
      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('browse', app($dataType->model_name));

      if(!isset($request->date)) {
          $start_of_month = Carbon::now()->startOfMonth()->format('Y-m-d');
          $end_of_month = Carbon::now()->endOfMonth()->format('Y-m-d');
          $date = Carbon::now()->startOfMonth()->format('M-Y');
      } else {
          $start_of_month = Carbon::createFromFormat('M-Y',$request->date)->startOfMonth()->format('Y-m-d');
          $end_of_month = Carbon::createFromFormat('M-Y',$request->date)->endOfMonth()->format('Y-m-d');
          $date = $request->date;
      }

      $records = TbDocument::orderBy('id','DESC')->take(20)->get();

          return view('admin.documents.browse')->with([
              'dataType' => $dataType,
              'date' => $date,
              'records' => $records,


          ]);
  }

  //***************************************
  //                _____
  //               |  __ \
  //               | |__) |
  //               |  _  /
  //               | | \ \
  //               |_|  \_\
  //
  //  Read an item of our Data Type B(R)EAD
  //
  //****************************************

  public function show(Request $request, $id)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      $isSoftDeleted = false;

      if (strlen($dataType->model_name) != 0) {
          $model = app($dataType->model_name);

          // Use withTrashed() if model uses SoftDeletes and if toggle is selected
          if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
              $model = $model->withTrashed();
          }
          if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
              $model = $model->{$dataType->scope}();
          }
          $dataTypeContent = call_user_func([$model, 'findOrFail'], $id);
          if ($dataTypeContent->deleted_at) {
              $isSoftDeleted = true;
          }
      } else {
          // If Model doest exist, get data from table name
          $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
      }

      // Replace relationships' keys for labels and create READ links if a slug is provided.
      $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

      // If a column has a relationship associated with it, we do not want to show that field
      $this->removeRelationshipField($dataType, 'read');

      // Check permission
      $this->authorize('read', $dataTypeContent);

      // Check if BREAD is Translatable
      $isModelTranslatable = is_bread_translatable($dataTypeContent);

      // Eagerload Relations
      $this->eagerLoadRelations($dataTypeContent, $dataType, 'read', $isModelTranslatable);

      $view = 'voyager::bread.read';

      if (view()->exists("voyager::$slug.read")) {
          $view = "voyager::$slug.read";
      }

      return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'isSoftDeleted'));
  }

  //***************************************
  //                ______
  //               |  ____|
  //               | |__
  //               |  __|
  //               | |____
  //               |______|
  //
  //  Edit an item of our Data Type BR(E)AD
  //
  //****************************************

  public function create(Request $request)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('add', app($dataType->model_name));

      $record = new TbDocument();
      $clients = TbCompany::orderBy('id','DESC')->get();
      $companies = TbCompany::where('is_main',1)->orWhere('is_sub',1)
          ->orderBy('company_name','asc')
          ->get();
      $record->date = Carbon::now()->format('d-M-Y');

      return view('admin.documents.edit-add')->with([
          'dataType' => $dataType,
          'edit' => false,
          'record' => $record,
          'clients' => $clients,
          'companies' => $companies,

      ]);
  }








  public function store(Request $request)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('add', app($dataType->model_name));

      // Validate fields with ajax
      $auth = Auth::user()->id;
      $comp = \DB::table('users')->where('id', $auth)->first();


      if($request->input('document_type') == "Documentation"){

                if(!$request->file('documentation_image')) {
                    $attachment = null;
                } else {
                    $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.jpg';
                    while(TbDocument::where('documentation_image',$filename)->first()) {
                        $filename = Carbon::now()->format('YmdHis').substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 10)), 0, 5).'.jpg';
                    }
                    Image::make($request->file('documentation_image'))->save(public_path().'/uploads/documents/'.$filename,75);
                    $attachment = $filename;

                }


                DB::table('tb_documents')->insert([
                    'company_name' => $request->input('company_name'),
                    'client_name' => $request->input('client_name'),
                    'document_type' => $request->input('document_type'),
                    'documentation_title' => $request->input('documentation_title'),
                    'documentation_description' => $request->input('documentation_description'),
                    'documentation_image' => $attachment,
                    'created_at' => Carbon::today(),
                      ]);

      }

      if($request->input('document_type') == "Proposal"){


              if($request->input('proposal_type') == "standard"){
              DB::table('tb_documents')->insert([
                  'company_name' => $request->input('company_name'),
                  'client_name' => $request->input('client_name'),
                  'document_type' => $request->input('document_type'),
                  'proposal_type' => $request->input('proposal_type'),
                  'proposal_product' => $request->input('proposal_product_standard'),
                  'proposal_working_days' => $request->input('proposal_working_days_standard'),
                  'proposal_inclusions' => $request->input('proposal_inclusions_standard'),
                  'proposal_price_description' => $request->input('proposal_price_description_standard'),
                  'proposal_price_cost' => $request->input('proposal_price_cost_standard'),
                  'proposal_timeframe' => $request->input('proposal_timeframe_standard'),
                  'proposal_start_date' => $request->input('proposal_start_date_standard'),
                  'proposal_terms' => $request->input('proposal_terms_standard'),
                  'client_contact_person' => $request->input('client_contact_person'),
                  'client_contact_person_position' => $request->input('client_contact_person_position'),
                  'proposal_resources' => $request->input('proposal_resources_standard'),
                  'proposal_scope' => $request->input('proposal_scope_standard'),
                  'company_contact_person' => $comp->name,
                  'created_at' => Carbon::today(),
                    ]);
                }
                else if($request->input('proposal_type') == "dedicated"){
                DB::table('tb_documents')->insert([
                    'company_name' => $request->input('company_name'),
                    'client_name' => $request->input('client_name'),
                    'document_type' => $request->input('document_type'),
                    'proposal_type' => $request->input('proposal_type'),
                    'proposal_product' => $request->input('proposal_product_dedicated'),
                    'proposal_working_days' => $request->input('proposal_working_days_dedicated'),
                    'proposal_inclusions' => $request->input('proposal_inclusions_dedicated'),
                    'proposal_price_description' => $request->input('proposal_price_description_dedicated'),
                    'proposal_price_cost' => $request->input('proposal_price_cost_dedicated'),
                    'proposal_timeframe' => $request->input('proposal_timeframe_dedicated'),
                    'proposal_start_date' => $request->input('proposal_start_date_dedicated'),
                    'proposal_terms' => $request->input('proposal_terms_dedicated'),
                    'client_contact_person' => $request->input('client_contact_person'),
                    'client_contact_person_position' => $request->input('client_contact_person_position'),
                    'proposal_resources' => $request->input('proposal_resources_dedicated'),
                    'proposal_scope' => $request->input('proposal_scope_dedicated'),
                    'company_contact_person' => $comp->name,
                    'created_at' => Carbon::today(),
                      ]);
                  }

          }



      if (!$request->has('_tagging')) {
          if (auth()->user()->can('browse', app($dataType->model_name))) {
              $redirect = redirect()->route("voyager.{$dataType->slug}.index");
          } else {
              $redirect = redirect()->back();
          }

          return $redirect->with([
              'message'    => __('voyager::generic.successfully_added_new')." Ang Paborito ni Boss Order",
              'alert-type' => 'success',
          ]);
      } else {
          return response()->json(['success' => true, 'data' => $data]);
      }
  }




  public function edit(Request $request, $id)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('edit', app($dataType->model_name));

      $record = TbDocument::findOrFail($id);
      $companies = TbCompany::where('is_main',1)->orWhere('is_sub',1)
          ->orderBy('company_name','asc')
          ->get();
      $clients = TbCompany::orderBy('id','DESC')->get();

      $employees = User::where('role_id',4)
          ->get();

      return view('admin.documents.edit-add')->with([
          'dataType' => $dataType,
          'edit' => true,
          'record' => $record,
          'companies' => $companies,
          'employees' => $employees,
          'clients'=>$clients,
      ]);

  }

  //***************************************
  //                _____
  //               |  __ \
  //               | |  | |
  //               | |  | |
  //               | |__| |
  //               |_____/
  //
  //         Delete an item BREA(D)
  //
  //****************************************

  public function destroy(Request $request, $id)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Init array of IDs
      $ids = [];
      if (empty($id)) {
          // Bulk delete, get IDs from POST
          $ids = explode(',', $request->ids);
      } else {
          // Single item delete, get ID from URL
          $ids[] = $id;
      }
      foreach ($ids as $id) {
          $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

          // Check permission
          $this->authorize('delete', $data);

          $model = app($dataType->model_name);
          if (!($model && in_array(SoftDeletes::class, class_uses_recursive($model)))) {
              $this->cleanup($dataType, $data);
          }
      }

      $displayName = count($ids) > 1 ? $dataType->getTranslatedAttribute('display_name_plural') : $dataType->getTranslatedAttribute('display_name_singular');

      $res = $data->destroy($ids);
      $data = $res
          ? [
              'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
              'alert-type' => 'success',
          ]
          : [
              'message'    => __('voyager::generic.error_deleting')." {$displayName}",
              'alert-type' => 'error',
          ];

      if ($res) {
          event(new BreadDataDeleted($dataType, $data));
      }

      return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
  }

  public function restore(Request $request, $id)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('delete', app($dataType->model_name));

      // Get record
      $model = call_user_func([$dataType->model_name, 'withTrashed']);
      if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
          $model = $model->{$dataType->scope}();
      }
      $data = $model->findOrFail($id);

      $displayName = $dataType->getTranslatedAttribute('display_name_singular');

      $res = $data->restore($id);
      $data = $res
          ? [
              'message'    => __('voyager::generic.successfully_restored')." {$displayName}",
              'alert-type' => 'success',
          ]
          : [
              'message'    => __('voyager::generic.error_restoring')." {$displayName}",
              'alert-type' => 'error',
          ];

      if ($res) {
          event(new BreadDataRestored($dataType, $data));
      }

      return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
  }

  //***************************************
  //
  //  Delete uploaded file
  //
  //****************************************

  public function remove_media(Request $request)
  {
      try {
          // GET THE SLUG, ex. 'posts', 'pages', etc.
          $slug = $request->get('slug');

          // GET file name
          $filename = $request->get('filename');

          // GET record id
          $id = $request->get('id');

          // GET field name
          $field = $request->get('field');

          // GET multi value
          $multi = $request->get('multi');

          $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

          // Load model and find record
          $model = app($dataType->model_name);
          $data = $model::find([$id])->first();

          // Check if field exists
          if (!isset($data->{$field})) {
              throw new Exception(__('voyager::generic.field_does_not_exist'), 400);
          }

          // Check permission
          $this->authorize('edit', $data);

          if (@json_decode($multi)) {
              // Check if valid json
              if (is_null(@json_decode($data->{$field}))) {
                  throw new Exception(__('voyager::json.invalid'), 500);
              }

              // Decode field value
              $fieldData = @json_decode($data->{$field}, true);
              $key = null;

              // Check if we're dealing with a nested array for the case of multiple files
              if (is_array($fieldData[0])) {
                  foreach ($fieldData as $index=>$file) {
                      // file type has a different structure than images
                      if (!empty($file['original_name'])) {
                          if ($file['original_name'] == $filename) {
                              $key = $index;
                              break;
                          }
                      } else {
                          $file = array_flip($file);
                          if (array_key_exists($filename, $file)) {
                              $key = $index;
                              break;
                          }
                      }
                  }
              } else {
                  $key = array_search($filename, $fieldData);
              }

              // Check if file was found in array
              if (is_null($key) || $key === false) {
                  throw new Exception(__('voyager::media.file_does_not_exist'), 400);
              }

              $fileToRemove = $fieldData[$key]['download_link'] ?? $fieldData[$key];

              // Remove file from array
              unset($fieldData[$key]);

              // Generate json and update field
              $data->{$field} = empty($fieldData) ? null : json_encode(array_values($fieldData));
          } else {
              if ($filename == $data->{$field}) {
                  $fileToRemove = $data->{$field};

                  $data->{$field} = null;
              } else {
                  throw new Exception(__('voyager::media.file_does_not_exist'), 400);
              }
          }

          $row = $dataType->rows->where('field', $field)->first();

          // Remove file from filesystem
          if (in_array($row->type, ['image', 'multiple_images'])) {
              $this->deleteBreadImages($data, [$row], $fileToRemove);
          } else {
              $this->deleteFileIfExists($fileToRemove);
          }

          $data->save();

          return response()->json([
              'data' => [
                  'status'  => 200,
                  'message' => __('voyager::media.file_removed'),
              ],
          ]);
      } catch (Exception $e) {
          $code = 500;
          $message = __('voyager::generic.internal_error');

          if ($e->getCode()) {
              $code = $e->getCode();
          }

          if ($e->getMessage()) {
              $message = $e->getMessage();
          }

          return response()->json([
              'data' => [
                  'status'  => $code,
                  'message' => $message,
              ],
          ], $code);
      }
  }

  /**
   * Remove translations, images and files related to a BREAD item.
   *
   * @param \Illuminate\Database\Eloquent\Model $dataType
   * @param \Illuminate\Database\Eloquent\Model $data
   *
   * @return void
   */
  protected function cleanup($dataType, $data)
  {
      // Delete Translations, if present
      if (is_bread_translatable($data)) {
          $data->deleteAttributeTranslations($data->getTranslatableAttributes());
      }

      // Delete Images
      $this->deleteBreadImages($data, $dataType->deleteRows->whereIn('type', ['image', 'multiple_images']));

      // Delete Files
      foreach ($dataType->deleteRows->where('type', 'file') as $row) {
          if (isset($data->{$row->field})) {
              foreach (json_decode($data->{$row->field}) as $file) {
                  $this->deleteFileIfExists($file->download_link);
              }
          }
      }

      // Delete media-picker files
      $dataType->rows->where('type', 'media_picker')->where('details.delete_files', true)->each(function ($row) use ($data) {
          $content = $data->{$row->field};
          if (isset($content)) {
              if (!is_array($content)) {
                  $content = json_decode($content);
              }
              if (is_array($content)) {
                  foreach ($content as $file) {
                      $this->deleteFileIfExists($file);
                  }
              } else {
                  $this->deleteFileIfExists($content);
              }
          }
      });
  }

  /**
   * Delete all images related to a BREAD item.
   *
   * @param \Illuminate\Database\Eloquent\Model $data
   * @param \Illuminate\Database\Eloquent\Model $rows
   *
   * @return void
   */
  public function deleteBreadImages($data, $rows, $single_image = null)
  {
      $imagesDeleted = false;

      foreach ($rows as $row) {
          if ($row->type == 'multiple_images') {
              $images_to_remove = json_decode($data->getOriginal($row->field), true) ?? [];
          } else {
              $images_to_remove = [$data->getOriginal($row->field)];
          }

          foreach ($images_to_remove as $image) {
              // Remove only $single_image if we are removing from bread edit
              if ($image != config('voyager.user.default_avatar') && (is_null($single_image) || $single_image == $image)) {
                  $this->deleteFileIfExists($image);
                  $imagesDeleted = true;

                  if (isset($row->details->thumbnails)) {
                      foreach ($row->details->thumbnails as $thumbnail) {
                          $ext = explode('.', $image);
                          $extension = '.'.$ext[count($ext) - 1];

                          $path = str_replace($extension, '', $image);

                          $thumb_name = $thumbnail->name;

                          $this->deleteFileIfExists($path.'-'.$thumb_name.$extension);
                      }
                  }
              }
          }
      }

      if ($imagesDeleted) {
          event(new BreadImagesDeleted($data, $rows));
      }
  }

  /**
   * Order BREAD items.
   *
   * @param string $table
   *
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
  public function order(Request $request)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('edit', app($dataType->model_name));

      if (!isset($dataType->order_column) || !isset($dataType->order_display_column)) {
          return redirect()
          ->route("voyager.{$dataType->slug}.index")
          ->with([
              'message'    => __('voyager::bread.ordering_not_set'),
              'alert-type' => 'error',
          ]);
      }

      $model = app($dataType->model_name);
      if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
          $model = $model->withTrashed();
      }
      $results = $model->orderBy($dataType->order_column, $dataType->order_direction)->get();

      $display_column = $dataType->order_display_column;

      $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->whereField($display_column)->first();

      $view = 'voyager::bread.order';

      if (view()->exists("voyager::$slug.order")) {
          $view = "voyager::$slug.order";
      }

      return Voyager::view($view, compact(
          'dataType',
          'display_column',
          'dataRow',
          'results'
      ));
  }

  public function update_order(Request $request)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('edit', app($dataType->model_name));

      $model = app($dataType->model_name);

      $record = json_decode($request->input('order'));
      $column = $dataType->order_column;
      foreach ($record as $key => $item) {
          if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
              $i = $model->withTrashed()->findOrFail($item->id);
          } else {
              $i = $model->findOrFail($item->id);
          }
          $i->$column = ($key + 1);
          $i->save();
      }
  }

  public function action(Request $request)
  {
      $slug = $this->getSlug($request);
      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      $action = new $request->action($dataType, null);

      return $action->massAction(explode(',', $request->ids), $request->headers->get('referer'));
  }

  /**
   * Get BREAD relations data.
   *
   * @param Request $request
   *
   * @return mixed
   */
  public function relation(Request $request)
  {
      $slug = $this->getSlug($request);
      $page = $request->input('page');
      $on_page = 50;
      $search = $request->input('search', false);
      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      $method = $request->input('method', 'add');

      $model = app($dataType->model_name);
      if ($method != 'add') {
          $model = $model->find($request->input('id'));
      }

      $this->authorize($method, $model);

      $rows = $dataType->{$method.'Rows'};
      foreach ($rows as $key => $row) {
          if ($row->field === $request->input('type')) {
              $options = $row->details;
              $model = app($options->model);
              $skip = $on_page * ($page - 1);

              // If search query, use LIKE to filter results depending on field label
              if ($search) {
                  // If we are using additional_attribute as label
                  if (in_array($options->label, $model->additional_attributes ?? [])) {
                      $relationshipOptions = $model->all();
                      $relationshipOptions = $relationshipOptions->filter(function ($model) use ($search, $options) {
                          return stripos($model->{$options->label}, $search) !== false;
                      });
                      $total_count = $relationshipOptions->count();
                      $relationshipOptions = $relationshipOptions->forPage($page, $on_page);
                  } else {
                      $total_count = $model->where($options->label, 'LIKE', '%'.$search.'%')->count();
                      $relationshipOptions = $model->take($on_page)->skip($skip)
                          ->where($options->label, 'LIKE', '%'.$search.'%')
                          ->get();
                  }
              } else {
                  $total_count = $model->count();
                  $relationshipOptions = $model->take($on_page)->skip($skip)->get();
              }

              $results = [];

              if (!$row->required && !$search) {
                  $results[] = [
                      'id'   => '',
                      'text' => __('voyager::generic.none'),
                  ];
              }

              foreach ($relationshipOptions as $relationshipOption) {
                  $results[] = [
                      'id'   => $relationshipOption->{$options->key},
                      'text' => $relationshipOption->{$options->label},
                  ];
              }

              return response()->json([
                  'results'    => $results,
                  'pagination' => [
                      'more' => ($total_count > ($skip + $on_page)),
                  ],
              ]);
          }
      }

      // No result found, return empty array
      return response()->json([], 404);
  }

  public function PDF(Request $request, $id)
  {
      $datatest = DB::table("tb_documents")->where('id',$id)->first();


        $data = [
          'id' => $id,
          'company_name' => $datatest->company_name,
          'client_name' => $datatest->client_name,
          'document_type' => $datatest->document_type,
          'documentation_title' => $datatest->documentation_title,
          'documentation_description' => $datatest->documentation_description,
          'documentation_image' => $datatest->documentation_image,
          'proposal_type' => $datatest->proposal_type,
          'proposal_product' => $datatest->proposal_product,
          'proposal_working_days' => $datatest->proposal_working_days,
          'proposal_inclusions' => $datatest->proposal_inclusions,
          'proposal_price_description' => $datatest->proposal_price_description,
          'proposal_price_cost' => $datatest->proposal_price_cost,
          'proposal_timeframe' => $datatest->proposal_timeframe,
          'proposal_start_date' => $datatest->proposal_start_date,
          'proposal_terms' => $datatest->proposal_terms,
          'client_contact_person' => $datatest->client_contact_person,
          'client_contact_person_position' => $datatest->client_contact_person_position,
          'proposal_resources' => $datatest->proposal_resources,
          'proposal_scope' => $datatest->proposal_scope,
          'company_contact_person' => $datatest->company_contact_person,
          'client_signatory' => $datatest->client_signatory,

        ];

            
        if($datatest->document_type == "Documentation")
        {
          return PDF::loadView('admin.documents.pdf.documentation', $data)->inline($datatest->company_name.'-'.$datatest->document_type.'-'.$datatest->client_name.'-'.$datatest->created_at.'.pdf');


        }

        if($datatest->proposal_type == "workflow"){
          return PDF::loadView('admin.documents.pdf.workflow', $data)->inline($datatest->company_name.'-'.$datatest->document_type.'-'.$datatest->client_name.'-'.$datatest->created_at.'.pdf');
        }
        else if ($datatest->proposal_type == "standard"){
          return PDF::loadView('admin.documents.pdf.standard', $data)->inline($datatest->company_name.'-'.$datatest->document_type.'-'.$datatest->client_name.'-'.$datatest->created_at.'.pdf');
        }
        else if ($datatest->proposal_type == "dedicated"){
          return PDF::loadView('admin.documents.pdf.dedicatedserver', $data)->inline($datatest->company_name.'-'.$datatest->document_type.'-'.$datatest->client_name.'-'.$datatest->created_at.'.pdf');
        }
        else{

        }

      }


}
