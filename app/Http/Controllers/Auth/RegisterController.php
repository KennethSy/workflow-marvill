<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/pnbwings';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],


          //  'regcode' => ['required', 'exists:tb_pnbcodes,code,status,0'],

        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {



      if($data['typeofform'] == 'pnbwings'){
        $user = User::create([
          'name' => $data['name'],
          'email' => $data['email'],
          'password' => Hash::make($data['password']),

        ]);
        $code = $data['regcode'];

        $pnbdeets = \DB::table('users')->where('email', $data['email'])->first();

        \DB::table('tb_pnbdetails')->insert([
            'user_id' => $pnbdeets->id,
            'user_number' => $data['number'],
            'user_address' => $data['address'],
            'user_city' => $data['city'],
            'user_barangay' => $data['barangay'],
        ]);


        \DB::table('tb_pnbcodes')->where('code',$code)->update([
          'status' => '1',
          'user' => $data['name'],

        ]);

        \DB::table('users')->where('email', $data['email'])->update([
          'role_id' => '11',
        ]);


        return $user;

      }

      else if($data['typeofform'] == 'divers'){
        $user = User::create([
          'name' => $data['name'],
          'email' => $data['email'],
          'password' => Hash::make($data['password']),

        ]);


        $diverinfo = \DB::table('users')->where('email', $data['email'])->first();



        \DB::table('users')->where('email', $data['email'])->update([
          'role_id' => '18',
        ]);


        return $user;

      }




        else{
          return User::create([
              'name' => $data['name'],
              'email' => $data['email'],
              'password' => Hash::make($data['password']),
          ]);
        }

    }
}
