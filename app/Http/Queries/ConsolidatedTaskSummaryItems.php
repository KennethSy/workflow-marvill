<?php

namespace App\Http\Queries;

use DB;
use Carbon\Carbon;

class ConsolidatedTaskSummaryItems
{

    public function execute($company_id = null, $selected_project = null)
    {
        return $this->consolidatedTasks($company_id, $selected_project)
            ->orderBy('sort_date','desc')
            ->simplePaginate(10);
    }

    private function consolidatedTasks($company_id = null, $selected_project = null)
    {
        $tasks =  DB::table('tb_companies')
            ->leftjoin('tb_projects','tb_companies.id','=','tb_projects.company_id')
            ->leftjoin('tb_daily_log_sub_tasks','tb_projects.id','=','tb_daily_log_sub_tasks.project_id')
            ->leftjoin('tb_daily_main_tasks','tb_daily_main_tasks.id','=','tb_daily_log_sub_tasks.main_task_id')
            ->leftjoin('tb_daily_task_notes','tb_daily_task_notes.task_id','=','tb_daily_log_sub_tasks.id')
            ->leftjoin('users','users.id','=','tb_daily_log_sub_tasks.assigned_to')
            ->select([
                'tb_daily_log_sub_tasks.description',
                'tb_daily_log_sub_tasks.created_at',
                'tb_daily_log_sub_tasks.completed_at',
                DB::raw('null as note_id'),
                DB::raw("IFNULL(tb_daily_log_sub_tasks.main_task_id, -tb_daily_log_sub_tasks.id) as unique_task_id"),
                DB::raw("IFNULL(MAX(tb_daily_task_notes.created_at), IFNULL(tb_daily_log_sub_tasks.completed_at, tb_daily_log_sub_tasks.created_at ) ) as sort_date"),
            ])
            ->where('tb_daily_log_sub_tasks.created_at','<',Carbon::now())
            ->where('tb_companies.id','=',$company_id)
            ->groupBy('unique_task_id');

        if($selected_project != null) {
            $tasks = $tasks->where('tb_projects.id','=',$selected_project);
        }

        return $tasks;
    }

    private function notesTableOnly($company_id = null, $selected_project = null)
    {
        $notes =  DB::table('tb_companies')
            ->leftjoin('tb_projects','tb_companies.id','=','tb_projects.company_id')
            ->leftjoin('tb_daily_log_sub_tasks','tb_projects.id','=','tb_daily_log_sub_tasks.project_id')
            ->leftjoin('tb_daily_task_notes','tb_daily_log_sub_tasks.id','=','tb_daily_task_notes.task_id')
            ->leftjoin('users','users.id','=','tb_daily_task_notes.created_by')
            ->select([
                'tb_daily_log_sub_tasks.description',
                'tb_daily_task_notes.note',
                'tb_daily_task_notes.created_at',
                'tb_daily_log_sub_tasks.completed_at',
                'users.name as user',
                'tb_daily_task_notes.id as note_id',
                'tb_daily_log_sub_tasks.main_task_id as main_id',
            ])
            ->where('tb_daily_task_notes.id','!=',null)
            ->where('tb_daily_log_sub_tasks.created_at','<',Carbon::now())
            ->where('tb_companies.id','=',$company_id);

        if($selected_project != null) {
            $notes = $notes->where('tb_projects.id','=',$selected_project);
        }

        return $notes;
    }

    private function tasksTableOnly($company_id = null, $selected_project = null)
    {
        $tasks = DB::table('tb_companies')
            ->leftjoin('tb_projects','tb_companies.id','=','tb_projects.company_id')
            ->leftjoin('tb_daily_log_sub_tasks','tb_projects.id','=','tb_daily_log_sub_tasks.project_id')
            ->leftjoin('users','users.id','=','tb_daily_log_sub_tasks.assigned_to')
            ->select([
                'tb_daily_log_sub_tasks.description',
                DB::raw('null as note'),
                'tb_daily_log_sub_tasks.created_at',
                'tb_daily_log_sub_tasks.completed_at',
                'users.name as user',
                DB::raw('null as note_id'),
                'tb_daily_log_sub_tasks.main_task_id as main_id',
            ])
            ->where('tb_daily_log_sub_tasks.created_at','<',Carbon::now());

        $tasks = $tasks->where('tb_companies.id','=',$company_id);

        if($selected_project != null) {
            $tasks = $tasks->where('tb_projects.id','=',$selected_project);
        }

        return $tasks;
    }
}
