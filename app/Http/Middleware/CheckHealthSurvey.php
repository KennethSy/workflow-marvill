<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;
use Carbon\Carbon;
use Route;

class CheckHealthSurvey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //return $next($request);
        if(Route::currentRouteName() == 'voyager.logout'
            || Route::currentRouteName() == 'voyager.login'
            || Route::currentRouteName() == 'voyager.postlogin'
        ) {
            return $next($request);
        }

        if(!str_contains(url()->current(), '/admin') || str_contains(url()->current(), '/admin/voyager-assets')) {
            return $next($request);
        }

        if (!Auth::guest()) {

            if ( Auth::user()->role_id == 4 &&
                !DB::table('tb_health_survey_employees')
                    ->where('user_id',Auth::user()->id)
                    ->where('created_at','>=',Carbon::now()->startOfDay())
                    ->where('created_at','<=',Carbon::now()->endofDay())
                    ->first()) {
                return redirect()->route('healthsurvey.questions');
            }

            return $next($request);
        }

        $urlLogin = route('voyager.login');

        return redirect()->guest($urlLogin);
    }
}
