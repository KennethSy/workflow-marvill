<?php

namespace App\Http\Actions;

use DB;
use Carbon\Carbon;

class CalculatePNBNetSales
{

    public function run($date) {
        $sumup = DB::table("tb_pnbwings")->where('location','makati')->whereDate('created_at', $date)->get()->sum("price");

        $totalorders = DB::table("tb_pnbwings")->where('location','makati')->whereDate('created_at', $date)->get();
        $totalnetpanda = DB::table("tb_pnbwings")->where('location','makati')->where('order_type','foodpanda')->whereDate('created_at', $date)->get()->sum("price");
        $totalnetdel = DB::table("tb_pnbwings")->where('location','makati')->where('order_type','delivery')->where('order_status','confirmed')->whereDate('created_at', $date)->get()->sum("price");
        $totalnetlala = DB::table("tb_pnbwings")->where('location','makati')->where('order_type','lalafood')->whereDate('created_at', $date)->get()->sum("price");
        $totes = count($totalorders);
        $net = (($totalnetpanda * 0.7) +  $totalnetdel + $totalnetlala);

        return [
            'net_sales' => $net,
            'total_orders' => $totes,
        ];
    }
}
