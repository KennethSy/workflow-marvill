<?php

namespace App\Http\Actions;

use DB;
use Carbon\Carbon;
use Auth;

class MarkTaskRequestsAsRead
{

    public function run() {
        if(Auth::user()->role_id == 1 || Auth::user()->role_id == 4) {
            $requests = DB::table('tb_request_tasks')
                ->leftJoin('tb_request_tasks_notification','tb_request_tasks_notification.request_id','=','tb_request_tasks.id')
                ->where('tb_request_tasks.status','=',0)
                ->where('tb_request_tasks_notification.is_read','=', null)
                ->where('tb_request_tasks_notification.user_id','=', Auth::user()->id)
                ->select('tb_request_tasks.id')
                ->get();

            foreach($requests as $request) {
                DB::table('tb_request_tasks_notification')
                    ->insert([
                        'request_id' => $request->id,
                        'user_id' => Auth::user()->id,
                        'is_read' => 1,
                    ]);
            }
        }

        return true;
    }
}
