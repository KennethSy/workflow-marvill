<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class InquiryRead extends AbstractAction
{
    public function getTitle()
    {
        return 'Mark as Replied';
    }

    public function getIcon()
    {
        return 'voyager-eye';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-right',
        ];
    }

    public function shouldActionDisplayOnDataType()
    {
        // show or hide the action button, in this case will show for posts model
        return $this->dataType->slug == 'inquiries';
    }
    public function getDefaultRoute()
    {
         return route('inquiry.replied', array("id"=>$this->data->{$this->data->getKeyName()}));
    }
}
