<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TbCarModel extends Model
{
    public function brands()
    {
        return $this->belongsTo('App\TbCarBrand', 'brand_id');
    }

    public function scopeEnabled($query)
    {
        return $query->where('status', '=', 1);
    }
}
