<?php

namespace App\Console;

use Carbon\Carbon;
use DB;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $action = new \App\Http\Actions\CalculatePNBNetSales();
            $data = $action->run(Carbon::today()->subDays(1));

            $date = Carbon::today()->subDays(1)->format('Y-m-d');

            DB::table('tb_bookkeeping')->insert([
                'description' => 'Revenue - '.$date,
                'company_id' => 146,
                'income' => $data['net_sales'],
                'expense' => 0,
                'tin_no' => null,
                'date' => $date,
                'cheque_no' => null,
                'attachment' => null,
                'type' => null,
            ]);

        })->dailyAt('00:05');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
