<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Frontend\PageController@index')->name('frontend.home');
Route::get('/offline', function () {
    return view('vendor.laravelpwa.offline');
});
Route::get('/about', 'Frontend\PageController@about')->name('frontend.about');
Route::get('/services', 'Frontend\PageController@services')->name('frontend.services');
Route::get('/careers', 'Frontend\PageController@careers')->name('frontend.careers');
Route::get('/contact-us', 'Frontend\PageController@contactus')->name('frontend.contactus');
Route::group(['middleware' => ['cors']], function() {
    Route::post('/submit-inquiry', 'Frontend\PageController@submitInquiry')->name('submit.inquiry');
    Route::get('/rmjauto/getModels', 'Admin\CarLoanController@getModels')->name('rmjauto.getmodels');
    Route::post('/rmjauto/apply', 'Admin\CarLoanController@applyPost')->name('rmjauto.applypost');
});
Route::get('/dailylog/rollover/{any?}', 'Admin\DailylogController@rollover');
Route::get('/overdue/notif', 'Admin\DailylogController@overdue');
Route::get('/pnbwings', 'Admin\PnbController@test');
Route::post('/pnbwings', 'Auth\LoginController@loginpnb')->name('pnbwings.login');
Route::get('/mariscoral', 'Admin\DiverController@test');
Route::get('/pnbwings/order', 'Admin\PnbController@custorder')->name('pnbwings.order');
Route::get('/pnbwings/checkout/{id}', 'Admin\PnbController@checkout');
Route::get('/pnbwings/checkedout/{id}', 'Admin\PnbController@checkedout')->name('pnbwings.checkedout');
Route::get('/rmjauto/apply', 'Admin\CarLoanController@apply')->name('rmjauto.apply');

Route::get('/rmjauto/apply-success', 'Admin\CarLoanController@applySuccess')->name('rmjauto.applysuccess');

Route::group(['prefix' => 'admin'], function () {
    Route::group(['middleware' => 'admin.user'], function () {
        Route::group(['middleware' => 'check.healthsurvey'], function () {
            Route::get('/FAQ','Admin\UserController@faq');
            Route::get('/documents/print/{id?}', 'Admin\DocumentController@PDF')->name('documents.print');
            Route::get('/mrv-tracking', 'Admin\MRVController@index');
            Route::post('/mrv-tracking/login', 'Admin\MRVController@login');
            Route::get('/pos/suppliers', 'Admin\POSController@index');
            Route::get('/tb-pnbwings/allorders', 'Admin\PnbController@all_orders');
            Route::get('/tb-pnbwings/print/{id?}', 'Admin\PnbController@receipt');
            Route::get('/tb-pnbwings/cooking/{id}','Admin\PnbController@cooking')->name('pnbwings.cooking');
            Route::get('/tb-pnbwings/preparing/{id}','Admin\PnbController@preparing')->name('pnbwings.preparing');
            Route::get('/tb-pnbwings/released/{id}','Admin\PnbController@released')->name('pnbwings.released');
            Route::get('/tb-pnbwings/test','Admin\PnbController@test2')->name('pnbwings.test');
            Route::get('/requesttask/testMail', 'Admin\RequestTaskController@testMail');
            Route::get('/requesttask/data', 'Admin\RequestTaskController@data')->name('requesttask.data');
            Route::post('/requesttask/approve/{is}', 'Admin\RequestTaskController@approve')->name('requesttask.approve');
            Route::get('task_summary/{company_id?}', 'Admin\TaskController@summary')->name('tasksummary');
            Route::get('/payroll', 'Admin\PayrollController@test')->name('payroll.index');
            Route::post('/payroll/markpaid', 'Admin\PayrollController@markPaid')->name('payroll.markpaid');
            Route::get('/payroll/adjustment/{id}', 'Admin\PayrollController@adjustment')->name('payroll.adjustment');
            Route::post('/payroll/adjustment/{id}', 'Admin\PayrollController@saveAjustment')->name('payroll.saveadjustment');
            Route::get('/payroll/view/{id}', 'Admin\PayrollController@view')->name('payroll.view');
            Route::get('/payroll/edit/{id}', 'Admin\PayrollController@edit')->name('payroll.edit');
            Route::get('/socials', 'Admin\MarketingController@socials')->name('marketing.socials');
            Route::get('/invoices/data', 'Admin\InvoiceController@data')->name('invoices.data');
            Route::post('/invoices/copy-and-edit/{id}', 'Admin\InvoiceController@copyAndEdit')->name('invoices.copy');
            Route::post('/invoices/markpaid/{id}', 'Admin\InvoiceController@markpaid')->name('invoices.markpaid');
            Route::get('/dailylog/data', 'Admin\DailylogController@data')->name('dailylog.data');
            Route::get('/dailylog/switchloc/{id}/{work}', 'Admin\DailylogController@switchloc')->name('dailylog.switchloc');
            Route::get('/dailytask/data', 'Admin\TaskController@data')->name('dailytask.data');
            Route::get('/dailytask/calendar-data', 'Admin\TaskController@calendarData')->name('dailytask.calendardata');
            Route::post('/dailytask/complete/{id}', 'Admin\TaskController@complete')->name('dailytask.complete');
            Route::post('/dailytask/open/{id}', 'Admin\TaskController@open')->name('dailytask.open');
            Route::post('/dailytask/confirm/{id}', 'Admin\TaskController@confirm')->name('dailytask.confirm');
            Route::post('/dailytask/reject/{id}', 'Admin\TaskController@reject')->name('dailytask.reject');
            Route::post('/dailytask/carryover/{id}', 'Admin\TaskController@carryover')->name('dailytask.carryover');
            Route::post('/dailytask/editnote/{id}', 'Admin\TaskController@editnote')->name('dailytask.editnote');
            Route::post('/dailytask/change-value/{id}', 'Admin\TaskController@changeValue')->name('dailytask.changevalue');
            Route::get('/dailytask/{id}/download/{file_id}', 'Admin\TaskController@downloadAttachment')->name('dailytask.downloadattachment');
            Route::post('/users/set-active/{id}', 'Admin\UserController@setActive')->name('voyager.users.active');
            Route::post('/users/set-inactive/{id}', 'Admin\UserController@setInactive')->name('voyager.users.inactive');
            Route::get('/inquiries/update/{id}','Admin\InquiryController@reply')->name('inquiry.replied');
            Route::get('/bookkeeping/export','Admin\BookkeepingController@export')->name('bookkeeping.export');
        });
        Route::get('/health-survey', 'Admin\HealthSurveyController@index')->name('healthsurvey.index');
        Route::get('/health-survey/questions', 'Admin\HealthSurveyController@survey')->name('healthsurvey.questions');
        Route::post('/health-survey/questions', 'Admin\HealthSurveyController@surveySubmit')->name('healthsurvey.questionsSubmit');
    });
    Route::group(['middleware' => 'check.healthsurvey'], function () {
        Voyager::routes();
    });
});

Route::get('/auth/redirect/{provider}', 'AuthController@redirect');
Route::get('/callback/{provider}', 'AuthController@callback');
