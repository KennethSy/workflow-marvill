@extends('voyager::master')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
.borderless table {
    border-top-style: none !important;
    border-left-style: none !important;
    border-right-style: none !important;
    border-bottom-style: none !important;
}
.image-area {
  position: relative;
  width: 120px;
  background: #333;
}
.image-area img{
  max-width: 100%;
  height: auto;
}
.remove-image {
  display: none;
  position: absolute;
  top: -10px;
  right: -10px;
  border-radius: 10em;
  padding: 2px 6px 3px;
  text-decoration: none;
  font: 700 21px/20px sans-serif;
  background: #555;
  border: 3px solid #fff;
  color: #FFF;
  box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
  text-shadow: 0 1px 2px rgba(0,0,0,0.5);
  -webkit-transition: background 0.5s;
  transition: background 0.5s;
}
.remove-image:hover {
  background: #E54E4E;
  padding: 3px 7px 5px;
  top: -11px;
  right: -11px;
}
.remove-image:active {
  background: #E54E4E;
  top: -10px;
  right: -11px;
}

.container {

  margin: 0 auto;
}

#cart {
  width: 100%;
}

#cart h1 {
  font-weight: 300;
}

#cart a {
  color: #ca1b49;
  text-decoration: none;

  -webkit-transition: color .2s linear;
  -moz-transition: color .2s linear;
  -ms-transition: color .2s linear;
  -o-transition: color .2s linear;
  transition: color .2s linear;
}

#cart a:hover {
  color: #000;
}

.product.removed {
  margin-left: 980px !important;
  opacity: 0;
}

.product {
  border: 1px solid #eee;
  margin: 20px 0;
  width: 100%;
  height: 250px;
  position: relative;

  -webkit-transition: margin .2s linear, opacity .2s linear;
  -moz-transition: margin .2s linear, opacity .2s linear;
  -ms-transition: margin .2s linear, opacity .2s linear;
  -o-transition: margin .2s linear, opacity .2s linear;
  transition: margin .2s linear, opacity .2s linear;
}

.product img {
  width: 100%;
  height: 100%;
}

.product header, .product .content {

  border: 1px solid #ccc;
  border-style: none none solid none;
  float: left;
}

.product header {
  background: #000;
  margin: 0 1% 20px 0;
  overflow: hidden;
  padding: 0;
  position: relative;
  width: 24%;
  height: 195px;
}

.product header:hover img {
  opacity: .7;
}

.product header:hover h3 {
  bottom: 73px;
}

.product header h3 {
  background: #ca1b49;
  color: #fff;
  font-size: 22px;
  font-weight: 300;
  line-height: 49px;
  margin: 0;
  padding: 0 30px;
  position: absolute;
  bottom: -50px;
  right: 0;
  left: 0;

  -webkit-transition: bottom .2s linear;
  -moz-transition: bottom .2s linear;
  -ms-transition: bottom .2s linear;
  -o-transition: bottom .2s linear;
  transition: bottom .2s linear;
}

.remove {
  cursor: pointer;
}

.product .content {
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 140px;
  padding: 0 20px;
  width: 75%;
}

.product h1 {
  color: #ca1b49;
  font-size: 25px;
  font-weight: 300;
  margin: 17px 0 20px 0;
}

.product footer.content {
  height: 50px;
  margin: 6px 0 0 0;
  padding: 0;
}

.product footer .price {
  background: #fcfcfc;
  color: #000;
  float: right;
  font-size: 15px;
  font-weight: 300;
  line-height: 49px;
  margin: 0;
  padding: 0 30px;
}

.product footer .full-price {
  background: #ca1b49;
  color: #fff;
  float: right;
  font-size: 22px;
  font-weight: 300;
  line-height: 49px;
  margin: 0;
  padding: 0 30px;

  -webkit-transition: margin .15s linear;
  -moz-transition: margin .15s linear;
  -ms-transition: margin .15s linear;
  -o-transition: margin .15s linear;
  transition: margin .15s linear;
}

.qt, .qt-plus, .qt-minus {
  display: block;
  float: left;
}

.qt {
  font-size: 19px;
  line-height: 50px;
  width: 70px;
  text-align: center;
}

.qt-plus, .qt-minus {
  background: #ca1b49;
  border: none;
  font-size: 30px;
  font-weight: 300;
  height: 100%;
  padding: 0 20px;
  -webkit-transition: background .2s linear;
  -moz-transition: background .2s linear;
  -ms-transition: background .2s linear;
  -o-transition: background .2s linear;
  transition: background .2s linear;
}

.qt-plus:hover, .qt-minus:hover {
  background: #53b5aa;
  color: #fff;
  cursor: pointer;
}

.qt-plus {
  line-height: 50px;
}

.qt-minus {
  line-height: 47px;
}

#site-footer {
  margin: 30px 0 0 0;
}

#site-footer {
  padding: 40px;
}

#site-footer h1 {
  background: #fcfcfc;
  border: 1px solid #ccc;
  border-style: none none solid none;
  font-size: 24px;
  font-weight: 300;
  margin: 0 0 7px 0;
  padding: 14px 40px;
  text-align: center;
}

#site-footer h2 {
  font-size: 24px;
  font-weight: 300;
  margin: 10px 0 0 0;
}

#site-footer h3 {
  font-size: 19px;
  font-weight: 300;
  margin: 15px 0;
}

.left {
  float: left;
}

.right {
  float: right;
}
 

.btn:hover {
  color: #fff;
  background: #000;
}

.type {
  background: #fcfcfc;
  font-size: 13px;
  padding: 10px 16px;
  left: 100%;
}

.type, .color {
  border: 1px solid #ccc;
  border-style: none none solid none;
  position: absolute;
}

.color {
  width: 40px;
  height: 40px;
  right: -40px;
}

.red {
  background: #cb5a5e;
}

.yellow {
  background: #f1c40f;
}

.blue {
  background: #3598dc;
}

.minused {
  margin: 0 50px 0 0 !important;
}

.added {
  margin: 0 -50px 0 0 !important;
}
.ordersec{
	background:#1e1e1e;
	border-radius:10px;
	padding:10px
}
.ordersec1{
	background:#404040;
	border-radius:10px;
	padding:10px
}
</style>
@stop

@section('page_title', ($edit ? 'Edit' : 'Add').' Ang Paborito ni Boss Order')

@section('page_header')
<h1 class="page-title">
  <i class="voyager-fire"></i>
  {{ ($edit ? 'Edit' : 'Add').' Ang Paborito ni Boss Order' }}
</h1>
@stop
<?php
  $categories = \DB::table('tb_pnbmenus')->select('item_category')->groupBy('item_category')->get();

?>
@section('content')
<div class="page-content edit-add container-fluid">
 
  <div class="row">
    <div class="col-md-12">
   <div class="panel panel-bordered">
    
	 
   <div class="col-md-12">Demo Only</div>
   <div class="col-md-8">
     <form role="form"
        class="form-edit-add form-horizontal"
        action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $order->id) : route('voyager.'.$dataType->slug.'.store') }}"  method="POST" enctype="multipart/form-data">
		    
			<div class="col-xs-12 "> 
			<div class="col-md-6">
		 <div class="form-group">

   
        <input type="hidden" name="orderedby"  value="inhouse"></input>
         
         
			 <label for="Order Type" class=" control-label "> Order Type <span class="asterix"> * </span></label>
              <select name="order_type22" id="order_type22" rows="5" class="select2 form-control" value="{{ $order['order_type'] }}">
                <option value=""> -- Please Select -- </option>
                <option value="foodpanda" <?php if (strpos($order['order_type'], 'foodpanda') !== false) { ?> selected <?php } ?> >foodpanda</option>
                <option value="lalafood" <?php if (strpos($order['order_type'], 'lalafood') !== false) { ?> selected <?php } ?> >lalafood</option>
                <option value="delivery" <?php if (strpos($order['order_type'], 'delivery') !== false) { ?> selected <?php } ?> >delivery</option>
              </select>
       
          </div>  
   </div>
    <div class="col-md-6"> 
		<div class="form-group">
            
			<label id="order_label22" for="order_number22" class=" control-label text-left " style="display:none;"> Order Number(Foodpanda) <span class="asterix"> * </span></label>
              <input type="text" style="display:none;" name="order_number" rows="1" id="order_number22" class="form-control input-md " value="{{ $order['order_number'] }}"></input> 
          </div>
   </div>
   </div>
   	<div class="col-xs-12  "> 
   	<div class="col-xs-12 col-md-6"> 
			
			  <div class="form-group">
           
    
			 <label id="ordernotes" for="ordernotes" class=" control-label text-left "> ORDER NOTES</label>
              <input type="text"  name="ordernotes" rows="1" id="ordernotes" class="form-control input-md " value="{{ $order['customer_name'] }}"></input>
       
          </div>
          </div>
			<div class="col-xs-12 col-md-6"> 
			
			  <div class="form-group">
           
    
			 <label id="ordernotes" for="ordernotes" class=" control-label text-left "> FOODA PANDA DISCOUNT</label>
              <input type="text"  name="ordernotes" rows="1" id="ordernotes" class="form-control input-md " value="{{ $order['customer_name'] }}"></input>
       
          </div>
			
			</div>
			</div>
   <div class="col-xs-12 "> 
   
    <div class="col-md-12"> 
		  <div class="form-group">
           
    
			 <label id="customer_name" for="customer_name" class=" control-label text-left "> Customer Name <span class="asterix"> * </span></label>
              <input type="text"  name="customer_name" rows="1" id="customer_name" class="form-control input-md " value="{{ $order['customer_name'] }}"></input>
       
          </div>
   </div>
    <div class="col-md-12" style="border-bottom: 1px solid #fff;  margin: 14px 0px;">  
   </div>
   </div> 
   
    
    <div class="col-md-10"> 
	<div class="col-xs-12 "> 
	 	 	<div class="form-group">
					<label for="Order Type" class=" control-label "> Order <span class="asterix"> * </span></label>
              <select name="order_type22" id="order_type22" rows="5" class="select2 form-control" value="{{ $order['order_type'] }}">
                <option value=""> -- Please Select -- </option>
               
          @foreach ($categories as $category)
			 <?php
              $items = \DB::table('tb_pnbmenus')->where('item_category', $category->item_category)->select('item_main')->groupBy('item_main')->get();
              ?>
              @foreach($items as $item)
			    <?php
                $variants = \DB::table('tb_pnbmenus')->where('item_main', $item->item_main)->where('item_status','Active')->select('item_variant','item_price')->groupBy('item_variant')->orderBy('item_price','ASC')->get();

                    ?>
					  @foreach($variants as $variant)
					    <option value="{{$item->item_main}}{{$variant->item_variant}}" >{{$item->item_main}} - {{$variant->item_variant}} /  PHP {{$variant->item_price}} </option>
						@endforeach
						
			  @endforeach
			  @endforeach
			       </select> 
			</div>
			</div> 
   </div>
     <div class="col-md-2">
	 <div class="col-xs-12 "> 
	 	<div class="form-group"> 
			<label id="qty" for="qty" class=" control-label text-left "  >QTY <span class="asterix"> * </span></label>
              <input type="number"   name="qty" rows="1"  class="form-control input-md " min="1" value="1"></input> 
          </div>
          </div>
		</div>	 
<div class="col-xs-12  "> <a class="btn btn-primary" href="#"> ADD NEW ITEM </a></div> 		
   	</form>
   </div>
   <div class="col-md-4">
   <h4> ORDER SUMMARY  </h4>
   
   <table class="table table-hover ">
<tbody>
<thead>
<td width="50%">ITEM</td>
<td class="text-center">PRICE</td>
<td class="text-center">QTY</td>
<td class="text-center">TOTAL</td>
</thead>
<tr>
<td>GARLIC PARMESAN 6 PCS</td>
<td class="text-center">225</td>
<td class="text-center">2</td>
<td class="text-center">PHP 450</td>
</tr>
<tr>
<td>SOY GARLIC 6 PCS</td>
<td class="text-center">225</td>
<td class="text-center">2</td>
<td class="text-center">PHP 450</td>
</tr> 
</tbody>
</table>

  <table class="borderless table ">
<tbody> 
<tr>
<td width="50%"></td>
<td class="text-center">SUBTOTAL</td>
<td class="text-center"> </td>
<td class="text-center">PHP VALUE SUBTOTAL</td>
</tr> 
<tr>
<td width="50%"></td>
<td class="text-center">DISCOUNT</td>
<td class="text-center"> </td>
<td class="text-center">PHP VALUE DISCOUNT</td>
</tr> 
<tr>
<td width="50%"></td>
<td class="text-center">TOTAL</td>
<td class="text-center"> </td>
<td class="text-center">PHP VALUE TOTAL</td>
</tr> 
</tbody>
</table>
   
   <a class="btn btn-primary btn-block" href="#"> PLACE ORDER </a>
   
   
   </div>
	</div>
	</div>
	</div> 
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-bordered">
        <!-- form start -->
        <form role="form"
        class="form-edit-add form-horizontal"
        action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $order->id) : route('voyager.'.$dataType->slug.'.store') }}"  method="POST" enctype="multipart/form-data">
        <!-- PUT Method if we are editing -->
        @if($edit)
        {{ method_field("PUT") }}
        @endif
        <!-- CSRF TOKEN -->
        {{ csrf_field() }}
        <div class="panel-body">
		   @if (count($errors) > 0)
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif
		<div class="form-group">
        <input type="hidden" name="orderedby"  value="inhouse"></input>
            <label for="Order Type" class=" control-label col-md-4 text-left"> Order Type <span class="asterix"> * </span></label>
            <div class="col-md-6">
              <select name="order_type" id="order_type" rows="5" class="select2 form-control" value="{{ $order['order_type'] }}">
                <option value=""> -- Please Select -- </option>
                <option value="foodpanda" <?php if (strpos($order['order_type'], 'foodpanda') !== false) { ?> selected <?php } ?> >foodpanda</option>
                <option value="lalafood" <?php if (strpos($order['order_type'], 'lalafood') !== false) { ?> selected <?php } ?> >lalafood</option>
                <option value="delivery" <?php if (strpos($order['order_type'], 'delivery') !== false) { ?> selected <?php } ?> >delivery</option>
              </select>
            </div>
          </div> 

          <div class="form-group">
            <label id="order_label" for="order_number" class=" control-label col-md-4 text-left " style="display:none;"> Order Number(Foodpanda) <span class="asterix"> * </span></label>
            <div class="col-md-6">
              <input type="text" style="display:none;" name="order_number" rows="1" id="order_number" class="form-control input-md " value="{{ $order['order_number'] }}"></input>
            </div>
            <div class="col-md-2">

            </div>
          </div>

          <div class="form-group">
            <label id="customer_name" for="customer_name" class=" control-label col-md-4 text-left "> Customer Name <span class="asterix"> * </span></label>
            <div class="col-md-6">
              <input type="text"  name="customer_name" rows="1" id="customer_name" class="form-control input-md " value="{{ $order['customer_name'] }}"></input>
            </div>
            <div class="col-md-2">

            </div>
          </div>



		  </div>


      <div class="panel-body">


        <label for="order" class=" control-label col-md-4 text-left"> Order <span class="asterix"> * </span></label>
        <div class="form-group">
          <div class="container">
          @foreach ($categories as $category)
            <section id="cart">

              <h2>{{$category->item_category}}</h2>
              <div class="row">


              <?php
              $items = \DB::table('tb_pnbmenus')->where('item_category', $category->item_category)->select('item_main')->groupBy('item_main')->get();
              ?>
              @foreach($items as $item)
              <div class="col-md-12">
                <?php
                $variants = \DB::table('tb_pnbmenus')->where('item_main', $item->item_main)->where('item_status','Active')->select('item_variant','item_price')->groupBy('item_variant')->orderBy('item_price','ASC')->get();

                    ?>

                <h2 class="col-xs-12">{{$item->item_main}}</h2>
                @foreach($variants as $variant)
                        <div class="col-md-4 col-xs-12 ordersec">


                            <h4>{{$variant->item_variant}}<small class="pull-right">PRICE: <span class="price1"> {{$variant->item_price}}  </span></small></h4>

                  <div class="col-xs-12">

                            <input type="hidden" name="order[]" class="ordervalue" id="" value="{{$item->item_main}} {{$variant->item_variant}}"></input>
                            <span class="qt-minus" id="">-</span>
                            <span class="qt" id="">0</span>
                            <input type="hidden" name="qty[]" class="orderqty" value="0"></input>
                            <span class="qt-plus" id="">+</span>


                  <span class="price hidden"> {{$variant->item_price}} </span>
                    <small class=" pull-right"> TOTAL:  </small><br/>
                  <h2 class="full-price pull-right" style="">   0 </h2>


                        </div>
                        </div>
                        @endforeach
              </div>
              <hr/>

              @endforeach
            </section>
          @endforeach

          </div>
        </div>

        <div class="form-group">
          <label id="order_labdisc" for="order_disc" class=" control-label col-md-4 text-left " style="display:none;"> Foodpanda Discount <span class="asterix">  </span></label>
          <div class="col-md-6">
            <input type="text" name="order_disc" rows="1" id="order_disc" class="form-control input-md " value='' style="display:none;"></input>
            <button type="button" class="btn btn-danger" id="btn_disc" style="display:none;">Add Discount</button>

          </div>
          <div class="col-md-2">

          </div>
        </div>
        <div class="form-group">
          <label id="order_note" for="order_note" class=" control-label col-md-4 text-left " > Note <span class="asterix">  </span></label>
          <div class="col-md-6">
            <input type="text" name="order_note" rows="3" id="order_note" class="form-control input-md " value='' ></input>


          </div>
          <div class="col-md-2">

          </div>
        </div>

        <div class="container clearfix">

          <div class="left">


          </div>

          <div class="right">
            <h1 id="disc" >Discount: 15% </h1>
            <h1 class="total">Total:    <span>0</span></h1>
            <input type="hidden" name="price" id="price" value=""> </input>
            @section('submit-buttons')
            <button type="submit" class="btn save">{{ __('voyager::generic.save') }}</button>
            @stop
            @yield('submit-buttons')
          </div>

        </div>








      </div>




      @if($errors->has('attachment'))
      @foreach ($errors->get('attachment') as $error)
      <span class="help-block">{{ $error }}</span>
      @endforeach
      @endif
    </div>
    </div><!-- panel-body -->


  </form>

</div> 




<div class="modal fade modal-danger" id="confirm_delete_modal">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"
        aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
      </div>

      <div class="modal-body">
        <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
        <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
      </div>
    </div>
<!-- End Delete File Modal -->





@stop

@section('css')
<link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
@stop

@section('javascript')
<script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script>
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#attachment_file_preview').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
    $('.image-area').toggleClass('hidden', false);
    $('#attachment_file_name').val('');
  }
}

$("#attachment").change(function(){
  readURL(this);
});


var params = {};
var $file;

function deleteHandler(tag, isMulti) {
  return function() {
    $file = $(this).siblings(tag);

    params = {
      slug:   '{{ $dataType->slug }}',
      filename:  $file.data('file-name'),
      id:     $file.data('id'),
      field:  $file.parent().data('field-name'),
      multi: isMulti,
      _token: '{{ csrf_token() }}'
    }

    $('.confirm_delete_name').text(params.filename);
    $('#confirm_delete_modal').modal('show');
  };
}

$('document').ready(function () {
  $('.toggleswitch').bootstrapToggle();
  var i = 1;
  //Init datepicker for date fields if data-datepicker attribute defined
  //or if browser does not handle date inputs



  $('.side-body input[data-slug-origin]').each(function(i, el) {
    $(el).slugify();
  });



  $('#confirm_delete').on('click', function(){
    $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
      if ( response
        && response.data
        && response.data.status
        && response.data.status == 200 ) {

          toastr.success(response.data.message);
          $file.parent().fadeOut(300, function() { $(this).remove(); })
        } else {
          toastr.error("Error removing file.");
        }
      });

      $('#confirm_delete_modal').modal('hide');
    });
    $('[data-toggle="tooltip"]').tooltip();
  });


  var check = false;

  function changeVal(el) {
    var qt = parseFloat(el.parent().children(".qt").html());
    var price = parseFloat(el.parent().children(".price").html());
    var eq = Math.round(price * qt * 100) / 100;

    el.parent().children(".full-price").html( " " + eq + " " );

    changeTotal();
  }

  function changeTotal() {

    var price = 0;

    $(".full-price").each(function(index){
      price += parseFloat($(".full-price").eq(index).html());
    });

    price = Math.round(price * 100) / 100;
    var tax = Math.round(price * 0.05 * 100) / 100
    var shipping = parseFloat($(".shipping span").html());
    var fullPrice = Math.round((price + tax + shipping) *100) / 100;


    if(price == 0) {
      fullPrice = 0;
    }

    $(".subtotal span").html(price);
    $(".tax span").html(tax);



    if(document.getElementById('order_type').value == "delivery"){
      deliveryprice = Math.round(price * 0.85);
      document.getElementById("price").value=""+deliveryprice;
        $(".total span").html(deliveryprice);

    }
    else if(document.getElementById('order_type').value == "foodpanda"){

      document.getElementById("price").value=""+price;
      $(".total span").html(price);

      $('#btn_disc').on('click', function(){
        if(document.getElementById('order_disc').value != ""){
          foodisc = document.getElementById('order_disc').value;
          discprice = Math.round(price - foodisc );
          $(".total span").html(discprice);
          document.getElementById("price").value=""+discprice;
        }

      });
    }
    else{
      document.getElementById("price").value=""+price;
      $(".total span").html(price);
    }

  }

  $(document).ready(function(){

    $(".remove").click(function(){
      var el = $(this);
      el.parent().parent().addClass("removed");
      window.setTimeout(
        function(){
          el.parent().parent().slideUp('fast', function() {
            el.parent().parent().remove();
            if($(".product").length == 0) {

            }
            changeTotal();
          });
        }, 200);
      });

      $(".qt-plus").click(function(){

        var new_value = parseInt($(this).parent().children(".qt").html()) + 1;

        $(this).parent().children(".qt").html(new_value);

        $(this).parent().children("input[name='qty[]']").val(new_value);

        $(this).parent().children(".full-price").addClass("added");
       //get value of black box



        var el = $(this);
        window.setTimeout(function(){el.parent().children(".full-price").removeClass("added"); changeVal(el);}, 150);
      });

      $(".qt-minus").click(function(){

        child = $(this).parent().children(".qt");

        if(parseInt(child.html()) > 0) {
            var old_value = parseInt($(this).parent().children(".qt").html()) - 1;

            $(this).parent().children(".qt").html(old_value);

            $(this).parent().children("input[name='qty[]']").val(old_value);
        }




        $(this).parent().children(".full-price").addClass("minused");

        var el = $(this);
        window.setTimeout(function(){el.parent().children(".full-price").removeClass("minused"); changeVal(el);}, 150);
      });

      window.setTimeout(function(){$(".is-open").removeClass("is-open")}, 1200);

      $(".btn").click(function(){
        check = true;
        $(".remove").click();
      });
    });

    $(document).on('change', '#order_type', function () {//do something}){
      if (document.getElementById('order_type').value == "foodpanda") { 
        $("#order_number").show(); 
        $("#order_label").show();
        $('#disc').hide();
        $('#order_labdisc').show();
        $('#order_disc').show();
        $('#btn_disc').show();
      }
      else if (document.getElementById('order_type').value == "delivery"){
        $('#disc').show(); 
        $("#order_number").hide(); 
        $("#order_label").hide();
        $('#order_labdisc').hide();
        $('#order_disc').hide();
        $('#btn_disc').hide();

      }
      else{
            $("#order_number").hide(); 
            $("#order_label").hide(); 
            $('#disc').hide();
            $('#order_labdisc').hide();
            $('#order_disc').hide();
            $('#btn_disc').hide();
      }
    });


    $(document).on('change', '#order_type', function () {//do something}){
      if (document.getElementById('order_type22').value == "foodpanda") {
        $("#order_number22").show(); 
        $("#order_label22").show();
        
      }
      else if (document.getElementById('order_type').value == "delivery"){
        $('#disc').show();
        $("#order_number22").hide(); 
        $("#order_label22").hide(); 

      }
      else{ 
            $("#order_number22").hide(); 
            $("#order_label22").hide(); 
      }
    });







    </script>
    @stop
