@extends('voyager::master')

@section('page_title', 'Ang Paborito ni Boss')
<?php
      $role = Auth::user()->role_id;

      if($role == '16'){
      $sumuppam = DB::table("tb_pnbwings")->where('location','pampanga')->whereDate('created_at', \Carbon\Carbon::today())->get()->sum("price");

      $totalorderspam = DB::table("tb_pnbwings")->where('location','pampanga')->whereDate('created_at', \Carbon\Carbon::today())->get();
      $totalnetpandapam = DB::table("tb_pnbwings")->where('location','pampanga')->where('order_type','foodpanda')->whereDate('created_at', \Carbon\Carbon::today())->get()->sum("price");
      $totalnetdelpam = DB::table("tb_pnbwings")->where('location','pampanga')->where('order_type','delivery')->where('order_status','confirmed')->whereDate('created_at', \Carbon\Carbon::today())->get()->sum("price");
      $totalnetlalapam = DB::table("tb_pnbwings")->where('location','pampanga')->where('order_type','lalafood')->whereDate('created_at', \Carbon\Carbon::today())->get()->sum("price");
      $totespam = count($totalorderspam);
      $netpam = (($totalnetpandapam * 0.7) +  $totalnetdelpam + $totalnetlalapam);
    }
    else {
    $sumup = DB::table("tb_pnbwings")->where('location','makati')->whereDate('created_at', \Carbon\Carbon::today())->get()->sum("price");

    $totalorders = DB::table("tb_pnbwings")->where('location','makati')->whereDate('created_at', \Carbon\Carbon::today())->get();
    $totalnetpanda = DB::table("tb_pnbwings")->where('location','makati')->where('order_type','foodpanda')->whereDate('created_at', \Carbon\Carbon::today())->get()->sum("price");
    $totalnetdel = DB::table("tb_pnbwings")->where('location','makati')->where('order_type','delivery')->where('order_status','confirmed')->whereDate('created_at', \Carbon\Carbon::today())->get()->sum("price");
    $totalnetlala = DB::table("tb_pnbwings")->where('location','makati')->where('order_type','lalafood')->whereDate('created_at', \Carbon\Carbon::today())->get()->sum("price");
    $totes = count($totalorders);
    $net = (($totalnetpanda * 0.7) +  $totalnetdel + $totalnetlala);
  }

?>

<style>
.saletitle{
	font-size:10px;
	color:#fff;
}
.saleamt{
	font-size:20px;
	color:#fff;
	padding:20px;
	background:#f30f2a;
	border-radius:10px;
}
.bgfoodpanda{
	background:#ef6079;
	padding:10px;
	color:#fff;
	border-radius:10px;
	min-height:25vh;
}
.ter{
	font-size:10px;
	line-height:10px;
}
</style>

@section('page_header')
    <div class="container-fluid">
	<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-7 col-lg-6">
        <h1 class="page-title">
            <i class="voyager-fire"></i> Ang Paborito ni Boss
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
	</div>
      <div class="col-xs-12 col-sm-12 col-md-5 col-lg-6">
        <div class="col-xs-4 ">
		<div class="saletitle">Total Sales: </div>
    @if($role == '16')
		<div class="saleamt text-center">PHP {{$sumuppam}}  </div>
    @else
    <div class="saleamt text-center">PHP {{$sumup}}  </div>
    @endif
		</div>
         <div class="col-xs-4 ">
		 <div class="saletitle">Net Sales:  </div>
     @if($role == '16')
		 <div class="saleamt text-center">PHP {{$netpam}}   </div>
     @else
      <div class="saleamt text-center">PHP {{$net}}   </div>
     @endif
		 </div>
        <div class="col-xs-4 ">
		<div class="saletitle">Total Orders</div>
     @if($role == '16')
		<div class="saleamt text-center">{{$totespam}} </div>
    @else
    <div class="saleamt text-center">{{$totes}} </div>
    @endif
		</div>
      </div>
    </div>
    </div>


<?php
   $url=$_SERVER['REQUEST_URI'];
   header("Refresh: 120; URL=$url");
?>
@stop

@section('content')


          <div id="receipt">



  <?php $o = 0; ?>
 <div id="owl-demo" class="owl-carousel owl-theme">
          @foreach($orders as $order)

          <?php
            $code =  \DB::table('tb_pnbcodes')->where('order_id',$order->id)->first();
          ?>



  @if ($order->order_type == "foodpanda")

                  @if($order->status == "pending")
                    <div class="col-sm-6 col-md-2 col-xs-6 cardp" >
                    <div   >
                  @elseif($order->status == "released")
                    <div  class="item"  >
                    <div class="bgfoodpanda "  >
                  @else
                    <div class="col-sm-6 col-md-2 col-xs-6 cardo" >
                    <div   >
                  @endif
<div id="receipt-{{$order->id}}" class="hiddenreceipt">

              <img src="https://v2workflow.marvill.com/uploads/receipt/logo.jpg"/>
                <p style="text-align:center;">
                  @if ($order->order_type != "foodpanda")
                  <u>Order #{{$order->id}}</u>
                @else
                <u>Foodpanda# {{$order->order_number}}</u>
                @endif
                <hr style=" border: 1px solid;">
                <?php $testorder = json_decode($order->order);
                     $testqty = json_decode($order->qty);


                     ?>
                <table>
                   <thead>
                  <tr>
                    <th>
                      QTY
                    </th>
                  <th style="padding-left: 50px;">
                    ITEM
                  </th>
                </tr>
              </thead>
              <tbody>
                  @if(!empty($testorder[0]))


                  <?php  $i = 0; ?>

                  @foreach ($testqty as $orderqty)

                  @if($orderqty != 0)
                  <tr>
                    <td>{{$testqty[$i]}} x</td>  <td>{{$testorder[$i]}} </td>
                  </tr>
                  <?php  $i++;?>
                  @else
                    <?php  $i++;?>
                  @endif
                    @endforeach


                    @endif
                  </tbody>
                    </table>

                    @if(!empty($order->notes))
                    <h4>NOTES:</h4>
                    {{$order->notes}}
                    @endif
                    <hr style=" border: 1px solid;">
                      </p>
                    <h4> Total {{$order->price}} PHP</h4>

                    <p style="text-align:center;">@if(!empty($order->customer_name))
                      Hi, {{$order->customer_name}}

                    @endif Thank you for ordering!
                    Follow us on instagram
                     @pnbwings<br>
                    Stay Safe!<br>
                    @if($order->order_type == 'delivery')
                    Register at PNBWings<br>
                    <u><strong>{{strtoupper($code->code)}}<strong></u>
                    @endif
                   </p>



          </div>

                  <div    >
                  <h3>{{$order->order_type}}
                      @if ($order->order_type != "foodpanda")

                    @else
						<span class="pull-right" style="font-size:30px;">	#{{$order->order_number}}</span>
                    @endif
					</h3>
					 <small >Order#{{$order->id}}</small>
                  <!--  <button type="button" style="float:right;" class="btn btn-dark" >{{$order->status}}</text> -->


					<h2 style="margin-top:10px;"><strong>PHP {{$order->price}} </strong>

					</h2>


                    <?php $testorder = json_decode($order->order);
                         $testqty = json_decode($order->qty);
                         ?>

                      @if(!empty($testorder[0]))


                      <?php  $i = 0; ?>
                      <h4>
                        @foreach ($testqty as $orderqty)

                        @if($orderqty != 0)
                      {{$testqty[$i]}} x {{$testorder[$i]}} <br>
                      <?php  $i++;?>
                      @else
                        <?php  $i++;?>
                      @endif
                        @endforeach

                      </h4>
                        @endif
                        @if(!empty($order->customer_name))
                        <h4>Name:</h4>
                        {{$order->customer_name}}
                        @endif
                        @if(!empty($order->notes))
                        <h4>NOTES:</h4>
                        {{$order->notes}}
                        @endif

<div class="col-xs-12">

                    <!-- <a href="{{route('voyager.'.$dataType->slug.'.edit',['id' => $order->id ])}}" class="btn btn-primary btn-sm"><i class="voyager-edit"></i></a> -->
                    @if($order->status == "pending")
                    <a   href="{{route('pnbwings.released',['id' => $order->id ])}}" class="btn btn-dark  center-block"><i class="voyager-fire"></i>Cook</a>

                    @endif
                    @if($order->status == "cooking")
                   <a   href="{{route('pnbwings.preparing',['id' => $order->id ])}}" class="btn btn-dark  center-block"><i class="voyager-fire"></i>Prepare</a>

                    @endif
                    @if($order->status == "preparing")
                   <a   href="{{route('pnbwings.released',['id' => $order->id ])}}" class="btn btn-dark  center-block" id="basic"><i class="voyager-fire"></i>Release</a>

                    @endif
                    @if($order->status == "released")
                    <a  class="receipt-btn btn btn-dark center-block" data-id="{{$order->id}}"><i class="voyager-fire"></i>Print</a>
                    @endif


					</div>

                  </div>
                </div>
                </div>
              @endif
          @endforeach
</div>


<div class="scrolling-wrapper">
        @foreach($orders as $order)


                  <?php
                    $code =  \DB::table('tb_pnbcodes')->where('order_id',$order->id)->first();
                  ?>

          @if ($order->order_type == "delivery" && $order->order_status == "confirmed")

          <?php

              $userName = \DB::table('users')->where('id',$order->ordered_by)->first();
              $userDeets = \DB::table('tb_pnbdetails')->where('user_id',$order->ordered_by)->first();


          ?>

        <div id="receipt-{{$order->id}}" class="hiddenreceipt">

            <img src="https://v2workflow.marvill.com/uploads/receipt/logo.jpg"></img>
              <p style="text-align:center;">
                @if ($order->order_type != "foodpanda")
                <u>Order #{{$order->id}}</u>
              @else
              <u>Foodpanda# {{$order->order_number}}</u>
              @endif
              <hr style=" border: 1px solid;">
              <?php $testorder = json_decode($order->order);
                   $testqty = json_decode($order->qty);


                   ?>
              <table>
                 <thead>
                <tr>
                  <th>
                    QTY
                  </th>
                <th style="padding-left: 50px;">
                  ITEM
                </th>
              </tr>
            </thead>
            <tbody>
                @if(!empty($testorder[0]))


                <?php  $i = 0; ?>

                  @foreach ($testqty as $orderqty)

                  @if($orderqty != 0)
                <tr>
                  <td>{{$testqty[$i]}} x</td>  <td>{{$testorder[$i]}} </td>
                </tr>
                @else
                @endif
                <?php  $i++;?>

                @endforeach


                  @endif
                </tbody>
                  </table>
                  @if(!empty($order->notes))
                  <h4>NOTES:</h4>
                  {{$order->notes}}
                  @endif
                  <hr style=" border: 1px solid;">
                    </p>
                  <h4> Total {{$order->price}} PHP</h4>
                  <p style="text-align:center;">
                    @if(!empty($order->customer_name))
                      Hi, {{$order->customer_name}}

                    @endif Thank you for ordering!
                  Follow us on instagram
                   @pnbwings<br>
                  Stay Safe!
                  @if($order->order_type == 'delivery' && $order->ordered_by == 0 )
                  Register at PNBWings<br>
                  <u><strong>{{strtoupper($code->code)}}<strong></u>
                  @endif
                 </p>
                 @if($order->ordered_by != 0)
                 <div class="col-xs-6">
               Customer:<br/>
                 <span class="ter"></span>{{$userName->name}} <br/>
                 <span class="ter"></span>{{$userDeets->user_number}}<br/>
                 <span class="ter"></span>{{$userName->email}}<br/>
                 <span class="ter"></span> {{$userDeets->user_address}}
                 <span class="ter"></span>{{$userDeets->user_barangay}}
                 <span class="ter"></span>{{$userDeets->user_city}}
                 <hr/>
               </div>
                @endif




        </div>




            <div class="cards" >
                @if($order->status == "released")
                  <div class="col-sm-12 col-md-2 col-xs-6  cardp" >
                @elseif($order->status == "pending")
                  <div class="col-sm-12 col-md-2 col-xs-6 cardr" >
                @else
                  <div class="col-sm-12 col-md-2 col-xs-6 cardo" >
                @endif


                <div  >
                <h3 style="text-transform:uppercase">PNB {{$order->order_type}}</h3>
				<p>Order#{{$order->id}}</p>

				 <h2  ><strong>{{$order->price}} PHP</strong></h2>
                    @if ($order->order_type != "foodpanda")

                  @else
                  #{{$order->order_number}}
                  @endif
                <!--  <button type="button" style="float:right;" class="btn btn-dark" >{{$order->status}}</text></h3> -->





                  <?php $testorder = json_decode($order->order);
                       $testqty = json_decode($order->qty);


                       ?>

                    @if(!empty($testorder[0]))


                    <?php  $i = 0; ?>
                    <h4>
                      @foreach ($testqty as $orderqty)

                      @if($orderqty != 0)

                    {{$testqty[$i]}} x {{$testorder[$i]}} <br>
                    <?php  $i++;?>
                    @else
                      <?php  $i++;?>
                    @endif
                      @endforeach

                    </h4>
                      @endif

					<div class="col-xs-6">
						<hr/>
            @if(!empty($order->notes))
            <h4>NOTES:</h4>
            {{$order->notes}}
            @endif
            @if($order->ordered_by != 0)
					CONTACT DETAILS:<br/>
						<span class="ter">Name:</span><br/> {{$userName->name}} <br/>
						<span class="ter">Number:</span><br/>{{$userDeets->user_number}}<br/>
						<span class="ter">Email:</span><br/>{{$userName->email}}<br/>
							<hr/>
					</div>
					<div class="col-xs-6">
						<hr/>
					DELIVERY DETAILS:<br/>
						<span class="ter">Address:</span><br/> {{$userDeets->user_address}}<br/>
						<span class="ter">Barangay:</span><br/>{{$userDeets->user_barangay}}<br/>
						<span class="ter">City:</span><br/>{{$userDeets->user_city}}<br/>
						<hr/>
					</div>
					  <br/><br/>
            @endif

<div class="col-xs-12">
                  <!-- <a href="{{route('voyager.'.$dataType->slug.'.edit',['id' => $order->id ])}}" class="btn btn-primary btn-sm"><i class="voyager-edit"></i></a> -->
                  @if($order->status == "pending")
                  <a  href="{{route('pnbwings.released',['id' => $order->id ])}}" class="btn btn-dark center-block"><i class="voyager-fire"></i>Cook</a>

                  @endif
                  @if($order->status == "cooking")
                 <a   href="{{route('pnbwings.preparing',['id' => $order->id ])}}" class="btn btn-dark center-block"><i class="voyager-fire"></i>Prepare</a>

                  @endif
                  @if($order->status == "preparing")
                 <a   href="{{route('pnbwings.released',['id' => $order->id ])}}" class="btn btn-dark center-block" id="basic"><i class="voyager-fire"></i>Release</a>

                  @endif
                  @if($order->status == "released")
                  <a  class="receipt-btn btn btn-dark center-block" data-id="{{$order->id}}"><i class="voyager-fire"></i>Print</a>
                  @endif
 </div>
                  </table>

                </div>
              </div>
            </div>
            @endif
        @endforeach
</div>


<div class="scrolling-wrapper">
        @foreach($orders as $order)



          @if ($order->order_type == "lalafood")
        <div id="receipt-{{$order->id}}" class="hiddenreceipt">

            <img src="https://v2workflow.marvill.com/uploads/receipt/logo.jpg"></img>
              <p style="text-align:center;">
                @if ($order->order_type != "foodpanda")
                <u>Order #{{$order->id}}</u>
              @else
              <u>Foodpanda# {{$order->order_number}}</u>
              @endif
              <hr style=" border: 1px solid;">
              <?php $testorder = json_decode($order->order);
                   $testqty = json_decode($order->qty);


                   ?>
              <table>
                 <thead>
                <tr>
                  <th>
                    QTY
                  </th>
                <th style="padding-left: 50px;">
                  ITEM
                </th>
              </tr>
            </thead>
            <tbody>
                @if(!empty($testorder[0]))


                <?php  $i = 0; ?>

                @foreach ($testqty as $orderqty)

                @if($orderqty != 0)
                <tr>
                  <td>{{$testqty[$i]}} x</td>  <td>{{$testorder[$i]}} </td>
                </tr>
                <?php  $i++;?>
                @else
                  <?php  $i++;?>
                  @endif
                  @endforeach


                  @endif
                </tbody>
                  </table>
                  <hr style=" border: 1px solid;">
                    </p>
                  <h4> Total {{$order->price}} PHP</h4>
                  <p style="text-align:center;">Thank you for ordering!
                  Follow us on instagram
                   @pnbwings<br>
                  Stay Safe!
                 </p>



        </div>




            <div class="cards" >
                @if($order->status == "ss")
                  <div class="col-sm-12 col-md-2 col-xs-6 cardp" >
                @elseif($order->status == "tt")
                  <div class="col-sm-12 col-md-2 col-xs-6 cardr" >
                @else
                  <div class="col-sm-12 col-md-2 col-xs-6 cardo" >
                @endif


                <div  >
                <h3>{{$order->order_type}}

                    @if ($order->order_type != "foodpanda")

                  @else
                  #{{$order->order_number}}
                  @endif
                <!--  <button type="button" style="float:right;" class="btn btn-dark" >{{$order->status}}</text></h3> -->





                  <?php $testorder = json_decode($order->order);
                       $testqty = json_decode($order->qty);


                       ?>

                    @if(!empty($testorder[0]))


                    <?php  $i = 0; ?>
                    <h3>
                      @foreach ($testqty as $orderqty)

                      @if($orderqty != 0)
                    {{$testqty[$i]}} x {{$testorder[$i]}} <br>
                    <?php  $i++;?>
                    @else
                      <?php  $i++;?>
                    @endif
                      @endforeach

                    </h3>
                      @endif




                  <h4></h4>

                  <h2 style="margin-top:50px;"><strong>{{$order->price}} PHP</strong></h2>
                  <text style="float:right; margin-bottom:50px;">Order#{{$order->id}}</text>


                  <!-- <a href="{{route('voyager.'.$dataType->slug.'.edit',['id' => $order->id ])}}" class="btn btn-primary btn-sm"><i class="voyager-edit"></i></a> -->
                  @if($order->status == "pending")
                  <a style="position:absolute; left:0; bottom:0; margin-left:25px;" href="{{route('pnbwings.released',['id' => $order->id ])}}" class="btn btn-dark"><i class="voyager-fire"></i>Cook</a>

                  @endif
                  @if($order->status == "cooking")
                 <a style="position:absolute; left:0; bottom:0;  margin-left:25px;" href="{{route('pnbwings.preparing',['id' => $order->id ])}}" class="btn btn-dark"><i class="voyager-fire"></i>Prepare</a>

                  @endif
                  @if($order->status == "preparing")
                 <a style="position:absolute; left:0; bottom:0;  margin-left:25px;" href="{{route('pnbwings.released',['id' => $order->id ])}}" class="btn btn-dark" id="basic"><i class="voyager-fire"></i>Release</a>

                  @endif
                  @if($order->status == "released")
                  <a style="position:absolute; left:0; bottom:0;  margin-left:25px;" class="receipt-btn btn btn-dark" data-id="{{$order->id}}"><i class="voyager-fire"></i>Print</a>
                  @endif

                  <button class="btn btn-danger btn-sm delete"  data-id="{{$order->id}}" style="position:absolute; right:0; bottom:0;  margin-right:10px;" id="delete-{{$order->id}}"><i class="voyager-x"></i></button>
                  </table>
                </div>
              </div>
            </div>
            @endif
        @endforeach
</div>
    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} Sales Record ?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
    <style>
	/* The heart of the matter */
.testimonial-group > .row {
  overflow-x: auto;
  white-space: nowrap;
}
.testimonial-group > .row > .col-sm-12 .col-md-2 .col-xs-6 {
  display: inline-block;
  float: none;
}
        .red-badge{
            background-color: #ed5564;
            color:white;
            padding:4px;
        }
        .green-badge{
            background-color: #43ac6e;
            color:white;
            padding:4px;
        }


        .scrolling-wrapper {
        overflow-x: scroll;
        overflow-y: hidden;
        white-space: nowrap;

        &::-webkit-scrollbar {
            display: none;
          }

        }


        .cards {
          /* Add shadows to create the "card" effect */
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
          transition: 0.3s;
          display: inline-block;
          min-height: 25vh;
          width: 20%;



        }

        /* On mouse-over, add a deeper shadow */
        .cards:hover {
          box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }

        /* Add some padding inside the card container */
        .container {
          padding: 2px 16px;
        }

        .cardo {
          min-height: 25vh;
          width:100%;
          color:white;
          padding: 0 20px;
          margin: 20px 20px 20px 20px;
          background-color: #ff8c00;


        }

        .cardp {
          min-height: 25vh;
          width:100%;
          color:white;
          padding: 0 20px;
          margin: 20px 20px 20px 20px;
          background-color: #206BA4;


        }
        .cardr {
          min-height: 25vh;
          width:100%;
          color:white;
          padding: 0 20px;
          margin: 20px 20px 20px 20px;
          background-color: #EF6079FF;


        }
        @media screen and (max-width: 600px) {
          .column {
            width: 100%;
            display: block;
            margin-bottom: 20px;
          }
        }
        .hiddenreceipt{
          display:none;
        }
          @media print {
              .hiddenreceipt {
                  display: block;
              }

            }


    </style>
@stop

@section('javascript')


    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.6.7/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="{{asset('plugins/printthis/printThis.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" integrity="sha256-qvCL5q5O0hEpOm1CgOLQUuHzMusAZqDcAZL9ijqfOdI=" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha256-kksNxjDRxd/5+jGurZUJd1sdR2v+ClrCl3svESBaJqw=" crossorigin="anonymous" />

    <script>
       $(document).ready(function() {

  var owl = $("#owl-demo");

  owl.owlCarousel({
	  
      items : 5, //10 items above 1000px browser width
      itemsDesktop : [1000,5], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0 
	      margin:10,
		  responsiveClass:true,
    responsive:{
        0:{
            items:2,
            nav:true
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:5,
            nav:true,
            loop:false
        }
    }
  });

  // Custom Navigation Events
  $(".next").click(function(){
    owl.trigger('owl.next');
  })
  $(".prev").click(function(){
    owl.trigger('owl.prev');
  })
  $(".play").click(function(){
    owl.trigger('owl.play',1000); //owl.play event accept autoPlay speed as second parameter
  })
  $(".stop").click(function(){
    owl.trigger('owl.stop');
  })

});
    </script>
    <script>
          $('.receipt-btn').on('click', function (e) {
            $('#receipt-'+ $(this).data('id')).printThis({

            });
          });
    </script>

    <script>
        $('.cards').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{route('voyager.tb-pnbwings.destroy',['id' => '__id'])}}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

    </script>





@stop
