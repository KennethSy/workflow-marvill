@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .borderless table {
            border-top-style: none !important;
            border-left-style: none !important;
            border-right-style: none !important;
            border-bottom-style: none !important;
        }
        .image-area {
            position: relative;
            width: 120px;
            background: #333;
        }
        .image-area img{
            max-width: 100%;
            height: auto;
        }
        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
            text-shadow: 0 1px 2px rgba(0,0,0,0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }
        .remove-image:hover {
            background: #E54E4E;
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }
        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }

        .container {

            margin: 0 auto;
        }

        #cart {
            width: 100%;
        }

        #cart h1 {
            font-weight: 300;
        }

        #cart a {
            color: #ca1b49;
            text-decoration: none;

            -webkit-transition: color .2s linear;
            -moz-transition: color .2s linear;
            -ms-transition: color .2s linear;
            -o-transition: color .2s linear;
            transition: color .2s linear;
        }

        #cart a:hover {
            color: #000;
        }

        .product.removed {
            margin-left: 980px !important;
            opacity: 0;
        }

        .product {
            border: 1px solid #eee;
            margin: 20px 0;
            width: 100%;
            height: 250px;
            position: relative;

            -webkit-transition: margin .2s linear, opacity .2s linear;
            -moz-transition: margin .2s linear, opacity .2s linear;
            -ms-transition: margin .2s linear, opacity .2s linear;
            -o-transition: margin .2s linear, opacity .2s linear;
            transition: margin .2s linear, opacity .2s linear;
        }

        .product img {
            width: 100%;
            height: 100%;
        }

        .product header, .product .content {

            border: 1px solid #ccc;
            border-style: none none solid none;
            float: left;
        }

        .product header {
            background: #000;
            margin: 0 1% 20px 0;
            overflow: hidden;
            padding: 0;
            position: relative;
            width: 24%;
            height: 195px;
        }

        .product header:hover img {
            opacity: .7;
        }

        .product header:hover h3 {
            bottom: 73px;
        }

        .product header h3 {
            background: #ca1b49;
            color: #fff;
            font-size: 22px;
            font-weight: 300;
            line-height: 49px;
            margin: 0;
            padding: 0 30px;
            position: absolute;
            bottom: -50px;
            right: 0;
            left: 0;

            -webkit-transition: bottom .2s linear;
            -moz-transition: bottom .2s linear;
            -ms-transition: bottom .2s linear;
            -o-transition: bottom .2s linear;
            transition: bottom .2s linear;
        }

        .remove {
            cursor: pointer;
        }

        .product .content {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 140px;
            padding: 0 20px;
            width: 75%;
        }

        .product h1 {
            color: #ca1b49;
            font-size: 25px;
            font-weight: 300;
            margin: 17px 0 20px 0;
        }

        .product footer.content {
            height: 50px;
            margin: 6px 0 0 0;
            padding: 0;
        }

        .product footer .price {
            background: #fcfcfc;
            color: #000;
            float: right;
            font-size: 15px;
            font-weight: 300;
            line-height: 49px;
            margin: 0;
            padding: 0 30px;
        }

        .product footer .full-price {
            background: #ca1b49;
            color: #fff;
            float: right;
            font-size: 22px;
            font-weight: 300;
            line-height: 49px;
            margin: 0;
            padding: 0 30px;

            -webkit-transition: margin .15s linear;
            -moz-transition: margin .15s linear;
            -ms-transition: margin .15s linear;
            -o-transition: margin .15s linear;
            transition: margin .15s linear;
        }

        .qt, .qt-plus, .qt-minus {
            display: block;
            float: left;
        }

        .qt {
            font-size: 19px;
            line-height: 50px;
            width: 70px;
            text-align: center;
        }

        .qt-plus, .qt-minus {
            background: #ca1b49;
            border: none;
            font-size: 30px;
            font-weight: 300;
            height: 100%;
            padding: 0 20px;
            -webkit-transition: background .2s linear;
            -moz-transition: background .2s linear;
            -ms-transition: background .2s linear;
            -o-transition: background .2s linear;
            transition: background .2s linear;
        }

        .qt-plus:hover, .qt-minus:hover {
            background: #53b5aa;
            color: #fff;
            cursor: pointer;
        }

        .qt-plus {
            line-height: 50px;
        }

        .qt-minus {
            line-height: 47px;
        }

        #site-footer {
            margin: 30px 0 0 0;
        }

        #site-footer {
            padding: 40px;
        }

        #site-footer h1 {
            background: #fcfcfc;
            border: 1px solid #ccc;
            border-style: none none solid none;
            font-size: 24px;
            font-weight: 300;
            margin: 0 0 7px 0;
            padding: 14px 40px;
            text-align: center;
        }

        #site-footer h2 {
            font-size: 24px;
            font-weight: 300;
            margin: 10px 0 0 0;
        }

        #site-footer h3 {
            font-size: 19px;
            font-weight: 300;
            margin: 15px 0;
        }

        .left {
            float: left;
        }

        .right {
            float: right;
        }


        .btn:hover {
            color: #fff;
            background: #000;
        }

        .type {
            background: #fcfcfc;
            font-size: 13px;
            padding: 10px 16px;
            left: 100%;
        }

        .type, .color {
            border: 1px solid #ccc;
            border-style: none none solid none;
            position: absolute;
        }

        .color {
            width: 40px;
            height: 40px;
            right: -40px;
        }

        .red {
            background: #cb5a5e;
        }

        .yellow {
            background: #f1c40f;
        }

        .blue {
            background: #3598dc;
        }

        .minused {
            margin: 0 50px 0 0 !important;
        }

        .added {
            margin: 0 -50px 0 0 !important;
        }
        .ordersec{
            background:#1e1e1e;
            border-radius:10px;
            padding:10px
        }
        .ordersec1{
            background:#404040;
            border-radius:10px;
            padding:10px
        }
    </style>
@stop

@section('page_title', ($edit ? 'Edit' : 'Add').' Ang Paborito ni Boss Order')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-fire"></i>
        {{ ($edit ? 'Edit' : 'Add').' Ang Paborito ni Boss Order' }}
    </h1>
@stop
<?php
$categories = \DB::table('tb_pnbmenus')->select('item_category')->groupBy('item_category')->get();

?>
@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered"> 
                    <form role="form"
                          class="form-edit-add form-horizontal"
                          action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $order->id) : route('voyager.'.$dataType->slug.'.store') }}"  method="POST" enctype="multipart/form-data">
                        <div class="col-md-8">
                            {{csrf_field()}}
                            <div class="col-xs-12 ">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="hidden" name="orderedby"  value="inhouse" />
                                        <label for="Order Type" class=" control-label "> Order Type <span class="asterix"> * </span></label>
                                        <select name="order_type" id="order_type" rows="5" class="select2 form-control" required>
                                            <option value=""> -- Please Select -- </option>
                                            <option value="foodpanda" <?php if (strpos($order['order_type'], 'foodpanda') !== false) { ?> selected <?php } ?> >foodpanda</option>
                                            <option value="lalafood" <?php if (strpos($order['order_type'], 'lalafood') !== false) { ?> selected <?php } ?> >lalafood</option>
                                            <option value="delivery" <?php if (strpos($order['order_type'], 'delivery') !== false) { ?> selected <?php } ?> >delivery</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="order_label" for="order_number" class=" control-label text-left " style="display:none;"> Order Number(Foodpanda) <span class="asterix"> * </span></label>
                                        <input type="text" style="display:none;" name="order_number" rows="1" id="order_number" class="form-control input-md " value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12  ">
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label id="ordernotes" for="ordernotes" class=" control-label text-left "> ORDER NOTES</label>
                                        <input type="text"  name="order_note" rows="1" id="order_note" class="form-control input-md " value="" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <label id="order_labdisc" for="order_disc" class=" control-label text-left " style="display:none;"> FOODA PANDA DISCOUNT</label>
                                        <input type="text"  name="order_disc" rows="1" id="order_disc" class="form-control input-md " value="" style="display:none;"/>
                                        <button type="button" class="btn btn-danger" id="btn_disc" style="display:none;">Add Discount</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 ">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="customer_name" for="customer_name" class=" control-label text-left "> Customer Name <span class="asterix"> * </span></label>
                                        <input type="text"  name="customer_name" rows="1" id="customer_name" class="form-control input-md " value="{{ $order['customer_name'] }}" required />
                                    </div>
                                </div>
                                <div class="col-md-12" style="border-bottom: 1px solid #fff;  margin: 14px 0px;">
                                </div>
                            </div>
                            <span id="items-form-area">
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="col-xs-12 ">
                                            <div class="form-group">
                                                <label for="Order Type" class=" control-label "> Order <span class="asterix"> * </span></label>
                                                <select name="order[]" class="item-options select2 form-control" required>
                                                    <option value=""> -- Please Select -- </option>
                                                    @foreach ($categories as $category)
                                                        <?php
                                                        $items = \DB::table('tb_pnbmenus')->where('item_category', $category->item_category)->select('item_main')->groupBy('item_main')->get();
                                                        ?>
                                                        @foreach($items as $item)
                                                            <?php
                                                            $variants = \DB::table('tb_pnbmenus')->where('item_main', $item->item_main)->where('item_status','Active')->groupBy('item_variant')->orderBy('item_price','ASC')->get();

                                                            ?>
                                                            @foreach($variants as $variant)
                                                                <option data-id="{{$variant->id}}" data-price="{{$variant->item_price}}" value="{{$item->item_main}} {{$variant->item_variant}}" >{{$item->item_main}} - {{$variant->item_variant}} /  PHP {{$variant->item_price}} </option>
                                                            @endforeach
                                                        @endforeach
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="col-xs-12 ">
                                            <div class="form-group">
                                                <label id="qty" for="qty" class=" control-label text-left "  >QTY <span class="asterix"> * </span></label>
                                                <input type="number" name="qty[]" rows="1"  class="form-control input-md item-qty" min="1" value="1" required />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </span>
                            <div class="row">
                            <div class="col-xs-12 "> <a class="btn btn-primary add-new-item-btn" href=""> ADD NEW ITEM </a></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                        <h4> ORDER SUMMARY  </h4>
                        <table class="table table-hover " id="order-summary-table">
                            <thead>
                            <td width="50%">ITEM</td>
                            <td class="text-right">PRICE</td>
                            <td class="text-right">QTY</td>
                            <td class="text-right">TOTAL</td>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                        <table class="borderless table " id="order-summary-total-table">
                            <tbody>
                            <tr>
                                <td width="50%"></td>
                                <td class="text-center">SUBTOTAL</td>
                                <td class="text-center"> </td>
                                <td class="text-right">PHP  <span class="sub-total">0</span></td>
                            </tr>
                            <tr id="foodpanda_disc" style="display:none;">
                                <td width="50%"></td>
                                <td class="text-center">DISCOUNT</td>
                                <td class="text-center"> </td>
                                <td class="text-right">PHP  <span class="discount">0</span></td>
                            </tr>
                            <tr id="disc">
                                <td width="50%"></td>
                                <td class="text-center">DISCOUNT</td>
                                <td class="text-center"> </td>
                                <td class="text-right">15%</td>
                            </tr>
                            <tr>
                                <td width="50%"></td>
                                <td class="text-center">TOTAL</td>
                                <td class="text-center"> </td>
                                <td class="text-right">PHP <span class="grand-total">0</span></td>
                            </tr>
                            </tbody>
                        </table>
                        <input type="hidden" class="grand-total-price" name="price" value="0"/>
                        <button class="btn btn-primary btn-block" type="submit"> PLACE ORDER </button>


                    </div>
                    </form>
                    <div id="add-new-item-div" class="hidden">
                        <div class="row order-row">
                            <div class="col-md-10">
                                <div class="col-xs-12 ">
                                    <div class="form-group">
                                        <select name="order[]" class="item-options form-control" required>
                                            <option value=""> -- Please Select -- </option>
                                            @foreach ($categories as $category)
                                                <?php
                                                $items = \DB::table('tb_pnbmenus')->where('item_category', $category->item_category)->select('item_main')->groupBy('item_main')->get();
                                                ?>
                                                @foreach($items as $item)
                                                    <?php
                                                    $variants = \DB::table('tb_pnbmenus')->where('item_main', $item->item_main)->where('item_status','Active')->groupBy('item_variant')->orderBy('item_price','ASC')->get();

                                                    ?>
                                                    @foreach($variants as $variant)
                                                        <option data-id="{{$variant->id}}" data-price="{{$variant->item_price}}" value="{{$item->item_main}} {{$variant->item_variant}}" >{{$item->item_main}} - {{$variant->item_variant}} /  PHP {{$variant->item_price}} </option>
                                                    @endforeach
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <input type="number" name="qty[]" rows="1"  class="form-control input-md item-qty " min="1" value="1" required />
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-danger remove-item-btn" style="line-height: 1"><i class="fa fa-times"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
@stop

@section('javascript')
    <script>
        function updateOrderSummary() {
            let order_row = '';
            let sub_total = 0;
            let grand_total = 0;
            let discount = 0;

            if (document.getElementById('order_type').value == "foodpanda") {
                if (document.getElementById('order_disc').value != "") {
                    discount = document.getElementById('order_disc').value;
                }
            }

            $("#order-summary-table").find('tbody').html('');
            let item_qty = $('#items-form-area').find('.item-qty');
            let order_items = [];

            $('#items-form-area').find('.item-options').each(function(i, obj) {
                if($(this).val() != '') {

                    let line_price = $(this).find(":selected").data("price");
                    let line_id = $(this).find(":selected").data("id");
                    let line_qty = parseInt(item_qty.eq(i).val());
                    let line_total = line_price * line_qty;

                    if (order_items[line_id] != undefined) {
                        line_qty += order_items[line_id].qty;
                        line_total = line_price * line_qty;
                    }

                    order_items[line_id] = {
                        "id": line_id,
                        "price": line_price,
                        "name": $(this).val(),
                        "qty": line_qty,
                        "line_total": line_total
                    };

                    sub_total += line_total;
                    grand_total += line_total;
                }
            });

            if (document.getElementById('order_type').value == "delivery") {
                discount = Math.round(sub_total * 0.15);
            }

            grand_total -= discount;

            for (key in order_items) {
                order_row = '';
                order_row += '<tr>';
                order_row += '<td class="">'+order_items[key].name+'</td>';
                order_row += '<td class="text-right">'+order_items[key].price+'</td>';
                order_row += '<td class="text-right">'+order_items[key].qty+'</td>';
                order_row += '<td class="text-right">PHP '+order_items[key].line_total+'</td>';
                order_row += '</tr>';
                $("#order-summary-table").find('tbody').append(order_row);
            }
            console.log(order_items);
            $("#order-summary-total-table").find(".discount").html(discount);
            $("#order-summary-total-table").find(".sub-total").html(sub_total);
            $("#order-summary-total-table").find(".grand-total").html(grand_total);
            $(".grand-total-price").val(grand_total);
        }

        $('.add-new-item-btn').on('click',function(e) {
            e.preventDefault();
            $('#add-new-item-div').children().clone().appendTo('#items-form-area');
            $('#items-form-area').find('.item-options').select2();
        });

        $('#items-form-area').on('change','.item-options, .item-qty',function(e) {
            updateOrderSummary();
            console.log($(this).val());
        });

        $('#items-form-area').on('click','.remove-item-btn',function(e) {
            e.preventDefault();
            $(this).closest('.order-row').remove();
            updateOrderSummary();
        });

        $('#btn_disc').on('click', function() {
            if (document.getElementById('order_disc').value != "") {
                updateOrderSummary();
            }
        });

        $(document).on('change', '#order_type', function () {//do something}){
            if (document.getElementById('order_type').value == "foodpanda") {
                $("#order_number").show();
                $("#order_label").show();
                $('#disc').hide();
                $('#order_labdisc').show();
                $('#order_disc').show();
                $('#foodpanda_disc').show();
                $('#btn_disc').show();
                updateOrderSummary();
            }
            else if (document.getElementById('order_type').value == "delivery"){
                $('#disc').show();
                $("#order_number").hide();
                $("#order_label").hide();
                $('#order_labdisc').hide();
                $('#order_disc').hide();
                $('#foodpanda_disc').hide();
                $('#btn_disc').hide();
                updateOrderSummary();
            }
            else{
                $("#order_number").hide();
                $("#order_label").hide();
                $('#disc').hide();
                $('#order_labdisc').hide();
                $('#order_disc').hide();
                $('#foodpanda_disc').hide();
                $('#btn_disc').hide();
                updateOrderSummary();
            }
        });

        $("form").on('submit',function(e) {
            var value = $(this).find("input[type=submit]").val();
            $(this).find("button[type=submit]").html('Please Wait..').prop('disabled',true);
        });
    </script>
@stop
