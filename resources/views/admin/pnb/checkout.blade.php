@extends('frontend.layouts.pnb')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
html{
		 background: url(https://pnbwings.com/pabotime.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
	}
        body {
             background: url(https://pnbwings.com/pabotime.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
        }
#voyager-loader {
    background:#fff !important;
}
.image-area {
  position: relative;
  width: 120px;
  background: #333;
}
.image-area img{
  max-width: 100%;
  height: auto;
}
.remove-image {
  display: none;
  position: absolute;
  top: -10px;
  right: -10px;
  border-radius: 10em;
  padding: 2px 6px 3px;
  text-decoration: none;
  font: 700 21px/20px sans-serif;
  background: #555;
  border: 3px solid #fff;
  color: #FFF;
  box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
  text-shadow: 0 1px 2px rgba(0,0,0,0.5);
  -webkit-transition: background 0.5s;
  transition: background 0.5s;
}
.remove-image:hover {
  background: #E54E4E;
  padding: 3px 7px 5px;
  top: -11px;
  right: -11px;
}
.remove-image:active {
  background: #E54E4E;
  top: -10px;
  right: -11px;
}

.container {

  margin: 0 auto;
}

#cart {
  width: 100%;
}

#cart h1 {
  font-weight: 300;
}

#cart a {
  color: #fff;
  text-decoration: none;

  -webkit-transition: color .2s linear;
  -moz-transition: color .2s linear;
  -ms-transition: color .2s linear;
  -o-transition: color .2s linear;
  transition: color .2s linear;
}

#cart a:hover {
  color: #c3c3c3;
}

.product.removed {
  margin-left: 980px !important;
  opacity: 0;
}

.product {
  border: 1px solid #eee;
  margin: 20px 0;
  width: 100%;
  height: 250px;
  position: relative;

  -webkit-transition: margin .2s linear, opacity .2s linear;
  -moz-transition: margin .2s linear, opacity .2s linear;
  -ms-transition: margin .2s linear, opacity .2s linear;
  -o-transition: margin .2s linear, opacity .2s linear;
  transition: margin .2s linear, opacity .2s linear;
}

.product img {
  width: 100%;
  height: 100%;
}

.product header, .product .content {

  border: 1px solid #ccc;
  border-style: none none solid none;
  float: left;
}

.product header {
  background: #000;
  margin: 0 1% 20px 0;
  overflow: hidden;
  padding: 0;
  position: relative;
  width: 24%;
  height: 195px;
}

.product header:hover img {
  opacity: .7;
}

.product header:hover h3 {
  bottom: 73px;
}

.product header h3 {
  background: #ca1b49;
  color: #fff;
  font-size: 22px;
  font-weight: 300;
  line-height: 49px;
  margin: 0;
  padding: 0 30px;
  position: absolute;
  bottom: -50px;
  right: 0;
  left: 0;

  -webkit-transition: bottom .2s linear;
  -moz-transition: bottom .2s linear;
  -ms-transition: bottom .2s linear;
  -o-transition: bottom .2s linear;
  transition: bottom .2s linear;
}

.remove {
  cursor: pointer;
}

.product .content {
  box-sizing: border-box;
  -moz-box-sizing: border-box;
  height: 140px;
  padding: 0 20px;
  width: 75%;
}

.product h1 {
  color: #ca1b49;
  font-size: 25px;
  font-weight: 300;
  margin: 17px 0 20px 0;
}

.product footer.content {
  height: 50px;
  margin: 6px 0 0 0;
  padding: 0;
}

.product footer .price {
  background: #fcfcfc;
  color: #000;
  float: right;
  font-size: 15px;
  font-weight: 300;
  line-height: 49px;
  margin: 0;
  padding: 0 30px;
}

.product footer .full-price {
  background: #ca1b49;
  color: #fff;
  float: right;
  font-size: 22px;
  font-weight: 300;
  line-height: 49px;
  margin: 0;
  padding: 0 30px;

  -webkit-transition: margin .15s linear;
  -moz-transition: margin .15s linear;
  -ms-transition: margin .15s linear;
  -o-transition: margin .15s linear;
  transition: margin .15s linear;
}

.qt, .qt-plus, .qt-minus {
  display: block;
  float: left;
}

.qt {
  font-size: 19px;
  line-height: 30px;
  width: 70px;
  text-align: center;
}

.qt-plus, .qt-minus {
  background: #ca1b49;
border: none;
    font-size: 13px;
    font-weight: 300;
    padding: 0 9px;
  -webkit-transition: background .2s linear;
  -moz-transition: background .2s linear;
  -ms-transition: background .2s linear;
  -o-transition: background .2s linear;
  transition: background .2s linear;
  color: #fff;
}

.qt-plus:hover, .qt-minus:hover {
  background: #53b5aa;
  color: #fff;
  cursor: pointer;
}
.voyager .panel {
    margin-bottom: 22px;
    background-color: #fff !important;
    border: 1px solid transparent;
    border-radius: 4px;
    box-shadow: 0 2px 10px rgba(0,0,0,.05);
    color: #192734 !important;
}
.qt-plus {
    line-height: 30px;
}

.qt-minus {
   line-height: 30px;
}

#site-footer {
  margin: 30px 0 0 0;
}

#site-footer {
  padding: 40px;
}

#site-footer h1 {
  background: #fcfcfc;
  border: 1px solid #ccc;
  border-style: none none solid none;
  font-size: 24px;
  font-weight: 300;
  margin: 0 0 7px 0;
  padding: 14px 40px;
  text-align: center;
}

#site-footer h2 {
  font-size: 24px;
  font-weight: 300;
  margin: 10px 0 0 0;
}

#site-footer h3 {
  font-size: 19px;
  font-weight: 300;
  margin: 15px 0;
}

.left {
  float: left;
}

.right {
  float: right;
}

.btn {
  background: #ca1b49;
  border: 1px solid #999;
  border-style: none none solid none;
  cursor: pointer;
  display: block;
  color: #fff;
  font-size: 20px;
  font-weight: 300;
  padding: 16px 0;
  width: 290px;
  text-align: center;

  -webkit-transition: all .2s linear;
  -moz-transition: all .2s linear;
  -ms-transition: all .2s linear;
  -o-transition: all .2s linear;
  transition: all .2s linear;
}

.btn:hover {
  color: #fff;
  background: #000;
}

.type {
  background: #fcfcfc;
  font-size: 13px;
  padding: 10px 16px;
  left: 100%;
}

.type, .color {
  border: 1px solid #ccc;
  border-style: none none solid none;
  position: absolute;
}

.color {
  width: 40px;
  height: 40px;
  right: -40px;
}

.red {
  background: #cb5a5e;
}

.yellow {
  background: #f1c40f;
}

.blue {
  background: #3598dc;
}

.minused {
  margin: 0 50px 0 0 !important;
}

.added {
  margin: 0 -50px 0 0 !important;
}
.ordersec{
	background:#1e1e1e;
	border-radius:10px;
	padding:10px
}
.ordersec1{
	background:#404040;
	border-radius:10px;
	padding:10px
}
.app-container .content-container .side-body.padding-top {
    padding-top: 0px;
}
.app-container {
    background:transparent !important;
    padding-bottom: 30px;
}
 body {
            background-color: {{ Voyager::setting("admin.bg_color", "#FFFFFF" ) }};
        }
        body.login .login-sidebar {
            border-top:5px solid {{ config('voyager.primary_color','#22A7F0') }};
        }
        @media (max-width: 767px) {
            body.login .login-sidebar {
                border-top:0px !important;
                border-left:5px solid {{ config('voyager.primary_color','#22A7F0') }};
            }
        }
        body.login .form-group-default.focused{
            border-color:{{ config('voyager.primary_color','#22A7F0') }};
        }
        .login-button, .bar:before, .bar:after{
            background:{{ config('voyager.primary_color','#22A7F0') }};
        }
        .remember-me-text{
            padding:0 5px;
        }
		.titlestyle{
			font-weight:900;
			font-size:18pt;
		}
		.login-sec{
			margin-top:3%;
		}
		body.login {
    overflow: auto !important;
}
#voyager-loader {
    background: #ffffff !important;
}
body.login .faded-bg {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: transparent !important;
}
.pnbtop{
	height:75px;
	background: #000 !important;
}
.voyager .panel.panel-default .panel-heading {
    border-bottom: 1px solid #ca1b49 !important;
    background-color: #ca1b49 !important;
    color: #fff !important;
}
.panel-title {
    padding: 20px 30px;
    color: #f9f9f9 !important;
}
.ter{
	font-size:10px;
	line-height:10px;
}

@media(max-width:767px){
    .logo-pnb {
        max-height: 75px;
    }
}
</style>
@stop

@section('page_title', ('Add').' Ang Paborito ni Boss Order')
 
<section class="pnbtop">
	<div class="container-fluid">
	<div class="row">
	<div class="col-md-4"> </div>
	<div class="col-md-4 text-center"><img class="logo-pnb center-block" src="https://pnbwings.com/webflow/images/pnbwings-logo.png"/> </div>
	<div class="col-md-4"> </div>
	</div>
	</div>
</section> 




@section('content')
<div class="page-content edit-add container">
  <div class="row" style="margin-top: 50px;">
    <div class="col-md-6 col-xs-12">
      <div class="panel panel-bordered"  style="min-height:575px;">
        <!-- form start -->

        <!-- PUT Method if we are editing -->

        <!-- CSRF TOKEN -->
        {{ csrf_field() }}
        <div class="panel-body">
		   @if (count($errors) > 0)
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          <div class="cards" >


                <div class=" col-xs-12  cardp" >
				<h3>ORDER SUMMARY</h3>
<hr/>
                <?php $testorder = json_decode($order->order);
                     $testqty = json_decode($order->qty);

                     ?>

                  @if(!empty($testorder[0]))




                  <?php  $i = 0; ?>
				  <table class="table">
				<tbody>
                 <tr>
					<td>ITEM</td>
					<td>QTY</td>
					<td>PRICE</td>
					<td>TOTAL</td>
				  </tr>
                    @foreach ($testqty as $orderqty)

                    @if($orderqty != 0)
				<tr>
				<td>{{$testorder[$i]}};</td>
				<td>{{$testqty[$i]}}</td>
				<td>
                    <?php
                        if($item = \DB::table('tb_pnbmenus')->where(DB::raw("CONCAT(item_main,' ',item_variant)"),'=',$testorder[$i])->first()) {
                            echo $item->item_price;
                        } elseif ($item = \DB::table('tb_pnbmenus')->where("item_main",'=',$testorder[$i])->first()) {
                            echo $item->item_price;
                        } else {
                            \Log::error([
                                'error type' => 'No Matched Item on Database',
                                'item' => $item,
                                'name' => $testorder[$i],
                            ]);
                        }
                    ?>

      </td>
				<td>
                    <?php
                    echo $item->item_price * $testqty[$i];
                    ?>
        </td>
				</tr>





                  <?php  $i++;?>
                  @else
                    <?php  $i++;?>
                  @endif


                    @endforeach

</tbody>
</table>

                    @endif

            </div>
          </div>

        </div>




		  </div>
      </div>



<div class="col-md-6 col-xs-12">
      <div class="panel panel-bordered">
      <div class="panel-body">

          <div class="cards" >


			 <div class="col-xs-12  cardp" >
			 <h3>DELIVERY DETAILS</h3>
			<hr/>
    <?php
                  $user = DB::table('users')->where('id',$userid)->first();
                  $details = DB::table('tb_pnbdetails')->where('user_id',$userid)->first();
                ?>
                <span class="ter">Name:</span><br/> {{$user->name}} <br/>
                 <span class="ter">Email:</span><br/> {{$user->email}}<br/>
                 <span class="ter">Number:</span><br/> {{$details->user_number}}<br/>
                 <span class="ter">Address:</span><br/> {{$details->user_address}}<br/>
                <span class="ter"> Barangay:</span><br/> {{$details->user_barangay}}<br/>
                <span class="ter"> City:</span><br/> {{$details->user_city}}<br/>

          </div>
		  <div class="col-xs-12 pull-right cardp" >
			<hr/>
				<h3 id="disc" >Discount: 15% </h3>
              <h3 class="total">Total: <span>{{$order->price}}</span></h3>
               <a   href="{{route('pnbwings.checkedout',['id' => $order->id ])}}" class="btn btn-xs btn-dark"><i class="voyager-fire"></i>Checkout</a>

          </div>
          </div>

        </div>




		  </div>
      </div> 
    </div><!-- panel-body -->





        @if($errors->has('attachment'))
        @foreach ($errors->get('attachment') as $error)
        <span class="help-block">{{ $error }}</span>
        @endforeach
        @endif

</div>
</div>
</div>




<div class="modal fade modal-danger" id="confirm_delete_modal">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"
        aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
      </div>

      <div class="modal-body">
        <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
        <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
      </div>
    </div>
<!-- End Delete File Modal -->
@stop

@section('css')
<link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
@stop

@section('javascript')
<script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script>
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#attachment_file_preview').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
    $('.image-area').toggleClass('hidden', false);
    $('#attachment_file_name').val('');
  }
}

$("#attachment").change(function(){
  readURL(this);
});


var params = {};
var $file;

function deleteHandler(tag, isMulti) {
  return function() {
    $file = $(this).siblings(tag);

    params = {

      filename:  $file.data('file-name'),
      id:     $file.data('id'),
      field:  $file.parent().data('field-name'),
      multi: isMulti,
      _token: '{{ csrf_token() }}'
    }

    $('.confirm_delete_name').text(params.filename);
    $('#confirm_delete_modal').modal('show');
  };
}

$('document').ready(function () {
  $('.toggleswitch').bootstrapToggle();
  var i = 1;
  //Init datepicker for date fields if data-datepicker attribute defined
  //or if browser does not handle date inputs



  $('.side-body input[data-slug-origin]').each(function(i, el) {
    $(el).slugify();
  });





  var check = false;

  function changeVal(el) {
    var qt = parseFloat(el.parent().children(".qt").html());
    var price = parseFloat(el.parent().children(".price").html());
    var eq = Math.round(price * qt * 100) / 100;

    el.parent().children(".full-price").html( " " + eq + " " );

    changeTotal();
  }

  function changeTotal() {

    var price = 0;

    $(".full-price").each(function(index){
      price += parseFloat($(".full-price").eq(index).html());
    });

    price = Math.round(price * 100) / 100;
    var tax = Math.round(price * 0.05 * 100) / 100
    var shipping = parseFloat($(".shipping span").html());
    var fullPrice = Math.round((price + tax + shipping) *100) / 100;


    if(price == 0) {
      fullPrice = 0;
    }

    $(".subtotal span").html(price);
    $(".tax span").html(tax);



    if(document.getElementById('order_type').value == "delivery"){
      deliveryprice = Math.round(price * 0.85);
      document.getElementById("price").value=""+deliveryprice;
        $(".total span").html(deliveryprice);

    }
    else if(document.getElementById('order_type').value == "foodpanda"){

      document.getElementById("price").value=""+price;
      $(".total span").html(price);

      $('#btn_disc').on('click', function(){
        if(document.getElementById('order_disc').value != ""){
          foodisc = document.getElementById('order_disc').value;
          discprice = Math.round(price - foodisc );
          $(".total span").html(discprice);
          document.getElementById("price").value=""+discprice;
        }

      });
    }
    else{
      document.getElementById("price").value=""+price;
      $(".total span").html(price);
    }

  }

  $(document).ready(function(){

    $(".remove").click(function(){
      var el = $(this);
      el.parent().parent().addClass("removed");
      window.setTimeout(
        function(){
          el.parent().parent().slideUp('fast', function() {
            el.parent().parent().remove();
            if($(".product").length == 0) {

            }
            changeTotal();
          });
        }, 200);
      });

      $(".addcart").click(function(){
        $(this).parent().children(".qt-minus").show();
        $(this).parent().children(".qt-plus").show();
        $(this).parent().children(".qt").show();
        $(this).parent().children(".qt-item").show();




      });

      $(".qt-plus").click(function(){

        var new_value = parseInt($(this).parent().children(".qt").html()) + 1;

        $(this).parent().children(".qt").html(new_value);

        $(this).parent().children("input[name='qty[]']").val(new_value);


        $(this).parent().children(".full-price").addClass("added");

       //get value of black box




        var el = $(this);
        window.setTimeout(function(){el.parent().children(".full-price").removeClass("added"); changeVal(el);}, 150);
      });

      $(".qt-minus").click(function(){

        child = $(this).parent().children(".qt");

        if(parseInt(child.html()) > 0) {
            var old_value = parseInt($(this).parent().children(".qt").html()) - 1;

            $(this).parent().children(".qt").html(old_value);

            $(this).parent().children("input[name='qty[]']").val(old_value);
        }

        if(parseInt(child.html()) == 0) {
          $(this).parent().children(".qt-minus").hide();
          $(this).parent().children(".qt-plus").hide();
          $(this).parent().children(".qt").hide();
          $(this).parent().children(".qt-item").hide();
          $(this).parent().children(".addcart").show();

        }


        $(this).parent().children(".full-price").addClass("minused");

        var el = $(this);
        window.setTimeout(function(){el.parent().children(".full-price").removeClass("minused"); changeVal(el);}, 150);
      });

      window.setTimeout(function(){$(".is-open").removeClass("is-open")}, 1200);

      $(".btn").click(function(){
        check = true;
        $(".remove").click();
      });
    });

    $(document).on('change', '#order_type', function () {//do something}){
      if (document.getElementById('order_type').value == "foodpanda") {
        $("#order_number").show();
        $("#order_label").show();
        $('#disc').hide();
        $('#order_labdisc').show();
        $('#order_disc').show();
        $('#btn_disc').show();
      }
      else if (document.getElementById('order_type').value == "delivery"){
        $('#disc').show();
        $("#order_number").hide();
        $("#order_label").hide();
        $('#order_labdisc').hide();
        $('#order_disc').hide();
        $('#btn_disc').hide();

      }
      else{
            $("#order_number").hide();
            $("#order_label").hide();
            $('#disc').hide();
            $('#order_labdisc').hide();
            $('#order_disc').hide();
            $('#btn_disc').hide();
      }
    });












    </script>
    @stop
