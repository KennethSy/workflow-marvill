<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <title>Marvill Web Development</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap_trimmed.min.css')}}" />
    <style type="text/css">
        html {
            height:100%;
        }
        body {
            font-family: 'DejaVu Sans', sans-serif;
            margin-left: 0.3in;
            margin-right: 0.3in;
        }
        h1 {
            font-size: 2em;
            font-weight: bold;
        }
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }
        .row {
            margin-bottom: 5px;
        }
        .page {
            page-break-after:always;
        }
        #invoice-items br{
            display: block;
            margin-top: -6px;
            content: "";
        }
        {
          font-size: 12px;
          font-family: 'Times New Roman';
        }

        td,
        th,
        tr,
        table {
            border-top: 1px solid black;
            border-collapse: collapse;
        }

        td.description,
        th.description {
            width: 75px;
            max-width: 75px;
        }

        td.quantity,
        th.quantity {
            width: 40px;
            max-width: 40px;
            word-break: break-all;
        }

        td.price,
        th.price {
            width: 40px;
            max-width: 40px;
            word-break: break-all;
        }

        .centered {
            text-align: center;
            align-content: center;
        }

        .ticket {
            width: 155px;
            max-width: 155px;
        }

        img {
            max-width: inherit;
            width: inherit;
        }

        @media print {
            .hidden-print,
            .hidden-print * {
                display: none !important;
            }
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <?php
        $provider = '';
        $total = 0;
        $is_subtotal = false;
    ?>

        <div class="page">
                  <div class="ticket">

                  <p class="centered">RECEIPT EXAMPLE
                      <br>Address line 1
                      <br>Address line 2</p>
                  <table>
                      <thead>
                          <tr>
                              <th class="quantity">Q.</th>
                              <th class="description">Description</th>
                              <th class="price">$$</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                              <td class="quantity">1.00</td>
                              <td class="description">ARDUINO UNO R3</td>
                              <td class="price">$25.00</td>
                          </tr>
                          <tr>
                              <td class="quantity">2.00</td>
                              <td class="description">JAVASCRIPT BOOK</td>
                              <td class="price">$10.00</td>
                          </tr>
                          <tr>
                              <td class="quantity">1.00</td>
                              <td class="description">STICKER PACK</td>
                              <td class="price">$10.00</td>
                          </tr>
                          <tr>
                              <td class="quantity"></td>
                              <td class="description">TOTAL</td>
                              <td class="price">$55.00</td>
                          </tr>
                      </tbody>
                  </table>
                  <p class="centered">Thanks for your purchase!
                      <br>parzibyte.me/blog</p>
              </div>
              <button id="btnPrint" class="hidden-print">Print</button>
              <script src="script.js"></script>


        </div>


</body>
</html>
