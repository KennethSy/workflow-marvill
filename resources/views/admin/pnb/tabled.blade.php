@extends('voyager::master')

@section('page_title', 'Ang Paborito ni Boss')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-fire"></i> Ang Paborito ni Boss
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan

        </table>

    </div>


<?php
   $url=$_SERVER['REQUEST_URI'];
   header("Refresh: 60; URL=$url");
?>
@stop

@section('content')




            <div class="page-content browse container-fluid">
            <div class="row">
             <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" class="select_all">
                                        </th>
                                        <th>Order ID</th>
                                        <th>Order Type</th>
                                        <th>Order Number(Foodpanda)</th>
                                        <th>Order</th>

                                        <th>Status</th>
                                        <th>Price</th>

                                    </tr>
                                </thead>
                                <tbody>

                                @foreach($orders as $order)
                                    <tr >
                                        <td><input type="checkbox" name="row_id" id="checkbox_{{ $order->id }}" value="{{ $order->id }}"></td>
                                        <td>{{$order->id}}</td>
                                        <td>{{$order->order_type}}</td>
                                        <td>{{$order->order_number}}</td>
                                        <?php $testorder = json_decode($order->order);
                                             $testqty = json_decode($order->qty);


                                             ?>

                                          @if(!empty($testorder[0]))


                                          <?php  $i = 0; ?>
                                          <td>
                                            @foreach (json_decode($order->order) as $orders)
                                            {{$testorder[$i]}} x {{$testqty[$i]}} <br>
                                          <?php  $i++;?>

                                            @endforeach

                                            </td>
                                            @endif

                                        <td>{{$order->status}}</td>
                                        <td>{{$order->price}}</td>
                                        <td>
                                            <a href="{{route('voyager.'.$dataType->slug.'.edit',['id' => $order->id ])}}" class="btn btn-primary btn-sm"><i class="voyager-edit"></i></a>
                                            <button class="btn btn-danger btn-sm delete" data-id="{{$order->id}}" id="delete-{{$order->id}}"><i class="voyager-x"></i></button>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>



    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} Sales Record ?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
    <style>
        .red-badge{
            background-color: #ed5564;
            color:white;
            padding:4px;
        }
        .green-badge{
            background-color: #43ac6e;
            color:white;
            padding:4px;
        }
        .cards {
          /* Add shadows to create the "card" effect */
          box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
          transition: 0.3s;


        }

        /* On mouse-over, add a deeper shadow */
        .cards:hover {
          box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }

        /* Add some padding inside the card container */
        .container {
          padding: 2px 16px;
        }

        .cardo {
          min-height: 25vh;
          color:white;
          padding: 0 10px;
          margin: 10px 10px 10px 10px;
          background-color: #8cc43d;


        }

        .cardp {
          min-height: 25vh;
          color:white;
          padding: 0 10px;
          margin: 10px 10px 10px 10px;
          background-color: #206BA4;


        }
        .cardr {
          min-height: 25vh;
          color:white;
          padding: 0 10px;
          margin: 10px 10px 10px 10px;
          background-color: #EF6079FF;


        }
        @media screen and (max-width: 600px) {
          .column {
            width: 100%;
            display: block;
            margin-bottom: 20px;
          }

    </style>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $('#dataTable').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{route('voyager.tb-pnbwings.destroy',['id' => '__id'])}}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

    </script>
@stop
