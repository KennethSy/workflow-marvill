@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .image-area {
            position: relative;
            width: 120px;
            background: #333;
        }
        .image-area img{
            max-width: 100%;
            height: auto;
        }
        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
            text-shadow: 0 1px 2px rgba(0,0,0,0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }
        .remove-image:hover {
            background: #E54E4E;
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }
        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }
    </style>
@stop

@section('page_title', ($edit ? 'Edit' : 'Add').' Bookkeeping Record')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-receipt"></i>
        {{ ($edit ? 'Edit' : 'Add').' Bookkeeping Record' }}
    </h1>
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add form-horizontal"
                          action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $record->id) : route('voyager.'.$dataType->slug.'.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif
                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}
                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="date">Date *</label>
                                <div class="input-group col-sm-10" style="padding-left: 15px;padding-right: 15px;">
                                    <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                    <input id="date" class="form-control" type="text" name="date" value="{{$record->date}}" required/>
                                </div>
                                @if($errors->has('date'))
                                    @foreach ($errors->get('date') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('company') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="company">Company *</label>
                                <div class="col-sm-10">
                                    <select id="company" class="form-control" name="company" required>
                                        <option value="">Please select one</option>
                                        @foreach($companies as $company)
                                            <option @if($record->company && $record->company == $company->id) selected @endif value="{{$company->id}}">{{$company->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('company'))
                                    @foreach ($errors->get('company') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('income_or_expense') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="income_or_expense">Income or Expense *</label>
                                <div class="col-sm-10">
                                    <select id="income_or_expense" class="form-control" name="income_or_expense" required>
                                        <option value="">Please select one</option>
                                        <option @if($record->income && $record->income > 0) selected @endif value="Income">Income</option>
                                        <option @if($record->expense && $record->expense > 0) selected @endif value="Expense">Expense</option>
                                    </select>
                                </div>
                                @if($errors->has('income_or_expense'))
                                    @foreach ($errors->get('income_or_expense') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="@if(!($record->income && $record->income > 0)) hidden @endif form-group {{ $errors->has('income') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="income">Income *</label>
                                <div class="col-sm-10">
                                    <input id="income" class="form-control" type="number" name="income" value="{{number_format($record->income,2,".","")}}"/>
                                </div>
                                @if($errors->has('income'))
                                    @foreach ($errors->get('income') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="@if(!($record->expense && $record->expense > 0)) hidden @endif form-group {{ $errors->has('expense') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="expense">Expense *</label>
                                <div class="col-sm-10">
                                    <input id="expense" class="form-control" type="number" name="expense" value="{{number_format($record->expense,2,".","")}}"/>
                                </div>
                                @if($errors->has('expense'))
                                    @foreach ($errors->get('expense') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('tin_no') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="tin_no">Tin Number</label>
                                <div class="col-sm-10">
                                    <input id="tin_no" class="form-control" type="text" name="tin_no" value="{{$record->tin_no}}"/>
                                </div>
                                @if($errors->has('tin_no'))
                                    @foreach ($errors->get('tin_no') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="description">Description *</label>
                                <div class="col-sm-10">
                                    <textarea id="description" class="form-control" name="description" rows="5" required>{!! $record->description !!}</textarea>
                                </div>
                                @if($errors->has('description'))
                                    @foreach ($errors->get('description') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('cheque_no') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="cheque_no">Cheque Number</label>
                                <div class="col-sm-10">
                                    <input id="cheque_no" class="form-control" type="text" name="cheque_no" value="{{$record->cheque_no}}"/>
                                </div>
                                @if($errors->has('cheque_no'))
                                    @foreach ($errors->get('cheque_no') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('attachment') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="attachment">Attachment</label>
                                <div class="col-sm-10">
                                    <input id="attachment" type="file" name="attachment" value="" accept=".jpg,.jpeg"/>
                                    <input id="attachment_file_name" name="attachment_file_name" class="hidden" value="{{$record->attachment}}" />
                                    <div class="image-area @if($record->attachment == null) hidden @endif">
                                        <img id="attachment_file_preview" class="" style="width: 120px; height:120px;" src="@if($record->attachment != null) {{asset('uploads/bookkeeping/'.$record->attachment)}} @endif" >
                                        <a class="remove-image" href="#" style="display: inline;">&#215;</a>
                                    </div>
                                </div>
                                @if($errors->has('attachment'))
                                    @foreach ($errors->get('attachment') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#attachment_file_preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
                $('.image-area').toggleClass('hidden', false);
                $('#attachment_file_name').val('');
            }
        }

        $("#attachment").change(function(){
            readURL(this);
        });

        $(".remove-image").on('click', function(e) {
            e.preventDefault();
            $('#attachment_file_name').val('');
            $('#attachment_file_preview').attr('src', '');
            $('.image-area').toggleClass('hidden', true);
        });

        $('#date').datepicker({
            format: "dd-M-yyyy",
            autoclose: true,
        });
        $('#income_or_expense').on('change', function() {
           if($(this).val() == 'Income') {
               $('#income').closest('.form-group').toggleClass('hidden',false);
               $('#expense').closest('.form-group').toggleClass('hidden',true);
           } else if($(this).val() == 'Expense') {
               $('#expense').closest('.form-group').toggleClass('hidden',false);
               $('#income').closest('.form-group').toggleClass('hidden',true);
           } else {
               $('#expense').closest('.form-group').toggleClass('hidden',true);
               $('#income').closest('.form-group').toggleClass('hidden',true);
           }
        });

        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
            return function() {
                $file = $(this).siblings(tag);

                params = {
                    slug:   '{{ $dataType->slug }}',
                    filename:  $file.data('file-name'),
                    id:     $file.data('id'),
                    field:  $file.parent().data('field-name'),
                    multi: isMulti,
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: [ 'YYYY-MM-DD' ]
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
