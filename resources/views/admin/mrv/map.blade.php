@extends('voyager::master')

@section('page_title', 'MRV Tracking')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="bx bx-compass"></i> MRV Tracking
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
						<div class="row">
							<div class="col-md-4">
								<div style="height:100vh;overflow: scroll;">
								<?php 
										$locations = \App\Libraries\gps::positions(\Session::get('sessionId'));
										$loc_response = json_decode($locations->response);
										//dd(\Session::get('sessionId'));
										//dd($locations);
										foreach ($data as $device)
										{
											echo '<hr>';
											$key = array_search($device->id, array_column($loc_response, 'deviceId'));
											$loc = $loc_response[$key];
											echo 'Device Name: '.$device->name.'<br>';
											echo 'IMEI: '.$device->uniqueId.'<br>';
											echo 'Phone: '.$device->phone.'<br>';
											echo 'Model: '.$device->model.'<br>';
											echo 'Status: '.$device->status.'<br>';
											echo 'Last Update: '.$device->lastUpdate.'<br>';
											if(false !== $key)
											{
												echo 'Latitude: '.$loc->latitude.'<br>';
												echo 'Longitude: '.$loc->longitude.'<br>';
												echo 'Address: '.$loc->address.'<br>';
												echo '<a href="https://www.google.com/maps/embed/v1/search?q='.$loc->latitude.','.$loc->longitude.'&key=AIzaSyDURixz2ZD3ep50vOOYp7sJomrfoWpacEM" target="iframe_a"> View in Google Maps </a><br>';
											}
											else
											{
												echo 'Latitude: N/A <br>';
												echo 'Longitude: N/A <br>';
												echo 'Address: N/A <br>';
												
											}
											echo '<hr>';
										}
								?>
								</div>
							</div>
							<div class="col-md-8">
								<!--<div id="map"></div>-->
								<iframe frameborder="0" height="700px" width="100%" name="iframe_a"></iframe>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 8
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDURixz2ZD3ep50vOOYp7sJomrfoWpacEM&callback=initMap"
    async defer></script>

@stop

@section('css')

@stop

@section('javascript')

@stop
