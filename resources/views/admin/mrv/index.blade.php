@extends('voyager::master')

@section('page_title', 'MRV Tracking')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="bx bx-compass"></i> MRV Tracking
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
						<div class="row">
							<div class="col-md-12">
							<div class="panel panel-bordered">

							<form role="form" class="form-edit-add" action="/admin/mrv-tracking/login" method="POST" enctype="multipart/form-data">
							@csrf
							<div class="form-group  col-md-12 ">
							<label class="control-label" for="name">Username</label>
							<input type="text" class="form-control" name="username" placeholder="..." value="">
							</div>

							<div class="form-group  col-md-12 ">
							<label class="control-label" for="name">Password</label>
							<input type="password" class="form-control" name="password" placeholder="..." value="">
							</div>

							</div>
							<div class="panel-footer">
							<button type="submit" class="btn btn-primary save">Login</button>
							</div>

							</div>
							</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('javascript')

@stop
