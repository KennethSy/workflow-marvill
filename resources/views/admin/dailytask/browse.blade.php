@extends('voyager::master')

@section('page_title', 'Tasks')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-list"></i> <a style="color:inherit;" href="{{ route('voyager.'.$dataType->slug.'.index') }}">Tasks</a>
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
    </div>
@stop

@section('content')
<style>
@media (min-width: 768px){
.col-xs-6 {
    width: 100% !important;
}
}
.tts{
font-size:10px;
color:#ddd;
line-height:9px
}
.fc-unthemed .fc-list-item:hover td {
    background-color: #15202b !important;
}
.fc-unthemed .fc-divider, .fc-unthemed .fc-list-heading td, .fc-unthemed .fc-popover .fc-header {
    background: #192734 !important;
}
.voyager .panel.panel-default .panel-heading {
    border-bottom: 1px solid #eaeaea;
    background-color: #f0f0f000;
    color: white !important;
}
.fc-unthemed td.fc-today {
    background: #192734 !important;
}
</style>
    <div class="page-content browse container-fluid">
        <div class="row">
		 @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
            <div id="myDIV"  class="col-md-12"  >
                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form id="create-task" role="form"
                          class="form-edit-add form-horizontal"
                          action="{{ route('voyager.'.$dataType->slug.'.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}
                        <div class="panel-body" style="padding-top:20px">
                            <div class="row" id="additional-task-container">
                                <div class="additional-task">
                                    <div class="col-xs-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="company">Company *</label>
                                            <div class="col-sm-10">
                                                <select class="company form-control" name="company[0]" required>
                                                    <option value="">Please select one</option>
                                                    @foreach($companies as $company)
                                                        <option  value="{{$company->id}}">{{$company->company_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="project">Project *</label>
                                            <div class="col-sm-10">
                                                <select class="project form-control" name="project[0]" required>
                                                    <option value="">Please select a company first</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-sm-12">
									 <div class="form-group">
                                            <label class="col-sm-2 control-label" for="value">PHP Value *</label>
                                            <div class="col-sm-10">
                                                <input class="value form-control" type="number" name="value[0]" required value=""/>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label class="col-sm-2 control-label" for="assigned_to">Assigned To *</label>
                                            <div class="col-sm-10">
                                                <select class="assigned_to form-control" name="assigned_to[0][]" multiple required>
                                                    <option value="">Please select one</option>
                                                    @foreach($all_users as $user)
                                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div class="col-xs-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="description">Description *</label>
                                            <div class="col-sm-10">
                                                <textarea class="description form-control" rows="5" name="description[0]"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-6 col-sm-12">
                                      
										 <div class="form-group ">
                                            <label class="col-sm-2 control-label" for="date">Date *</label>
                                            <div class=" col-sm-10">
                                                <div class="input-group"  >
                                                    <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                                    <input class="date form-control" type="text" name="date[0]" value="{{Carbon\Carbon::now()->format('m/d/Y')}}" required/>
                                                </div>
                                            </div>
                                        </div>
										
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="attachment">Attach Image/s</label>
                                            <div class="col-sm-10">
                                                <input class="attachment" type="file" name="attachment[0][]" value="" accept=".jpg,.jpeg,.png" multiple/>
                                                <div class="gallery">
                                                </div>
                                                <div id="attachment_file_preview">
                                                    <img class="" style="width: 250px; height:auto;" src="" >
                                                </div>
                                            </div>
                                        </div>
										
                                        <div class="form-group ">
                                            <label class="col-sm-2 control-label" for="attachment">Attach Document/s</label>
                                            <div class="col-sm-10">
                                                <input class="attachment_docs" type="file" name="attachment_docs[0][]" value="" accept=".xls,.xlsx,.doc,.docx,.pdf,.ppt,.pptx,.txt" multiple/>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary save">Save</button>
                        </div><!-- panel-body -->
                    </form>

                </div>
            </div>
            @endif
               <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        <div class="sbox">
                            <div class="sbox-content">
                                <form id="filter-form" method="GET" class="form-inline">
                                    <div class="form-group">
                                        <label>Company: </label>
                                        <select style="width:100%; max-width: 250px;" id="company" class="form-control select2" name="company">
                                            <option value="">All</option>
                                            @foreach($companies as $company)
                                                <option @if(isset($filter_params['company']) && $company->id == $filter_params['company']) selected @endif value="{{$company->id}}">{{$company->company_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Project: </label>
                                        <select style="width:100%; max-width: 250px;" id="project" class="form-control select2" name="project">
                                            <option value="">All</option>
                                            @if(isset($filter_params['company']))
                                                @foreach($projects->where('company_id','=',$filter_params['company'])->all() as $project)
                                                    <option @if(isset($filter_params['project']) && $filter_params['project'] == $project->id) selected @endif value="{{$project->id}}">{{$project->title}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Description: </label>
                                        <input style="width:100%; max-width: 250px;" class="form-control" name="description" value="{{isset($filter_params['description'])?$filter_params['description']:""}}" />
                                    </div>
                                    @if((Auth::user()->role_id==1)||(Auth::user()->role_id==3))
                                        <div class="form-group">
                                            <label>Assigned To: </label>
                                            <select class="form-control select2" name="assigned-to">
                                                <option value="">All</option>
                                                @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                                                    <option @if(isset($filter_params['assigned-to']) && Auth::user()->id == $filter_params['assigned-to']) selected @endif value="{{Auth::user()->id}}">{{Auth::user()->name}}</option>
                                                @endif
                                                @foreach($all_users as $user)
                                                    <option @if(isset($filter_params['assigned-to']) && $user->id == $filter_params['assigned-to']) selected @endif value="{{$user->id}}">{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label>Status: </label>
                                        <select class="form-control select2" name="status">
                                            <option value="">All</option>
                                            @foreach($task_status as $status)
                                                <option @if(isset($filter_params['status']) && $status->id == $filter_params['status']) selected @endif value="{{$status->id}}">{{$status->status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Created At: </label>
                                        <div class="input-group" style="width:100%;">
                                            <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                            <input class="date form-control" type="text" name="created-at" value="{{isset($filter_params['created-at'])?$filter_params['created-at']:""}}" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Completed At: </label>
                                        <div class="input-group" style="width:100%;">
                                            <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                            <input class="date form-control" type="text" name="completed-at" value="{{isset($filter_params['completed-at'])?$filter_params['completed-at']:""}}" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Confirmed At: </label>
                                        <div class="input-group" style="width:100%;">
                                            <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                            <input class="date form-control" type="text" name="confirmed-at" value="{{isset($filter_params['confirmed-at'])?$filter_params['confirmed-at']:""}}" />
                                        </div>
                                    </div>
                                    <button style="margin-top:30px;" type="submit" class="btn btn-primary">Submit</button>
                                    @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                                        <a style="margin-top:30px;float:right;" href="{{route('voyager.dailytask.index',['assigned-to' => Auth::user()->id])}}" class="btn btn-primary">My Tasks</a>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
                <div class="col-md-12">
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                           	 @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
							  <div class="sbox col-md-6 col-sm-12 col-xs-12">
							 @else
								   <div class="sbox ">
							 
							 @endif
                          
                                <div class="sbox-title">
                                    <h1> Summary <small> </small></h1>
                                    <div class="sbox-tools">

                                    </div>
                                </div>
                                <div class="sbox-content">

                                    <div id="tasksTable" style="padding-bottom: 10px;">
									<div class="panel-group" id="accordion">
                                            <?php foreach ($employees as $employee) { ?>
                                           <div class="col-md-6 col-sm-6 col-xs-12"  >
											  <div class="panel panel-default">
											  <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$employee['id']}}" >
												<div class="panel-heading">
												  <h5 class="panel-title" style="color:#fff;height: 100px;font-size: 13px;">

													<div class="col-xs-4"><span class="tts">Employee:</span><br/>{{$employee['name']}} </div>
													<div class="col-xs-4"><span class="tts">VTT:</span><br/> PHP {{number_format($employee['assigned'],2)}} </div>
													<div class="col-xs-4"><span class="tts">VCT:</span><br/>PHP {{number_format($employee['confirmed'],2)}} </div>
													<div class="col-xs-12 text-center">
													 @if((Auth::user()->role_id==1)||(Auth::user()->role_id==3))
														 <br/>

														<a href="{{route('voyager.dailytask.index', ['status' => 0 ,'assigned-to' => $employee['id']])}}">Open</a> |
														<a href="{{route('voyager.dailytask.index', ['status' => 1 ,'assigned-to' => $employee['id']])}}">Completed</a> |
														<a href="{{route('voyager.dailytask.index', ['status' => 2 ,'assigned-to' => $employee['id']])}}">Confirmed</a>

													@endif

													</div>

												  </h5>
												</div>
													</a>
												 @if((Auth::user()->role_id==1)||(Auth::user()->role_id==3))
												<div id="collapse{{$employee['id']}}" class="panel-collapse collapse">
												  <div class="panel-body">

                                                <div>
												<span class="tts">Employee Name:</span>
												<br/>{{$employee['name']}}
												</div>
                                                <div><span class="tts">Basic Salary:</span><br/> PHP {{number_format($employee['basic'],2)}} </div>
                                                <div><span class="tts">Value of Today's Tasks:</span><br/> PHP {{number_format($employee['assigned'],2)}} </div>
                                                <div><span class="tts">Value of Confirmed Tasks:</span><br/>PHP {{number_format($employee['confirmed'],2)}} </div>
												<div><span class="tts">Salary per Cutoff:</span><br/> PHP {{number_format($employee['salary_cutoff'],2)}} </div>
                                                <div><span class="tts">Total Value of Confirmed Tasks:</span><br/>PHP {{number_format($employee['total_confirmed'],2)}} </div>
                                                <div><span class="tts">Total Value of Confirmed and Unconfirmed Tasks:</span><br/> PHP {{number_format($employee['total_confirmed_and_unconfirmed'],2)}} </div>

													<a class="btn btn-xs btn-primary" href="{{route('voyager.dailytask.index',['assigned-to' => $employee['id']])}}"> View Details</a>

												  </div>
												</div>
												@endif
											  </div>

                                                  </div>

                                            <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
						
						 @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
							 <div class="col-md-6 col-sm-12 col-xs-12">
                            <div id='calendar'></div>
							</div>
						
                            @endif
							
                    </div>
                </div>
            
				 <div class="col-md-12">
				  <div class="panel">
                    <div class="panel-body">
                        <div class="sbox">
                           <a href="{{route('voyager.dailytask.index',['status' => 0, 'assigned-to' =>  $filter_params['assigned-to'] ?? null ])}}" class="btn btn-primary ">
							<i class="voyager-plus"></i> <span>View All Open Task</span>
						   </a>
                           <a href="{{route('voyager.dailytask.index',['status' => 1, 'assigned-to' =>  $filter_params['assigned-to'] ?? null])}}" class="btn btn-primary ">
							<i class="voyager-plus"></i> <span>View All Completed Task</span>
						   </a>
                           <a href="{{route('voyager.dailytask.index',['status' => 2, 'assigned-to' =>  $filter_params['assigned-to'] ?? null])}}" class="btn btn-primary ">
							<i class="voyager-plus"></i> <span>View All Confirmed Task</span>
						   </a>
						</div>
					</div>
				 </div>
				</div>
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover cards">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" class="select_all">
                                        </th>
                                        <th>Company</th>
                                        <th>Project</th>
                                        <th>Description</th>
                                        <th>Latest Note</th>
                                        <th>Assigned To</th>
                                        <th>PHP Value</th>
                                        <th>Created At</th>
                                        <th>Completed At</th>
                                        <th>Confirmed At</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} Task ?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal modal-success fade" tabindex="-1" id="complete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Mark Task as Complete?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="complete_form" method="POST">
                        {{ csrf_field() }}
                        <textarea class="form-control" style="background-color: white;color: black;" placeholder="Insert Note Here" name="note" required></textarea>
                        <input type="submit" class="btn btn-success pull-right" value="Mark Task as Complete">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal modal-success fade" tabindex="-1" id="open_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Mark Task as Open?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="open_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-success pull-right" value="Mark Task as Open">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal modal-success fade" tabindex="-1" id="confirm_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Task?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="confirm_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-success pull-right" value="Confirm Task">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal modal-danger fade" tabindex="-1" id="reject_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Reject Task Completion?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="reject_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right" value="Reject Task Completion">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal modal-warning fade" tabindex="-1" id="carryover_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Carry Over Task?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="carryover_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-warning pull-right" value="Carry Over Task">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('plugins/fullcalendar/packages/core/main.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('plugins/fullcalendar/packages/daygrid/main.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('plugins/fullcalendar/packages/list/main.min.css')}}"/>
    <style>
        .red-badge{
            background-color: #ed5564;
            color:white;
            padding:4px;
        }
        .green-badge{
            background-color: #43ac6e;
            color:white;
            padding:4px;
        }
        .tr-redbg {
            background-color: #2b1515!important;
        }
        .tr-orangebg {
            background-color: #2b2115 !important;
        }
    </style>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('plugins/fullcalendar/packages/core/main.min.js')}}"></script>
    <script src="{{asset('plugins/fullcalendar/packages/daygrid/main.min.js')}}"></script>
    <script src="{{asset('plugins/fullcalendar/packages/list/main.min.js')}}"></script>
    <script>
	function myFunction() {
  var x = document.getElementById("myDIV");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
    </script>
    <script>
                    $('#create-task').find(".company").select2();
                    $('#create-task').find(".assigned_to").select2();
                    $('#create-task').find(".project").select2({
                        tags: true,
                    });

                    $("#create-task").on('submit',function(e) {
                        var value = $(this).find("input[type=submit]").val();
                        $(this).find("button[type=submit]").html('Please Wait..').prop('disabled',true);
                    });

                    $(".project").select2({
                        tags: true,
                    });

                    $('.company').on('change',function() {
                        let data = @json($project_data) ;
                        $(".project").empty();
                        $(".project").select2({
                            tags: true,
                            data: data[$('.company').val()],
                        });
                    });

                    $('.date').datepicker({
                        startDate: '{{Carbon\Carbon::now()->format('m/d/Y')}}',
                        autoclose: true,
                    });

                    function imagesPreview(input, placeToInsertImagePreview) {
                        $(placeToInsertImagePreview).html("");
                        if (input.files) {
                            var filesAmount = input.files.length;

                            for (i = 0; i < filesAmount; i++) {
                                var reader = new FileReader();

                                reader.onload = function(event) {
                                    //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                                    $("#attachment_file_preview").children().clone().attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                                }

                                reader.readAsDataURL(input.files[i]);
                            }
                        }
                    };

                    $('#attachment').on('change', function() {
                        imagesPreview(this, '#gallery');
                    });

                    $('#additional-task-container').on('change','.attachment', function() {
                        $gallery = $(this).closest('.additional-task').find(".gallery");
                        $gallery.html("");
                        if (this.files) {
                            var filesAmount = this.files.length;
                            for (i = 0; i < filesAmount; i++) {
                                var reader = new FileReader();
                                reader.onload = function(event) {
                                    //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                                    $("#attachment_file_preview").children().clone().attr('src', event.target.result).appendTo($gallery);
                                }

                                reader.readAsDataURL(this.files[i]);
                            }
                        }
                    });

                    $(".remove-image").on('click', function(e) {
                        e.preventDefault();
                        /*$('#attachment_file_name').val('');
                        $('#attachment_file_preview').attr('src', '');*/
                        $(this).parent().toggleClass('hidden', true);
                        $(this).parent().find(".attachment_file_name").attr("disabled",true);
                    });
                </script>
    <script>
        @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                plugins: [ 'dayGrid','list' ],
                defaultView: 'dayGridMonth',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,listWeek'
                },
                events: '{{route('dailytask.calendardata',['assigned-to' =>  $filter_params['assigned-to'] ?? null])}}',
            });

            calendar.render();
        });
        @endif

        $('.date').datepicker({
            autoclose: true,
        });

        var deleteFormAction;
        $('#dataTable').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{route('voyager.dailytask.destroy',['id' => '__id'])}}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        $('#dataTable').on('click', '.complete-btn', function (e) {
            $('#complete_form').attr('data-id',$(this).attr('data-id'));
            $('#complete_form')[0].action = '{{route('dailytask.complete',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#complete_modal').modal('show');
        });

        $('#dataTable').on('click', '.open-btn', function (e) {
            $('#open_form').attr('data-id',$(this).attr('data-id'));
            $('#open_form')[0].action = '{{route('dailytask.open',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#open_modal').modal('show');
        });

        $('#dataTable').on('click', '.confirm-btn', function (e) {
            $('#confirm_form').attr('data-id',$(this).attr('data-id'));
            $('#confirm_form')[0].action = '{{route('dailytask.confirm',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#confirm_modal').modal('show');
        });

        $('#dataTable').on('click', '.reject-btn', function (e) {
            $('#reject_form').attr('data-id',$(this).attr('data-id'));
            $('#reject_form')[0].action = '{{route('dailytask.reject',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#reject_modal').modal('show');
        });

        $('#dataTable').on('click', '.carryover-btn', function (e) {
            $('#carryover_form').attr('data-id',$(this).attr('data-id'));
            $('#carryover_form')[0].action = '{{route('dailytask.carryover',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#carryover_modal').modal('show');
        });
        $('#dataTable').on('click', '.value-cell', function (e) {
            $('#edit-value-form-'+$(this).attr('data-id')).show();
            $(this).hide();
        });
        $('#dataTable').on('click', '.btn-edit-cancel', function (e) {
            e.preventDefault();
            $('#value-'+$(this).attr('data-id')).show();
            $(this).parent().hide();
        });

        $(document).ready(function () {
            var data = @json($project_data) ;

            $("#project").select2({
            });
            $('#company').on('change',function() {
                $("#project").empty();
                $("#project").select2({
                    data: data[$('#company').val()],
                });
            });

            $('#filter-form').submit(function () {
                $(this)
                    .find('input[name]')
                    .filter(function () {
                        return !this.value;
                    })
                    .prop('name', '');
                $(this)
                    .find('select[name]')
                    .filter(function () {
                        return !this.value;
                    })
                    .prop('name', '');
            });

            var table = $('#dataTable').DataTable({
                @if(Auth::user()->role_id == 4)
                order: [ 7, 'asc' ],
                @else
                order: [ 7, 'desc' ],
                @endif
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{route('dailytask.data')}}',
                    data: function (d) {
                        $('input[name=description]').val() != '' ? d['description'] =  $('input[name=description]').val() : true ;
                        $('select[name=assigned-to]').val() != '' ? d['assigned-to'] =  $('select[name=assigned-to]').val() : true ;
                        $('select[name=status]').val() != '' ? d['status'] =  $('select[name=status]').val() : true ;
                        $('select[name=company]').val() != '' ? d['company'] =  $('select[name=company]').val() : true ;
                        $('select[name=project]').val() != '' ? d['project'] =  $('select[name=project]').val() : true ;
                        $('input[name=completed-at]').val() != '' ? d['completed-at'] = $('input[name=completed-at]').val() : true;
                        $('input[name=created-at]').val() != '' ? d['created-at'] = $('input[name=created-at]').val() : true;
                        $('input[name=confirmed-at]').val() != '' ? d['confirmed-at'] = $('input[name=confirmed-at]').val() : true;
                    }
                },
                columns: [
                    {data: 'id', name: 'tb_daily_log_sub_tasks.id'},
                    {data: 'company', name: 'tb_companies.company_name'},
                    {data: 'project', name: 'tb_projects.title'},
                    {data: 'description', name: 'tb_daily_log_sub_tasks.description'},
                    {data: 'latest_note', name: 'tb_daily_task_notes.note', orderable: false, searchable: false},
                    {data: 'assigned_user', name: 'users.name'},
                    {data: 'value', name: 'tb_daily_log_sub_tasks.value'},
                    {data: 'created_at', name: 'tb_daily_log_sub_tasks.created_at'},
                    {data: 'completed_at', name: 'tb_daily_log_sub_tasks.completed_at'},
                    {data: 'confirmed_at', name: 'tb_daily_log_sub_tasks.confirmed_at'},
                    {data: 'task_status', name: 'tb_task_status.status'},
                    {data: 'actions', name: 'actions', orderable: false, searchable: false},
                ], 'drawCallback': function (settings) {
                    var api = this.api();
                    var $table = $(api.table().node());
                    if ($table.hasClass('cards')) {
                        // Create an array of labels containing all table headers
                        var labels = [];
                        $('thead th', $table).each(function () {
                            labels.push($(this).text());
                        });
                        // Add data-label attribute to each cell
                        $('tbody tr', $table).each(function () {
                            $(this).find('td').each(function (column) {
                                $(this).attr('data-label', labels[column]);
                            });
                        });
                    }
                }
            });

            $('#tasksTable tr').click(function () {
                window.open($(this).attr("data-href"),'_blank');
            });
        });

        $('#dataTable').on('submit', '.edit-value-form', function (e) {
            e.preventDefault();
            var value = $(this).find("input[type=submit]").val();
            $(this).find("button[type=submit]").html('<i class="fa fa-spinner fa-spin fa-fw"></i>').prop('disabled',true);
            var $t = $(this);
            $.ajax({
                url: $(this).attr('action'),
                type: "post",
                data: $(this).serialize() ,
                success: function (response) {
                    toastr.success(response.message);
                    $t.find("button[type=submit]").html('<i class="fa fa-check"></i>').prop('disabled',false);
                    $('#completed-at-'+$t.attr('data-id')).html(response.completed_at);
                    $('#confirmed-at-'+$t.attr('data-id')).html(response.confirmed_at);
                    $('#status-'+$t.attr('data-id')).html(response.status);
                    $('#button-'+$t.attr('data-id')).html(response.button);
                    $('#value-'+$t.attr('data-id')).html(response.value);
                    $('#value-'+$t.attr('data-id')).show();
                    $('.edit-value-form').hide();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    toastr.error('Something went wrong.');
                    $t.find("button[type=submit]").html('<i class="fa fa-check"></i>').prop('disabled',false);
                    $('.edit-value-form').hide();
                }
            });
        });

        $("#complete_form, #open_form, #confirm_form, #reject_form, #carryover_form").on('submit',function(e) {
            e.preventDefault();
            var value = $(this).find("input[type=submit]").val();
            $(this).find("input[type=submit]").val('Please Wait..').prop('disabled',true);
            var $t = $(this);
            $.ajax({
                url: $(this).attr('action'),
                type: "post",
                data: $(this).serialize() ,
                success: function (response) {
                    if(response.success) {
                        toastr.success(response.message);
                        $t.find("input[type=submit]").val(value).prop('disabled', false);
                        $('#completed-at-' + $t.attr('data-id')).html(response.completed_at);
                        $('#confirmed-at-' + $t.attr('data-id')).html(response.confirmed_at);
                        $('#status-' + $t.attr('data-id')).html(response.status);
                        $('#button-' + $t.attr('data-id')).html(response.button);
                        $('#value-' + $t.attr('data-id')).html(response.value);
                        $('#complete_modal').modal('hide');
                        $('#open_modal').modal('hide');
                        $('#confirm_modal').modal('hide');
                        $('#reject_modal').modal('hide');
                        $('#carryover_modal').modal('hide');
                    } else {
                        toastr.error(response.message);
                        $t.find("input[type=submit]").val(value).prop('disabled',false);
                        $('#complete_modal').modal('hide');
                        $('#open_modal').modal('hide');
                        $('#confirm_modal').modal('hide');
                        $('#reject_modal').modal('hide');
                        $('#carryover_modal').modal('hide');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    toastr.error('Something went wrong.');
                    $t.find("input[type=submit]").val(value).prop('disabled',false);
                    $('#complete_modal').modal('hide');
                    $('#open_modal').modal('hide');
                    $('#confirm_modal').modal('hide');
                    $('#reject_modal').modal('hide');
                    $('#carryover_modal').modal('hide');
                }
            });
        });

        $("#delete_form").on('submit',function(e) {
            var value = $(this).find("input[type=submit]").val();
            $(this).find("input[type=submit]").val('Please Wait..').prop('disabled',true);
        });
    </script>
    <script>
        var time = new Date().getTime();
        $(document.body).bind("mousemove keypress", function(e) {
            time = new Date().getTime();
        });

        function refresh() {
            if(new Date().getTime() - time >= 300000)
                window.location.reload(true);
            else
                setTimeout(refresh, 10000);
        }

        setTimeout(refresh, 10000);
    </script>
@stop
