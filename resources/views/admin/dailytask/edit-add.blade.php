@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .image-area {
            position: relative;
            width: 120px;
            background: #333;
        }
        .image-area img{
            max-width: 100%;
            height: auto;
        }
        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
            text-shadow: 0 1px 2px rgba(0,0,0,0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }
        .remove-image:hover {
            background: #E54E4E;
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }
        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }

        .table-responsive>.fixed-column {
            position: absolute;
            display: inline-block;
            width: auto;
            background-color: #192734;
            border-right: 1px solid #ddd;
        }
        @media(min-width:768px) {
            .table-responsive>.fixed-column {
                display: none;
            }
        }
    </style>
@stop

@section('page_title', ($edit ? 'Edit' : 'Add').' Task')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-list"></i>
        {{ ($edit ? 'Edit' : 'Add').' Task' }}
    </h1>
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="sbox">
                            <div class="sbox-title">
                                <h1> Summary <small> </small></h1>
                                <div class="sbox-tools">

                                </div>
                            </div>
                            <div class="sbox-content">
                                <div class="table-responsive" style="padding-bottom: 10px;">
                                    <table class="table table-hover" id="tasksTable">
                                        <thead>
                                        <tr>
                                            @if((Auth::user()->role_id==1)||(Auth::user()->role_id==3))
                                                <td><strong>Employee Name</strong></td>
                                                <td><strong>Basic Salary</strong></td>
                                            @endif
                                            <td><strong>Value of Today's Tasks</strong></td>
                                            <td><strong>Value of Confirmed Tasks</strong></td>
                                            @if((Auth::user()->role_id==1)||(Auth::user()->role_id==3))
                                                <td><strong>Salary per Cutoff</strong></td>
                                                <td><strong>Total Value of Confirmed Tasks</strong></td>
                                                <td><strong>Total Value of Confirmed and Unconfirmed Tasks</strong></td>
                                        @endif
                                        <tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($employees as $employee) { ?>
                                        <tr @if((Auth::user()->role_id==1)||(Auth::user()->role_id==3)) class="clickable-row" data-href="{{route('voyager.dailytask.index',['assigned-to' => $employee['id']])}}" @endif style="cursor:pointer;">
                                            @if((Auth::user()->role_id==1)||(Auth::user()->role_id==3))
                                                <td>{{$employee['name']}} </td>
                                                <td>PHP {{number_format($employee['basic'],2)}} </td>
                                            @endif
                                            <td>PHP {{number_format($employee['assigned'],2)}} </td>
                                            <td>PHP {{number_format($employee['confirmed'],2)}} </td>
                                            @if((Auth::user()->role_id==1)||(Auth::user()->role_id==3))
                                                <td>PHP {{number_format($employee['salary_cutoff'],2)}} </td>
                                                <td>PHP {{number_format($employee['total_confirmed'],2)}} </td>
                                                <td>PHP {{number_format($employee['total_confirmed_and_unconfirmed'],2)}} </td>
                                            @endif
                                        </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add form-horizontal"
                          action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $task->id) : route('voyager.'.$dataType->slug.'.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif
                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}
                        @if(isset($_GET['request_id']))
                            <input id="request_id" class="form-control" type="hidden" name="request_id" required value="{{$_GET['request_id']}}"/>
                        @endif
                        @if(isset($_GET['return']))
                            <input id="return" class="form-control" type="hidden" name="return" required value="{{$_GET['return']}}"/>
                        @endif
                        <div class="panel-body" style="padding-top:20px">
                            @if($edit)
                            <div class="form-group {{ $errors->has('company') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="company">Company *</label>
                                <div class="col-sm-10">
                                    <select id="company" class="form-control select2" name="company" required>
                                        <option value="">Please select one</option>
                                        @foreach($companies as $company)
                                            <option @if($task->company_id && $task->company_id == $company->id) selected @endif value="{{$company->id}}">{{$company->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('company'))
                                    @foreach ($errors->get('company') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

			<div class="col-xs-12 col-md-6 col-sm-12">
                            <div class="form-group {{ $errors->has('project') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="project">Project *</label>
                                <div class="col-sm-10">
                                    <select id="project" class="form-control" name="project" required>
                                        <option value="">Please select a company first</option>
                                        @if($task->project_id != null)
                                        @foreach($projects->where('company_id','=',$task->company_id)->all() as $project)
                                            <option @if($task->project_id && $task->project_id == $project->id) selected @endif value="{{$project->id}}">{{$project->title}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                @if($errors->has('assigned_to'))
                                    @foreach ($errors->get('assigned_to') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>
							
			</div>
			<div class="col-xs-12 col-md-6 col-sm-12">

							      <div class="form-group {{ $errors->has('value') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="value">PHP Value *</label>
                                <div class="col-sm-10">
                                    <input id="value" class="form-control" type="number" name="value" required value="{{$task->value}}"/>
                                </div>
                                @if($errors->has('value'))
                                    @foreach ($errors->get('value') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>
							<div class="form-group {{ $errors->has('assigned_to') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="assigned_to">Assigned To *</label>
                                <div class="col-sm-10">
                                    <select id="assigned_to" class="form-control select2" @if($edit) name="assigned_to" @else name="assigned_to[]" multiple @endif required>
                                        <option value="">Please select one</option>
                                        @foreach($users as $user)
                                            <option @if($task->assigned_to && $task->assigned_to == $user->id) selected @endif value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('assigned_to'))
                                    @foreach ($errors->get('assigned_to') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>
<div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="date">Date *</label>
								<div class=" col-sm-10 {{ $errors->has('date') ? 'has-error' : '' }}">

                                    <div class="input-group"  >
                                        <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                        <input id="date" class="form-control" type="text" name="date" value="{{$task->created_at==null?Carbon\Carbon::now()->format('m/d/Y'):Carbon\Carbon::parse($task->created_at)->format('m/d/Y')}}" required/>
                                    </div>
                                    @if($errors->has('date'))
                                        @foreach ($errors->get('date') as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                                </div>
                            

                  </div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="description">Description *</label>
                                <div class="col-sm-10">
                                    <textarea id="description" class="form-control" rows="3" name="description">{{$task->description}}</textarea>
                                </div>
                                @if($errors->has('description'))
                                    @foreach ($errors->get('description') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>
                            

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="attachment">Attach Image/s</label>
                                <div class="col-sm-10">
                                    <input id="attachment" type="file" name="attachment[]" value="" accept=".jpg,.jpeg,.png" multiple/>
                                    <div id="gallery">
                                    </div>
                                    @if($attachments != null && count($attachments) > 0)
                                        <div>
                                            @foreach($attachments as $attachment)
                                                <div class="image-area" style="display:inline-block;margin-right:20px;">
                                                    <input name="attachment_file_name[]" class="hidden attachment_file_name" value="{{$attachment->attachment}}" />
                                                    <img class="" style="width: 120px; height:120px;" src="{{url('uploads/notes/'.$attachment->attachment)}}" >
                                                    <a class="remove-image" href="#" style="display: inline;text-decoration: none">&#215;</a>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                                <label class="col-sm-2 control-label" for="attachment">Attach Document/s</label>
                                <div class="col-sm-10">
                                    <input id="attachment_docs" type="file" name="attachment_docs[]" value="" accept=".xls,.xlsx,.doc,.docx,.pdf,.ppt,.pptx" multiple/>
                                    @if($attachment_docs != null && count($attachment_docs) > 0)
                                        <div>
                                            @foreach($attachment_docs as $attachment)
                                                <div class="image-area" style="display:inline-block;margin-right:20px;">
                                                    <input name="attachment_doc_file_name[]" class="hidden attachment_file_name" value="{{$attachment->attachment}}" />
                                                    <i class="far fa-file"></i>&nbsp;{{$attachment->file_name}}
                                                    <a class="remove-image" href="#" style="display: inline;text-decoration: none">&#215;</a>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            @else
                                <div class="row" id="additional-task-container">
                                    <div class="additional-task">

			<div class="col-xs-12 col-md-6 col-sm-12">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="company">Company *</label>
                                            <div class="col-sm-10">
                                                <select class="company form-control" name="company[0]" required>
                                                    <option value="">Please select one</option>
                                                    @foreach($companies as $company)
                                                        <option @if($task->company_id && $task->company_id == $company->id) selected @endif value="{{$company->id}}">{{$company->company_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="project">Project *</label>
                                            <div class="col-sm-10">
                                                <select class="project form-control" name="project[0]" required>
                                                    <option value="">Please select a company first</option>
                                                    @if($task->project_id != null)
                                                        @foreach($projects->where('company_id','=',$task->company_id)->all() as $project)
                                                            <option @if($task->project_id && $task->project_id == $project->id) selected @endif value="{{$project->id}}">{{$project->title}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                             </div>
			<div class="col-xs-12 col-md-6 col-sm-12">
			  <div class="form-group">
                                            <label class="col-sm-2 control-label" for="value">PHP Value *</label>
                                            <div class="col-sm-10">
                                                <input class="value form-control" type="number" name="value[0]" required value="{{$task->value}}"/>
                                            </div>
                                        </div>
										 <div class="form-group">
                                            <label class="col-sm-2 control-label" for="assigned_to">Assigned To *</label>
                                            <div class="col-sm-10">
                                                <select class="assigned_to form-control" name="assigned_to[0][]" multiple required>
                                                    <option value="">Please select one</option>
                                                    @if(isset($request_assigned_to) && count($request_assigned_to) > 0)
                                                        @foreach($users as $user)
                                                            <option @if($request_assigned_to->where('user_id',$user->id)->first()) selected @endif value="{{$user->id}}">{{$user->name}}</option>
                                                        @endforeach
                                                    @else
                                                        @foreach($users as $user)
                                                            <option @if($task->assigned_to && $task->assigned_to == $user->id) selected @endif value="{{$user->id}}">{{$user->name}}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
										  <div class="form-group ">
                                            <label class="col-sm-2 control-label" for="date">Date *</label>
                                            <div class=" col-sm-10">
                                                <div class="input-group"  >
                                                    <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                                    <input class="date form-control" type="text" name="date[0]" value="{{$task->created_at==null?Carbon\Carbon::now()->format('m/d/Y'):Carbon\Carbon::parse($task->created_at)->format('m/d/Y')}}" required/>
                                                </div>
                                            </div>
                                            </div>
                                          
										</div>

									<div class="col-xs-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="description">Description *</label>
                                            <div class="col-sm-10">
                                                <textarea class="description form-control" rows="5" name="description[0]">{{$task->description}}</textarea>
                                            </div>
                                        </div>
									</div>

									<div class="col-xs-12 col-md-6 col-sm-12">
                                       
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="attachment">Attach Image/s</label>
                                            <div class="col-sm-10">
                                                <input class="attachment" type="file" name="attachment[0][]" value="" accept=".jpg,.jpeg,.png" multiple/>
                                                <div class="gallery">
                                                </div>
                                                @if($attachments != null && count($attachments) > 0)
                                                    <div>
                                                        @foreach($attachments as $attachment)
                                                            <div class="image-area" style="display:inline-block;margin-right:20px;">
                                                                <input name="attachment_file_name[0][]" class="hidden attachment_file_name" value="{{$attachment->attachment}}" />
                                                                <img class="" style="width: 120px; height:120px;" src="{{url('uploads/notes/'.$attachment->attachment)}}" >
                                                                <a class="remove-image" href="#" style="display: inline;text-decoration: none">&#215;</a>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="attachment">Attach Document/s</label>
                                            <div class="col-sm-10">
                                                <input class="attachment_docs" type="file" name="attachment_docs[0][]" value="" accept=".xls,.xlsx,.doc,.docx,.pdf,.ppt,.pptx,.txt" multiple/>
                                                @if($attachment_docs != null && count($attachment_docs) > 0)
                                                    <div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
										</div>

                                    </div>
                                </div>
                            @endif

                            @section('submit-buttons')
							  <div class="row"  >
							  <div class="col-xs-12"  >
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                                @if(!$edit)
                                    <button type="button" id="add-more-task" class="btn btn-primary pull-right">Add More</button>
                                @endif
								</div>
								</div>
                            @stop
                            @yield('submit-buttons')
                        </div><!-- panel-body -->
                    </form>

                </div>
            </div>
        </div>
    </div>
    <div id="attachment_file_preview">
        <img class="" style="width: 250px; height:auto;" src="" >
    </div>
    <div id="additional-task-div" class="hidden">
        <div class="additional-task">
            <div class="col-xs-12  ">
                <hr/>
            </div>
            <button class="pull-right btn btn-danger delete-task"><i class="fas fa-times"></i></button>
            <div class="col-xs-12  ">

            </div>


			<div class="col-xs-12 col-md-6 col-sm-12">
            <div class="form-group">
                <label class="col-sm-2 control-label" for="company">Company *</label>
                <div class="col-sm-10">
                    <select class="company form-control " name="company[0]" required>
                        <option value="">Please select one</option>
                        @foreach($companies as $company)
                            <option @if($task->company_id && $task->company_id == $company->id) selected @endif value="{{$company->id}}">{{$company->company_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="project">Project *</label>
                <div class="col-sm-10">
                    <select class="project form-control" name="project[0]" required>
                        <option value="">Please select a company first</option>
                        @if($task->project_id != null)
                            @foreach($projects->where('company_id','=',$task->company_id)->all() as $project)
                                <option @if($task->project_id && $task->project_id == $project->id) selected @endif value="{{$project->id}}">{{$project->title}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
</div>
			<div class="col-xs-12 col-md-6 col-sm-12">
			  

            <div class="form-group">
                <label class="col-sm-2 control-label" for="value">PHP Value *</label>
                <div class="col-sm-10">
                    <input class="value form-control" type="number" name="value[0]" required value="{{$task->value}}"/>
                </div>
            </div>
			<div class="form-group">
                <label class="col-sm-2 control-label" for="assigned_to">Assigned To *</label>
                <div class="col-sm-10">
                    <select class="assigned_to form-control" name="assigned_to[0][]" multiple required>
                        <option value="">Please select one</option>
                        @foreach($users as $user)
                            <option @if($task->assigned_to && $task->assigned_to == $user->id) selected @endif value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
			<div class="form-group ">
                <label class="col-sm-2 control-label" for="date">Date *</label>
                <div class="col-sm-10">
                    <div class="input-group" >
                        <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                        <input class="date form-control" type="text" name="date[0]" value="{{$task->created==null?Carbon\Carbon::now()->format('m/d/Y'):Carbon\Carbon::parse($task->created)->format('m/d/Y')}}" required/>
                    </div>
                </div>
                </div>
			</div>
			<div class="col-xs-12 col-md-6 col-sm-12">
            <div class="form-group">
                <label class="col-sm-2 control-label" for="description">Description *</label>
                <div class="col-sm-10">
                    <textarea class="description form-control" rows="3" name="description[0]">{{$task->description}}</textarea>
                </div>
            </div>
			</div>
			<div class="col-xs-12 col-md-6 col-sm-12">
            
            <div class="form-group">
                <label class="col-sm-2 control-label" for="attachment">Attach Image/s</label>
                <div class="col-sm-10">
                    <input class="attachment" type="file" name="attachment[0][]" value="" accept=".jpg,.jpeg,.png" multiple/>
                    <div class="gallery">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="attachment">Attach Document/s</label>
                <div class="col-sm-10">
                    <input class="attachment_docs" type="file" name="attachment_docs[0][]" value="" accept=".xls,.xlsx,.doc,.docx,.pdf,.ppt,.pptx,.txt" multiple/>
                </div>
            </div>
			</div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        var tasksTable = $('#tasksTable');
        var $fixedColumn = tasksTable.clone().insertBefore(tasksTable).addClass('fixed-column');

        $fixedColumn.find('th:not(:first-child),td:not(:first-child)').remove();

        $fixedColumn.find('tr').each(function (i, elem) {
            $(this).height(tasksTable.find('tr:eq(' + i + ')').height());
        });

        $(".clickable-row").on('click', function() {
            window.location.href = $(this).attr("data-href");
        });

        var index_count = 1;
        $("#add-more-task").on('click', function(){
            $("#additional-task-div").find('.company').attr('name', 'company[' + index_count + ']');
            $("#additional-task-div").find('.project').attr('name', 'project[' + index_count + ']');
            $("#additional-task-div").find('.date').attr('name', 'date[' + index_count + ']');
            $("#additional-task-div").find('.description').attr('name', 'description[' + index_count + ']');
            $("#additional-task-div").find('.value').attr('name', 'value[' + index_count + ']');
            $("#additional-task-div").find('.assigned_to').attr('name', 'assigned_to[' + index_count + '][]');
            $("#additional-task-div").find('.attachment').attr('name', 'attachment[' + index_count + '][]');
            $("#additional-task-div").find('.attachment_docs').attr('name', 'attachment_docs[' + index_count + '][]');
            $("#additional-task-div").find('.additional-task').attr('data-task' , index_count );
            $('#additional-task-container').append($("#additional-task-div").children().clone());
            $('#additional-task-container').find('.additional-task[data-task='+index_count+']').find('.company').select2();
            $('#additional-task-container').find('.additional-task[data-task='+index_count+']').find('.assigned_to').select2();
            $('#additional-task-container').find('.additional-task[data-task='+index_count+']').find('.project').select2({
                tags: true,
            });
            $('.company').on('change',function() {
                $(this).closest('.additional-task').find(".project").empty();
                $(this).closest('.additional-task').find(".project").select2({
                    tags: true,
                    data: data[$(this).val()],
                });
            });
            index_count++;
            $('textarea').trigger('input');
        });

        function imagesPreview(input, placeToInsertImagePreview) {
            $(placeToInsertImagePreview).html("");
            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                        $("#attachment_file_preview").children().clone().attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }
        };

        $('.form-edit-add').on('click','.delete-task', function() {
            $(this).parent().remove();
        });

        $('#attachment').on('change', function() {
            imagesPreview(this, '#gallery');
        });

        $('#additional-task-container').on('change','.attachment', function() {
            $gallery = $(this).closest('.additional-task').find(".gallery");
            $gallery.html("");
            if (this.files) {
                var filesAmount = this.files.length;
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                        $("#attachment_file_preview").children().clone().attr('src', event.target.result).appendTo($gallery);
                    }

                    reader.readAsDataURL(this.files[i]);
                }
            }
        });

        $(".remove-image").on('click', function(e) {
            e.preventDefault();
            /*$('#attachment_file_name').val('');
            $('#attachment_file_preview').attr('src', '');*/
            $(this).parent().toggleClass('hidden', true);
            $(this).parent().find(".attachment_file_name").attr("disabled",true);
        });

        var data = @json($project_data) ;

        @if($edit)
        $("#project").select2({
            tags: true,
        });
        $('#company').on('change',function() {
            $("#project").empty();
            $("#project").select2({
                tags: true,
                data: data[$('#company').val()],
            });
        });

        $('#date').datepicker({
            startDate: '{{Carbon\Carbon::now()->format('m/d/Y')}}',
            autoclose: true,
        });
        @else
        $('#additional-task-container').find(".company").select2();
        $('#additional-task-container').find(".assigned_to").select2();
        $('#additional-task-container').find(".project").select2({
            tags: true,
        });
        $('.company').on('change',function() {
            $(this).closest('.additional-task').find(".project").empty();
            $(this).closest('.additional-task').find(".project").select2({
                tags: true,
                data: data[$(this).val()],
            });
        });

        $('body').on('focus', '.date', function() {
            $(this).datepicker({
                startDate: '{{Carbon\Carbon::now()->format('m/d/Y')}}',
                autoclose: true,
            });
        });
        @endif

        $('textarea').each(function () {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
        });

        $('form').on('input', 'textarea', function () {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
        });

        $("form").on('submit',function(e) {
            var value = $(this).find("input[type=submit]").val();
            $(this).find("button[type=submit]").html('Please Wait..').prop('disabled',true);
        });
    </script>
@stop
