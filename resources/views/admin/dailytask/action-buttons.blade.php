@if(Carbon\Carbon::parse($task->created_at)->startOfDay() <= Carbon\Carbon::now() && $task->status == 0 && $task->assigned_to == Auth::user()->id)
    <button class="btn btn-primary btn-xs complete-btn" type="button" data-id="{{$task->id}}"> Mark Complete</button>
@endif
@if($task->status == 1 && $task->assigned_to == Auth::user()->id && Carbon\Carbon::parse($task->completed_at) >= Carbon\Carbon::now()->startOfDay() && Carbon\Carbon::parse($task->completed_at) <= Carbon\Carbon::now()->endOfDay())
    <button class="btn btn-primary btn-xs open-btn" type="button" data-id="{{$task->id}}"> Mark as Open</button>
@endif
@if($task->status == 1 && (Auth::user()->role_id == 1 || Auth::user()->role_id == 2))
    <button class="btn btn-primary btn-xs confirm-btn" type="button" data-id="{{$task->id}}"> Confirm Completion</button>
@endif
@if($task->status == 1 && (Auth::user()->role_id == 1 || Auth::user()->role_id == 2))
    <button class="btn btn-primary btn-xs reject-btn" type="button" data-id="{{$task->id}}"> Reject</button>
@endif
@if($task->status == 1 && (Auth::user()->role_id == 1 || Auth::user()->role_id == 2) && Carbon\Carbon::parse($task->completed_at) < Carbon\Carbon::now()->startOfDay())
    <button class="btn btn-primary btn-xs carryover-btn" type="button" data-id="{{$task->id}}"> Carry Over</button>
@endif
@if($canView)
    <a class="btn btn-primary btn-xs" href="{{route('voyager.dailytask.show',['id' => $task->id ])}}" data-id="{{$task->id}}"> View</a>
@endif
@if((Auth::user()->role_id == 1 || Auth::user()->role_id == 2  && $canEdit) || ($task->status == 0 && $canEdit))
    @if(isset($return) && $return != null)
        <a class="btn btn-primary btn-xs" href="{{route('voyager.dailytask.edit',['id' => $task->id,'return'=>$return ])}}" data-id="{{$task->id}}"> Edit</a>
    @else
        <a class="btn btn-primary btn-xs" href="{{route('voyager.dailytask.edit',['id' => $task->id ])}}" data-id="{{$task->id}}"> Edit</a>
    @endif
@endif
@if($canDelete)
    <a class="btn btn-primary btn-xs delete" href="javascript:;" data-id="{{$task->id}}"> Delete</a>
@endif
