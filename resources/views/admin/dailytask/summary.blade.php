@extends('voyager::master')

@section('page_title', 'Tasks')

@section('page_header')
@stop

@section('content')
<style>
.voyager .panel.panel-default .panel-heading {
    border-bottom: 1px solid #eaeaea;
    background-color: #f0f0f000;
    color: white !important;
}
.expand_caret {
    transform: scale(1.6);
    margin-left: 8px;
    margin-top: -4px;
}
a[aria-expanded='false'] > .expand_caret {
    transform: scale(1.6) rotate(-90deg);
}
</style>
    <div class="container-fluid">
        <div class="row"> 
            <div class="col-md-3 col-sm-12 col-xs-12" id="company-list-area" style="position:relative;z-index: 2;">
			
            		<div class="panel-group" id="accordion">  
			<div class="col-md-12 col-sm-6 col-xs-12">
			  <div class="panel panel-default">
			  <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" >
				<div class="panel-heading">
				   <h1 class="page-title" style="padding-left: 25px;"> <i class="fa fa-building"></i>&nbsp;Companies
				   <div class="expand_caret caret"></div>
				   </h1> 
				</div>
					</a>
				 
			  </div>
			  <div id="collapse1" class="panel-collapse collapse">
				  <div class="panel-body">
				
			 
                   
                <div   id="company-list" style="overflow:auto;  max-height: 40vh;">
                    @foreach($companies as $company)
                        <div data-href="{{route('tasksummary',$company->id)}}" class="company-list-item @if($company->id == $selected_company) active @endif" style="margin:5px 10px;padding:10px 0px;flex-direction: row!important;display: flex!important;">
                            {{--<div class="avatar-circle" style="margin-left: 15px;">
                                <span class="initials">JD</span>
                            </div>--}}
                            <div class="company-avatar" style="">
                                <span  >{{$company->company_name[0]}}</span>
                            </div>
                            <div style="margin-left: 15px;">
                                <div class="name" style="font-weight: bold;">{{$company->company_name}}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
                  <hr class="text-center hidden-sm hidden-xs" style="width:50%;border-top:1px solid #fff">
              
			
				  </div>
				</div>
				  </div>
				  <div class="col-md-12  col-sm-6 col-xs-12">
				 <div class="panel panel-default">
			  <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" >
				<div class="panel-heading">
				   <h1 class="page-title" style="padding-left: 25px;"><i class="fa  fa-cubes"></i>&nbsp;Projects
				     <div class="  expand_caret caret"></div>
				   </h1>  
				</div>
					</a>
				 
			  </div>
			  <div id="collapse2" class="panel-collapse collapse">
				  <div class="panel-body">
					
			
		<div   id="project-list" style="overflow:auto;  max-height: 40vh;" >
                <div  style="overflow:auto;  max-height: 35vh;" > 
                @if($selected_company != null)
                    <div data-href="{{route('tasksummary',$selected_company)}}" class="project-list-item @if($selected_project == null) active @endif"  style="margin: 5px 10px;  padding: 10px 10px; font-weight: bold;flex-direction: row!important;display: flex!important;cursor:pointer;">
                        <div style="">
                            <div class="name"><i class="far fa-circle"></i>&nbsp;All</div>
                        </div>
                    </div>
                    @foreach($projects as $project)
                        <div data-href="{{route('tasksummary',['company_id' => $selected_company, 'project' => $project->id])}}" class="project-list-item @if($selected_project == $project->id) active @endif"  style="margin: 5px 10px;  padding: 10px 10px; font-weight: bold;;flex-direction: row!important;display: flex!important;cursor:pointer;">
                            <div style="">
                                <div class="name"><i class="far fa-circle"></i>&nbsp;{{$project->title}}</div>
                            </div>
                        </div>
                    @endforeach
                @endif
				 </div>
				 
				   <div  style="overflow:auto;  max-height: 65vh;" >
				    <hr class="text-center  " style="width:50%;border-top:1px solid #fff">
                   <div class="protit"><i class="fa fa-image"></i>&nbsp; File Attachment</div>
                       @if($selected_company != null)
                           @foreach($note_attachments_with_main as $attachment)
                               <div class="col-md-4 col-sm-3 col-xs-12 ">
                                   <div class="attachments_gallery" style="">
                                       <a data-toggle="modal" data-target="#exampleModal2{{$attachment->id}}">
                                           <img class="center-block " style="max-width:100%;max-height:250px;height:auto;cursor: pointer" src="{{url('uploads/notes/'.$attachment->attachment)}}" >
                                       </a>
                                       <div class="modal fade" id="exampleModal2{{$attachment->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                           <div class="modal-dialog" role="document">
                                               <div class="modal-content  modal-xl">
                                                   <div class="modal-header">

                                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                           <span aria-hidden="true">&times;</span>
                                                       </button>
                                                   </div>
                                                   <div class="modal-body">
                                                       <img class="img-responsive center-block"   src="{{url('uploads/notes/'.$attachment->attachment)}}" >
                                                   </div>
                                                   <div class="modal-footer">
                                                       <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           @endforeach
                       @endif
				 </div>
				 
				 
            </div>
            </div>
			
				  </div>
				</div>
			 
           </div>

            
			</div>
            <!-- Message Area -->
            <div class="col-md-9" id="message-area"   >
              <hr class="text-center hidden-lg hidden-md" style="width:50%;border-top:1px solid #fff">
                    <h1 class="page-title" style="padding-left: 25px;"><i class="fa  fa-list"></i>&nbsp; Tasks Summary
					<a style="margin-right:10px;" href="{{route('voyager.request-tasks.create')}}" class="btn btn-success pull-right"><i class="bx bx-add-to-queue"></i>&nbsp; Request Task</a>
					</h1>
                    
                
                <!-- Messages -->
                <div class="row" id="messages" >
                    @if($selected_company == null)
                        <h1 class="  text-center" ><i class="fa fa-building"></i><br/>Please select a Company First</h1>
                    @else
                        @include('admin.dailytask.summary-task-items',[
                        'note_attachments_without_main' => $note_attachments_without_main,
                        'note_attachments_with_main' => $note_attachments_with_main,
                        'notes_without_main' => $notes_without_main,
                        'notes_with_main' => $notes_with_main,
                        'users_without_main' => $users_without_main,
                        'users_with_main' => $users_with_main,
                        ])
                    @endif
                </div>
            </div>
           
        </div>
    </div> 

<!-- Modal -->
@stop

@section('css')
    <style> 
	.modal-content{
    background-color: #5a4d4d00 !important; 
}
	.imfs{
		    margin-top: 10px;
    padding: 5px;
    border: 1px solid;
	}
	.imfs:hover{
		    margin-top: 10px;
    padding: 5px;
    border: 3px solid;
	zoom:1.2;
	transition:all 0.3s ease;
	}
	.clientfb{
		opacity:.6;background: #15202b;padding:15px 10px
	}
	.clientfb:hover{
		opacity:1;background: #15202b;padding:15px 10px;
		transition:all 0.3s ease;
	}
	.updaterow{
	padding: 15px 15px;
    margin-top: 20px;
    border: 1px solid #1da1f2;
    border-radius: 10px;
    color: white;
	}
	.asscss{
		color: white;
    font-size: 12px; 
	}
	.asscss2{
		color: white;
    font-size: 12px; 
	text-align:center;
	}
	.taskdesc{
		font-weight: 800; 
    margin-top: 10px; 
    color: #8fc241;
	}
      
	.taskdesc2{
	 font-weight: 800; 
    margin-top: 10px; 
    color: #1da1f2; 
	}
	
	.protit{
		font-size:18px; 
		    color: #8fc241;
			font-weight: 700;
			text-align:left;
			    height: 50px;
				padding-left: 20px;
}
    line-height: 43px;
	}
	
        .avatar-circle {
            width:  50px;
            height: 50px;
            background-color: #8fc241;
            text-align: center;
            border-radius: 50%;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
        }

        .initials {
            position: relative;
            top: 10px; /* 25% of parent */
            font-size: 25px; /* 50% of parent */
            line-height: 25px; /* 50% of parent */
            color: #fff;
            font-weight: bold;
        }

        .company-avatar {
            margin-left: 15px;
			background: #1da1f2 ;color:white;
			padding: 0;
            font-weight: 500;font-size: 11px;
            line-height: 20px;
            width: 20px;
            height: 20px;
            border-radius: 25%;
            text-transform: uppercase;
            text-align: center;
            position: relative;
        }

        ::-webkit-scrollbar {
            width: 10px;
        }

        ::-webkit-scrollbar-track {
            background: none;
        }

        ::-webkit-scrollbar-thumb {
            background: rgba(0, 0, 0, 0.2);
        }
        ::-webkit-scrollbar-thumb:hover {
            background: rgba(0, 0, 0, 0.3);
        }

        #main-container {
            width: 100vw;
            height: 100vh;
        }

        .company-list-item, .project-list-item {
            cursor: pointer;
        }

        .company-list-item:hover, .project-list-item:hover {
            background: #1da1f2;
			color:white;
			transition:all 0.3s ease;
			border-radius: 10px;
        }

        .company-list-item:active, .project-list-item:active {
            background: #1da1f2;
			color:white;
			transition:all 0.3s ease;
			border-radius: 10px;
        }

        .company-list-item.active, .project-list-item.active {
             background: #1da1f2;
			color:white;
			transition:all 0.3s ease;
			border-radius: 10px;
        }
		.updaterow:hover {
    border: 1px solid #fff;
	transition:all 0.3s ease;
}
span.taskdesc2:hover { 
    color: #ffffff;
	transition:all 0.3s ease;
}
        #message-area {
            border-left: 1px solid white;
            border-right: 1px solid white;
        }

        #message-area .overlay {
            background: hsl(0, 0%, 80%);
        }

        #input-area {
            background: hsl(0, 0%, 95%);
        }

        #input-area #input {
            outline: none;
        }

        .message-item {
            position:relative;
            max-width: 75%;
            word-break: break-word;
        }
        .message-item.self {
            background: #dcf8c6!important;
        }
        .message-item .number {
            color: #1f7aec !important;
        }
        .message-item .options {
            position: absolute;
            top: 0;
            right: -3px;
            opacity: 0;
            transition: all .2s ease-in-out;
            -moz-transition: all .2s ease-in-out;
            -webkit-transition: all .2s ease-in-out;
        }

        .task-note-item {
                background-color: #2b343b42;
				border-bottom: 3px solid #1da1f2;
				border-radius:10px
        }

        .task-note-item:hover {
            background: #384047;
            border-color: #384047;
            cursor: pointer;
        }

        #messages {
            /*flex: 1!important;*/
           background: #192734;
			max-height: 100vh;
			overflow: auto;
			margin: 0px 20px;
			padding: 15px;
			border-radius:10px;
			border: solid 1px #fff;
        }

        .bg-white {
            background-color: #fff!important;
        }
        .rounded {
            border-radius: .25rem!important;
        }

        .shadow-sm {
            box-shadow: 0 .125rem .25rem rgba(0,0,0,.075)!important;
        }

        .align-self-start {
            -ms-flex-item-align: start!important;
            align-self: flex-start!important;
        }

        .align-self-end {
            -ms-flex-item-align: end!important;
            align-self: flex-end!important;
        }
    </style>
@stop

@section('javascript')
    <script>
        
        $('.company-list-item').click(function () {
            location.href = $(this).attr("data-href");
        });
        $('.project-list-item').click(function () {
            location.href = $(this).attr("data-href");
        });

        var $scroll_ajax = false;
        var $page = 1;
                @if($selected_project != null)
        var $project = {{$selected_project}};
        @endif

        @if($selected_company != null)
        $('#messages').on('scroll', function() {
            let div = $(this).get(0);
            console.log([div.scrollTop , div.clientHeight , div.scrollHeight]);

            if(div.scrollTop + div.clientHeight + 1 >= div.scrollHeight && $scroll_ajax == false) {
                $scroll_ajax = true;
                $('#loading-div-'+$page).toggleClass('hidden',false);
                $.ajax({
                    url: '{{route('tasksummary',['company_id'=> $selected_company])}}',
                    type: "GET",
                    data: {
                        page: $page+1,
                        @if($selected_project != null)
                        project: $project,
                        @endif
                    },
                    success: function (response) {
                        $('#loading-div-'+$page).toggleClass('hidden',true);
                        $('#messages').append(response.html);
                        $page++;
                        if(response.hasPages) {
                            $scroll_ajax = false;
                        } else {
                            $scroll_ajax = true;
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {

                    }
                });
            }
        });
        @endif
    </script>
@stop
