@foreach($tasks_and_notes as $tan)
    <?php
    $users = '';
    if($tan->unique_task_id < 0) {
        $users = $users_without_main->where('id',abs($tan->unique_task_id))->first()->name;
        $notes = $notes_without_main->where('task_id',abs($tan->unique_task_id))->all();
        $attachments = $note_attachments_without_main->where('task_id',abs($tan->unique_task_id));
    } else {
        $users = implode($users_with_main->where('main_task_id',$tan->unique_task_id)->pluck('name')->all(),', ');
        $notes = $notes_with_main->where('main_task_id',$tan->unique_task_id)->all();
        $attachments = $note_attachments_with_main->where('main_task_id',$tan->unique_task_id);
    }

    ?>
    <div class="row task-note-item rounded shadow-sm" style="margin:10px 10px;">
        <div class="col-md-6 col-xs-12" style="padding-right:20px;padding-top:15px;">
            <div >
                <span style="color:white;font-size: 11px;"><span style="color:#8fc241;" class="icon bx bx-task"></span> Task:  {{Carbon\Carbon::parse($tan->created_at)->format('l, F j, Y')}} | Created at {{Carbon\Carbon::parse($tan->created_at)->format('h:i a')}}</span><br/>
                <span class="taskdesc" >{!!nl2br($tan->description)!!} </span>
                <span  class="asscss"> has been assigned to <i class="far fa-user"></i>  <em>{{$users}} </em></span>
            </div>
        </div>
        <div class="col-md-6 col-xs-12 clientfb" >
            <div >
                <span style="color:white;font-size: 11px;"><i style="color:#8fc241;" class="fa fa-bullhorn"></i> Client Feedback Task:  {{Carbon\Carbon::parse($tan->created_at)->format('l, F j, Y')}} | Sent at {{Carbon\Carbon::parse($tan->created_at)->format('h:i a')}}</span><br/>
                <span class="taskdesc" >Lorem Ipsum Sample Client Feedback</span> <br/>
                <span  class="asscss"> Noted by <i class="far fa-user"></i>  <em>Client Name </em></span>
            </div>
        </div>
        <div class="col-xs-12"  >
            @if($notes != null )
                @foreach($notes as $note)
                    <div class="attachments_gallery" style="z-index:9999">
                        @foreach($attachments->where('note_id','=',$note->id)->all() as $attachment)

                            <a data-toggle="modal" data-target="#exampleModal{{$attachment->id}}">
                                <img class="center-block" style="max-width:250px;max-height:250px;height:auto;cursor: pointer" src="{{url('uploads/notes/'.$attachment->attachment)}}" >
                            </a>
                            <div class="modal fade" id="exampleModal{{$attachment->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content modal-xl">
                                        <div class="modal-header">

                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <img class="img-responsive center-block"   src="{{url('uploads/notes/'.$attachment->attachment)}}" >
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div >
                        <div class="updaterow">
                                                    <span class="asscss">
                                                        <i class="fa fa-list-alt"></i>
                                                        @if($tan->completed_at != null) Completed on {{Carbon\Carbon::parse($tan->completed_at)->format('h:i a')}} : @endif
                                                        <i class="far fa-user"></i>  <em>{{$note->user}} </em>
                                                    </span>
                            <br/>
                            <span class="taskdesc2" >   <?php
                                $url = '@(http(s)?)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
                                $parsed_string = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a><br/>', $note->note);
                                ?>
                                {!! nl2br($parsed_string) !!}
                                                    </span>
                        </div>
                    </div>
                @endforeach
            @elseif($tan->completed_at != null)
                <div >
                    <div class="updaterow">
                        <span class="asscss">
                            <i class="fa fa-list-alt"></i>
                            @if($tan->completed_at != null) Completed on {{Carbon\Carbon::parse($tan->completed_at)->format('h:i a')}} @endif
                        </span>
                    </div>
                </div>
            @endif
        </div>

    </div>
@endforeach
@if($tasks_and_notes->hasMorePages())
<div id="loading-div-{{$tasks_and_notes->currentPage()}}" class="col-xs-12 hidden" style="text-align: center">
    PLEASE WAIT...
</div>
@endif
