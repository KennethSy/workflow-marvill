@extends('voyager::master')

@section('page_title', 'Task')

@section('css')
    <style>
        .image-area {
            position: relative;
            width: 120px;
            background: #333;
        }
        .image-area img{
            max-width: 100%;
            height: auto;
        }
        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
            text-shadow: 0 1px 2px rgba(0,0,0,0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }
        .remove-image:hover {
            background: #E54E4E;
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }
        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }
    </style>
@stop

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-list"></i> <a style="color:inherit;" href="{{route('voyager.dailytask.index')}}"> Task</a>
			
        </h1>
        <span class="pull-right">
            @if($task->status != 2)
                @include('admin.dailytask.action-buttons',['task' => $task,'canView' => false,'canDelete' => false, 'canEdit' => false])
            @endif
			<div class="pull-right">
				<a href="https://v2workflow.marvill.com/public/admin/dailytask?status=0" class="btn btn-primary ">
				<i class="voyager-plus"></i> <span>View All Open Task</span>
				</a>
				<a href="https://v2workflow.marvill.com/public/admin/dailytask?status=1" class="btn btn-primary ">
				<i class="voyager-plus"></i> <span>View All Completed Task</span>
				</a>
				<a href="https://v2workflow.marvill.com/public/admin/dailytask?status=2" class="btn btn-primary ">
				<i class="voyager-plus"></i> <span>View All Confirmed Task</span>
				</a>
				</div>
        </span>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="task-table" class="table table-hover">
                                <tbody>
                                <tr>
                                    <td><b>Company</b></td>
                                    <td>{{$task->company}}</td>
                                </tr>
                                <tr>
                                    <td><b>Project</b></td>
                                    <td>{{$task->project}}</td>
                                </tr>
                                <tr>
                                    <td><b>Description</b></td>
                                    <td>{!! nl2br($task->description) !!}</td>
                                </tr>
                                <tr>
                                    <td><b>Value</b></td>
                                    <td>{{$task->value}}</td>
                                </tr>
                                <tr>
                                    <td><b>Created At</b></td>
                                    <td>{{$task->created_at}}</td>
                                </tr>
                                <tr>
                                    <td><b>Completed At</b></td>
                                    <td>{{$task->completed_at}}</td>
                                </tr>
                                <tr>
                                    <td><b>Confirmed At</b></td>
                                    <td>{{$task->confirmed_at}}</td>
                                </tr>
                                <tr>
                                    <td><b>Status</b></td>
                                    <td>{{$status->status}}</td>
                                </tr>
                                <tr>
                                    <td><b>Assigned To</b></td>
                                    <td>{{$user->name}}</td>
                                </tr>
                                <tr>
                                    <td><b>Attachments</b></td>
                                    <td>
                                        <div class="attachments_gallery">
                                            @foreach($task_attachments as $attachment)
                                                <img class="" style="width: 250px; height:auto;cursor: pointer" src="{{url('uploads/notes/'.$attachment->attachment)}}" >
                                            @endforeach
                                        </div>
                                        <div class="attachments_docs">
                                            <ul class="nav nav-stacked">
                                                @foreach($task_attachment_docs as $attachment)
                                                    <li>
                                                        <a target="none" href="{{route('dailytask.downloadattachment',['id' =>$task->id,'file_id'=>$attachment->id])}}"><i class="far fa-file"></i>&nbsp;{{$attachment->file_name}}</a>
                                                    </li>
                                                @endforeach
                                            </ul>

                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Note</b></td>
                                    <td>
                                        @foreach($notes as $note)
                                            @if(Carbon\Carbon::now()->isSameDay(Carbon\Carbon::parse($note->created_at)))
                                                <div id="note-div">
                                                    <button id="edit-btn" class="btn btn-default pull-right">Edit Note</button>
                                                    <?php
                                                    $today_note = $note;
                                                    $today_attachment = $attachments->where('note_id',$note->id)->all();
                                                    ?>
                                                    @else
                                                        <div>
                                                            <span class="pull-right">{{Carbon\Carbon::parse($note->created_at)->format('F j, Y')}}</span>
                                                            @endif
                                                            {!! nl2br($note->note) !!}

                                                            <div class="attachments_gallery">
                                                                @foreach($attachments->where('note_id',$note->id)->all() as $attachment)
                                                                    <img class="" style="width: 250px; height:auto;cursor: pointer" src="{{url('uploads/notes/'.$attachment->attachment)}}" >
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <hr/>
                                                        @endforeach
                                                        @if(!isset($today_note))
                                                            <br/>
                                                            <br/>
                                                            <div id="note-div">
                                                                <button id="edit-btn" class="btn btn-default pull-right">Edit Note</button>
                                                            </div>
                                                        @endif
                                                        <div class="hidden" id="note-form">
                                                            <form method="POST" action="{{route('dailytask.editnote',['id'=>$task->id])}}" enctype="multipart/form-data" >
                                                                {{csrf_field()}}
                                                                <textarea class="form-control" style="width:100%" name="note">{{ isset($today_note)?$today_note->note:'' }}</textarea>
                                                                <input id="attachment" type="file" name="attachment[]" value="" accept=".jpg,.jpeg,.png" multiple/>
                                                                <div id="gallery">
                                                                </div>
                                                                @if(isset($today_note))
                                                                    <div>
                                                                        @foreach($today_attachment as $attachment)
                                                                            <div class="image-area" style="display:inline-block;margin-right:20px;">
                                                                                <input name="attachment_file_name[]" class="hidden attachment_file_name" value="{{$attachment->attachment}}" />
                                                                                <img class="" style="width: 120px; height:120px;" src="{{url('uploads/notes/'.$attachment->attachment)}}" >
                                                                                <a class="remove-image" href="#" style="display: inline;text-decoration: none">&#215;</a>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                @endif
                                                                <input name="save"  class="btn btn-primary" type="submit" value="Submit"></input>
                                                                @if($task->status == 0)
                                                                <input type="submit" name="complete" class="btn btn-primary save" value="Submit & Mark as Complete"></input>
                                                                @endif
                                                            </form>
                                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div id="attachment_file_preview">
                                <img class="" style="width: 250px; height:auto;" src="" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <form role="form"
                          class="form-edit-add form-horizontal"
                          action="{{route('voyager.dailytask.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}
                        <div class="panel-body" style="padding-top:20px">
                            <div id="additional-task-container">
                                <div class="additional-task">
                                    <div class="col-xs-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="company">Company *</label>
                                            <div class="col-sm-10">
                                                <select class="company form-control" name="company[0]" required>
                                                    <option value="">Please select one</option>
                                                    @foreach($companies as $company)
                                                    <option value="{{$company->id}}">{{$company->company_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="project">Project *</label>
                                            <div class="col-sm-10">
                                                <select class="project form-control" name="project[0]" required>
                                                    <option value="">Please select a company first</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-sm-12">
                                        <div class="form-group ">
                                            <label class="col-sm-2 control-label" for="date">Date *</label>
                                            <div class=" col-sm-10">
                                                <div class="input-group"  >
                                                    <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                                    <input class="date form-control" type="text" name="date[0]" value="{{Carbon\Carbon::now()->format('m/d/Y')}}" required/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="value">PHP Value *</label>
                                            <div class="col-sm-10">
                                                <input class="value form-control" type="number" name="value[0]" required value=""/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="description">Description *</label>
                                            <div class="col-sm-10">
                                                <textarea class="description form-control" rows="5" name="description[0]"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="assigned_to">Assigned To *</label>
                                            <div class="col-sm-10">
                                                <select class="assigned_to form-control" name="assigned_to[0][]" multiple required>
                                                    <option value="">Please select one</option>
                                                    @foreach($users as $user)
                                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="attachment">Attach Image/s</label>
                                            <div class="col-sm-10">
                                                <input id="add-task-attachment" class="attachment" type="file" name="attachment[0][]" value="" accept=".jpg,.jpeg,.png" multiple/>
                                                <div id="add-task-gallery">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="attachment_docs">Attach Document/s</label>
                                            <div class="col-sm-10">
                                                <input id="attachment_docs" class="attachment_docs" type="file" name="attachment_docs[0][]" value="" accept=".xls,.xlsx,.doc,.docx,.pdf,.ppt,.pptx,.txt" multiple/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @section('submit-buttons')
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div><!-- panel-body -->
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="modal modal-success fade" tabindex="-1" id="complete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Mark Task as Complete?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="complete_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-success pull-right" value="Mark Task as Complete">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal modal-success fade" tabindex="-1" id="open_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Mark Task as Open?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="open_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-success pull-right" value="Mark Task as Open">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal modal-success fade" tabindex="-1" id="confirm_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Task?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="confirm_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-success pull-right" value="Confirm Task">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal modal-danger fade" tabindex="-1" id="reject_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Reject Task Completion?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="reject_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right" value="Reject Task Completion">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal modal-warning fade" tabindex="-1" id="carryover_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Carry Over Task?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="carryover_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-warning pull-right" value="Carry Over Task">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        function imagesPreview(input, placeToInsertImagePreview) {
            $(placeToInsertImagePreview).html("");
            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                        $("#attachment_file_preview").children().clone().attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }
        };

        $('#attachment').on('change', function() {
            imagesPreview(this, '#gallery');
        });

        $('.attachments_gallery img').click(function () {
            window.open($(this).attr("src"),'_blank');
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#attachment_file_preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
                $('.image-area').toggleClass('hidden', false);
                $('#attachment_file_name').val('');
            }
        }

        /*$("#attachment").change(function(){
            readURL(this);
        });*/

        $(".remove-image").on('click', function(e) {
            e.preventDefault();
            /*$('#attachment_file_name').val('');
            $('#attachment_file_preview').attr('src', '');*/
            $(this).parent().toggleClass('hidden', true);
            $(this).parent().find(".attachment_file_name").attr("disabled",true);
        });

        $('textarea').each(function () {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
        });

        $('#note-form').on('input', 'textarea', function () {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
        });

        $('#edit-btn').click(function () {
            $('#note-form').toggleClass('hidden');
            $('#note-div').toggleClass('hidden');
            $('#edit-btn').html('Edit Note');
            $('textarea').trigger('input');
        });

        $("#complete_form, #open_form, #confirm_form, #reject_form, #carryover_form").on('submit',function(e) {
            var value = $(this).find("input[type=submit]").val();
            $(this).find("input[type=submit]").val('Please Wait..').prop('disabled',true);
        });

        $('.complete-btn').on('click', function (e) {
            $('#complete_form').attr('data-id',$(this).attr('data-id'));
            $('#complete_form')[0].action = '{{route('dailytask.complete',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#complete_modal').modal('show');
        });
        $('.open-btn').on('click', function (e) {
            $('#open_form').attr('data-id',$(this).attr('data-id'));
            $('#open_form')[0].action = '{{route('dailytask.open',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#open_modal').modal('show');
        });
        $('.confirm-btn').on('click', function (e) {
            $('#confirm_form').attr('data-id',$(this).attr('data-id'));
            $('#confirm_form')[0].action = '{{route('dailytask.confirm',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#confirm_modal').modal('show');
        });
        $('.reject-btn').on('click', function (e) {
            $('#reject_form').attr('data-id',$(this).attr('data-id'));
            $('#reject_form')[0].action = '{{route('dailytask.reject',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#reject_modal').modal('show');
        });
        $('.carryover-btn').on('click', function (e) {
            $('#carryover_form').attr('data-id',$(this).attr('data-id'));
            $('#carryover_form')[0].action = '{{route('dailytask.carryover',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#carryover_modal').modal('show');
        });

        $("form").on('submit',function(e) {
            var value = $(this).find("input[type=submit]").val();
            $(this).find("button[type=submit]").html('Please Wait..').prop('disabled',true);
        });

        var data = @json($project_data)

        $(".company").select2();
        $(".assigned_to").select2();
        $(".project").select2({
            tags: true,
        });

        $('.company').on('change',function() {
            $(this).closest('.additional-task').find(".project").empty();
            $(this).closest('.additional-task').find(".project").select2({
                tags: true,
                data: data[$(this).val()],
            });
        });

        $('.date').datepicker({
            startDate: '{{Carbon\Carbon::now()->format('m/d/Y')}}',
            autoclose: true,
        });

        $('#add-task-attachment').on('change', function() {
            imagesPreview(this, '#add-task-gallery');
        });
    </script>
@stop
