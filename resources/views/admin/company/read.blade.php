@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
<style>
.cpanel{
	background:#192734;
	color:white;
	border-radius:10px
}
.ccpan{
 height:60px; 
 
}
.ctit{
	color:white;
	font-weight:800;
	font-size:10px;
	text-transform:uppercase;
	letter-spacing:1px
}
.updaterow {
    padding: 15px 15px;
    margin-top: 20px;
    border: 1px solid #1da1f2;
    border-radius: 10px;
    color: white;
}
</style>




    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->getTranslatedAttribute('display_name_singular')) }} &nbsp;

        @can('edit', $dataTypeContent)
            <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
                <span class="glyphicon glyphicon-pencil"></span>&nbsp;
                {{ __('voyager::generic.edit') }}
            </a>
        @endcan
        @can('delete', $dataTypeContent)
            @if($isSoftDeleted)
                <a href="{{ route('voyager.'.$dataType->slug.'.restore', $dataTypeContent->getKey()) }}" title="{{ __('voyager::generic.restore') }}" class="btn btn-default restore" data-id="{{ $dataTypeContent->getKey() }}" id="restore-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.restore') }}</span>
                </a>
            @else
                <a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger delete" data-id="{{ $dataTypeContent->getKey() }}" id="delete-{{ $dataTypeContent->getKey() }}">
                    <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
                </a>
            @endif
        @endcan

        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="col-xs-12 col-md-7"> 
	<h3>  <i class="fa fa-info-circle" aria-hidden="true"></i> Client Info</h3>				
                    <!-- form start -->
                    @foreach($dataType->readRows as $row)
                        @php
                        if ($dataTypeContent->{$row->field.'_read'}) {
                            $dataTypeContent->{$row->field} = $dataTypeContent->{$row->field.'_read'};
                        }
                        @endphp
						<div class="col-xs-12 updaterow">
                        <div class="col-md-12  "  >
                            <div style="color:white;font-weight:800;font-size:10px;text-transform:uppercase;letter-spacing:1px">{{ $row->getTranslatedAttribute('display_name') }}</div>
                        </div>

                        <div class="col-md-12 "  >
                            @if (isset($row->details->view))
                                @include($row->details->view, ['row' => $row, 'dataType' => $dataType, 'dataTypeContent' => $dataTypeContent, 'content' => $dataTypeContent->{$row->field}, 'action' => 'read', 'view' => 'read', 'options' => $row->details])
                            @elseif($row->type == "image")
                                <img class="img-responsive"
                                     src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                            @elseif($row->type == 'multiple_images')
                                @if(json_decode($dataTypeContent->{$row->field}))
                                    @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                        <img class="img-responsive"
                                             src="{{ filter_var($file, FILTER_VALIDATE_URL) ? $file : Voyager::image($file) }}">
                                    @endforeach
                                @else
                                    <img class="img-responsive"
                                         src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                                @endif
                            @elseif($row->type == 'relationship')
                                 @include('voyager::formfields.relationship', ['view' => 'read', 'options' => $row->details])
                            @elseif($row->type == 'select_dropdown' && property_exists($row->details, 'options') &&
                                    !empty($row->details->options->{$dataTypeContent->{$row->field}})
                            )
                                <?php echo $row->details->options->{$dataTypeContent->{$row->field}};?>
                            @elseif($row->type == 'select_multiple')
                                @if(property_exists($row->details, 'relationship'))

                                    @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                        {{ $item->{$row->field}  }}
                                    @endforeach

                                @elseif(property_exists($row->details, 'options'))
                                    @if (!empty(json_decode($dataTypeContent->{$row->field})))
                                        @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                            @if (@$row->details->options->{$item})
                                                {{ $row->details->options->{$item} . (!$loop->last ? ', ' : '') }}
                                            @endif
                                        @endforeach
                                    @else
                                        {{ __('voyager::generic.none') }}
                                    @endif
                                @endif
                            @elseif($row->type == 'date' || $row->type == 'timestamp')
                                @if ( property_exists($row->details, 'format') && !is_null($dataTypeContent->{$row->field}) )
                                    {{ \Carbon\Carbon::parse($dataTypeContent->{$row->field})->formatLocalized($row->details->format) }}
                                @else
                                    {{ $dataTypeContent->{$row->field} }}
                                @endif
                            @elseif($row->type == 'checkbox')
                                @if(property_exists($row->details, 'on') && property_exists($row->details, 'off'))
                                    @if($dataTypeContent->{$row->field})
                                    <span class="label label-info">{{ $row->details->on }}</span>
                                    @else
                                    <span class="label label-primary">{{ $row->details->off }}</span>
                                    @endif
                                @else
                                {{ $dataTypeContent->{$row->field} }}
                                @endif
                            @elseif($row->type == 'color')
                                <span class="badge badge-lg" style="background-color: {{ $dataTypeContent->{$row->field} }}">{{ $dataTypeContent->{$row->field} }}</span>
                            @elseif($row->type == 'coordinates')
                                @include('voyager::partials.coordinates')
                            @elseif($row->type == 'rich_text_box')
                                @include('voyager::multilingual.input-hidden-bread-read')
                                {!! $dataTypeContent->{$row->field} !!}
                            @elseif($row->type == 'file')
                                @if(json_decode($dataTypeContent->{$row->field}))
                                    @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                        <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}">
                                            {{ $file->original_name ?: '' }}
                                        </a>
                                        <br/>
                                    @endforeach
                                @else
                                    <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($row->field) ?: '' }}">
                                        {{ __('voyager::generic.download') }}
                                    </a>
                                @endif
                            @else
                                @include('voyager::multilingual.input-hidden-bread-read')
                                <p>{{ $dataTypeContent->{$row->field} }}</p>
                            @endif
                        </div><!-- panel-body -->
                        </div><!-- panel-body -->
                        @if(!$loop->last)
                            
                        @endif
                    @endforeach

                   

                </div>
               <div class="col-xs-12 col-md-5 cpanel" style="padding-bottom:5px;">
			 
			   <h3 style="color:white">  <i class="fa fa-server" aria-hidden="true"></i> Domain / Hosting Info</h3>
				 @include('admin.domainhosting.partials.read')
				 </div>
				
				
				
            </div>
        </div>
        <div class="row">
        <div class="col-xs-12 ">
       <h1 class="page-title" style="padding-left: 25px;"><svg class="svg-inline--fa fa-list fa-w-16" aria-hidden="true" focusable="false" data-prefix="fa" data-icon="list" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M80 368H16a16 16 0 0 0-16 16v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16v-64a16 16 0 0 0-16-16zm0-320H16A16 16 0 0 0 0 64v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16V64a16 16 0 0 0-16-16zm0 160H16a16 16 0 0 0-16 16v64a16 16 0 0 0 16 16h64a16 16 0 0 0 16-16v-64a16 16 0 0 0-16-16zm416 176H176a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zm0-320H176a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16V80a16 16 0 0 0-16-16zm0 160H176a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16z"></path></svg><!-- <i class="fa  fa-list"></i> -->&nbsp; Tasks Summary</h1>
      
		</div >
		 </div>
		   <div class="row">
		<div class="col-xs-12 cpanel">
            @foreach($tasks_and_notes as $tan)
                <div class="row task-note-item rounded shadow-sm" style="margin:10px 10px;">
                    <div class="col-md-7 col-xs-12" style="padding-right:20px;padding-top:15px;">
                        <div >
                            <span style="color:white;font-size: 11px;"><span style="color:#8fc241;" class="icon bx bx-task"></span> Task:  {{Carbon\Carbon::parse($tan->created_at)->format('l, F j, Y')}} | Created at {{Carbon\Carbon::parse($tan->created_at)->format('h:i a')}}</span><br/>
                            <span class="taskdesc" >{{$tan->description}} @if($tan->note == null )</span>
                            <br/>
                            <span  class="asscss"> has been assigned to <i class="far fa-user"></i>  <em>{{$tan->user}} @endif</em></span>
                        </div>
                    </div>
                    <div class="col-md-5 col-xs-12 clientfb" >
                        <div >
                            <span style="color:white;font-size: 11px;"><i style="color:#8fc241;" class="fa fa-bullhorn"></i> Client Feedback Task:  {{Carbon\Carbon::parse($tan->created_at)->format('l, F j, Y')}} | Sent at {{Carbon\Carbon::parse($tan->created_at)->format('h:i a')}}</span><br/>
                            <span class="taskdesc" >Lorem Ipsum Sample Client Feedback</span> <br/>
                            <span  class="asscss"> Noted by <i class="far fa-user"></i>  <em>Client Name </em></span>
                        </div>
                    </div>
                    <div class="col-xs-12"  >
                        @if($tan->note != null )
                            <div class="attachments_gallery" style="">
                                @foreach($attachments->where('note_id',$tan->note_id)->all() as $attachment)

                                    <a data-toggle="modal" data-target="#exampleModal{{$attachment->id}}">
                                        <img class="center-block" style="max-width:250px;max-height:250px;height:auto;cursor: pointer" src="{{url('uploads/notes/'.$attachment->attachment)}}" >
                                    </a>

                                    <div class="modal fade" id="exampleModal{{$attachment->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content modal-xl">
                                                <div class="modal-header">

                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <img class="img-responsive center-block"   src="{{url('uploads/notes/'.$attachment->attachment)}}" >
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach

                            </div>

                            <div >
                                <div class="updaterow">
                                             <span  class="asscss">
                                                 <i class="fa fa-list-alt"></i>
                                                 @if($tan->completed_at != null)
                                                     Completed on {{Carbon\Carbon::parse($tan->completed_at)->format('l, F j, Y, h:i a')}} :
                                                 @else
                                                     Dev Update :
                                                 @endif
                                                 <i class="far fa-user"></i>  <em>{{$tan->user}} </em>
                                             </span>
                                    <br/>
                                    <span class="taskdesc2" >   <?php
                                        $url = '@(http(s)?)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
                                        $parsed_string = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a><br/>', $tan->note);
                                        ?>
                                        {!! nl2br($parsed_string) !!}


                                                  </span>

                                </div>

                            </div>
                        @else
                            <div >
                                <div class="updaterow">
                                             <span  class="asscss">
                                                 <i class="fa fa-list-alt"></i>
                                                 @if($tan->completed_at != null)
                                                     Completed at {{Carbon\Carbon::parse($tan->completed_at)->format('l, F j, Y, h:i a')}} :
                                                 @endif
                                             </span>
                                </div>
                            </div>
                        @endif
                    </div>

                </div>
            @endforeach
		</div>
        </div>
        </div>
       
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->getTranslatedAttribute('display_name_singular')) }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    @if ($isModelTranslatable)
        <script>
            $(document).ready(function () {
                $('.side-body').multilingual();
            });
        </script>
    @endif
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) {
                // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');

            $('#delete_modal').modal('show');
        });

    </script>
@stop
