
<div style="margin-top:30px;" class="form-group col-md-12 {{ $errors->has('domain_name') ? 'has-error' : '' }}">
    <label class=" control-label" for="domain_name">Domain Name</label>
    <input id="domain_name" class="form-control" type="text" name="domain_name" value="{{$domain_and_hosting->domain_name}}"/>
    @if($errors->has('domain_name'))
        @foreach ($errors->get('domain_name') as $error)
            <span class="help-block">{{ $error }}</span>
        @endforeach
    @endif
</div>

<div class="form-group col-md-12 {{ $errors->has('domain_under') ? 'has-error' : '' }}">
    <label class="control-label" for="domain_under">Domain Under</label>
    <select id="domain_under" class="form-control select2" name="domain_under">
        <option value="">Please select one</option>
        <option @if($domain_and_hosting->domain_under == 'Marvill') selected @endif value="Marvill">Marvill</option>
        <option @if($domain_and_hosting->domain_under == 'Client') selected @endif value="Client">Client</option>
    </select>
    @if($errors->has('domain_under'))
        @foreach ($errors->get('domain_under') as $error)
            <span class="help-block">{{ $error }}</span>
        @endforeach
    @endif
</div>

<div id="domain_marvill" class="@if($domain_and_hosting->domain_under != 'Marvill') hidden @endif">

<div class="form-group col-md-12 {{ $errors->has('domain_start_billing_date') ? 'has-error' : '' }}">
    <label class="control-label" for="domain_start_billing_date">Domain Start Billing Date</label>
    <input id="domain_start_billing_date" class="form-control" type="text" name="domain_start_billing_date" value="{{$domain_and_hosting->domain_start_billing_date==null?'':Carbon\Carbon::parse($domain_and_hosting->domain_start_billing_date)->format('m/d/Y')}}"/>
    @if($errors->has('domain_start_billing_date'))
        @foreach ($errors->get('domain_start_billing_date') as $error)
            <span class="help-block">{{ $error }}</span>
        @endforeach
    @endif
</div>

<div class="form-group col-md-12 {{ $errors->has('domain_billing_plan') ? 'has-error' : '' }}">
    <label class="control-label" for="domain_billing_plan">Domain Billing Plan</label>
    <select id="domain_billing_plan" class="form-control select2" name="domain_billing_plan">
        <option value="">Please select one</option>
        <option  @if($domain_and_hosting->domain_billing_plan == 'Annually') selected @endif value="Annually">Annually</option>
        <option  @if($domain_and_hosting->domain_billing_plan == 'Monthly') selected @endif value="Monthly">Monthly</option>
    </select>
    @if($errors->has('domain_billing_plan'))
        @foreach ($errors->get('domain_billing_plan') as $error)
            <span class="help-block">{{ $error }}</span>
        @endforeach
    @endif
</div>

</div>

<div style="margin-top:30px;"  class="form-group col-md-12 {{ $errors->has('hosting_type') ? 'has-error' : '' }}">
    <label class="control-label" for="hosting_type">Hosting Type</label>
    <select id="hosting_type" class="form-control select2" name="hosting_type">
        <option value="">Please select one</option>
        <option @if($domain_and_hosting->hosting_type == 'Standard') selected @endif value="Standard">Standard</option>
        <option @if($domain_and_hosting->hosting_type == 'VPS') selected @endif value="VPS">VPS</option>
        <option @if($domain_and_hosting->hosting_type == 'Dedicated') selected @endif value="Dedicated">Dedicated</option>
    </select>
    @if($errors->has('hosting_type'))
        @foreach ($errors->get('hosting_type') as $error)
            <span class="help-block">{{ $error }}</span>
        @endforeach
    @endif
</div>

<div class="form-group col-md-12 {{ $errors->has('hosting_under') ? 'has-error' : '' }}">
    <label class="control-label" for="hosting_under">Hosting Under</label>
    <select id="hosting_under" class="form-control select2" name="hosting_under">
        <option value="">Please select one</option>
        <option @if($domain_and_hosting->hosting_under == 'Marvill') selected @endif value="Marvill">Marvill</option>
        <option @if($domain_and_hosting->hosting_under == 'Client') selected @endif value="Client">Client</option>
    </select>
    @if($errors->has('hosting_under'))
        @foreach ($errors->get('hosting_under') as $error)
            <span class="help-block">{{ $error }}</span>
        @endforeach
    @endif
</div>

<div id="hosting_marvill" class="@if($domain_and_hosting->hosting_under != 'Marvill') hidden @endif">

<div class="form-group col-md-12 {{ $errors->has('hosting_start_billing_date') ? 'has-error' : '' }}">
    <label class="control-label" for="hosting_start_billing_date">Hosting Start Billing Date</label>
    <input id="hosting_start_billing_date" class="form-control" type="text" name="hosting_start_billing_date" value="{{$domain_and_hosting->hosting_start_billing_date==null?'':Carbon\Carbon::parse($domain_and_hosting->hosting_start_billing_date)->format('m/d/Y')}}"/>
    @if($errors->has('hosting_start_billing_date'))
        @foreach ($errors->get('hosting_start_billing_date') as $error)
            <span class="help-block">{{ $error }}</span>
        @endforeach
    @endif
</div>

<div class="form-group col-md-12 {{ $errors->has('hosting_billing_plan') ? 'has-error' : '' }}">
    <label class="control-label" for="hosting_billing_plan">Hosting Billing Plan</label>
    <select id="hosting_billing_plan" class="form-control select2" name="hosting_billing_plan">
        <option value="">Please select one</option>
        <option @if($domain_and_hosting->hosting_billing_plan == 'Annually') selected @endif value="Annually">Annually</option>
        <option @if($domain_and_hosting->hosting_billing_plan == 'Monthly') selected @endif value="Monthly">Monthly</option>
    </select>
    @if($errors->has('hosting_billing_plan'))
        @foreach ($errors->get('hosting_billing_plan') as $error)
            <span class="help-block">{{ $error }}</span>
        @endforeach
    @endif
</div>

</div>

<div style="margin-top:30px;" class="form-group col-md-12 {{ $errors->has('email_provider_under') ? 'has-error' : '' }}">
    <label class="control-label" for="email_provider_under">Email Provider Under</label>
    <select id="email_provider_under" class="form-control select2" name="email_provider_under">
        <option value="">Please select one</option>
        <option @if($domain_and_hosting->email_provider_under == 'Gmail Free') selected @endif value="Gmail Free">Gmail Free</option>
        <option @if($domain_and_hosting->email_provider_under == 'Gmail Marvill') selected @endif value="Gmail Marvill">Gmail Marvill</option>
        <option @if($domain_and_hosting->email_provider_under == 'Gmail') selected @endif value="Gmail">Gmail</option>
    </select>
    @if($errors->has('email_provider_under'))
        @foreach ($errors->get('email_provider_under') as $error)
            <span class="help-block">{{ $error }}</span>
        @endforeach
    @endif
</div>

<div id="email_provider_marvill" class="@if($domain_and_hosting->email_provider_under != 'Gmail Marvill') hidden @endif">

<div class="form-group col-md-12 {{ $errors->has('email_provider_start_billing_date') ? 'has-error' : '' }}">
    <label class="control-label" for="email_provider_start_billing_date">Email Provider Start Billing Date</label>
    <input id="email_provider_start_billing_date" class="form-control" type="text" name="email_provider_start_billing_date" value="{{$domain_and_hosting->email_provider_start_billing_date==null?'':Carbon\Carbon::parse($domain_and_hosting->email_provider_start_billing_date)->format('m/d/Y')}}"/>
    @if($errors->has('email_provider_start_billing_date'))
        @foreach ($errors->get('email_provider_start_billing_date') as $error)
            <span class="help-block">{{ $error }}</span>
        @endforeach
    @endif
</div>

<div class="form-group col-md-12 {{ $errors->has('email_provider_billing_plan') ? 'has-error' : '' }}">
    <label class="control-label" for="email_provider_billing_plan">Email Provider  Billing Plan</label>
    <select id="email_provider_billing_plan" class="form-control select2" name="email_provider_billing_plan">
        <option value="">Please select one</option>
        <option @if($domain_and_hosting->email_provider_billing_plan == 'Annually') selected @endif value="Annually">Annually</option>
        <option @if($domain_and_hosting->email_provider_billing_plan == 'Monthly') selected @endif value="Monthly">Monthly</option>
    </select>
    @if($errors->has('email_provider_billing_plan'))
        @foreach ($errors->get('email_provider_billing_plan') as $error)
            <span class="help-block">{{ $error }}</span>
        @endforeach
    @endif
</div>

</div>

@section('javascript')
    @parent

    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>

        $('#domain_under').on('change',function() {
            if($(this).val() == 'Marvill') {
                $('#domain_marvill').toggleClass('hidden',false)
            } else {
                $('#domain_marvill').toggleClass('hidden',true)
            }
        });
        $('#hosting_under').on('change',function() {
            if($(this).val() == 'Marvill') {
                $('#hosting_marvill').toggleClass('hidden',false)
            } else {
                $('#hosting_marvill').toggleClass('hidden',true)
            }
        });
        $('#email_provider_under').on('change',function() {
            if($(this).val() == 'Gmail Marvill') {
                $('#email_provider_marvill').toggleClass('hidden',false)
            } else {
                $('#email_provider_marvill').toggleClass('hidden',true)
            }
        });

        $('#domain_start_billing_date').datepicker({autoclose:true});
        $('#hosting_start_billing_date').datepicker({autoclose:true});
        $('#email_provider_start_billing_date').datepicker({autoclose:true});
    </script>
    </script>
@endsection
