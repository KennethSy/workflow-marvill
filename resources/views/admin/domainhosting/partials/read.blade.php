 <div style="height:35px"></div>
<div class="col-xs-12" style="border-bottom:0;">
    <div class="ctit" >Domain Name</div>
</div>
<div class="col-xs-12" style="padding: 0px 30px 0px 15px;">
    <h4><b>{{$domain_and_hosting->domain_name}}</b>&nbsp;<a href="https://{{$domain_and_hosting->domain_name}}" target="_blank"><i class="fa fa-link" aria-hidden="true"></i>
</a></h4>
</div>
 
<div class="col-xs-12" style="border-bottom:0;">
     <div class="ctit" >Domain Under</div>
</div>
<div class="col-xs-12" style="padding: 0px 30px 0px 15px;">
    <h4><b>{{$domain_and_hosting->domain_under}}</b></h4>
</div>

<div class="@if($domain_and_hosting->domain_under != 'Marvill') hidden @endif">

     
    <div class="col-xs-12" style="border-bottom:0;">
       <div class="ctit" >Domain Start Billing Date</div>
    </div>
    <div class="col-xs-12" style="padding: 0px 30px 0px 15px;">
        <h4><b>{{$domain_and_hosting->domain_start_billing_date}}</b></h4>
    </div>

    <hr style="margin:0;">
    <div class="col-xs-12" style="border-bottom:0;">
       <div class="ctit" >Domain Billing Plan</div>
    </div>
    <div class="col-xs-12" style="padding: 0px 30px 0px 15px;">
        <h4><b>{{$domain_and_hosting->domain_billing_plan}}</b></h4>
    </div>

</div>

<hr style="margin:0;">
<div class="col-xs-12" style="border-bottom:0;">
   <div class="ctit" >Hosting Type</div>
</div>
<div class="col-xs-12" style="padding: 0px 30px 0px 15px;">
    <h4><b>{{$domain_and_hosting->hosting_type}}</b></h4>
</div>

<hr style="margin:0;">
<div class="col-xs-12" style="border-bottom:0;">
   <div class="ctit" >Hosting Under</div>
</div>
<div class="col-xs-12" style="padding: 0px 30px 0px 15px;">
    <h4><b>{{$domain_and_hosting->hosting_under}}</b></h4>
</div>

<div class="@if($domain_and_hosting->hosting_under != 'Marvill') hidden @endif">

    <hr style="margin:0;">
    <div class="col-xs-12" style="border-bottom:0;">
       <div class="ctit" >Hosting Start Billing Date</div>
    </div>
    <div class="col-xs-12" style="padding: 0px 30px 0px 15px;">
        <h4><b>{{$domain_and_hosting->hosting_start_billing_date}}</b></h4>
    </div>

    <hr style="margin:0;">
    <div class="col-xs-12" style="border-bottom:0;">
       <div class="ctit" >Hosting Billing Plan</div>
    </div>
    <div class="col-xs-12" style="padding: 0px 30px 0px 15px;">
        <h4><b>{{$domain_and_hosting->hosting_billing_plan}}</b></h4>
    </div>

</div>

<hr style="margin:0;">
<div class="col-xs-12" style="border-bottom:0;">
   <div class="ctit" >Email Provider Under</div>
</div>
<div class="col-xs-12" style="padding: 0px 30px 0px 15px;">
    <h4><b>{{$domain_and_hosting->email_provider_under}}</b></h4>
</div>

<div class="@if($domain_and_hosting->email_provider_under != 'Gmail Marvill') hidden @endif">

    <hr style="margin:0;">
    <div class="col-xs-12" style="border-bottom:0;">
       <div class="ctit" >Email Provider Start Billing Date</div>
    </div>
    <div class="col-xs-12" style="padding: 0px 30px 0px 15px;">
        <h4><b>{{$domain_and_hosting->email_provider_start_billing_date}}</b></h4>
    </div>

    <hr style="margin:0;">
    <div class="col-xs-12" style="border-bottom:0;">
       <div class="ctit" >Email Provider Billing Plan</div>
    </div>
    <div class="col-xs-12" style="padding: 0px 30px 0px 15px;">
        <h4><b>{{$domain_and_hosting->email_provider_billing_plan}}</b></h4>
    </div>

</div>

