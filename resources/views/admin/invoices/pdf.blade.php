<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <title>Marvill WEB Development</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap_trimmed.min.css')}}" />
    <style type="text/css">
        html {
            height:100%;
        }
        body {
            font-family: 'DejaVu Sans', sans-serif;
            margin-left: 0.3in;
            margin-right: 0.3in;
        }
        h1 {
            font-size: 2em;
            font-weight: bold;
        }
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }
        .row {
            margin-bottom: 5px;
        }
        .page {
            page-break-after:always;
        }
        #invoice-items br{
            display: block;
            margin-top: -6px;
            content: "";
        }
        .container-fluid{
            min-height: 100%;
            height: auto !important;
            height: 100%;
            margin: 0 auto -305px;
        }
        .footer, .push {
            height: 305px; /* .push must be the same height as .footer */
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <?php
        $provider = '';
        $total = 0;
        $is_subtotal = false;
        $grand_total = DB::table('tb_invoice_items')->where('invoice_id',$invoice->id)->groupBy('invoice_id')->select([
            DB::raw('COALESCE(SUM(cost),0) - COALESCE(SUM(discount),0) as grand_total')
        ])->first()->grand_total;
    ?>
    @foreach($pages as $line_page => $page)
        <div class="page">
            <div class="row" style="white-space:nowrap;">
                <div class="col-xs-2 vcenter" style="padding-left: 0px;">
                    <img src="https://v2workflow.marvill.com/images/logo-big.png" width="150"/>
                </div>
                <div class="col-xs-10 vcenter" >
                    <h1 style="margin-left: 10px;">Marvill Web Development Services</h1>
                </div>
            </div>
            <div class="row" style="white-space:nowrap;margin-top:35px;">
                <div class="col-xs-7 vcenter" style="padding-left: 0px;">
                    @if($invoice->status == 1)
                        <h1 style="text-align: left">ACKNOWLEDGEMENT RECEIPT</h1>
                    @else
                        <h1 style="text-align: left">{{strtoupper($invoice_form_type->name)}}</h1>
                    @endif
                </div>
                <div class="col-xs-5 vcenter" style="padding-left: 10px;">
                    <span style="margin-right:12px">Page</span>: {{$line_page}} of {{count($pages)}}
                </div>
            </div>
            @if($invoice->form_type == 'AR' || $invoice->status == 1)
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <span style="margin-right:47px">Date</span>: {{Carbon\Carbon::parse($invoice->date)->format('m/d/Y')}}
                    </div>
                    <div class="col-xs-5 ">
                        <b>Bill To:</b><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <span style="margin-right:17.5px">Cust Ref.</span>: {{$invoice->cust_ref}}
                    </div>
                    <div class="col-xs-5 ">
                        <b>{{$invoice->client}}</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <span style="margin-right:0px">Client Name</span>: {{strtoupper((!empty($invoice->last_name) ? $invoice->last_name : '').(!empty($invoice->first_name) ? ', '.$invoice->first_name : '').(!empty($invoice->middle_name) ? ' '.$invoice->middle_name : ''))}}
                    </div>
                    <div class="col-xs-5 ">
                        {{$invoice->client_address}}
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <span style="margin-right:20px">Invoice #</span>: {{$invoice->invoice_no}}
                    </div>
                    <div class="col-xs-5 ">
                        <b>Bill To:</b><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <span style="margin-right:47px">Date</span>: {{Carbon\Carbon::parse($invoice->date)->format('m/d/Y')}}
                    </div>
                    <div class="col-xs-5 ">
                        <b>{{$invoice->client}}</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <span style="margin-right:17.5px">Cust Ref.</span>: {{$invoice->cust_ref}}
                    </div>
                    <div class="col-xs-5 ">
                        {{$invoice->client_address}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="padding-left: 0px;">
                        <span style="margin-right:0px">Client Name</span>: {{strtoupper((!empty($invoice->last_name) ? $invoice->last_name : '').(!empty($invoice->first_name) ? ', '.$invoice->first_name : '').(!empty($invoice->middle_name) ? ' '.$invoice->middle_name : ''))}}
                    </div>
                </div>
            @endif


            <div class="row" style="margin-top:30px;border-top: 1px solid black; border-bottom: 1px solid black;">
                <div class="col-xs-7" style="padding-left: 0px;">
                    DESCRIPTION OF SERVICES
                </div>
                <div class="col-xs-2" style="text-align:right;">

                </div>
                <div class="col-xs-3" style="text-align:right;">
                    USD EQUIVALENT
                </div>
            </div>

            <div id="invoice-items">
            @foreach($page['invoice_items'] as $invoice_item)
                <?php
                $total += $invoice_item->cost;
                $total -= $invoice_item->discount;
                if($invoice_item->discount != null && $is_subtotal == false) {
                    $is_subtotal = true;
                }
                ?>
                @if($provider != $invoice_item->provider)
                    @if($is_subtotal)
                        <div class="row" style="padding-left:22.5px;">
                            <div class="col-xs-7" style="padding-left: 22.5px;">
                                <b>SUBTOTAL</b>
                            </div>
                            <div class="col-xs-2" style="text-align:right;">
                                Php {{number_format($total,2)}}
                            </div>
                            <div class="col-xs-3" style="text-align:right;">
                                $ 0.00
                            </div>
                        </div>
                    @endif
                    <?php
                    $provider = $invoice_item->provider;
                    $is_subtotal = false;
                    ?>
                    <div class="row" style="padding-left:7.5px;">
                        <div class="col-xs-7" style="padding-left: 0px;">
                            <b>{{$invoice_item->provider}}</b>
                        </div>
                        <div class="col-xs-2" style="text-align:right;">
                        </div>
                        <div class="col-xs-3" style="text-align:right;">
                        </div>
                    </div>
                @endif
                <div class="row" style="padding-left:15px;">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <?php
                        $line_provider = explode("\n", wordwrap($invoice_item->service, 50));
                        foreach($line_provider as $lpkey=>$str_provider) {
                            echo "&nbsp;&nbsp;&nbsp;".strtoupper($str_provider);
                            if($lpkey+1!=count($line_provider)) echo "<br>";
                        }
                        ?>
                    </div>
                    <div class="col-xs-2" style="text-align:right;">
                        @if($invoice_item->cost != null)
                            Php {{number_format($invoice_item->cost,2)}}
                        @else
                            (Php -{{number_format($invoice_item->discount,2)}})
                        @endif
                    </div>
                    <div class="col-xs-3" style="text-align:right;">
                        $ 0.00
                    </div>
                </div>
            @endforeach
            </div>

            @if($is_subtotal)
                <div class="row" style="padding-left:22.5px;">
                    <div class="col-xs-7" style="padding-left: 22.5px;">
                        <b>SUBTOTAL</b>
                    </div>
                    <div class="col-xs-2" style="text-align:right;">
                        Php {{number_format($total,2)}}
                    </div>
                    <div class="col-xs-3" style="text-align:right;">
                        $ 0.00
                    </div>
                </div>
            @endif
            <div class="row" style="border-top: 1px solid black;">
                <div class="col-xs-6" style="padding-left: 0px;">
                </div>
                <div class="col-xs-3" style="padding-left: 0px;text-align:right;">
                    <b>TOTAL&nbsp;Php {{number_format($grand_total,2)}}</b>
                </div>
                <div class="col-xs-3" style="text-align:right;">
                    <b>$ 0.00</b>
                </div>
            </div>

        </div>
    @endforeach
</div>
<div class="footer">
    @if($invoice->status == 1)
        <div id="logo" style="position:absolute;opacity: 1;">
            <img style="width:170px; height:170px; margin-left:640px;" src="https://v2workflow.marvill.com/images/PAID.png" alt="">
        </div>
    @endif
    <div style="">
        <i>
            Please make check payable to:<br>
            <b>MARVILL WEB DEVELOPMENT SERVICES</b><br><br>
            <u>Bank Details:</u><br>
            Bank Name: BPI Pedro Gil Branch<br>
            Account Name: Ryan John L. Villanueva <br>
            Account Number: 4930 0007 31 - Current Account<br>
            <br>
            Bank Name: China Banking Corporation - Pasong Tamo Bagtikan Branch <br>
            Account Name: Marvill Web Development Services<br>
            Account Number: 326-0091-814 - Savings Account<br>
            Swift Code:CHBKPHMM <br>
            <br><br>
            Thank you for your business!
            @if($created_by != null)
            <span style="margin-left:450px;text-align: right;">
                Prepared by: {{$created_by}}
            </span>
            @endif
        </i>
    </div>

</div>

</body>
</html>
