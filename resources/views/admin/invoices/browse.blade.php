@extends('voyager::master')

@section('page_title', 'Invoices')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-documentation"></i> Invoices
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" class="select_all">
                                        </th>
                                        <th>Client</th>
                                        <th>Invoice No.</th>
                                        <th>Date</th>
                                        <th>Cust. Ref</th>
                                        <th>Created By</th>
                                        <th>Updated By</th>
                                        <th>Status</th>
                                        <th style="text-align: right;">Total</th>
                                        <th style="min-width:300px;">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} Invoice ?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal modal-success fade" tabindex="-1" id="copy_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class=""></i> Copy and Edit Invoice ?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="copy_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-success pull-right delete-confirm" value="Yes, Copy It!">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal modal-success fade" tabindex="-1" id="paid_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Mark as Paid?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="paid_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-success pull-right" value="Mark as Paid">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
    <style>
        table .btn.btn-success {
            color: #2CBB6B !important;
            opacity: .9;
            background: transparent !important;
            border: 2px solid #2CBB6B !important;
            border-radius: 10px!important;
            font-weight: 900;
        }
        table .btn.btn-danger {
            color: #E32A05 !important;
            opacity: .9;
            background: transparent !important;
            border: 2px solid #E32A05 !important;
            border-radius: 10px!important;
            font-weight: 900;
            margin-left: 0px!important;
        }
    </style>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $('#dataTable').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{route('voyager.invoices.destroy',['id' => '__id'])}}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        $('#dataTable').on('click', '.copy', function (e) {
            $('#copy_form')[0].action = '{{route('invoices.copy',['id' => '__id'])}}'.replace('__id', $(this).data('id'));
            $('#copy_modal').modal('show');
        });

        $('#dataTable').on('click', '.paid-btn', function (e) {
            $('#paid_form').attr('data-id',$(this).attr('data-id'));
            $('#paid_form')[0].action = '{{route('invoices.markpaid',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#paid_modal').modal('show');
        });

        $(document).ready(function () {
            var table = $('#dataTable').DataTable({
                order: [ 0, 'desc' ],
                processing: true,
                serverSide: true,
                ajax: '{{route('invoices.data')}}',
                columnDefs: [
                    {
                        targets: 8,
                        className: 'dt-body-right'
                    }
                ],
                columns: [
                    {data: 'id', name: 'tb_invoices.id'},
                    {data: 'client', name: 'tb_invoices.client'},
                    {data: 'invoice_no', name: 'tb_invoices.invoice_no'},
                    {data: 'date', name: 'tb_invoices.date', searchable: false},
                    {data: 'cust_ref', name: 'tb_invoices.cust_ref'},
                    {data: 'created_by', name: 'create_user.name', searchable: false},
                    {data: 'updated_by', name: 'update_user.name', searchable: false},
                    {data: 'status', name: 'tb_invoice_status.value', searchable: false},
                    {data: 'total', name: 'total', searchable: false},
                    {data: 'actions', name: 'actions', orderable: false, searchable: false},
                ]
            });
        });

        $("#paid_form").on('submit',function(e) {
            e.preventDefault();
            var value = $(this).find("input[type=submit]").val();
            $(this).find("input[type=submit]").val('Please Wait..').prop('disabled',true);
            var $t = $(this);
            $.ajax({
                url: $(this).attr('action'),
                type: "post",
                data: $(this).serialize() ,
                success: function (response) {
                    toastr.success(response.message);
                    $t.find("input[type=submit]").val(value).prop('disabled',false);
                    $('#status-'+$t.attr('data-id')).html(response.status);
                    $('#button-'+$t.attr('data-id')).html(response.button);
                    $('#paid_modal').modal('hide');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    toastr.error('Something went wrong.');
                    $t.find("input[type=submit]").val(value).prop('disabled',false);
                    $('#paid_modal').modal('hide');
                }
            });
        });

        $("#delete_form").on('submit',function(e) {
            var value = $(this).find("input[type=submit]").val();
            $(this).find("input[type=submit]").val('Please Wait..').prop('disabled',true);
        });

        $("#copy_form").on('submit',function(e) {
            var value = $(this).find("input[type=submit]").val();
            $(this).find("input[type=submit]").val('Please Wait..').prop('disabled',true);
        });
    </script>
@stop
