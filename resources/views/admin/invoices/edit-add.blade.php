@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .image-area {
            position: relative;
            width: 120px;
            background: #333;
        }

        .image-area img {
            max-width: 100%;
            height: auto;
        }

        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.5), inset 0 2px 4px rgba(0, 0, 0, 0.3);
            text-shadow: 0 1px 2px rgba(0, 0, 0, 0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }

        .remove-image:hover {
            background: #E54E4E;
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }

        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }
    </style>
@stop

@section('page_title', ($edit ? 'Edit' : 'Add').' Invoice')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-documentation"></i>
        {{ ($edit ? 'Edit' : 'Add').' Invoice' }}
    </h1>
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add form-horizontal"
                          action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $invoice->id) : route('voyager.'.$dataType->slug.'.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif
                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}
                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="row">
                                <div class="form-group col-md-6 {{ $errors->has('form_type') ? 'has-error' : '' }}">
                                    <label class="col-md-4 control-label" for="form_type">Form Type</label>
                                    <div class="col-md-8">
                                        <select id="form_type" class="form-control" name="form_type" required>
                                            @foreach($form_types as $form_type)
                                                <option @if($invoice->form_type && $invoice->form_type == $form_type->value) selected @endif value="{{$form_type->value}}">{{$form_type->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if($errors->has('form_type'))
                                        @foreach ($errors->get('form_type') as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="form-group col-md-6 {{ $errors->has('company') ? 'has-error' : '' }}">
                                    <label class="col-md-8 control-label" for="company">Bill to: </label>
                                    <div class="col-md-4">
                                        <select id="company" class="form-control select2" name="company">
                                            <option value="">Please select one</option>
                                            @foreach($companies as $company)
                                                <option value="{{$company->id}}" @if($invoice->client == $company->company_name ) selected @endif>{{$company->company_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if($errors->has('company'))
                                        @foreach ($errors->get('company') as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6 {{ $errors->has('invoice_no') ? 'has-error' : '' }}">
                                    <label class="col-md-4 control-label" for="invoice_no">Invoice No.</label>
                                    <div class="col-md-8">
                                        <input id="invoice_no" class="form-control" type="text" name="invoice_no" value="{{$invoice->invoice_no}}" required/>
                                    </div>
                                    @if($errors->has('invoice_no'))
                                        @foreach ($errors->get('invoice_no') as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="col-md-6"></div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 {{ $errors->has('date') ? 'has-error' : '' }}">
                                    <label class="col-md-4 control-label" for="date">Date *</label>
                                    <div class="input-group col-md-8" style="padding-left: 15px;padding-right: 15px;">
                                        <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                        <input id="date" class="form-control" type="text" name="date" value="{{$invoice->date}}" required/>
                                    </div>
                                    @if($errors->has('date'))
                                        @foreach ($errors->get('date') as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="col-md-6"></div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 {{ $errors->has('cust_ref') ? 'has-error' : '' }}">
                                    <label class="col-md-4 control-label" for="cust_ref">Cust Ref.</label>
                                    <div class="col-md-8">
                                        <input id="cust_ref" class="form-control" type="text" name="cust_ref" value="{{$invoice->cust_ref}}" required/>
                                    </div>
                                    @if($errors->has('cust_ref'))
                                        @foreach ($errors->get('cust_ref') as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="col-md-6"></div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="col-md-2 control-label" for="">Client Name</label>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="last_name" value="{{$invoice->last_name}}" placeholder="Last Name"/>
                                    </div>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="first_name" value="{{$invoice->first_name}}" placeholder="First Name"/>
                                    </div>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="middle_name" value="{{$invoice->middle_name}}" placeholder="Middle Name"/>
                                    </div>
                                </div>
                            </div>

                            <table id="invoice-table" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Service Type</th>
                                    <th>
                                        <span class="col-md-5">Specific Service</span>
                                        <span class="col-md-2">Cost</span>
                                        <span class="col-md-2">Discount</span>
                                        <span class="col-md-3">Currency</span>
                                    </th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody id="tableBody">
                                @if(count($invoice_items)>0)
                                    <?php
                                        $provider = null;
                                        $line_page = '';
                                        $invoice_key = 0;
                                        $service_key = 0;
                                    ?>
                                    @foreach($invoice_items as $invoice_item_key => $invoice_item)
                                        <?php
                                            $repeat_provider = true;

                                            if($provider != $invoice_item->provider) {
                                                $provider = $invoice_item->provider;
                                                $repeat_provider = false;
                                            }
                                        ?>
                                        @if(!$repeat_provider)
                                            <tr>
                                                <td>
                                                    <div width="100%" style="padding-top: 5px; padding-bottom: 5px;">
                                                        <textarea class="provider form-control" rows="3" name="provider[{{$invoice_key}}]" style=" width: 92%;">{{$provider}}</textarea>
                                                    </div>
                                                </td>
                                                <td>
                                        @endif

                                        <div class="row serviceSpan" style="margin-bottom: 0px;">
                                            <div class="col-md-5">
                                                <textarea rows="3" class="service form-control" name="service[{{$invoice_key}}][{{$service_key}}]" style="">{{$invoice_item->service}}</textarea>&nbsp;
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" name="cost[{{$invoice_key}}][{{$service_key}}]" class="cost input-small form-control" maxlength="12" value="{{$invoice_item->cost?number_format($invoice_item->cost,2,".",""):''}}" placeholder="Cost"/>&nbsp;
                                            </div>
                                            <div class="col-md-2">
                                                <input type="text" name="discount[{{$invoice_key}}][{{$service_key}}]" class="discount input-small form-control" maxlength="12" value="{{$invoice_item->discount?number_format($invoice_item->discount,2,".",""):""}}" placeholder="Discount"/>
                                            </div>
                                            <div class="col-md-2">
                                                <select class="currency input-small form-control" name="currency[{{$invoice_key}}][{{$service_key}}]">
                                                    <option value="PHP" @if($invoice_item->currency == 'PHP') selected @endif>PHP</option>
                                                    <option value="USD" @if($invoice_item->currency == 'USD') selected @endif>USD</option>
                                                </select>
                                            </div>
                                            <div class="col-md-1">
                                                <button type="button" class="btn deleteService">
                                                    <span class="voyager-x"></span>
                                                </button>
                                            </div>
                                        </div>
                                        <?php
                                        $repeat_line_page = true;

                                        if(!isset($invoice_items[$invoice_item_key + 1])) {
                                            $repeat_line_page = false;
                                            $invoice_key++;
                                        } else if($provider != $invoice_items[$invoice_item_key + 1]->provider) {
                                            $line_page = $invoice_item->line_page;
                                            $repeat_line_page = false;
                                            $invoice_key++;
                                        }

                                        $service_key++;
                                        ?>
                                        @if(!$repeat_line_page)
                                                </td>
                                                <td>
                                                    <div width="100%" style="padding-top: 5px; padding-bottom: 5px;">
                                                        <button type="button" class="btn addServiceBtn" style="vertical-align: top;">Add Service</button>&nbsp;
                                                        <button type="button" class="btn deleteSingle">
                                                            <span class="voyager-x"></span>
                                                        </button>
                                                    </div>
                                                    <div width="100%" style="padding-top: 5px; padding-bottom: 5px;">
                                                        <select class="line_page input-small  form-control" name="line_page[{{$invoice_key-1}}]">
                                                            <option value="1" @if($line_page == 1) selected @endif>Page 1</option>
                                                            <option value="2" @if($line_page == 2) selected @endif>Page 2</option>
                                                            <option value="3" @if($line_page == 3) selected @endif>Page 3</option>
                                                            <option value="4" @if($line_page == 4) selected @endif>Page 4</option>
                                                            <option value="5" @if($line_page == 5) selected @endif>Page 5</option>
                                                        </select>
                                                    </div>
                                                </td>

                                            </tr>
                                        @endif
                                    @endforeach
                                @else
                                    <tr>
                                        <td>
                                            <div width="100%" style="padding-top: 5px; padding-bottom: 5px;">
                                                <textarea class="provider form-control" rows="3" name="provider[]" style=" width: 92%;"></textarea>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="row serviceSpan" style="margin-bottom: 0px;">
                                                <div class="col-md-5">
                                                    <textarea rows="3" class="service form-control" name="service[][]" style=""></textarea>&nbsp;
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" name="cost[][]" class="cost input-small form-control" maxlength="12" value="" placeholder="Cost"/>&nbsp;
                                                </div>
                                                <div class="col-md-2">
                                                    <input type="text" name="discount[][]" class="discount input-small form-control" maxlength="12" value="" placeholder="Discount"/>
                                                </div>
                                                <div class="col-md-2">
                                                    <select class="currency input-small form-control" name="currency[][]">
                                                        <option value="PHP" selected>PHP</option>
                                                        <option value="USD">USD</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-1">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div width="100%" style="padding-top: 5px; padding-bottom: 5px;">
                                                <button type="button" class="btn addServiceBtn" style="vertical-align: top;">Add Service</button>&nbsp;
                                                <button type="button" class="btn deleteSingle">
                                                    <span class="voyager-x"></span>
                                                </button>
                                            </div>
                                            <div width="100%" style="padding-top: 5px; padding-bottom: 5px;">
                                                <select class="line_page input-small  form-control" name="line_page[]">
                                                    <option value="1" selected>Page 1</option>
                                                    <option value="2">Page 2</option>
                                                    <option value="3">Page 3</option>
                                                    <option value="4">Page 4</option>
                                                    <option value="5">Page 5</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                            <br>
                            <div style="float: right;">
                                <button type="button" id="add_row" class="btn btn-default">Add New Row</button>
                            </div>
                            <div style="clear: both;"></div>
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div><!-- panel-body -->

                    </form>

                </div>
            </div>
        </div>

        <div id="service-template" class="hidden">
            <div class="row serviceSpan" style="margin-bottom: 0px;">
                <div class="col-md-5">
                    <textarea rows="3" class="service form-control" name="service[][]" style=""></textarea>&nbsp;
                </div>
                <div class="col-md-2">
                    <input type="number" name="cost[][]" class="cost input-small form-control" value="" placeholder="Cost"/>&nbsp;
                </div>
                <div class="col-md-2">
                    <input type="number" name="discount[][]" class="discount input-small form-control" value="" placeholder="Discount"/>
                </div>
                <div class="col-md-2">
                    <select class="currency input-small form-control" name="currency[][]">
                        <option value="PHP" selected>PHP</option>
                        <option value="USD">USD</option>
                    </select>
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn deleteService">
                        <span class="voyager-x"></span>
                    </button>
                </div>
            </div>
        </div>

        <table id="row-template" class="hidden">
            <tr>
                <td>
                    <div width="100%" style="padding-top: 5px; padding-bottom: 5px;">
                        <textarea class="provider form-control" rows="3" name="provider[]" style=" width: 92%;"></textarea>
                    </div>
                </td>
                <td>
                    <div class="row serviceSpan" style="margin-bottom: 0px;">
                        <div class="col-md-5">
                            <textarea rows="3" class="service form-control" name="service[][]" style=""></textarea>&nbsp;
                        </div>
                        <div class="col-md-2">
                            <input type="text" name="cost[][]" class="cost input-small form-control" maxlength="12" value="" placeholder="Cost"/>&nbsp;
                        </div>
                        <div class="col-md-2">
                            <input type="text" name="discount[][]" class="discount input-small form-control" maxlength="12" value="" placeholder="Discount"/>
                        </div>
                        <div class="col-md-2">
                            <select class="currency input-small form-control" name="currency[][]">
                                <option value="PHP" selected>PHP</option>
                                <option value="USD">USD</option>
                            </select>
                        </div>
                        <div class="col-md-1">
                        </div>
                    </div>
                </td>
                <td>
                    <div width="100%" style="padding-top: 5px; padding-bottom: 5px;">
                        <button type="button" class="btn addServiceBtn" style="vertical-align: top;">Add Service</button>&nbsp;
                        <button type="button" class="btn deleteSingle">
                            <span class="voyager-x"></span>
                        </button>
                    </div>
                    <div width="100%" style="padding-top: 5px; padding-bottom: 5px;">
                        <select class="line_page input-small  form-control" name="line_page[]">
                            <option value="1" selected>Page 1</option>
                            <option value="2">Page 2</option>
                            <option value="3">Page 3</option>
                            <option value="4">Page 4</option>
                            <option value="5">Page 5</option>
                        </select>
                    </div>
                </td>
            </tr>
        </table>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $('textarea').each(function () {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
        });

        $('#invoice-table').on('input', 'textarea', function () {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
        });

        $('#date').datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true,
        });

        function manageServiceForm() {
            $('#invoice-table').find('tbody>tr').each(function (i) {
                var $tr = $(this);
                $tr.find('.serviceSpan').each(function (j) {
                    var $serviceSpan = $(this);
                    $serviceSpan.find('.service').attr('name', 'service[' + i + '][' + j + ']');
                    $serviceSpan.find('.cost').attr('name', 'cost[' + i + '][' + j + ']');
                    $serviceSpan.find('.discount').attr('name', 'discount[' + i + '][' + j + ']');
                    $serviceSpan.find('.currency').attr('name', 'currency[' + i + '][' + j + ']');
                });
            });
        }

        $('#invoice-table').on('click', '.deleteService', function (e) {
            $(e.target).closest('.serviceSpan').remove();
            manageServiceForm();
        });

        $('#invoice-table').on('click', '.addServiceBtn', function (e) {
            $(e.target).closest('tr').find('.serviceSpan').closest('td').append($("#service-template").children().clone());
            manageServiceForm();
            $('textarea').trigger('input');
        });

        $('#add_row').on('click', function () {
            $('#tableBody').append($("#row-template tbody").children().clone());
            manageServiceForm();
            $('textarea').trigger('input');
        });

        $('#invoice-table').on('click', '.deleteSingle', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var $this = $(this),
                c = confirm('Are you sure you want to delete this row?');
            if (!c) {
                return false;
            }
            $(e.target).closest('tr').fadeOut(function () {
                $(this).remove();
                manageServiceForm();
            });

        });

    </script>
@stop
