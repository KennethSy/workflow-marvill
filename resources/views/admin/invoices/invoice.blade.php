<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Marvill WEB Development</title>
    <style type="text/css">
        html {
            height: 100% !important;
            width: 100%; !important;
        }
        body {
            font-family: 'DejaVu Sans', sans-serif;
            font-size: 0.11in;
            height: 100% !important;
            width: 100%; !important;
            margin-left: 0.75in;
            margin-right: 0.75in;
        }
        .page { page-break-before:always; }
        h1, h2, h3 {
            padding: 3px 0px 3px 0px;
            margin: 3px 0px 3px 0px;
        }

        #logo {
            position: fixed;
            bottom: 28%;
            margin-right: 100px;
            opacity: 1;
        }
    </style>
</head>
<body>
    <table align="center" style="width: 100%;" cellspacing="">
        <tr>
            <td width="20%">
                <img src="https://v2workflow.marvill.com/images/logo-big.png" width="120"/></td><td width="80%">
                <h1>Marvill Web Development Services</h1>
            </td>
        </tr>
    </table>
    <div class="page-first">
    <br><br><br>
    <table style="table-layout: fixed; width: 670px;">
        <tbody><tr>
            <td width="435px"><h1 style="text-align: left">ACKNOWLEDGEMENT RECEIPT</h1></td><td width="235px"><span style="margin-right:12px">Page</span>: 1 of 1</td>
        </tr>
        <tr>
            <td width="435px"><span style="margin-right:41px">Date</span>: 03/06/2020</td>
            <td width="235px" rowspan="4" valign="top">
                <b>Bill To:</b><br>
                <b>Kane Chua</b><br>69 A.C Raymond Brgy. Rosario Pasig City</td>
        </tr>
        <tr>
            <td width="435px"><span style="margin-right:17.5px">Cust Ref.</span>: GPS Monthly Subscription</td>
        </tr>
        <tr>
            <td width="435px"><span style="margin-right:0px">Client Name</span>: CHUA, KANE</td>
        </tr>
        </tbody>
    </table>
    <br>
    <table style="table-layout: fixed; width: 670px; border-bottom: 1px solid black;">
        <tbody><tr>
            <td colspan="3">
                <table style="width: 100%; border-top: 1px solid black; border-bottom: 1px solid black;">
                    <tbody><tr>
                        <td width="60%">DESCRIPTION OF SERVICES</td>
                        <td width="20%" align="right">&nbsp;</td>
                        <td width="20%" align="right">USD EQUIVALENT</td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="3">
                <b>
                    &nbsp;&nbsp;&nbsp;MONTHLY SUBSCRIPTION 2019 </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table style="width: 100%;">
                    <tbody><tr>
                        <td width="60%">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MAY TO DEC 2019 AT 2600 PER MONTH </td>
                        <td width="20%" align="right" valign="bottom"><span style="font-family: DejaVu Sans;">₱</span> 20,800.00&nbsp;</td>
                        <td width="20%" align="right" valign="bottom">$ 0.00&nbsp;</td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td width="100%" colspan="3">
                <b>
                    &nbsp;&nbsp;&nbsp;MONTHLY SUBSCRIPTION 2020 </b>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table style="width: 100%;">
                    <tbody><tr>
                        <td width="60%">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JAN TO MARCH 2020 AT 2600 PER MONTH </td>
                        <td width="20%" align="right" valign="bottom"><span style="font-family: DejaVu Sans;">₱</span> 10,400.00&nbsp;</td>
                        <td width="20%" align="right" valign="bottom">$ 0.00&nbsp;</td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        </tbody></table>
    <table style="table-layout: fixed; width: 670px;">
        <tbody><tr>
            <td width="60%">&nbsp;</td>
            <td width="20%" align="right"><b>TOTAL&nbsp;<span style="font-family: DejaVu Sans;">₱</span>31,200.00</b></td>
            <td width="20%" align="right"><b>$ 0.00</b>&nbsp;</td>
        </tr>
        </tbody>
    </table>
    <div style="position:fixed; bottom:0px; height: 245px;">
        <i>
            Please make check payable to:<br>
            <b>MARVILL WEB DEVELOPMENT SERVICES</b><br><br>
            <u>Bank Details:</u><br>
            Bank Name: BPI Pedro Gil Branch<br>
            Account Name: Ryan John L. Villanueva <br>
            Account Number: 4930 0007 31 - Current Account<br>
            <br>
            Bank Name: China Banking Corporation - Pasong Tamo Bagtikan Branch <br>
            Account Name: Marvill Web Development Services<br>
            Account Number: 326-0091-814 - Savings Account<br>
            Swift Code:CHBKPHMM <br>
            <br><br>
            Thank you for your business!
        </i>
    </div>
    <div id="logo" style="">
        <img style="width:170px; height:170px; margin-left:500px;" src="https://v2workflow.marvill.com/images/PAID.png" alt="">
    </div>
    <div style="position:fixed; bottom:0px; text-align: right; height: 25px;">
        <i>
            Prepared by: Ryan John Villanueva
        </i>
    </div>
    </div>
</body>
</html>
