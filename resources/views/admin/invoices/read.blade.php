<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Marvill WEB Development</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap_trimmed.min.css')}}" />
    <style type="text/css">
        html {
            height:100%;
        }
        body {
            font-family: 'DejaVu Sans', sans-serif;
            margin-left: 0.3in;
            margin-right: 0.3in;
            background: #64696f;
        }
        h1 {
            font-size: 2em;
            font-weight: bold;
        }
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }
        .row {
            margin-bottom: 5px;
        }
        .invoice-status-0 {
            background: 0 0;
            color: #d9534f;
        }
        .invoice-status-2 {
            background: 0 0;
            color: #eea236;
        }
        .invoice-status-1 {
            background: 0 0;
            color: #5cb85c;
        }
        .panel_paper {
            border: none;
            -webkit-box-shadow: 0 1px 15px 1px rgba(90,90,90,.08);
            box-shadow: 0 1px 15px 1px rgba(90,90,90,.08);
            padding-top: 30px;
            padding-bottom: 100px;
            background-color: white;
            margin-top: 100px;
        }
    </style>
</head>
<body>
<div class="container">
    <div  class="row" style="z-index: 10; position: fixed; width: 100%; top: 0px;left: 1%;background-color: white">
        <div class="col-md-12">
            <div class="pull-left">
                <h3>{{$invoice->invoice_no}}</h3>
                <h4 class="invoice-html-status">
                    <span class="label invoice-status-{{$invoice_status->id}}">
                        {{$invoice_status->value}}
                    </span>
                </h4>
            </div>
            <form action="#" method="post" class="pull-right" style="margin-top: 20px;">
                <a href="{{route('voyager.invoices.index')}}" class="btn btn-default">
                    Back to Invoices
                </a>
                <a target="_blank" href="{{route('voyager.invoices.show',['id' => $invoice->id,'type' => 'pdf'])}}" class="btn btn-success">
                    Preview
                </a>
                {{csrf_field()}}
                <a target="_blank" href="{{route('voyager.invoices.show',['id' => $invoice->id,'type' => 'download'])}}" class="btn btn-default download-btn">
                    Download
                </a>
            </form>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="panel_paper row">
        <div class="col-md-10 col-md-offset-1">
            <div class="row" style="white-space:nowrap;">
                <div class="col-xs-2 vcenter" style="padding-left: 0px;">
                    <img src="https://v2workflow.marvill.com/images/logo-big.png" width="150"/>
                </div>
                <div class="col-xs-10 vcenter" >
                    <h1 style="margin-left: 10px;">Marvill Web Development Services</h1>
                </div>
            </div>
            <div class="row" style="white-space:nowrap;margin-top:35px;">
                <div class="col-xs-7 vcenter" style="padding-left: 0px;">
                    @if($invoice->status == 1)
                        <h1 style="text-align: left">ACKNOWLEDGEMENT RECEIPT</h1>
                    @else
                        <h1 style="text-align: left">{{strtoupper($invoice_form_type->name)}}</h1>
                    @endif
                </div>
                <div class="col-xs-5 vcenter" style="padding-left: 10px;">
                    <span style="margin-right:12px">Page</span>: 1 of 1
                </div>
            </div>
            @if($invoice->form_type == 'AR' || $invoice->status == 1)
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <span style="margin-right:47px">Date</span>: {{Carbon\Carbon::parse($invoice->date)->format('m/d/Y')}}
                    </div>
                    <div class="col-xs-5 ">
                        <b>Bill To:</b><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <span style="margin-right:17.5px">Cust Ref.</span>: {{$invoice->cust_ref}}
                    </div>
                    <div class="col-xs-5 ">
                        <b>{{$invoice->client}}</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <span style="margin-right:0px">Client Name</span>: {{strtoupper((!empty($invoice->last_name) ? $invoice->last_name : '').(!empty($invoice->first_name) ? ', '.$invoice->first_name : '').(!empty($invoice->middle_name) ? ' '.$invoice->middle_name : ''))}}
                    </div>
                    <div class="col-xs-5 ">
                        {{$invoice->client_address}}
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <span style="margin-right:20px">Invoice #</span>: {{$invoice->invoice_no}}
                    </div>
                    <div class="col-xs-5 ">
                        <b>Bill To:</b><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <span style="margin-right:47px">Date</span>: {{Carbon\Carbon::parse($invoice->date)->format('m/d/Y')}}
                    </div>
                    <div class="col-xs-5 ">
                        <b>{{$invoice->client}}</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <span style="margin-right:17.5px">Cust Ref.</span>: {{$invoice->cust_ref}}
                    </div>
                    <div class="col-xs-5 ">
                        {{$invoice->client_address}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="padding-left: 0px;">
                        <span style="margin-right:0px">Client Name</span>: {{strtoupper((!empty($invoice->last_name) ? $invoice->last_name : '').(!empty($invoice->first_name) ? ', '.$invoice->first_name : '').(!empty($invoice->middle_name) ? ' '.$invoice->middle_name : ''))}}
                    </div>
                </div>
            @endif

            <div class="row" style="margin-top:30px;border-top: 1px solid black; border-bottom: 1px solid black;">
                <div class="col-xs-7" style="padding-left: 0px;">
                    DESCRIPTION OF SERVICES
                </div>
                <div class="col-xs-2" style="text-align:right;">

                </div>
                <div class="col-xs-3" style="text-align:right;">
                    USD EQUIVALENT
                </div>
            </div>
            <?php
            $provider = '';
            $total = 0;
            $is_subtotal = false;
            ?>
            @foreach($invoice_items as $invoice_item)
                <?php
                $total += $invoice_item->cost;
                $total -= $invoice_item->discount;
                if($invoice_item->discount != null && $is_subtotal == false) {
                    $is_subtotal = true;
                }
                ?>
                @if($provider != $invoice_item->provider)
                    @if($is_subtotal)
                        <div class="row" style="padding-left:22.5px;">
                            <div class="col-xs-7" style="padding-left: 22.5px;">
                                <b>SUBTOTAL</b>
                            </div>
                            <div class="col-xs-2" style="text-align:right;">
                                <span style="font-family: DejaVu Sans;">₱</span> {{number_format($total,2)}}
                            </div>
                            <div class="col-xs-3" style="text-align:right;">
                                $ 0.00
                            </div>
                        </div>
                    @endif
                    <?php
                    $provider = $invoice_item->provider;
                    $is_subtotal = false;
                    ?>
                    <div class="row" style="padding-left:7.5px;">
                        <div class="col-xs-7" style="padding-left: 0px;">
                            <b>{{$invoice_item->provider}}</b>
                        </div>
                        <div class="col-xs-2" style="text-align:right;">
                        </div>
                        <div class="col-xs-3" style="text-align:right;">
                        </div>
                    </div>
                @endif
                <div class="row" style="padding-left:15px;">
                    <div class="col-xs-7" style="padding-left: 0px;">
                        <?php
                        $line_provider = explode("\n", wordwrap($invoice_item->service, 50));
                        foreach($line_provider as $lpkey=>$str_provider) {
                            echo "&nbsp;&nbsp;&nbsp;".strtoupper($str_provider);
                            if($lpkey+1!=count($line_provider)) echo "<br>";
                        }
                        ?>
                    </div>
                    <div class="col-xs-2" style="text-align:right;">
                        @if($invoice_item->cost != null)
                            <span style="font-family: DejaVu Sans;">₱</span> {{number_format($invoice_item->cost,2)}}
                        @else
                            <span style="font-family: DejaVu Sans;">(-₱</span> {{number_format($invoice_item->discount,2)}})
                        @endif
                    </div>
                    <div class="col-xs-3" style="text-align:right;">
                        $ 0.00
                    </div>
                </div>
            @endforeach
            @if($is_subtotal)
                <div class="row" style="padding-left:22.5px;">
                    <div class="col-xs-7" style="padding-left: 22.5px;">
                        <b>SUBTOTAL</b>
                    </div>
                    <div class="col-xs-2" style="text-align:right;">
                        <span style="font-family: DejaVu Sans;">₱</span> {{number_format($total,2)}}
                    </div>
                    <div class="col-xs-3" style="text-align:right;">
                        $ 0.00
                    </div>
                </div>
            @endif
            <div class="row" style="border-top: 1px solid black;">
                <div class="col-xs-6" style="padding-left: 0px;">
                </div>
                <div class="col-xs-3" style="padding-left: 0px;text-align:right;">
                    <b>TOTAL&nbsp;<span style="font-family: DejaVu Sans;">₱</span> {{number_format($total,2)}}</b>
                </div>
                <div class="col-xs-3" style="text-align:right;">
                    <b>$ 0.00</b>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="position:fixed; bottom:0px; height: 245px;" class="hidden">
    <i>
        Please make check payable to:<br>
        <b>MARVILL WEB DEVELOPMENT SERVICES</b><br><br>
        <u>Bank Details:</u><br>
        Bank Name: BPI Pedro Gil Branch<br>
        Account Name: Ryan John L. Villanueva <br>
        Account Number: 4930 0007 31 - Current Account<br>
        <br>
        Bank Name: China Banking Corporation - Pasong Tamo Bagtikan Branch <br>
        Account Name: Marvill Web Development Services<br>
        Account Number: 326-0091-814 - Savings Account<br>
        Swift Code:CHBKPHMM <br>
        <br><br>
        Thank you for your business!
    </i>
</div>
<div id="logo" style="position: fixed;bottom: 100px;right:0px;opacity: 1;"  class="hidden">
    <img style="width:170px; height:170px; margin-left:500px;" src="https://v2workflow.marvill.com/images/PAID.png" alt="">
</div>
<div style="position:fixed; bottom:0px;right:0px; text-align: right; height: 25px;"  class="hidden">
    <i>
        Prepared by: Ryan John Villanueva
    </i>
</div>
</body>
</html>
