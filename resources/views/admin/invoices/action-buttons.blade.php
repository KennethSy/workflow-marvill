@if($canEdit)
    <a style="text-decoration:none;" href="{{route('voyager.invoices.edit',['id' => $invoice->id ])}}" target="_blank" title="Edit" class="btn btn-primary"><i class="fas fa-edit"></i></a>
    <a style="text-decoration:none;" href="javascript:;" data-id="{{$invoice->id}}" title="Copy and Edit" class="btn btn-primary copy"><i class="fas fa-copy"></i></a>
@endif
@if($canView)
    <a style="text-decoration:none;" href="{{route('voyager.invoices.show',['id' => $invoice->id ])}}" target="_blank" title="View" class="btn btn-primary"><i class="fas fa-eye"></i></a>
    <a style="text-decoration:none;" href="{{route('voyager.invoices.show',['id' => $invoice->id, 'type' => 'download' ])}}" target="none" title="Download" class="btn btn-primary"><i class="fas fa-file-download"></i></a>
@endif
@if($invoice->status != 1 )
    <a style="text-decoration:none;" href="javascript:;" data-id="{{$invoice->id}}" class="btn btn-success paid-btn" title="Mark as Paid"><i class="fas fa-ruble-sign"></i></a>
@endif
@if($canDelete)
    <a style="text-decoration:none;" href="javascript:;" data-id="{{$invoice->id}}" title="Delete" class="btn btn-danger delete"><i class="fas fa-trash"></i></a>
@endif
