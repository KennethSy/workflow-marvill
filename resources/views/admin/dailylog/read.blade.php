@extends('voyager::master')

@section('page_title', 'Daily Log')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-bar-chart"></i> Daily Log
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td><b>Assigned To</b></td>
                                        <td>{{$user->name}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Working From</b></td>
                                        <td>{{$log->location}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Created At</b></td>
                                        <td>{{$log->created_at}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Updated At</b></td>
                                        <td>{{$log->updated_at}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Target Daily Rate</b></td>
                                        <td>{{$log->target_daily_rate}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Achieved Rate</b></td>
                                        <td>{{$log->achieved_rate}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Status</b></td>
                                        <td>{{$status->status}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Description</th>
                                        <th>PHP Value</th>
                                        <th>Created At</th>
                                        <th>Completed At</th>
                                        <th>Confirmed At</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($tasks as $task)
                                        <tr>
                                            <td>{{$task->description}}</td>
                                            <td>{{$task->value}}</td>
                                            <td>{{$task->created_at}}</td>
                                            <td>{{$task->completed_at}}</td>
                                            <td>{{$task->confirmed_at}}</td>
                                            <td>{{$task->task_status}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            var table = $('#dataTable').DataTable({
                order: [ 0, 'desc' ],
                processing: true,
                serverSide: false,
            });
        });
    </script>
@stop
