@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .image-area {
            position: relative;
            width: 120px;
            background: #333;
        }
        .image-area img{
            max-width: 100%;
            height: auto;
        }
        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
            text-shadow: 0 1px 2px rgba(0,0,0,0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }
        .remove-image:hover {
            background: #E54E4E;
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }
        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }
    </style>
@stop

@section('page_title', ($edit ? 'Edit' : 'Add').' Daily Log')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-bar-chart"></i>
        {{ ($edit ? 'Edit' : 'Add').' Daily Log' }}
    </h1>
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add form-horizontal"
                          action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $log->id) : route('voyager.'.$dataType->slug.'.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif
                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}
                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('assigned_to') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="assigned_to">Assigned To *</label>
                                <div class="col-sm-10">
                                    <select id="assigned_to" class="form-control select2" name="assigned_to" required>
                                        <option value="">Please select one</option>
                                        @foreach($users as $user)
                                            <option @if($log->assigned_to && $log->assigned_to == $user->id) selected @endif value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('assigned_to'))
                                    @foreach ($errors->get('assigned_to') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('working_from') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="working_from">Working From *</label>
                                <div class="col-sm-10">
                                    <select id="working_from" class="form-control select2" name="working_from" required>
                                        <option value="">Please select one</option>
                                        <option value="Home">Home</option>
                                        <option value="Office">Office</option>
                                    </select>
                                </div>
                                @if($errors->has('working_from'))
                                    @foreach ($errors->get('working_from') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>

    </script>
@stop
