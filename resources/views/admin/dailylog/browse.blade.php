@extends('voyager::master')

@section('page_title', 'Daily Log')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-bar-chart"></i> Daily Log
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" class="select_all">
                                        </th>
                                        <th>Assigned To</th>
                                        <th>Working From</th>
                                        <th>Created At</th>
                                        <th>Updated At</th>
                                        <th>Target Daily Rate</th>
                                        <th>Achieved Rate</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} Log ?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
    <style>
        .red-badge{
            background-color: #ed5564;
            color:white;
            padding:4px;
        }
        .green-badge{
            background-color: #43ac6e;
            color:white;
            padding:4px;
        }
    </style>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            var table = $('#dataTable').DataTable({
                order: [ 0, 'desc' ],
                processing: true,
                serverSide: true,
                ajax: '{{route('dailylog.data')}}',
                columns: [
                    {data: 'id', name: 'tb_daily_log.id'},
                    {data: 'assigned_user', name: 'users.name'},
                    {data: 'location', name: 'tb_daily_log.location'},
                    {data: 'created_at', name: 'tb_daily_log.created_at'},
                    {data: 'updated_at', name: 'tb_daily_log.updated_at'},
                    {data: 'target_daily_rate', name: 'tb_daily_log.target_daily_rate'},
                    {data: 'achieved_rate', name: 'tb_daily_log.achieved_rate'},
                    {data: 'log_status', name: 'tb_log_status.status'},
                    {data: 'actions', name: 'tb_daily_log', orderable: false, searchable: false},
                ]
            });
        });
    </script>
@stop
