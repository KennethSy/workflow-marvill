@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .image-area {
            position: relative;
            width: 120px;
            background: #333;
        }
        .image-area img{
            max-width: 100%;
            height: auto;
        }
        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
            text-shadow: 0 1px 2px rgba(0,0,0,0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }
        .remove-image:hover {
            background: #E54E4E;
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }
        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }
    </style>
@stop

@section('page_title', ($edit ? 'Edit' : 'Add').' Document')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-documentation"></i>
        {{ ($edit ? 'Edit' : 'Add').' Document' }}
    </h1>
@stop

@section('content')


    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add form-horizontal"
                          action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $record->id) : route('voyager.'.$dataType->slug.'.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif
                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}
                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            <div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="company">Company*</label>
                                <div class="col-sm-10">
                                    <select id="company_name" class="form-control" name="company_name">
                                        <option value="">Please select one</option>
                                        @foreach($companies as $company)
                                            <option @if($record->company_name && $record->company_name == $company->id) selected @endif value="{{$company->id}}">{{$company->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('comapny_name'))
                                    @foreach ($errors->get('company_name') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>


                            <div class="form-group {{ $errors->has('client_name') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="company">Client*</label>
                                <div class="col-sm-10">
                                    <select id="client_name" class="form-control" name="client_name">
                                        <option value="">Please select one</option>
                                        @foreach($clients as $client)
                                            <option @if($record->client_name && $record->client == $client->id) selected @endif value="{{$client->id}}">{{$client->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('client_name'))
                                    @foreach ($errors->get('client_name') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('client_contact_person') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="client_contact_person">Client Contact Person*</label>
                                <div class="col-sm-10">
                                    <input id="client_contact_person" class="form-control" type="text" name="client_contact_person" value="{{$record->client_contact_person}}" required/>
                                </div>
                                @if($errors->has('client_contact_person'))
                                    @foreach ($errors->get('client_contact_person') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('client_contact_person_position') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="client_contact_person_position">Client Contact Position*</label>
                                <div class="col-sm-10">
                                    <input id="client_contact_person_position" class="form-control" type="text" name="client_contact_person_position" value="{{$record->client_contact_person_position}}" required/>
                                </div>
                                @if($errors->has('client_contact_person_position'))
                                    @foreach ($errors->get('client_contact_person_position') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('document_type') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="document_type">Document Type*</label>
                                <div class="col-sm-10">
                                    <select id="document_type" class="form-control" name="document_type" required>
                                        <option value="">Please select one</option>
                                        <option @if($record->document_type == "Proposal") selected @endif value="Proposal">Proposal</option>
                                        <option @if($record->document_type == "Documentation") selected @endif value="Documentation">Documentation</option>

                                    </select>
                                </div>
                                @if($errors->has('document_type'))
                                    @foreach ($errors->get('document_type') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('proposal_type') ? 'has-error' : '' }} hidden">
                                <label class="col-sm-2 control-label" for="proposal_type">Proposal Type*</label>
                                <div class="col-sm-10">
                                    <select id="proposal_type" class="form-control" name="proposal_type" >
                                        <option value="">Please select one</option>
                                        <option @if($record->proposal_type == "standard") selected @endif value="standard">Standard</option>
                                        <option @if($record->proposal_type == "dedicated") selected @endif value="dedicated">Dedicated Server</option>
                                    </select>
                                </div>
                                @if($errors->has('proposal_type'))
                                    @foreach ($errors->get('proposal_type') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>


                          <!-- FOR STANDARD DOCUMENTS -->
                          <div id="standard" name="standard" hidden>

                            <div class="form-group {{ $errors->has('proposal_product_standard') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="proposal_product_standard">Product*</label>
                                <div class="col-sm-10">
                                    <input id="proposal_product_standard" class="form-control" type="text" name="proposal_product_standard" value="{{$record->proposal_product}}" />
                                </div>
                                @if($errors->has('proposal_product_standard'))
                                    @foreach ($errors->get('proposal_product_standard') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('proposal_resources_standard') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="proposal_resources_standard">Product Resources*</label>
                                <div class="col-sm-10">

                                      <textarea class="proposal_resources_standard" name="proposal_resources_standard" value="{!! $record->proposal_resources !!}"   style=" width:100%;" ></textarea>
                                </div>
                                @if($errors->has('proposal_resources_standard'))
                                    @foreach ($errors->get('proposal_resources_standard') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('proposal_scope_standard') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="proposal_scope_standard">Product Services*</label>
                                <div class="col-sm-10">

                                      <textarea class="proposal_scope_standard" name="proposal_scope_standard" value="{!! $record->proposal_scope!!}"   style=" visibility:hidden;" ></textarea>
                                </div>
                                @if($errors->has('proposal_scope_standard'))
                                    @foreach ($errors->get('proposal_scope_standard') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>


                            <div class="form-group {{ $errors->has('proposal_terms_standard') ? 'has-error' : '' }} ">
                                <label class="col-sm-2 control-label" for="proposal_terms_standard">Terms*</label>
                                <div class="col-sm-10">
                                    <select id="proposal_terms_standard" class="form-control" name="proposal_terms_standard" >
                                        <option value="">Please select one</option>
                                        <option @if($record->proposal_terms == "monthly") selected @endif value="monthly">Monthly</option>
                                        <option @if($record->proposal_terms == "yearly") selected @endif value="yearly">Yearly</option>

                                    </select>
                                </div>
                                @if($errors->has('proposal_terms_standard'))
                                    @foreach ($errors->get('proposal_terms_standard') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('proposal_price_description_standard') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="proposal_price_description_standard">Product Price* (In full text)</label>
                                <div class="col-sm-10">

                                      <textarea class="proposal_price_description_standard" name="proposal_price_description_standard" value="{!! $record->proposal_price_description!!}"   style=" width:100%;" ></textarea>
                                </div>
                                @if($errors->has('proposal_price_description_standard'))
                                    @foreach ($errors->get('proposal_price_description_standard') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>



                            <div class="form-group {{ $errors->has('proposal_start_date_standard') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="date">Start Date *</label>
                                <div class="input-group col-sm-10" style="padding-left: 15px;padding-right: 15px;">
                                    <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                    <input id="proposal_start_date_standard" class="form-control" type="text" name="proposal_start_date_standard" value="{{$record->proposal_start_date}}" />
                                </div>
                                @if($errors->has('proposal_start_date_standard'))
                                    @foreach ($errors->get('proposal_start_date_standard') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                          </div>

                            <!-- END STANDARD -->

                            <!-- DEDICATED -->

                            <div id="dedicated" name="dedicated" hidden>


                              <div class="form-group {{ $errors->has('proposal_terms_dedicated') ? 'has-error' : '' }} ">
                                  <label class="col-sm-2 control-label" for="proposal_terms_dedicated">Terms*</label>
                                  <div class="col-sm-10">
                                      <select id="proposal_terms_dedicated" class="form-control" name="proposal_terms_dedicated" >
                                          <option value="">Please select one</option>
                                          <option @if($record->proposal_terms == "monthly") selected @endif value="monthly">Monthly</option>
                                          <option @if($record->proposal_terms == "yearly") selected @endif value="yearly">Yearly</option>

                                      </select>
                                  </div>
                                  @if($errors->has('proposal_terms_dedicated'))
                                      @foreach ($errors->get('proposal_terms_dedicated') as $error)
                                          <span class="help-block">{{ $error }}</span>
                                      @endforeach
                                  @endif
                              </div>

                              <div class="form-group {{ $errors->has('proposal_price_description_dedicated') ? 'has-error' : '' }}">
                                  <label class="col-sm-2 control-label" for="proposal_price_description_dedicated">Product Price* (In full text)</label>
                                  <div class="col-sm-10">

                                        <textarea class="proposal_price_description_dedicated" name="proposal_price_description_dedicated" value="{!! $record->proposal_price_description!!}"   style=" width:100%;" ></textarea>
                                  </div>
                                  @if($errors->has('proposal_price_description_dedicated'))
                                      @foreach ($errors->get('proposal_price_description_dedicated') as $error)
                                          <span class="help-block">{{ $error }}</span>
                                      @endforeach
                                  @endif
                              </div>



                              <div class="form-group {{ $errors->has('proposal_start_date_dedicated') ? 'has-error' : '' }}">
                                  <label class="col-sm-2 control-label" for="date">Start Date *</label>
                                  <div class="input-group col-sm-10" style="padding-left: 15px;padding-right: 15px;">
                                      <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                      <input id="proposal_start_date_dedicated" class="form-control" type="text" name="proposal_start_date_dedicated" value="{{$record->proposal_start_date}}" />
                                  </div>
                                  @if($errors->has('proposal_start_date_dedicated'))
                                      @foreach ($errors->get('proposal_start_date_dedicated') as $error)
                                          <span class="help-block">{{ $error }}</span>
                                      @endforeach
                                  @endif
                              </div>

                              <div class="form-group {{ $errors->has('proposal_timeframe_dedicated') ? 'has-error' : '' }}">
                                  <label class="col-sm-2 control-label" for="proposal_scope_standard">Summary*</label>
                                  <div class="col-sm-10">

                                        <textarea class="proposal_timeframe_dedicated" name="proposal_timeframe_dedicated" value="{!! $record->proposal_timeframe_dedicated!!}"   style=" visibility:hidden;" ></textarea>
                                  </div>
                                  @if($errors->has('proposal_timeframe_dedicated'))
                                      @foreach ($errors->get('proposal_timeframe_dedicated') as $error)
                                          <span class="help-block">{{ $error }}</span>
                                      @endforeach
                                  @endif
                              </div>
                            </div>
                            <!-- END DEDICATED -->

                            <!-- DOCUMENTATION -->

                            <div id="documentation" name="documentation" hidden>



                              <div class="form-group {{ $errors->has('documentation_title') ? 'has-error' : '' }}">
                                  <label class="col-sm-2 control-label" for="documentation_title">Documentation Title</label>
                                  <div class="col-sm-10">

                                        <textarea class="documentation_title" name="documentation_title" value="{!! $record->documentation_title!!}"   style=" width:100%;" ></textarea>
                                  </div>
                                  @if($errors->has('documentation_title'))
                                      @foreach ($errors->get('documentation_title') as $error)
                                          <span class="help-block">{{ $error }}</span>
                                      @endforeach
                                  @endif
                              </div>


                              <div class="form-group {{ $errors->has('documentation_description') ? 'has-error' : '' }}">
                                  <label class="col-sm-2 control-label" for="documentation_description">Document Description</label>
                                  <div class="col-sm-10">

                                        <textarea class="documentation_description" name="documentation_description" value="{!! $record->documentation_description !!}"   style=" visibility:hidden;" ></textarea>
                                  </div>
                                  @if($errors->has('documentation_description'))
                                      @foreach ($errors->get('documentation_description') as $error)
                                          <span class="help-block">{{ $error }}</span>
                                      @endforeach
                                  @endif
                              </div>

                              <div class="form-group {{ $errors->has('attachment') ? 'has-error' : '' }}">
                                  <label class="col-sm-2 control-label" for="attachment">Document Image</label>
                                  <div class="col-sm-10">
                                      <input id="attachment" type="file" name="documentation_image" value="" accept=".jpg,.jpeg"/>
                                      <input id="attachment_file_name" name="documentation_image" class="hidden" value="{{$record->documentation_image}}" />
                                      <div class="image-area @if($record->attachment == null) hidden @endif">
                                          <img id="attachment_file_preview" class="" style="width: 120px; height:120px;" src="@if($record->attachment != null) {{asset('uploads/bookkeeping/'.$record->attachment)}} @endif" >
                                          <a class="remove-image" href="#" style="display: inline;">&#215;</a>
                                      </div>
                                  </div>
                                  @if($errors->has('attachment'))
                                      @foreach ($errors->get('attachment') as $error)
                                          <span class="help-block">{{ $error }}</span>
                                      @endforeach
                                  @endif
                              </div>


                              </div>


                            <!-- END DOCUMENTATION-->






                      <!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=5g5faf78gvk6yfq9bd3bbfjo858kjx1q8o0nbiwtygo2e4er"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#attachment_file_preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
                $('.image-area').toggleClass('hidden', false);
                $('#attachment_file_name').val('');
            }
        }


        tinymce.init({
        selector:'textarea.proposal_scope_standard',
        width: 900,
        height: 300
        });
        tinymce.init({
        selector:'textarea.documentation_description',
        width: 900,
        height: 300
        });
        tinymce.init({
        selector:'textarea.proposal_timeframe_dedicated',
        width: 900,
        height: 300
        });


        $("#attachment").change(function(){
            readURL(this);
        });

        $(".remove-image").on('click', function(e) {
            e.preventDefault();
            $('#attachment_file_name').val('');
            $('#attachment_file_preview').attr('src', '');
            $('.image-area').toggleClass('hidden', true);
        });

        $('#proposal_start_date_standard').datepicker({
            format: "MM d, yyyy",
            autoclose: true,
        });

        $('#proposal_start_date_dedicated').datepicker({
            format: "MM d, yyyy",
            autoclose: true,
        });
        $('#document_type').on('change', function() {
            if($(this).val() == 'Proposal') {
                $('#proposal_type').closest('.form-group').toggleClass('hidden',false);

                  $('#documentation').hide();

            }
            else if($(this).val() == 'Documentation') {
                  $('#documentation').show();
                  $('#proposal_type').closest('.form-group').toggleClass('hidden',true);
            }

             else {
                $('#proposal_type').closest('.form-group').toggleClass('hidden',true);
                    $('#documentation').hide();
            }
        });

        $('#proposal_type').on('change', function() {
            if($(this).val() == 'standard') {
                $('#standard').show();

            }
            else if($(this).val() == 'dedicated') {
                $('#dedicated').show();

            }
             else {
                $('#standard').hide();
                $('#dedicated').hide();
            }
        });

        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
            return function() {
                $file = $(this).siblings(tag);

                params = {
                    slug:   '{{ $dataType->slug }}',
                    filename:  $file.data('file-name'),
                    id:     $file.data('id'),
                    field:  $file.parent().data('field-name'),
                    multi: isMulti,
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: [ 'YYYY-MM-DD' ]
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
