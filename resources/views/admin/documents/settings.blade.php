@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .image-area {
            position: relative;
            width: 120px;
            background: #333;
        }
        .image-area img{
            max-width: 100%;
            height: auto;
        }
        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
            text-shadow: 0 1px 2px rgba(0,0,0,0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }
        .remove-image:hover {
            background: #E54E4E;
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }
        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }
    </style>
@stop

@section('page_title', ($edit ? 'Edit' : 'Add').' Document Settings)

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-documentation"></i>
        {{ ($edit ? 'Edit' : 'Add').' Document Settings' }}
    </h1>
@stop

@section('content')


    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add form-horizontal"
                          action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $record->id) : route('voyager.'.$dataType->slug.'.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif
                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}
                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                            <div class="form-group {{ $errors->has('company_name') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="company">Company*</label>
                                <div class="col-sm-10">
                                    <select id="company_name" class="form-control" name="company_name" required>
                                        <option value="">Please select one</option>

                                        @foreach($companies as $company)

                                            <option @if($record->company_name && $record->company_name == $company->id) selected @endif value="{{$company->id}}">{{$company->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('comapny_name'))
                                    @foreach ($errors->get('company_name') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('attachment') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="attachment">Header</label>
                                <div class="col-sm-10">
                                    <input id="attachment" type="file" name="attachment" value="" accept=".jpg,.jpeg"/>
                                    <input id="attachment_file_name" name="attachment_file_name" class="hidden" value="{{$record->attachment}}" />
                                    <div class="image-area @if($record->attachment == null) hidden @endif">
                                        <img id="attachment_file_preview" class="" style="width: 120px; height:120px;" src="@if($record->attachment != null) {{asset('uploads/bookkeeping/'.$record->attachment)}} @endif" >
                                        <a class="remove-image" href="#" style="display: inline;">&#215;</a>
                                    </div>
                                </div>
                                @if($errors->has('attachment'))
                                    @foreach ($errors->get('attachment') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>



                            <!-- WORKFLOW -->

                            <!-- END WORKFLOW-->


                          </div>



                      <!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=5g5faf78gvk6yfq9bd3bbfjo858kjx1q8o0nbiwtygo2e4er"></script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#attachment_file_preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
                $('.image-area').toggleClass('hidden', false);
                $('#attachment_file_name').val('');
            }
        }


        tinymce.init({
        selector:'textarea.proposal_scope_standard',
        width: 900,
        height: 300
        });
        tinymce.init({
        selector:'textarea.proposal_timeframe_dedicated',
        width: 900,
        height: 300
        });


        $("#attachment").change(function(){
            readURL(this);
        });

        $(".remove-image").on('click', function(e) {
            e.preventDefault();
            $('#attachment_file_name').val('');
            $('#attachment_file_preview').attr('src', '');
            $('.image-area').toggleClass('hidden', true);
        });

        $('#proposal_start_date_standard').datepicker({
            format: "MM d, yyyy",
            autoclose: true,
        });

        $('#proposal_start_date_dedicated').datepicker({
            format: "MM d, yyyy",
            autoclose: true,
        });
        $('#document_type').on('change', function() {
            if($(this).val() == 'Proposal') {
                $('#proposal_type').closest('.form-group').toggleClass('hidden',false);



            }
             else {
                $('#proposal_type').closest('.form-group').toggleClass('hidden',true);
            }
        });

        $('#proposal_type').on('change', function() {
            if($(this).val() == 'standard') {
                $('#standard').show();

            }
            else if($(this).val() == 'dedicated') {
                $('#dedicated').show();

            }
             else {
                $('#standard').hide();
                $('#dedicated').hide();
            }
        });

        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
            return function() {
                $file = $(this).siblings(tag);

                params = {
                    slug:   '{{ $dataType->slug }}',
                    filename:  $file.data('file-name'),
                    id:     $file.data('id'),
                    field:  $file.parent().data('field-name'),
                    multi: isMulti,
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: [ 'YYYY-MM-DD' ]
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
