<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <title>Marvill Web Development</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap_trimmed.min.css')}}" />
    <style type="text/css">
        html {
            height:100%;
        }
        body {
            font-family: 'DejaVu Sans', sans-serif;
            margin-left: 0.3in;
            margin-right: 0.3in;
        }
        h1 {
            font-size: 2em;
            font-weight: bold;
        }
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }
        .row {
            margin-bottom: 5px;
        }
        .page {
            page-break-after:always;
        }
        #invoice-items br{
            display: block;
            margin-top: -6px;
            content: "";
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <?php
        $provider = '';
        $total = 0;
        $is_subtotal = false;
    ?>
    <?php
      $addme = DB::table('tb_companies')->where('id',$client_name)->first();
      $addmecomp = DB::table('tb_companies')->where('id',$company_name)->first();
      $header = DB::table('tb_documents_settings')->where('company_id', $company_name)->first();


    ?>
        <div class="page">
            <div class="row" style="white-space:nowrap;">
                <div class="col-xs-2 vcenter" style="padding-left: 0px;">
                  @if($header->document_header == "")
                    <img src="https://workflow.marvill.com/assets/img/logo-big.png" width="150"/>
                  @else
                    <img src="https://v2workflow.marvill.com/storage/{{$header->document_header}}"width="150"/>
                  @endif
                </div>
                <div class="col-xs-10 vcenter" >
                    <h1 style="margin-left: 10px;">{{$addmecomp->company_name}}</h1>
                </div>
            </div>

            <div name="intro" style=" padding-top:20px; font-size:36px;  " class="col-sm-4">
              <p style="width:30%; ">
                <strong>
                  {{$documentation_title}}
                </strong>
              </p>

              <hr style="width:40%;text-align:left;margin-left:0;border:1px solid;">
            </p>
            </div>

            <div name="description" style="font-size:24px;">
              {!! $documentation_description !!}
            </div>

            <div name="image" style="font-size:24px;">
              <img src="https://v2workflow.marvill.com/public/uploads/documents/{{$documentation_image}}" alt="alternatetext">
            </div>

          </div>


</div>


</body>
</html>
