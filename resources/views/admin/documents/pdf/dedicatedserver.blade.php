<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <title>Marvill Web Development</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap_trimmed.min.css')}}" />
    <style type="text/css">
        html {
            height:100%;
        }
        body {
            font-family: 'DejaVu Sans', sans-serif;
            margin-left: 0.3in;
            margin-right: 0.3in;
        }
        h1 {
            font-size: 2em;
            font-weight: bold;
        }
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }
        .row {
            margin-bottom: 5px;
        }
        .page {
            page-break-after:always;
        }
        #invoice-items br{
            display: block;
            margin-top: -6px;
            content: "";
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <?php
        $provider = '';
        $total = 0;
        $is_subtotal = false;
    ?>
    <?php
      $addme = DB::table('tb_companies')->where('id',$client_name)->first();
      $addmecomp = DB::table('tb_companies')->where('id',$company_name)->first();
      $header = DB::table('tb_documents_settings')->where('company_id', $company_name)->first();

    ?>
        <div class="page">
            <div class="row" style="white-space:nowrap;">
                <div class="col-xs-2 vcenter" style="padding-left: 0px;">
                  @if($header->document_header == "")
                    <img src="https://workflow.marvill.com/assets/img/logo-big.png" width="150"/>
                  @else
                    <img src="https://v2workflow.marvill.com/storage/{{$header->document_header}}"width="150"/>
                  @endif
                </div>
                <div class="col-xs-10 vcenter" >
                    <h1 style="margin-left: 10px;">{{$addmecomp->company_name}}</h1>
                </div>
            </div>
            <div style="padding-top:20px; padding-left:2%; font-size:16px;">
            <strong>{{Carbon\Carbon::now()->format('F d, Y')}}</strong>
          </div>
          <div name="intro" style=" padding-top:20px; font-size:16px;  " class="col-sm-4">
            <p style="width:30%;">
              <strong>
                {{strtoupper($addme->company_name)}}
              </strong>
            </p>
            <p style="width:30%;">
              {{$addme->company_address}}
            </p>
            <p style="padding-top:10px;">
            <strong>
              Thru:    {{$client_contact_person}}
                    <p style="padding-left:45px;">{{$client_contact_person_position}}</p>
            </strong>
            <hr style="width:40%;text-align:left;margin-left:0;border:1px solid;">
          </p>
          </div>


          <div name="scope" style=" font-size:16px; padding-top:20px;">
            <p> This is to submit our engagement proposal for services we will provide for <strong>{{strtoupper($addme->company_name)}}</strong> The scope of the services we will provide are outlined below: </p>
          </div>

          <div name="costing" style=" font-size:16px; padding-top:20px;">
            <p><u><strong>DEDICATED SERVER</u></strong></p>
            <p>Resources: Management of dedicated server for {{strtoupper($addme->company_name)}}</p>
            <p>Scope of services:
              <ul style="padding-left:20%;">
                <li> Management of dedicated server </li>
                <li> Ensure server security </li>
                <li> Perform periodic backup of server files </li>
            </p>
          </div>

          <div name="serverspec" style=" font-size:16px; padding-top:20px;">
            <p><u><strong>SERVER SPECIFICATION</u></strong></p>
            <p>
              <ul style="padding-left:10%;">
                <li> INTEL XEON X3440</li>
                <li> 2.93 GHZ</li>
                <li> 8 CORES</li>
                <li> 16 GB RAM</li>
                <li> 1TB PRIMARY HDD</li>
                <li> 1TB SATA BACKUP HDD</li>
                <li> 10TB MONTHLY BANDWIDTH</li>
                <li> CPANEL & WHM</li>
                <li> BASIC CLOUDFLARE</li>
            </p>
          </div>

          <div name="timeframe" style="font-size:16px;  padding-top:15px; color:black;">
              <p><u><strong>PROFESSIONAL FEE</u></strong></p>
              <p style="display:inline;">In connection with the above services, we propose to bill you {{$proposal_terms}} of <strong>{{ $proposal_price_description }}</strong></p> The payment will be due every 20th day of the month. Corresponding withholding tax will be deducted for every payment, which will be supported by BIR form 2307.
              <p style="padding-top:20px;"> This will take effect {{$proposal_start_date}} and will be enforced until a written notice from client to terminate services prior to the next billing cycle.</p>



              <p><u><strong>IN SUMMARY</u></strong></p>
              {!! $proposal_timeframe !!}


              <p> Please sign and return the attached copy of this letter to indicate that it is in accordance with your understanding of the arrangements for our engagement. </p>
          </div>

          <div name="terms" style="padding-top:20px; font-size:16px">
            <p> Thank you and God Bless.</p>
            <p style="padding-top:20px"> Very truly yours,</p>
            <p style="display:inline;"><strong>{{strtoupper($addmecomp->company_name)}}<span style="float:right;">{{strtoupper($addme->company_name)}}</span></strong></p>
            <p style="padding-top:20px;">By:<span style="float:right; padding-right:20%">By:</span></p>
            <p style="display:inline;"><strong>{!! strtoupper($company_contact_person) !!}<span style="float:right;">{{strtoupper($client_contact_person)}}</span></strong></p>
            <br>
            <p style="display:inline;">FOR THE FIRM<span style="float:right;">{{strtoupper($client_contact_person_position)}}</span></p>
          </div>

          </div>

          <div name="sender">

          </div>


</div>


</body>
</html>
