<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <title>Marvill Web Development</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap_trimmed.min.css')}}" />
    <style type="text/css">
        html {
            height:100%;
        }
        body {
            font-family: 'DejaVu Sans', sans-serif;
            margin-left: 0.3in;
            margin-right: 0.3in;
        }
        h1 {
            font-size: 2em;
            font-weight: bold;
        }
        .vcenter {
            display: inline-block;
            vertical-align: middle;
            float: none;
        }
        .row {
            margin-bottom: 5px;
        }
        .page {
            page-break-after:always;
        }
        #invoice-items br{
            display: block;
            margin-top: -6px;
            content: "";
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <?php
        $provider = '';
        $total = 0;
        $is_subtotal = false;
    ?>

        <div class="page">
            <div class="row" style="white-space:nowrap;">
                <div class="col-xs-2 vcenter" style="padding-left: 0px;">
                    <img src="https://workflow.marvill.com/assets/img/logo-big.png" width="150"/>
                </div>
                <div class="col-xs-10 vcenter" >
                    <h1 style="margin-left: 10px;">{{$addmecomp->company_name}}</h1>
                </div>
            </div>
            <div style="padding-top:20px;">
            <strong>{{Carbon\Carbon::now()->format('F d, Y')}}</strong>
          </div>
          <div name="intro" style=" word-wrap: break-word; padding-top:20px; font-size:16px;">
            <p>Attention:</p>
            <p>Good Day!</p>
            <p>Below is our proposal for your <strong>{{strtoupper($proposal_product)}}</strong></p>
          </div>

          <div name="scope" style="padding-left:30px; font-size:16px; padding-top:20px;">
            <p><strong>I. SCOPE OF SERVICES </strong></p>
            <p><span style="font-weight:900;">{{$company_name}}</span> shall provide a <span style="font-weight:900;">{{$proposal_product}}</span> for your company. In accordance to your needs and preferences we will also provide consultation on several suggestive approaches for you and assist you in finding the modules for your Workflow. </p>
          </div>

          <div name="costing" style="padding-left:30px; font-size:16px; padding-top:20px;">
            <p><strong>II. COSTING </strong></p>
            <p><span style="font-weight:900;"><i> Subscription Package</span></i> </p>
            <p style="text-indent: 50px;">This will cover the full subscription of <span style="font-weight:900;">{{$proposal_product}}</span>. The project will be constructed as follows: </p>
            <ul>
              <li>
                <b>Installation of base system shall commence </b>upon receipt of necessary data such as about the company, logo, vision, mission, photos and other pertinent data which shall be necessary for the subscription. Marvill Web Development Services shall proceed with the setup for <b>{{$proposal_working_days}}</b>
              </li>
              <li>
                <b>Custom Module:</b> Each client shall be entitled to <b> 1 Premium Module (This are modules that can interact with several functions of the base module and can stand alone as a system to be optimized for the needs of the client ex. Order Module, Booking Module, and Blasting Module)</b>
              </li>
              <li>
                <b>TSM Appointment Module:</b> This will be a custom system which will provide backend access to TSM to set available schedule for its clients or workforce to schedule interviews or other consultation as they may require. This module will also send E-mail or SMS (Optional Third-Party Integration using Globe API SMS App) This will require <b></b> for implementation and <b></b>
              </li>
              <li>	After which it will be sent to client for final approval.
                <ul>
                  <li>In case of minor revisions Marvill Web Development Services shall have 3 working days to apply changes.</li>
                  <li>In case of major revisions Marvill Web Development Services shall have a period of 7 days to comply. </li>
                </ul>
            </ul>
            <p><b>The following services will be included in the package:</b>{!! $proposal_inclusions !!}</p>
            <p style="text-align:center"><b>Price and Costing</b></p>
            <p>{!! $proposal_price_description !!}</p>
          </div>

          <div name="timeframe" style="font-size:16px;  color:black;">
              <p><strong>TIMEFRAME </strong></p>
              <p>The subscription will start upon receipt of <b>60% down payment</b>  and submission of necessary data for the subscription. Day 1 shall start upon the submission of all contents and the 60% down payment.  The working period shall be for  <b>{{$proposal_working_days}}</b> and upon the setup of workflow to its index the remaining <b>40% shall be due and demandable.</b></p>
              <ul>
                <li>
                  Base Installation: {{$proposal_working_days}}
                </li>
                <li>
                  Custom Module Development:
                </li>
                <li>
                  Testing Days:
                </li>
                <li>
                  <b> Please take note that workings days shall cover only Monday thru Friday excluding Saturday, Sunday, and Holidays</b>
                </li>
              </ul>
          </div>

          <div name="terms" style="font-size:16px">
            <p><strong>Terms:</strong></p>
            <ol>
              <li><b>INITIAL:</b> payment of subscription and submission of necessary data.</li>
              <li><b>UPGRADES:</b> Third party applications are not included except those applications which are free and customary for your workflow.</li>
              <li><b>UPDATES:</b> Updates will only be published upon approval of the screen shots sent via email.</li>
              <li><b>MAINTENANCE:</b> After the first free month trial of the maintenance from the signing of this agreement a monthly retainer’s fee shall be applied equivalent to Twelve Thousand Pesos (P12, 000) due every 1st Day of the Month. If Client should choose not to avail this service, they must inform Marvill Web Development Services 5 days before the end of the first free maintenance month.</li>
            </ol>
          </div>

          <div name="sender">
            <p style="text-align:center;">
              <strong> Thank you! God Bless!</strong>
              <hr  style="border: 1px solid">
            </p>
            <p><strong> Joshua V. Cabatan</strong></p>
            <p><strong> Developer</strong></p>
            <p><strong> Marvill Web Development Services</strong></p>
          </div>


</div>


</body>
</html>
