@extends('voyager::master')

@section('page_title', 'Documents')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-documentation"></i> Documents
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan

        <div class="row">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <form id="date-form" method="GET">

                        <div class="col-sm-4">
                            <div class="input-group" >
                                <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                <input id="datepicker" class="form-control" type="text" name="date" value="{{$date}}">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')



    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" class="select_all">
                                        </th>
                                        <th>Date</th>
                                        <th>Company</th>
                                        <th>Client</th>
                                        <th>Document Type</th>

                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>


                                @foreach($records as $record)
                                <?php
                                  $addme = DB::table('tb_companies')->where('id',$record->client_name)->first();
                                  $addmecomp = DB::table('tb_companies')->where('id',$record->company_name)->first();

                                ?>
                                    <tr >
                                        <td><input type="checkbox" name="row_id" id="checkbox_{{ $record->id }}" value="{{ $record->id }}"></td>
                                        <td>{{ date('d-M-y', strtotime($record->created_at)) }}</td>
                                        <td>{{strtoupper($addmecomp->company_name)}}</td>
                                        <td>{{strtoupper($addme->company_name)}}</td>
                                        <td>{{$record->document_type}} - {{$record->proposal_type}}</td>
                                        <td style="text-align: center;"><a target="_blank" href="{{url('admin/documents/print/'.$record->id)}}">Print</a</td>
                                        <td>
                                            <a href="{{route('voyager.'.$dataType->slug.'.edit',['id' => $record->id ])}}" class="btn btn-primary btn-sm"><i class="voyager-edit"></i></a>
                                            <button class="btn btn-danger btn-sm delete" data-id="{{$record->id}}" id="delete-{{$record->id}}"><i class="voyager-x"></i></button>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} Document?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
    <style>
        .red-badge{
            background-color: #ed5564;
            color:white;
            padding:4px;
        }
        .green-badge{
            background-color: #43ac6e;
            color:white;
            padding:4px;
        }
    </style>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $('#dataTable').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{route('voyager.documents.destroy',['id' => '__id'])}}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });
        $(document).ready(function () {
            var table = $('#dataTable').DataTable({
                order: [ 1, 'desc' ],
                "drawCallback": function( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/,/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var income_total = 0;
                    var expense_total = 0;
                    var filteredRows = api.rows( {order:'index', search:'applied', filter:'applied'} ).data();

                    for (var i=0; i<filteredRows.length; i++) {
                        income_total += intVal(filteredRows[i][5]);
                        expense_total += intVal(filteredRows[i][6]);
                    };
                    var total_balance = (income_total+expense_total);

                    $("#income_total").html(income_total.toLocaleString('en-US',{ minimumFractionDigits: 2, maximumFractionDigits: 2 }));
                    $("#expense_total").html(expense_total.toLocaleString('en-US',{ minimumFractionDigits: 2, maximumFractionDigits: 2 }));
                    $("#total_balance").html(total_balance.toLocaleString('en-US',{ minimumFractionDigits: 2, maximumFractionDigits: 2 }));
                    if(total_balance <= 0) {
                        $("#total_balance").toggleClass("red-badge",true);
                        $("#total_balance").toggleClass("green-badge",false);
                    } else {
                        $("#total_balance").toggleClass("green-badge",true);
                        $("#total_balance").toggleClass("red-badge",false);
                    }

                }
            });



            $('#datepicker').datepicker({
                format: 'M-yyyy',
                autoclose: true,
                startView: "year",
                minViewMode: "months",
            }).on('changeDate', function(e) {
                $('#date-form').submit();
            });

            $('#company').on('change', function(e) {
                $('#date-form').submit();
            });
        });
    </script>
@stop
