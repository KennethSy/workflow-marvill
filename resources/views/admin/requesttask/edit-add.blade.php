@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .image-area {
            position: relative;
            width: 120px;
            background: #333;
        }
        .image-area img{
            max-width: 100%;
            height: auto;
        }
        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
            text-shadow: 0 1px 2px rgba(0,0,0,0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }
        .remove-image:hover {
            background: #E54E4E;
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }
        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }
    </style>
@stop

@section('page_title', 'Request Task')

@section('page_header')
    <h1 class="page-title">
        <i class="bx bx-add-to-queue"></i>
        {{ 'Request Task' }}
    </h1>
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add form-horizontal"
                          action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $request->id) : route('voyager.'.$dataType->slug.'.store') }}"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if($edit)
                        {{ method_field("PUT") }}
                    @endif
                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}
                        <div class="panel-body">
                            <div class="form-group {{ $errors->has('company') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="company">Company *</label>
                                <div class="col-sm-10">
                                    <select id="company" class="form-control select2" name="company" required>
                                        <option value="">Please select one</option>
                                        @foreach($companies as $company)
                                            <option @if($request->company_id && $request->company_id == $company->id) selected @endif value="{{$company->id}}">{{$company->company_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('company'))
                                    @foreach ($errors->get('company') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('project') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="project">Project *</label>
                                <div class="col-sm-10">
                                    <select id="project" class="form-control" name="project" required>
                                        <option value="">Please select a company first</option>
                                        @if($request->project_id != null)
                                            @foreach($projects->where('company_id','=',$request->company_id)->all() as $project)
                                                <option @if($request->project_id && $request->project_id == $project->id) selected @endif value="{{$project->id}}">{{$project->title}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                @if($errors->has('project'))
                                    @foreach ($errors->get('project') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>

                          

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="assigned_to">Assigned To *</label>
                                <div class="col-sm-10">
                                    <select class="select2 assigned_to form-control" name="assigned_to[]" multiple required>
                                        <option value="">Please select one</option>
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
							<div class="form-group {{ $errors->has('requested_date') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="requested_date">Date *</label>
                                <div class="col-sm-10">
                                    <div class="input-group" >
                                        <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                        <input id="date" class=" form-control" type="text" name="requested_date" value="{{$request->requested_date==null?Carbon\Carbon::now()->format('m/d/Y'):Carbon\Carbon::parse($request->requested_date)->format('m/d/Y')}}" required/>
                                    </div>
                                </div>
                                @if($errors->has('requested_date'))
                                    @foreach ($errors->get('requested_date') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label" for="description">Description *</label>
                                <div class="col-sm-10">
                                    <textarea id="description" class="form-control" rows="3" name="description">{{$request->description}}</textarea>
                                </div>
                                @if($errors->has('description'))
                                    @foreach ($errors->get('description') as $error)
                                        <span class="help-block">{{ $error }}</span>
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="attachment">Attachments</label>
                                <div class="col-sm-10">
                                    <input id="attachment" type="file" name="attachment[]" value="" accept=".jpg,.jpeg,.png" multiple/>
                                    <div id="gallery">
                                    </div>
                                </div>
                            </div>

                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                            @stop
                            @yield('submit-buttons')
                        </div><!-- panel-body -->
                    </form>

                </div>
            </div>
        </div>
    </div>
    <div id="attachment_file_preview">
        <img class="" style="width: 250px; height:auto;" src="" >
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
    <style>
        .image-area {
            position: relative;
            width: 120px;
            background: #333;
        }
        .image-area img{
            max-width: 100%;
            height: auto;
        }
        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
            text-shadow: 0 1px 2px rgba(0,0,0,0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }
        .remove-image:hover {
            background: #E54E4E;
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }
        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }
    </style>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        function imagesPreview(input, placeToInsertImagePreview) {
            $(placeToInsertImagePreview).html("");
            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                        $("#attachment_file_preview").children().clone().attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }
        };

        $('#date').datepicker({
            startDate: '{{Carbon\Carbon::now()->format('m/d/Y')}}',
            autoclose: true,
        });

        var data = @json($project_data) ;

        $("#project").select2({
            tags: true,
        });
        $('#company').on('change',function() {
            $("#project").empty();
            $("#project").select2({
                tags: true,
                data: data[$('#company').val()],
            });
        });

        $('#attachment').on('change', function() {
            imagesPreview(this, '#gallery');
        });

        $('form').on('input', 'textarea', function () {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
        });

        $("form").on('submit',function(e) {
            var value = $(this).find("input[type=submit]").val();
            $(this).find("button[type=submit]").html('Please Wait..').prop('disabled',true);
        });
    </script>
@stop
