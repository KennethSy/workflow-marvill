@extends('voyager::master')

@section('page_title', 'Request Task')

@section('css')
    <style>
        .image-area {
            position: relative;
            width: 120px;
            background: #333;
        }
        .image-area img{
            max-width: 100%;
            height: auto;
        }
        .remove-image {
            display: none;
            position: absolute;
            top: -10px;
            right: -10px;
            border-radius: 10em;
            padding: 2px 6px 3px;
            text-decoration: none;
            font: 700 21px/20px sans-serif;
            background: #555;
            border: 3px solid #fff;
            color: #FFF;
            box-shadow: 0 2px 6px rgba(0,0,0,0.5), inset 0 2px 4px rgba(0,0,0,0.3);
            text-shadow: 0 1px 2px rgba(0,0,0,0.5);
            -webkit-transition: background 0.5s;
            transition: background 0.5s;
        }
        .remove-image:hover {
            background: #E54E4E;
            padding: 3px 7px 5px;
            top: -11px;
            right: -11px;
        }
        .remove-image:active {
            background: #E54E4E;
            top: -10px;
            right: -11px;
        }
    </style>
@stop

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="bx bx-add-to-queue"></i> <a style="color:inherit;" href="{{route('voyager.dailytask.index')}}">Request Task</a>
        </h1>
        <span class="pull-right">
            @if($request->status != 2)
                @include('admin.requesttask.action-buttons',['request' => $request,'canView' => false,'canDelete' => false, 'canEdit' => false])
            @endif
        </span>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="task-table" class="table table-hover">
                                <tbody>
                                <tr>
                                    <td><b>Company</b></td>
                                    <td>{{$request->company}}</td>
                                </tr>
                                <tr>
                                    <td><b>Project</b></td>
                                    <td>{{$request->project}}</td>
                                </tr>
                                <tr>
                                    <td><b>Description</b></td>
                                    <td>{!! nl2br($request->description) !!}</td>
                                </tr>
                                <tr>
                                    <td><b>Created At</b></td>
                                    <td>{{$request->created_at}}</td>
                                </tr>
                                <tr>
                                    <td><b>Status</b></td>
                                    <td>{{$status}}</td>
                                </tr>
                                <tr>
                                    <td><b>Requested By</b></td>
                                    <td>{{$user->name}}</td>
                                </tr>
                                <tr>
                                    <td><b>Assigned To</b></td>
                                    <td>{{$request->assigned_employees}}</td>
                                </tr>
                                <tr>
                                    <td><b>Attachments</b></td>
                                    <td>
                                        <div class="attachments_gallery">
                                            @foreach($attachments as $attachment)
                                                <img class="" style="width: 250px; height:auto;cursor: pointer" src="{{url('uploads/notes/'.$attachment->attachment)}}" >
                                            @endforeach
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="attachment_file_preview">
        <img class="" style="width: 250px; height:auto;" src="" >
    </div>
    <div class="modal modal-success fade" tabindex="-1" id="approve_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Approve Request?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="approve_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-success pull-right" value="Approve Request">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        function imagesPreview(input, placeToInsertImagePreview) {
            $(placeToInsertImagePreview).html("");
            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function(event) {
                        //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                        $("#attachment_file_preview").children().clone().attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }
        };

        $('.approve-btn').on('click', function (e) {
            $('#approve_form').attr('data-id',$(this).attr('data-id'));
            $('#approve_form')[0].action = '{{route('requesttask.approve',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#approve_modal').modal('show');
        });

        $('#attachment').on('change', function() {
            imagesPreview(this, '#gallery');
        });

        $('.attachments_gallery img').click(function () {
            window.open($(this).attr("src"),'_blank');
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#attachment_file_preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
                $('.image-area').toggleClass('hidden', false);
                $('#attachment_file_name').val('');
            }
        }

        /*$("#attachment").change(function(){
            readURL(this);
        });*/

        $(".remove-image").on('click', function(e) {
            e.preventDefault();
            /*$('#attachment_file_name').val('');
            $('#attachment_file_preview').attr('src', '');*/
            $(this).parent().toggleClass('hidden', true);
            $(this).parent().find(".attachment_file_name").attr("disabled",true);
        });

        $('textarea').each(function () {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
        });

        $('#note-form').on('input', 'textarea', function () {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
        });

        $('#edit-btn').click(function () {
            $('#note-form').toggleClass('hidden');
            $('#note-div').toggleClass('hidden');
            $('#edit-btn').html('Edit Note');
            $('textarea').trigger('input');
        });

        $("#complete_form, #open_form, #confirm_form, #reject_form, #carryover_form").on('submit',function(e) {
            var value = $(this).find("input[type=submit]").val();
            $(this).find("input[type=submit]").val('Please Wait..').prop('disabled',true);
        });

        $('.complete-btn').on('click', function (e) {
            $('#complete_form').attr('data-id',$(this).attr('data-id'));
            $('#complete_form')[0].action = '{{route('dailytask.complete',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#complete_modal').modal('show');
        });
        $('.open-btn').on('click', function (e) {
            $('#open_form').attr('data-id',$(this).attr('data-id'));
            $('#open_form')[0].action = '{{route('dailytask.open',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#open_modal').modal('show');
        });
        $('.confirm-btn').on('click', function (e) {
            $('#confirm_form').attr('data-id',$(this).attr('data-id'));
            $('#confirm_form')[0].action = '{{route('dailytask.confirm',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#confirm_modal').modal('show');
        });
        $('.reject-btn').on('click', function (e) {
            $('#reject_form').attr('data-id',$(this).attr('data-id'));
            $('#reject_form')[0].action = '{{route('dailytask.reject',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#reject_modal').modal('show');
        });
        $('.carryover-btn').on('click', function (e) {
            $('#carryover_form').attr('data-id',$(this).attr('data-id'));
            $('#carryover_form')[0].action = '{{route('dailytask.carryover',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#carryover_modal').modal('show');
        });

        $("form").on('submit',function(e) {
            var value = $(this).find("input[type=submit]").val();
            $(this).find("button[type=submit]").html('Please Wait..').prop('disabled',true);
        });
    </script>
@stop
