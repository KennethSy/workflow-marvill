@extends('voyager::master')

@section('page_title', 'Request Task')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="bx bx-add-to-queue"></i> <a style="color:inherit;" href="{{ route('voyager.'.$dataType->slug.'.index') }}">Request Task</a>
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Company</th>
                                        <th>Project</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                                        <th>Requested By</th>
                                        @endif
                                        <th>Assigned To</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} Request ?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal modal-success fade" tabindex="-1" id="approve_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Approve Request?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="approve_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-success pull-right" value="Approve Request">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal modal-success fade" tabindex="-1" id="confirm_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Confirm Task?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="confirm_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-success pull-right" value="Confirm Task">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal modal-danger fade" tabindex="-1" id="reject_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Reject Task Completion?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="reject_form" method="POST">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right" value="Reject Task Completion">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
    <style>
        .red-badge{
            background-color: #ed5564;
            color:white;
            padding:4px;
        }
        .green-badge{
            background-color: #43ac6e;
            color:white;
            padding:4px;
        }
        .tr-redbg {
            background-color: #2b1515!important;
        }
    </style>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        var deleteFormAction;
        $('#dataTable').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{route('voyager.'.$dataType->slug.'.destroy',['id' => '__id'])}}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        $('#dataTable').on('click', '.approve-btn', function (e) {
            $('#approve_form').attr('data-id',$(this).attr('data-id'));
            $('#approve_form')[0].action = '{{route('requesttask.approve',['id' => '__id'])}}'.replace('__id', $(this).attr('data-id'));
            $('#approve_modal').modal('show');
        });
        $(document).ready(function () {

            var table = $('#dataTable').DataTable({
                @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                    order: [ 6, 'desc' ],
                @else
                    order: [ 5, 'desc' ],
                @endif
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{route('requesttask.data')}}',
                },
                columns: [
                    {data: 'company', name: 'tb_companies.company_name'},
                    {data: 'project', name: 'tb_projects.title'},
                    {data: 'description', name: 'tb_request_tasks.description'},
                    {data: 'status', name: 'tb_request_tasks.status'},
                    @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 3)
                    {data: 'request_user', name: 'users.name'},
                    @endif
                    {data: 'assigned_employees', name: 'assigned_users.name'},
                    {data: 'created_at', name: 'tb_request_tasks.created_at'},
                    {data: 'actions', name: 'actions', orderable: false, searchable: false},
                ]
            });

        });

        $("#reject_form").on('submit',function(e) {
            e.preventDefault();
            var value = $(this).find("input[type=submit]").val();
            $(this).find("input[type=submit]").val('Please Wait..').prop('disabled',true);
            var $t = $(this);
            $.ajax({
                url: $(this).attr('action'),
                type: "post",
                data: $(this).serialize() ,
                success: function (response) {
                    toastr.success(response.message);
                    $t.find("input[type=submit]").val(value).prop('disabled',false);
                    $('#completed-at-'+$t.attr('data-id')).html(response.completed_at);
                    $('#confirmed-at-'+$t.attr('data-id')).html(response.confirmed_at);
                    $('#status-'+$t.attr('data-id')).html(response.status);
                    $('#button-'+$t.attr('data-id')).html(response.button);
                    $('#value-'+$t.attr('data-id')).html(response.value);
                    $('#complete_modal').modal('hide');
                    $('#open_modal').modal('hide');
                    $('#confirm_modal').modal('hide');
                    $('#reject_modal').modal('hide');
                    $('#carryover_modal').modal('hide');
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    toastr.error('Something went wrong.');
                    $t.find("input[type=submit]").val(value).prop('disabled',false);
                    $('#complete_modal').modal('hide');
                    $('#open_modal').modal('hide');
                    $('#confirm_modal').modal('hide');
                    $('#reject_modal').modal('hide');
                    $('#carryover_modal').modal('hide');
                }
            });
        });

        $("#delete_form").on('submit',function(e) {
            var value = $(this).find("input[type=submit]").val();
            $(this).find("input[type=submit]").val('Please Wait..').prop('disabled',true);
        });
    </script>
@stop
