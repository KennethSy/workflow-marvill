@if($request->status == 0 && (Auth::user()->role_id == 1 || Auth::user()->role_id == 3))
    <button class="btn btn-primary btn-xs approve-btn" type="button" data-id="{{$request->id}}"> Approve</button>
@endif
@if($request->status == 0 && (Auth::user()->role_id == 1 || Auth::user()->role_id == 3))
    <button class="btn btn-primary btn-xs reject-btn" type="button" data-id="{{$request->id}}"> Reject</button>
@endif
@if($canView)
    <a class="btn btn-primary btn-xs" href="{{route('voyager.request-tasks.show',['id' => $request->id ])}}" data-id="{{$request->id}}"> View</a>
@endif
@if($request->status == 0 && $canEdit)
    <a class="btn btn-primary btn-xs" href="{{route('voyager.request-tasks.edit',['id' => $request->id ])}}" data-id="{{$request->id}}"> Edit</a>
@endif
@if($canDelete)
    <a class="btn btn-primary btn-xs delete" href="javascript:;" data-id="{{$request->id}}"> Delete</a>
@endif
