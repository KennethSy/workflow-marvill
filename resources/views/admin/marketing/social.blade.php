@extends('voyager::master')

@section('page_title', 'Socials')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-facebook"></i> Socials
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div id="exTab1" class="container">
                            <ul  class="nav nav-pills">
                                <li class="active"><a  href="#intagram" data-toggle="tab">Instagram</a></li>
                                <li><a href="#facebook-review" data-toggle="tab">Facebook Reviews</a></li>
                                <li><a href="#facebook-feed" data-toggle="tab">Facebook Feed</a></li>
                            </ul>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="intagram">
                                    <div class="elfsight-app-2f6fac6c-d585-4233-b83c-f8032b634f59"></div>
                                </div>
                                <div class="tab-pane" id="facebook-review">
                                    <div class="elfsight-app-d1f3f311-8aa4-4ff7-8f0d-e316ca5f5f86"></div>
                                </div>
                                <div class="tab-pane" id="facebook-feed">
                                    <div class="elfsight-app-af652da8-ed44-4cab-a517-2927a5c5dcde"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('css')

@stop

@section('javascript')
    <script src="https://apps.elfsight.com/p/platform.js" defer></script>
@stop
