@extends('voyager::master')
@section('content')

<style>
    .accordion {
    background-color: #648b2d;
    color: #FFF;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
    text-transform: uppercase;
    }

   .accordion:hover, .accordion:active {
    background-color: #8cc43d;
    }

    .panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
    }

</style>


  <div style="padding-bottom: 25px; text-align:center;">
    <h1>FAQ</1>
  </div>

      <button class="accordion" >How to set user permissions</button>
      <div class="panel">
      <ol>
        <li> Click on Users located on the sidebar </li>
        <li> Search for the user on the upper right </li>
        <li> Click on Edit on the desired user </li>
        <li> Change the role</li>
        <li> Click Save</li>
        <li> You're all done!</li>
      </ol>
      </div>

      <button class="accordion">How to assign a task</button>
      <div class="panel">
        <ol>
          <li> Click on Tasks located on the sidebar </li>
          <li> Click on Add New located on the top left </li>
          <li> Fill up the form </li>
          <li> Click Save</li>
          <li> You're all done!</li>
        </ol>
      </div>

      <button class="accordion">What is Rollover?</button>
      <div class="panel">
      <p>Rollover means that the value of the task will be cut in half.</p>
      </div>

      <button class="accordion">What is carryover?</button>
      <div class="panel">
      <p>Carryover means that the value of the task wouldn't change and would be assigned again the next day. </p>
      </div>


      <button class="accordion">What are the Employee - Action Buttons?</button>
      <div class="panel">
        <ul>
          <li> Mark Complete - This marks the task as completed and would be submitted to the manager for verification. </li>
          <li> View - This shows the complete details of the task and this is where the employees would be able to attach files and notes for the task given. </li>
        </ul>
      </div>

      <button class="accordion">What is Task Summary</button>
      <div class="panel">
      <p>Task summary contains the summarization of every task done for your clients by your employees. It is basically a trail of tasks so that your clients can also check on the progress for their respective projects. </p>
      </div>

      <button class="accordion">How to encode a client company</button>
      <div class="panel">
        <ol>
          <li> Click on Companies located on the sidebar </li>
          <li> Click on Add New located on the top left </li>
          <li> Fill up the form </li>
          <li> Click Save</li>
          <li> You're all done!</li>
        </ol>
      </div>

      <button class="accordion">How to create a directory listing</button>
      <div class="panel">
        <ol>
          <li> Click on Directory located on the sidebar </li>
          <li> Click on Add New located on the top left </li>
          <li> Fill up the form </li>
          <li> Click Save</li>
          <li> You're all done!</li>
        </ol>
      </div>



    <!--
        <script src="https://apps.elfsight.com/p/platform.js" defer></script>
        <div style="text-align:center;">
    <div class="elfsight-app-d9696b9e-aa67-40f9-b08d-c9deff95dbf4"></div>
  </div> -->



<script>
      var acc = document.getElementsByClassName("accordion");
      var i;

      for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
          this.classList.toggle("active");
          var panel = this.nextElementSibling;
          if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
          } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
          }
        });
      }
</script>
@stop
