@extends('voyager::master')

@section('page_title', 'Health Survey')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="bx bx-edit-alt"></i> Health Survey
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{route('healthsurvey.questionsSubmit')}}" method="POST">
                                    {{csrf_field()}}
                                    <div class="panel panel-bordered table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Question</th>
                                                    <th>Yes</th>
                                                    <th>No</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($questions as $question)
                                                <tr>
                                                    <td>{{$question->description}}</td>
                                                    <td><input value="Yes" type="radio" name="question[{{$question->id}}]" required/></td>
                                                    <td><input value="No" type="radio" name="question[{{$question->id}}]" required/></td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="panel-footer">
                                        <input type="submit" class="btn btn-primary save" value="Submit"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')

@stop

@section('javascript')
    <script>
        $("form").on('submit',function(e) {
            var value = $(this).find("input[type=submit]").val();
            $(this).find("input[type=submit]").val('Please Wait..').prop('disabled',true);
        });
    </script>
@stop
