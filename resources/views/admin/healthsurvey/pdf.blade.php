<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <title>Health Survey Results - {{\Carbon\Carbon::parse($date)->format('l, F d, Y')}}</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap_trimmed.min.css')}}" />
    <style type="text/css">

    </style>
</head>
<body>
<div class="container-fluid">
    <h2>{{\Carbon\Carbon::parse($date)->format('l, F d, Y')}}</h2>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Employee</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach($survey_results as $result)
            <tr>
                <td>{{$result->name}}</td>
                <td>{{$result->status ?? "Pending"}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

</body>
</html>
