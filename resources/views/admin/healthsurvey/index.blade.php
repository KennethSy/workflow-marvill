@extends('voyager::master')

@section('page_title', 'Health Survey Results')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="bx bx-edit-alt"></i> Health Survey Results - {{\Carbon\Carbon::parse($date)->format('l, F d, Y')}}
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-bordered table-responsive">
                                    <a href="{{route('healthsurvey.index',['action' => 'print'])}}" class="pull-right btn btn-primary">Print</a>
                                    <table id="survey-table" class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Employee</th>
                                                <th>Status</th>
                                                <th>"Yes" Answers</th>
                                                <th>"No" Answers</th>
                                                <th>Date and Time of Submission</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($survey_results as $result)
                                            <tr>
                                                <td>{{$result->name}}</td>
                                                <td>{{$result->status ?? "Pending"}}</td>
                                                <td>{{$result->yes_answers}}</td>
                                                <td>{{$result->no_answers}}</td>
                                                <td>{{$result->created_at}}</td>
                                                <td>
                                                    @if($result->status != null)
                                                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal{{$result->id}}">
                                                            View
                                                        </button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach($survey_results as $survey_result)
        @if(isset($survey_answers[$survey_result->id]))
            <div class="modal fade" id="modal{{$survey_result->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">{{$survey_result->name}}</h4>
                        </div>
                        <div class="modal-body">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Question</th>
                                    <th>Yes</th>
                                    <th>No</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($survey_questions as $survey_question)
                                    <tr>
                                        <td>{{$survey_question->description}}</td>
                                        <td>@if($survey_answers[$survey_result->id][$survey_question->id-1]->answer == 'Yes') <i class="fa fa-check"></i> @else &nbsp; @endif</td>
                                        <td>@if($survey_answers[$survey_result->id][$survey_question->id-1]->answer == 'No') <i class="fa fa-check"></i> @else &nbsp; @endif</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
@stop

@section('css')

@stop

@section('javascript')
    <script>
        $("form").on('submit',function(e) {
            var value = $(this).find("input[type=submit]").val();
            $(this).find("input[type=submit]").val('Please Wait..').prop('disabled',true);
        });
        $("#survey-table").DataTable({
            order: [ 1, 'asc' ],
        });
    </script>
@stop
