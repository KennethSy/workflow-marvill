@extends('voyager::master')

@section('page_title', 'Payroll')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class=""></i> Payroll
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="sbox">
                    <div class="sbox-title">
                        <h1> Payroll of {{$employee->name}} for {{$payroll_start}} to {{$payroll_end}} <small> </small></h1>
                        <div class="sbox-tools">
                        </div>
                    </div>
                    <div class="sbox-content">
                        <div class="table-responsive" style="padding-bottom: 10px;">
                            <table class="table table-hover">
                                <tr>
                                    <td><strong>EARNINGS</strong></td>
                                    <td><strong>AMOUNT</strong></td>
                                <tr>
                                <tbody>
                                @foreach ($earnings as $key => $earning)
                                <tr>
                                    <td>{{$key}} </td>
                                    <td>PHP {{number_format($earning,2)}} </td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td><strong>DEDUCTIONS</strong></td>
                                    <td><strong>AMOUNT</strong></td>
                                <tr>
                                @foreach ($deductions as $key => $deduction)
                                    <tr>
                                        <td>{{$key}} </td>
                                        <td>PHP {{number_format($deduction,2)}} </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td><strong>TOTAL</strong></td>
                                    <td><strong>PHP {{number_format($total_net_pay,2)}}</strong></td>
                                <tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('css')

@stop

@section('javascript')
    <script>
        $('#payrollTable tr').click(function () {
            window.open($(this).attr("data-href"),'_blank');
        });
    </script>
@stop
