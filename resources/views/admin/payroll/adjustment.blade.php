@extends('voyager::master')

@section('page_title', 'Payroll')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class=""></i> Payroll
        </h1>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="sbox">
                    <div class="sbox-title">
                        <h1> Payroll Adjustment  for {{$payroll_start}} to {{$payroll_end}} <small> </small></h1>
                        <div class="sbox-tools">
                        </div>
                    </div>
                    <div class="sbox-content">
                        <form role="form"
                              class="form-edit-add form-horizontal"
                              action="{{ $edit ? route('payroll.saveadjustment', $payroll_id) : route('payroll.saveadjustment', $payroll_id) }}"
                              method="POST" enctype="multipart/form-data">
                            <!-- PUT Method if we are editing -->
                        @if($edit)
                            {{ method_field("PUT") }}
                        @endif
                        <!-- CSRF TOKEN -->
                            {{ csrf_field() }}
                            <div class="panel-body">
                                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label" for="name">Name *</label>
                                    <div class="col-sm-10">
                                        <select id="name" class="form-control" name="name" required>
                                            <option value="">Please select one or type a new one</option>
                                            @foreach($payroll_settings as $payroll_setting)
                                                <option value="{{$payroll_setting->name}}">{{$payroll_setting->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if($errors->has('name'))
                                        @foreach ($errors->get('name') as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label" for="type">Type *</label>
                                    <div class="col-sm-10">
                                        <select id="type" class="form-control" name="type" required>
                                            <option value="Earning">Earning</option>
                                            <option value="Deduction">Deduction</option>
                                        </select>
                                    </div>
                                    @if($errors->has('type'))
                                        @foreach ($errors->get('type') as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('value') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label" for="value">Value *</label>
                                    <div class="col-sm-10">
                                        <input id="value" class="form-control" type="number" name="value" required value="" step=".01" />
                                    </div>
                                    @if($errors->has('value'))
                                        @foreach ($errors->get('value') as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('employees') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label" for="employees">Employees *</label>
                                    <div class="col-sm-10">
                                        <select id="employees" class="form-control select2" @if($edit) name="employees" @else name="employees[]" multiple @endif required>
                                            <option value="">Please select one</option>
                                            @foreach($employees as $employee)
                                                <option value="{{$employee->id}}">{{$employee->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if($errors->has('employees'))
                                        @foreach ($errors->get('employees') as $error)
                                            <span class="help-block">{{ $error }}</span>
                                        @endforeach
                                    @endif
                                </div>
                                @section('submit-buttons')
                                    <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                                @stop
                                @yield('submit-buttons')
                            </div><!-- panel-body -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('css')

@stop

@section('javascript')
    <script>
        $("#name").select2({
            tags: true,
        });
    </script>
@stop
