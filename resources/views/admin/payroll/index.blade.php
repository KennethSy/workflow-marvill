@extends('voyager::master')

@section('page_title', 'Payroll')

@section('page_header')
  
    <div class="container-fluid"> 
    <div class="row"> 
    <div class="col-md-6 col-xs-12"> 
        <h1 class="page-title">
           <span class="icon bx bxs-spreadsheet"></span> Payroll
        </h1>
        
            @if($previous_payroll != null)
            <a href="{{ route('payroll.index', ['id' => $previous_payroll->id]) }}" class="btn btn-success" >
                <span>Previous Payroll</span>
            </a>&nbsp;&nbsp;
            @endif
            @if($next_payroll != null)
            <a href="{{ route('payroll.index', ['id' => $next_payroll->id]) }}" class="btn btn-success" >
                <span>Next Payroll</span>
            </a>&nbsp;&nbsp;
            @endif
            <a href="{{ route('payroll.adjustment', $payroll->id) }}" class="btn btn-success btn-primary" >
                <span>Add Adjustment</span>&nbsp;&nbsp;
            </a>
        
    </div>
	  <div class="col-md-6 col-xs-12 text-center" > 
	  <div class="col-md-6 col-xs-12"> 
	  <div style="padding:20px;border:1px solid #1da1f2;border-radius:5px;">
	  <div style="color:white">TOTAL RELEASED: </div>
                <div   class="green-badge">PHP {{number_format($total_unreleased_net_pay,2)}}</div> 
                               
                            </div>
                            </div>
	  <div class="col-md-6 col-xs-12 text-center"> 
<div style="padding:20px;border:1px solid #1da1f2;border-radius:5px;">
                <div style="color:white">TOTAL CUTOFF:</div>
                <div class="green-badge">PHP {{number_format($total_net_pay,2)}}</div>
			</div> 	  
                              
                            </div>
       </div>
    </div>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="sbox">
                    <div class="sbox-title text-center">
                        <h3 style="color:white"> <i class="fa fa-info-circle" aria-hidden="true"></i> Payroll Summary for {{$payroll_start}} to {{$payroll_end}}</h3>
						<br/>
						<br/>
                        <div class="sbox-tools">

                        </div>
                    </div>
                    <div class="sbox-content">
                        <div class="table-responsive" style="padding-bottom: 10px;">
                            <table class="table table-hover" id="payrollTable">
                                <thead>
                                <tr>
                                    <td><input type="checkbox" class="select_all"></td>
                                    <td style="text-align: center;"><strong>Employee Name</strong></td>
                                    <td style="text-align: center;"><strong>Expected Salary</strong></td>
                                    <td style="text-align: center;"><strong>Basic Salary</strong></td>
                                    <td class="padding-r-16" style="text-align: center;"><strong>Allowance</strong></td>
                                    @foreach($payroll_earnings as $earning)
                                        <td style="text-align: center;"><strong>{{$earning->name}}</strong></td>
                                    @endforeach
                                    @foreach($payroll_deductions as $deduction)
                                        <td  class="@if($loop->first) padding-l-16 @endif" style="text-align: center;"><strong>{{$deduction->name}}</strong></td>
                                    @endforeach
                                    <td class="padding-l-16" style="text-align: center;"><strong>Other Earnings</strong></td>
                                    <td style="text-align: center;"><strong>Other Deductions</strong></td>
                                    <td style="text-align: center;"><strong>Total with Amenities</strong></td>
                                    <td style="text-align: center;"><strong>Net Pay</strong></td>
                                <tr>
                                </thead>
                                <tbody>
                                @foreach ($employees as $employee)
                                    @if(isset($employee_payroll[$employee->id]['payroll_employee_id']))
                                    <tr data-href="{{route('payroll.view',['id' => $employee_payroll[$employee->id]['payroll_employee_id']])}}" style="cursor:pointer;">
                                        <td>
                                            @if($employee_payroll[$employee->id]['is_paid'] == 0)
                                                <input type="checkbox" name="row_id" id="checkbox_{{ $employee_payroll[$employee->id]['payroll_employee_id']}}" value="{{ $employee_payroll[$employee->id]['payroll_employee_id']}}"/>
                                            @else
                                                <i class="fas fa-check"></i>
                                            @endif
                                        </td>
                                        <td class="clickable_row">{{$employee->name}}</td>
                                        <td class="clickable_row">PHP {{number_format($employee_payroll[$employee->id]['expected_salary'],2)}}</td>
                                        <td class="clickable_row">PHP {{number_format($employee_payroll[$employee->id]['basic_pay'],2)}} </td>
                                        <td class="clickable_row padding-r-16">PHP {{number_format($employee_payroll[$employee->id]['allowance'],2)}} </td>
                                        @foreach($payroll_earnings as $earning)
                                            <td class="clickable_row">PHP {{number_format(isset($employee_payroll[$employee->id][$earning->name])?$employee_payroll[$employee->id][$earning->name]:0,2)}}</td>
                                        @endforeach
                                        @foreach($payroll_deductions as $deduction)
                                            <td class="clickable_row @if($loop->first) padding-l-16 @endif">PHP {{number_format(isset($employee_payroll[$employee->id][$deduction->name])?$employee_payroll[$employee->id][$deduction->name]:0,2)}} </td>
                                        @endforeach
                                        <td class="clickable_row padding-l-16">
                                            @if(count($employee_payroll[$employee->id]['other_earnings']) > 0)
                                                @foreach($employee_payroll[$employee->id]['other_earnings'] as $name => $other_earning)
                                                    @if (!$loop->first)
                                                        <br/>
                                                    @endif
                                                    PHP {{number_format($other_earning,2)}} - {{$name}}
                                                @endforeach
                                            @else
                                                PHP 0.00
                                            @endif
                                        </td>
                                        <td class="clickable_row">
                                            @if(count($employee_payroll[$employee->id]['other_deductions']) > 0)
                                                @foreach($employee_payroll[$employee->id]['other_deductions'] as $name => $other_deduction)
                                                    @if (!$loop->first)
                                                        <br/>
                                                    @endif
                                                    PHP {{number_format($other_deduction,2)}} - {{$name}}
                                                @endforeach
                                            @else
                                                PHP 0.00
                                            @endif
                                        </td>
                                        <td class="clickable_row"><strong>PHP {{number_format($employee_payroll[$employee->id]['total'],2)}} </strong></td>
                                        <td class="clickable_row"><strong>PHP {{number_format($employee_payroll[$employee->id]['net_pay'],2)}} </strong></td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                           
                            <a class="btn btn-primary" id="mark_paid_btn"><span>Mark as Released</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- Bulk delete modal --}}
    <div class="modal modal-primary fade" tabindex="-1" id="mark_paid_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">
                        Are you sure you want to mark it as released?
                    </h4>
                </div>
                <div class="modal-body" id="mark_paid_modal_body">
                </div>
                <div class="modal-footer">
                    <form action="{{route('payroll.markpaid')}}" id="bulk_delete_form" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="ids" id="selected_ids" value="">
                        <input type="submit" class="btn btn-primary pull-right delete-confirm"
                               value="Confirm">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                        {{ __('voyager::generic.cancel') }}
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
     
    <style>
        .red-badge{
            background-color: #ed5564;
            color:white;
            padding:4px;
			border-radius:5px;
        }
        .green-badge{
            background-color: #43ac6e;
            color:white;
            padding:4px;
			border-radius:5px;
        }
        .padding-r-16{
            padding-right: 24px!important;
        }
        .padding-l-16{
            padding-left: 24px!important;
        }
    </style>
@stop

@section('javascript')
    <script>
        $('.clickable_row').click(function () {
            window.open($(this).parent('tr').attr("data-href"),'_blank');
        });
        $('.select_all').on('click', function(e) {
            $('input[name="row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
        });

        $('#mark_paid_btn').click(function () {
            var ids = [];
            var $checkedBoxes = $('#payrollTable input[type=checkbox]:checked').not('.select_all');
            var count = $checkedBoxes.length;
            if (count) {
                // Reset input value
                $('#selected_ids').val('');

                // Gather IDs
                $.each($checkedBoxes, function () {
                    var value = $(this).val();
                    ids.push(value);
                })
                // Set input value
                $('#selected_ids').val(ids);
                // Show modal
                $('#mark_paid_modal').modal('show');
            } else {
                // No row selected
                toastr.warning('You didn\'t selected anything');
            }
        });
    </script>
@stop
