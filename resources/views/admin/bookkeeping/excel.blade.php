<?php



$content = '<table border="1">';
$content .= '<tr>';
$content .= '<th>Date</th>';
$content .= '<th>Company</th>';
$content .= '<th>Tin Number</th>';
$content .= '<th>Description</th>';
$content .= '<th>Income</th>';
$content .= '<th>Expense</th>';
$content .= '</tr>';

$check_id =0;
foreach ($records as $index => $record)
{
    $content .= '<tr>';
    $content .= '<td>'.$record->date.'</td>';
    $content .= '<td>'.$record->company.'</td>';
    $content .= '<td>'.$record->tin_no.'</td>';
    $content .= '<td>'.$record->description.'</td>';
    $content .= '<td>'.($record->income > 0 ? $record->income : '').'</td>';
    $content .= '<td>'.($record->expense > 0 ? $record->expense : '').'</td>';
    $content .= '</tr>';
}
$content .= '</table>';

@header('Content-Type: application/ms-excel');
@header('Content-Length: '.strlen($content));
@header('Content-disposition: inline; filename="'.$title.' '.date("d/m/Y").'.xls"');

echo $content;
exit;
?>
