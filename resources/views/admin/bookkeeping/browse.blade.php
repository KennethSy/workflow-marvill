@extends('voyager::master')

@section('page_title', 'Cash Flow')

@section('page_header')
    <div class="container-fluid">
	<div class="row">
	<div class="col-md-6 col-xs-12">

        <h1 class="page-title">
            <i class="voyager-receipt"></i> Cash Flow
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-primary btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>

            <form method="GET" action="{{route('bookkeeping.export')}}" style="display:inline-block">
                @csrf()
                @if($selected_company == 'all')
                    <input type="hidden" name="company" value="all">
                @else
                    @foreach($companies as $company)
                        <input type="hidden" name="company" value="{{$company->id}}">
                    @endforeach
                @endif
                <input type="hidden" name="date" value="{{$date}}">
                <button type="submit" class="btn btn-primary btn-add-new">
                    <i class="voyager-download"></i> <span>Export</span>
                </button>
            </form>
        @endcan
		</div>
		<div class="col-md-6 col-xs-12">

		<div class="col-md-4 col-xs-12 text-center">
			<div style="padding:20px;border:1px solid #1da1f2;border-radius:5px;">
                <div style="color:white">Income Total:</div>
                <div id="income_total" class="green-badge">{{number_format($summary->total_income,2)}}</div>
			</div>
        </div>
		<div class="col-md-4 col-xs-12 text-center">
		<div style="padding:20px;border:1px solid #1da1f2;border-radius:5px;">
                <div style="color:white">Expense Total:</div>
                <div id="expense_total" class="red-badge">-{{number_format($summary->total_expense,2)}}</div>
         </div>
         </div>
	    <div class="col-md-4 col-xs-12 text-center">
		<div style="padding:20px;border:1px solid #1da1f2;border-radius:5px;">
                <div style="color:white">Total Balance:</div>
                <div id="total_balance" class="@if($summary->total_balance > 0) green-badge @else red-badge @endif ">{{number_format($summary->total_balance,2)}}</div>
         </div>
         </div>




		</div>
		</div>
        <div class="row">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <form id="date-form" method="GET">
                        <div class="col-sm-6">
                            <select id="company" class="form-control select2" name="company">
                                <option @if($selected_company == 'all') selected @endif  value="all">All</option>
                                @foreach($companies as $company)
                                    <option @if($selected_company == $company->id) selected @endif value="{{$company->id}}">{{$company->company_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group" >
                                <span class="input-group-addon"><i class="voyager-calendar"></i></span>
                                <input id="datepicker" class="form-control" type="text" name="date" value="{{$date}}">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" class="select_all">
                                        </th>
                                        <th>Date</th>
                                        <th>Company</th>
                                        <th>Tin Number</th>
                                        <th>Remarks</th>
                                        <th>Account Code</th>
                                        <th>Account Title</th>
                                        <th style="text-align: right;">Cash In</th>
                                        <th style="text-align: right;">Cash Out</th>
                                        <th style="text-align: right;">Current Balance</th>
                                        <th>Cheque Number</th>
                                        <th>Attachment</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($records as $record)
                                    <tr >
                                        <td data-order="{{ $record->id }}" ><input type="checkbox" name="row_id" id="checkbox_{{ $record->id }}" value="{{ $record->id }}"></td>
                                        <td>{{Carbon\Carbon::parse($record->date)->format('d-M-y')}}</td>
                                        <td>{{$record->company}}</td>
                                        <td>{{$record->tin_no}}</td>
                                        <td>{!! $record->description  !!}</td>
                                        <td>{{$record->account_code}}</td>
                                        <td>{{$record->account_name}}</td>
                                        <td style="color:green;text-align: right;">{{$record->income > 0 ? number_format($record->income,2) : ''}}</td>
                                        <td style="color:red;text-align: right;">{{$record->expense > 0 ? '-'.number_format($record->expense,2) : ''}}</td>
                                        <td  style="text-align: right;" ><span class="@if($current_balance > 0) green-badge @else red-badge @endif ">{{number_format($current_balance,2)}}</span></td>
                                        <td><div>{{$record->cheque_no}}</div></td>
                                        <td style="text-align: center;">@if($record->attachment)<a target="_blank" href="{{url('uploads/bookkeeping/'.$record->attachment)}}">View</a>@endif</td>
                                        <td>
                                            <a href="{{route('voyager.'.$dataType->slug.'.edit',['id' => $record->id ])}}" class="btn btn-primary btn-sm"><i class="voyager-edit"></i></a>
                                            <button class="btn btn-danger btn-sm delete" data-id="{{$record->id}}" id="delete-{{$record->id}}"><i class="voyager-x"></i></button>
                                        </td>
                                    </tr>
                                    <?php $current_balance = $current_balance - $record->income + $record->expense; ?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} Bookkeeping Record ?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('plugins\bootstrap-datepicker\css\bootstrap-datepicker3.min.css')}}"></link>
    <style>
        .red-badge{
            background-color: #ed5564;
            color:white;
            padding:4px;
			border-radius:5px;
        }
        .green-badge{
            background-color: #43ac6e;
            color:white;
            padding:4px;
			border-radius:5px;
        }
    </style>
@stop

@section('javascript')
    <script src="{{asset('plugins/datarangepicker/moment.min.js')}}"></script>
    <script src="{{asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $('#dataTable').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{route('voyager.bookkeeping.destroy',['id' => '__id'])}}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });
        $(document).ready(function () {
            var table = $('#dataTable').DataTable({
                order: [ 0, 'desc' ],
                "drawCallback": function( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/,/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    var income_total = 0;
                    var expense_total = 0;
                    var filteredRows = api.rows( {order:'index', search:'applied', filter:'applied'} ).data();

                    for (var i=0; i<filteredRows.length; i++) {
                        income_total += intVal(filteredRows[i][5]);
                        expense_total += intVal(filteredRows[i][6]);
                    };
                    var total_balance = (income_total+expense_total);

                    $("#income_total").html(income_total.toLocaleString('en-US',{ minimumFractionDigits: 2, maximumFractionDigits: 2 }));
                    $("#expense_total").html(expense_total.toLocaleString('en-US',{ minimumFractionDigits: 2, maximumFractionDigits: 2 }));
                    $("#total_balance").html(total_balance.toLocaleString('en-US',{ minimumFractionDigits: 2, maximumFractionDigits: 2 }));
                    if(total_balance <= 0) {
                        $("#total_balance").toggleClass("red-badge",true);
                        $("#total_balance").toggleClass("green-badge",false);
                    } else {
                        $("#total_balance").toggleClass("green-badge",true);
                        $("#total_balance").toggleClass("red-badge",false);
                    }

                }
            });



            $('#datepicker').datepicker({
                format: 'M-yyyy',
                autoclose: true,
                startView: "year",
                minViewMode: "months",
            }).on('changeDate', function(e) {
                $('#date-form').submit();
            });

            $('#company').on('change', function(e) {
                $('#date-form').submit();
            });
        });
    </script>
@stop
