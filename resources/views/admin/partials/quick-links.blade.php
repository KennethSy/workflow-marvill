<div id="quicklinksModal" class="modal modal-success fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Quick Links</h4>
            </div>
            <div class="modal-body">
                <ul class="nav nav-stacked">
                    @if(Auth::user()->can('add', app('App\TbDailyLogSubTask')))
                        <li>
                            <a target="_self" href="{{route('voyager.dailytask.create')}}">
                                Add Task
                            </a>
                        </li>
                    @endif
                    @if(Auth::user()->can('add', app('App\TbRequestTask')))
                    <li>
                        <a target="_self" href="{{route('voyager.request-tasks.create')}}">
                            Request Task
                        </a>
                    </li>
                    @endif
                    @if(Auth::user()->can('add', app('App\TbBookkeeping')))
                    <li>
                        <a target="_self" href="{{route('voyager.bookkeeping.create')}}">
                            Add Expense/Income
                        </a>
                    </li>
                    @endif
                    @if(Auth::user()->can('add', app('App\TbInvoice')))
                        <li>
                            <a target="_self" href="{{route('voyager.invoices.create')}}">
                                Add Invoice
                            </a>
                        </li>
                    @endif
                    @if(Auth::user()->can('add', app('App\TbBookkeeping')))
                    <li>
                        <a target="_self" href="{{route('voyager.companies.create')}}">
                            Add Company
                        </a>
                    </li>
                    @endif
                    @if(Auth::user()->can('add', app('App\TbDirectory')))
                    <li>
                        <a target="_self" href="{{route('voyager.directory.create')}}">
                            Add Directory
                        </a>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
