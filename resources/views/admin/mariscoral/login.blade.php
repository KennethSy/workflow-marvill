<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" dir="{{ __('voyager::generic.is_rtl') == 'true' ? 'rtl' : 'ltr' }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="none" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="admin login">
    <title>Admin - {{ Voyager::setting("admin.title") }}</title>
    <link rel="stylesheet" href="{{ voyager_asset('css/app.css') }}">
    @if (__('voyager::generic.is_rtl') == 'true')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.css">
        <link rel="stylesheet" href="{{ voyager_asset('css/rtl.css') }}">
    @endif
    <style>
        html{
            background: url(https://pnbwings.com/pabotime.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        body {
            background: url(https://pnbwings.com/pabotime.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        body.login .login-sidebar {
            border-top:5px solid {{ config('voyager.primary_color','#22A7F0') }};
        }
        @media (max-width: 767px) {
            body.login .login-sidebar {
                border-top:0px !important;
                border-left:5px solid {{ config('voyager.primary_color','#22A7F0') }};
            }
        }
        body.login .form-group-default.focused{
            border-color:{{ config('voyager.primary_color','#22A7F0') }};
        }
        .login-button, .bar:before, .bar:after{
            background:{{ config('voyager.primary_color','#22A7F0') }};
        }
        .remember-me-text{
            padding:0 5px;
        }
        .titlestyle{
            font-weight:900;
            font-size:18pt;
        }
        .login-sec{
            margin-top:3%;
        }
        body.login {
            overflow: auto !important;
        }
        body.login .faded-bg {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background: transparent !important;
        }
        .pnbtop{
            height:75px;
            background: #000 !important;
        }
        @media(max-width:767px){
            .logo-pnb {
                max-height: 75px;
            }
        }
    </style>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
</head>
<body class="login">
<section class="pnbtop">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4"> </div>
            <div class="col-md-4 text-center"><img class="logo-pnb center-block" src="https://pnbwings.com/webflow/images/pnbwings-logo.png"/> </div>
            <div class="col-md-4"> </div>
        </div>
    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="faded-bg animated"></div>
        <div class="hidden-xs col-sm-7 col-md-8">
            <div class="clearfix">
                <div class="col-sm-12 col-md-10 col-md-offset-2 hidden">
                    <div class="logo-title-container">
                        <?php $admin_logo_img = Voyager::setting('admin.icon_image', ''); ?>
                        @if($admin_logo_img == '')
                            <img class="img-responsive pull-left flip logo hidden-xs animated fadeIn" src="{{ voyager_asset('images/logo-icon-light.png') }}" alt="Logo Icon">
                        @else
                            <img class="img-responsive pull-left flip logo hidden-xs animated fadeIn" src="{{ Voyager::image($admin_logo_img) }}" alt="Logo Icon">
                        @endif
                        <div class="copy animated fadeIn">
                            <h1>PNB WINGS</h1>
                            <p>Welcome to PNBWINGS ordering system. v1.0</p>
                        </div>
                    </div> <!-- .logo-title-container -->
                </div>
            </div>
        </div>
        <div class="col-sm-offset-2 col-sm-8 login-sec">
            <div class="col-xs-12 col-md-6 col-sm-6 ">

                <div class="col-xs-12">
                    <span class="titlestyle">SIGN IN</span>
                </div>
                
                <form action="{{ url('login') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="col-xs-12 ">
                        <div class="form-group form-group-default" id="emailGroup">
                            <label>{{ __('voyager::generic.email') }}</label>
                            <div class="controls">
                                <input type="text" name="email" id="email" value="{{ old('email') }}" placeholder="{{ __('voyager::generic.email') }}" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 ">
                        <div class="form-group form-group-default" id="passwordGroup">
                            <label>{{ __('voyager::generic.password') }}</label>
                            <div class="controls">
                                <input type="password" name="password" placeholder="{{ __('voyager::generic.password') }}" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 ">
                        <div class="form-group" id="rememberMeGroup">
                            <div class="controls">
                                <input type="checkbox" name="remember" id="remember" value="1"><label for="remember" class="remember-me-text">{{ __('voyager::generic.remember_me') }}</label>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-block login-button">
                            <span class="signingin hidden"><span class="voyager-refresh"></span> {{ __('voyager::login.loggingin') }}...</span>
                            <span class="signin">{{ __('voyager::generic.login') }}</span>
                        </button>
                    </div>
                <!--  <div class="form-group row mb-0">
                   <div class="col-md-8 offset-md-4">
                      <a href="{{ url('/auth/redirect/facebook') }}" class="btn btn-primary"><i class="fa fa-facebook"></i> Facebook</a>
                   </div>
                 </div>-->
                </form>
            </div> <!-- .login-sidebar -->
            <div class="col-xs-12 col-md-6 col-sm-6 ">
                <form class="register-form" method="POST" action="{{ route('register') }}">
                    @csrf


                    <div class="col-xs-12">
                        <span class="titlestyle">REGISTER</span>
                    </div>
                    @if (!empty($errors) && $errors->has('name'))
                        <div class="callout small alert text-center" id="nameHelpText">
                            <p>{{ $errors->first('name') }}</p>
                        </div>
                    @endif


                    <div class="col-xs-12 ">
                        <div class="form-group form-group-default" id="name">
                            <label for="email">
                                Full Name*
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" aria-describedby="nameHelpText" required autofocus>
                            </label>
                        </div>
                    </div>



                    @if (!empty($errors) && $errors->has('email'))
                        <div class="callout small alert text-center" id="emailHelpText">
                            <p>{{ $errors->first('email') }}</p>
                        </div>
                    @endif
                    <div class="col-xs-12 ">
                        <div class="form-group form-group-default" id="email">
                            <label for="email">
                                E-Mail Address*
                                <input id="email" class="form-control" type="email" name="email" value="{{ old('email') }}" aria-describedby="emailHelpText" required>
                            </label>
                        </div>
                    </div>
                        <input type="hidden" id="typeofform" name="typeofform" value="divers">
                    @if (!empty($errors) && $errors->has('password'))
                        <div class="callout small alert text-center" id="passwordHelpText">
                            <p>{{ $errors->first('password') }}</p>
                        </div>
                    @endif
                    <div class="col-xs-12 ">
                        <div class="form-group form-group-default" id="password">
                            <label for="password">
                                Password
                                <input id="password" class="form-control" type="password" name="password" aria-describedby="passwordHelpText" required>
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 ">
                        <div class="form-group form-group-default" id="confirmpassword">
                            <label for="password-confirm">
                                Confirm Password
                                <input id="password-confirm" class="form-control" type="password" name="password_confirmation" required>
                            </label>
                        </div>
                    </div>




                    <div class="col-xs-12 ">
                        <button type="submit" class="btn btn-block login-button">Register</button>
                    </div>
                </form>
            </div>

            <div style="clear:both"></div>

            @if(!$errors->isEmpty())
                <div class="alert alert-red">
                    <ul class="list-unstyled">
                        @foreach($errors->all() as $err)
                            <li>{{ $err }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>







    </div> <!-- .row -->
</div> <!-- .container-fluid -->





<div class="section-7" style="
  padding-top: 5%;
  padding-bottom: 2%;
  background-color: #fff;
  text-align: center;
">
    <div class="div-block-5">
        <div class="row">
            <div class="col-md-3"><img src="images/pnb-footerlogo.jpg" width="82" alt=""></div>
            <div class="col-md-6">
                <p class="paragraph-3" style="padding-top: 20px;
    font-size: 20px;
    text-transform: uppercase;"><br>POWERED BY MARVILL WEBDEVELOPMENT SERVICES ©2020</p>
            </div>
            <div class="col-md-3"><img src="images/marvill.jpg" width="299" alt=""></div>
        </div>
    </div>
</div>



<script>
    var btn = document.querySelector('button[type="submit"]');
    var form = document.forms[0];
    var email = document.querySelector('[name="email"]');
    var password = document.querySelector('[name="password"]');
    btn.addEventListener('click', function(ev){
        if (form.checkValidity()) {
            btn.querySelector('.signingin').className = 'signingin';
            btn.querySelector('.signin').className = 'signin hidden';
        } else {
            ev.preventDefault();
        }
    });
    email.focus();
    document.getElementById('emailGroup').classList.add("focused");

    // Focus events for email and password fields
    email.addEventListener('focusin', function(e){
        document.getElementById('emailGroup').classList.add("focused");
    });
    email.addEventListener('focusout', function(e){
        document.getElementById('emailGroup').classList.remove("focused");
    });

    password.addEventListener('focusin', function(e){
        document.getElementById('passwordGroup').classList.add("focused");
    });
    password.addEventListener('focusout', function(e){
        document.getElementById('passwordGroup').classList.remove("focused");
    });

</script>
</body>
</html>
