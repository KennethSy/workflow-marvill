@extends('frontend.layouts.master')

@section('data-wf-page', '5eabbb96c6f8e93a4aaeb1e6')

@section('data-wf-site', '5e952c81d56b94c967145826')

@section('title', 'About')

@section('description', 'About')

@section('body-class','body-2')

@section('content')
    <div class="section-9">
        <div class="div-block-6">
            <h1 data-w-id="40a55fd1-f71f-682d-44d9-2ab92728fccb" style="-webkit-transform:translate3d(-83PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(-83PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(-83PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(-83PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="heaing-10">WHO<br>WE ARE</h1>
            <div data-w-id="427706a2-1476-796b-d99f-000b56ef90aa" style="-webkit-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="text-block-15">-- since 2003</div>
        </div>
        <div class="div-block-7">
            <div data-w-id="c7c13009-864e-0931-4d73-946b3fc241ee" style="-webkit-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="text-block-15">YEAR</div>
            <h1 data-w-id="83ad974f-f284-cc86-e281-5690b1e5ea63" style="-webkit-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="heaing-10">2003</h1>
            <p data-w-id="db1b8d8d-3787-a497-f92a-1e66cd8edade" style="-webkit-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="paragraph-8">Marvill Web Development was conceptualized back in 2003 by RJ Villanueva. Through media and web development, he sought to provide higher quality solutions for information technologies. From there, he sought to place key people into the cores of each of those fields, with the goal of creating a team of experts to work with each other, as they work around the challenges presented.</p>
        </div>
    </div>
    <div class="section-10">
        <div class="div-block-8">
            <h1 data-w-id="9f23becc-22dc-81ca-db59-18289338ed23" style="-webkit-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="heading-10">2009</h1>
            <p class="paragraph-9">Years were spent in study and conceptualization, and in 2009 RJ decided to put his plans into fruition and start the business. Angelo Marco Cantada was brought on board to take the role of senior designer and training and development head, with a broad background of production development.<br><br>Accompanying Cantada, Jay Justalero took on the role as production head over development operations. The key people of what would become Marvill Web Development were now present. The team had begun to take shape.</p>
        </div>
    </div>
    <div class="section-11">
        <div class="div-block-9">
            <h1 style="-webkit-transform:translate3d(-109PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(-109PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(-109PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(-109PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="heading-11">Present</h1>
            <p style="opacity:0" class="paragraph-10">Succeeding years were spent in training, building the basic foundations to allow a company to grow. This structure led Marvill to prove their mark time and again in the fast-expanding field of web development in the Philippines.<br><br>Utilizing grassroots and family-oriented culture, Marvill approached the business by building up young IT’s from the ground up. This consistent training focused them on their individual potentials, instilling the same values held by the pioneering team.</p>
        </div>
        <div class="div-block-10">
            <p style="opacity:0" class="paragraph-11">Nobody is too small, and nobody was left out. This was the philosophy that Marvill maintained and built their work ethic upon. As the company grew, they made sure to grow within the morals that made them more family, than business.<br><br>These skills and attributes, among more, were maintained and enhanced over the years to provide the best possible solution for their clients. A business must continuously adapt to its environment, and more importantly its clients, in order to prosper in an ever changing world. By keeping up with trends in the fields of Philippine web development and information technologies, Marvill maintains their place in at the forefront of the business’ growth.<br><br>Among Marvill Web Development’s current achievements, they have been acknowledged through several IT channels by being a Google partner and reseller for the Philippine market, a Cloud Flare re-seller, and a Microsoft partner.<br><br>Marvill Web Development supports the visions of these organizations put forth for our market. Thus following its commitment to provide a better solution for users as it continues to grow towards a better future with its clients.</p>
            <h1 class="heading-12">Our Partners</h1>
            <div class="w-layout-grid grid-2"><img src="images/googleforwork.jpg" alt=""><img src="images/cloudflare.jpg" alt=""><img src="images/grammarly.jpg" alt=""><img src="images/mrvsol.jpg" alt=""><img src="images/rjv.jpg" alt=""><img src="images/JOOBLE.jpg" alt=""></div>
        </div>
    </div>
    <div class="section-13"></div>
@endsection
