@extends('frontend.layouts.master')

@section('title', 'RMJ Auto Loan Application Form')

@section('description', 'When you’re looking for a car, you’re not just looking for a vehicle that suits you, you’re looking for an investment: something that will last through the years, for yourself and your family.')

@section('content')
    <link href="{{asset('plugins/jquery-steps/demo/css/jquery.steps.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/bootstrap_trimmed.min.css')}}" rel="stylesheet" type="text/css">
    <script src="{{url('plugins/jquery-steps/lib/jquery-1.11.1.min.js')}}"></script>
    <script src="{{url('plugins/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{url('plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <style>
        .wizard > .content > .body {
            background-image: none;
        }
        @media screen and (max-width: 480px) {
            .steps > ul {
                padding-left:2px;
                padding-right:2px;
            }
            .steps > ul > li {
                display:block !important;
                width:100%;
                margin:0px;
            }
            .steps > ul > li.active {
                border-bottom:1px solid #ddd!important;
                margin: 0px;
            }
        }

        .loading:after {
            position: absolute;
            content: '';
            top: 50%;
            left: 50%;
            margin: -1.5em 0 0 -1.5em;
            width: 3em;
            height: 3em;
            -webkit-animation: spin .6s linear;
            animation: spin .6s linear;
            -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
            border-radius: 7000px;
            border-color: #767676 rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.1);
            border-style: solid;
            border-width: .2em;
            -webkit-box-shadow: 0 0 0 1px transparent;
            box-shadow: 0 0 0 1px transparent;
            visibility: visible;
            z-index: 101;
        }
        .loading{position:relative;cursor:default;pointer-events:none}
        .loading:before{position:absolute;top:0;left:0;background:rgba(255,255,255,.8);width:100%;height:100%;z-index:100}
    </style>
    <div class="row" style="margin-top: 30px;">
        <div class="col-md-8 col-md-offset-2">
            <form id="apply-form" action="{{route('rmjauto.applypost')}}" method="POST">
                {{csrf_field()}}
                <h3>Before we begin</h3>
                <section>
                    <div class="col-md-6">
                        <label for="car_brand">Car Brand <span style="color: #db2828;">*</span></label>
                        <select class="form-control" id="car_brand" name="car_brand" required>
                            <option value="">Please Select a Car Brand</option>
                            @foreach($brands as $brand)
                                <option value="{{$brand->id}}">{{$brand->name}}</option>
                            @endforeach
                        </select>
                        <div class="">
                        <label for="car_model">Car Model <span style="color: #db2828;">*</span></label>
                        <select class="form-control" id="car_model" name="car_model" required>

                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="loan_term">Loan Term <span style="color: #db2828;">*</span></label>
                        <select class="form-control " id="loan_term" name="loan_term" required>
                            @foreach($loan->getLoanTerm() as $value => $loan_term)
                                <option value="{{$value}}">{{$loan_term}}</option>
                            @endforeach
                        </select>
                    </div>
                </section>
                <h3>Personal Information</h3>
                <section>
                    <div class="col-md-4">
                        <label for="first_name">First Name<span style="color: #db2828;">*</span></label>
                        <input class="form-control" id="first_name" name="first_name" required />
                    </div>
                    <div class="col-md-4">
                        <label for="middle_name">Middle Name <span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="middle_name" name="middle_name" required />
                    </div>
                    <div class="col-md-4">
                        <label for="last_name">Last Name <span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="last_name" name="last_name" required />
                    </div>
                    <div class="col-md-4">
                        <label for="birthday">Birthday <span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="date" id="birthday" name="birthday" required />
                    </div>
                    <div class="col-md-4">
                        <label for="mobile_number">Mobile Number <span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="mobile_number" name="mobile_number" required />
                    </div>
                    <div class="col-md-4">
                        <label for="email_address">Email Address <span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="email" id="email_address" name="email_address" required />
                    </div>
                    <div class="col-md-6">
                        <label for="home_address">Home Address <span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="home_address" name="home_address" required />
                    </div>
                    <div class="col-md-6">
                        <label for="facebook_account">Facebook Account <span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="facebook_account" name="facebook_account" placeholder="https://www.facebook.com/youraccount" required />
                    </div>
                    <div class="col-md-4">
                        <label for="home_ownership">Home Ownership <span style="color: #db2828;">*</span></label>
                        <select class="form-control" id="home_ownership" name="home_ownership" required>
                            @foreach($loan->getHomeOwnership() as $home_ownership)
                                <option value="{{$home_ownership}}">{{$home_ownership}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="length_of_stay">Length of Stay <span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="number" id="length_of_stay" name="length_of_stay" required />
                    </div>
                    <div class="col-md-4">
                        <label for="civil_status">Civil Status <span style="color: #db2828;">*</span></label>
                        <select class="form-control" id="civil_status" name="civil_status" required>
                            @foreach($loan->getCivilStatus() as $civil_status)
                                <option value="{{$civil_status}}">{{$civil_status}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label for="employer_business_name">Employer Business Name<span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="employer_business_name" name="employer_business_name" required />
                    </div>
                    <div class="col-md-4">
                        <label for="tenure">Tenure<span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="tenure" name="tenure" required />
                    </div>
                    <div class="col-md-4">
                        <label for="office_telephone">Office Telephone<span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="office_telephone" name="office_telephone" required />
                    </div>
                    <div class="col-md-6">
                        <label for="office_address">Office/Business Address<span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="office_address" name="office_address" required />
                    </div>
                    <div class="col-md-6">
                        <label for="monthly_income">Monthly Income<span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="number" id="monthly_income" name="monthly_income" required />
                    </div>
                    <div class="col-md-6">
                        <label for="tin_number">TIN Number<span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="tin_number" name="tin_number" required />
                    </div>
                    <div class="col-md-6">
                        <label for="sss_number">SSS Number<span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="sss_number" name="sss_number" required />
                    </div>
                    <div class="col-md-12">
                        <label>I have a Co-Borrower
                            <input style="display: inline-block;" type="radio" id="yes_co_borrower" name="co_borrower" value="1">
                            <label for="yes_co_borrower">Yes</label>
                            <input style="display: inline-block;" type="radio" id="no_co_borrower" name="co_borrower" value="0" checked>
                            <label for="no_co_borrower">No</label>
                            <span style="color: #db2828;">*</span>
                        </label>
                    </div>
                    <div id="co_borrower_fields" style="display:none;">
                    <div class="col-md-4">
                        <label for="co_borrower_first_name">First Name<span style="color: #db2828;">*</span></label>
                        <input class="form-control" id="co_borrower_first_name" name="co_borrower_first_name" required />
                    </div>
                    <div class="col-md-4">
                        <label for="co_borrower_middle_name">Middle Name <span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="co_borrower_middle_name" name="co_borrower_middle_name" required />
                    </div>
                    <div class="col-md-4">
                        <label for="co_borrower_last_name">Last Name <span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="co_borrower_last_name" name="co_borrower_last_name" required />
                    </div>
                    <div class="col-md-6">
                        <label for="co_borrower_home_address">Home Address <span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="co_borrower_home_address" name="co_borrower_home_address" required />
                    </div>
                    <div class="col-md-6">
                        <label for="co_borrower_birthday">Birthday <span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="date" id="co_borrower_birthday" name="co_borrower_birthday" required />
                    </div>
                    <div class="col-md-4">
                        <label for="co_borrower_employer_business_name">Employer Business Name<span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="co_borrower_employer_business_name" name="co_borrower_employer_business_name" required />
                    </div>
                    <div class="col-md-4">
                        <label for="co_borrower_tenure">Tenure<span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="co_borrower_tenure" name="co_borrower_tenure" required />
                    </div>
                    <div class="col-md-4">
                        <label for="office_telephone">Office Telephone<span style="color: #db2828;">*</span></label>
                        <input class="form-control" type="text" id="co_borrower_office_telephone" name="co_borrower_office_telephone" required />
                    </div>
                    </div>
                </section>
                <h3>Requirements</h3>
                <section>
                    <div class="col-md-4">
                        <h4 style="font-weight: bold;">If Business Owned</h4>
                        <ul>
                            <li>2 Valid IDs</li>
                            <li>DTI</li>
                            <li>Three (3) Months Bank Statements</li>
                            <li>ITR with Financial Statements</li>
                            <li>Proof of Billing</li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h4 style="font-weight: bold;">If Employed</h4>
                        <ul>
                            <li>2 Valid IDs</li>
                            <li>Contract of employment with compensation </li>
                            <li>Three (3) Months Bank Statements</li>
                            <li>Updated Payslips</li>
                            <li>Proof of Billing</li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h4 style="font-weight: bold;">If OFW</h4>
                        <ul>
                            <li>2 Valid IDs</li>
                            <li>Latest Contract</li>
                            <li>Need co maker living in the Philippines </li>
                            <li>Proof of Billing</li>
                        </ul>
                    </div>
                </section>
            </form>
        </div>
    </div>

    <script>
        $(document).ready( function() {
            $("#co_borrower_fields input").attr('disabled',true);
            $('#car_brand').on('change', function () {
                $('#car_model').parent('div').addClass('loading');
                $.ajax({
                    url: '{{route('rmjauto.getmodels')}}'+'?id='+$(this).val(),
                    type: "get",
                    success: function (data) {
                        var options = '';
                        for (var x = 0; x < data.length; x++) {
                            options += '<option value="' + data[x]['id'] + '">' + data[x]['name'] + '</option>';
                        }
                        $('#car_model').parent('div').removeClass('loading');
                        $('#car_model').html(options);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {

                    }
                });
            });
            $("input[type=radio][name='co_borrower']").on('change', function () {
                if (this.value == 1) {
                    $("#co_borrower_fields").show();
                    $("#co_borrower_fields input").attr('disabled',false);
                }
                else if (this.value == 0) {
                    $("#co_borrower_fields").hide();
                    $("#co_borrower_fields input").attr('disabled',true);
                }
            });
        });

        var form = $("#apply-form").show();

        form.steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            onStepChanging: function (event, currentIndex, newIndex)
            {
                // Allways allow previous action even if the current form is not valid!
                if (currentIndex > newIndex)
                {
                    return true;
                }
                // Needed in some cases if the user went back (clean up)
                if (currentIndex < newIndex)
                {
                    // To remove error styles
                    form.find(".body:eq(" + newIndex + ") label.error").remove();
                    form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                }
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onStepChanged: function (event, currentIndex, priorIndex)
            {

            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                form.submit();
            }
        }).validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
        });
    </script>
@endsection
