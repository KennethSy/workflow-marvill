@extends('frontend.layouts.master')

@section('data-wf-page', '5eafb04b153c2820e96acf99')

@section('data-wf-site', '5e952c81d56b94c967145826')

@section('title', 'Marvill Web Development Rates')

@section('description', 'Marvill Web Development Rates')

@section('body-class','body-4')

@section('content')
    <div data-w-id="2460fcc4-b498-4161-9ef0-572823702422" class="section-15">
        <div class="div-block-12">
            <h1 data-w-id="b232c882-deaf-164b-e426-4320a5f44285" style="opacity:0;-webkit-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="heading-13">Plan for everyone.</h1>
            <p data-w-id="9acbe97b-8388-85d2-fc37-e01c0ca47873" style="-webkit-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(-57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="paragraph-13">A great way to publish your business online.</p>
            <div class="w-layout-grid grid-3">
                <div data-w-id="6ea644b3-abb6-e749-ff9c-5b964d026bbf" style="-webkit-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="div-block-ecom">
                    <p class="paragraph-14">One Custom Template Design,<br>CMS Based Website,<br>Joomla Platform or Wordpress.<br>Graphic Design of Logo &amp; Banners.<br>Mobile/Tablet Responsive.<br>Content Polishing before uploading website on hosting (under a subdomain).<br>Implementation of effective Meta Integration for On-Site SEO.<br>Content Polishing before uploading website on hosting (under a subdomain).<br>Social Network Integration (Facebook, Twitter and Instagram Plugins)<br>Proper Menu Structure and Custom Content Formation.<br>Front End Polishing and Finalization of site (Go Live).<br>Basic Documentation of Website Operation (CMS Manual)<br>1 Month free website maintenance (minor revisions &amp; data uploading)<br>1 Month free Off-Site SEO To get discounts,</p>
                </div>
                <div data-w-id="63342756-9215-8f42-835e-fdf67485b652" style="-webkit-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0">
                    <p class="paragraph-15">Basic Information Website</p><img src="images/package-price-basic.png" alt="" class="image-5">
                    <p class="paragraph-16">* No Hidden Fees     * 15 to 30 Working Days</p><a href="#" class="button-3 w-button">GET A QUOTE</a></div>
            </div>
        </div>
    </div>
    <div data-w-id="c010a5ab-cb4c-8add-07ad-13a2aa560a85" class="section-16">
        <div class="div-block-12">
            <h1 data-w-id="d14abbf4-f379-9161-08a2-3d48008a5e2d" style="-webkit-transform:translate3d(57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="heading-14">Full e-Commerce Package</h1>
            <p data-w-id="2eade619-bc2e-81d8-aebb-6ab0f027372f" style="-webkit-transform:translate3d(57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(57PX, 0, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="paragraph-17">We are doing it Custom. Don’t limit your ideas.</p>
            <div class="w-layout-grid grid-4">
                <div data-w-id="097958c1-ea35-65a7-02d4-2ee8d0313d8c" style="-webkit-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0">
                    <p class="paragraph-15">e-Commerce Website</p><img src="images/packagepriceecom.png" alt="" class="image-5">
                    <p class="paragraph-16">* No Hidden Fees     * 3 to 6  Months</p><a href="#" class="button-3 w-button">GET A QUOTE</a></div>
                <div data-w-id="770a38d2-1daa-e13b-1876-693f5619702c" style="-webkit-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, -100PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="div-block-ecom v2">
                    <p class="paragraph-14">One Custom Template Design,<br>Custom Based  or CMS Based Website, Joomla Platform.<br>Content Polishing before uploading website on hosting (under a subdomain).<br>Implementation of effective Meta Integration for On-Site SEO.<br>Social Network Integration (Facebook, Twitter and Instagram Plugins)<br>Proper Menu Structure and Custom Content Formation.<br>Front End Polishing and Finalization of site (Go Live).<br>Implementation of effective Meta Integration for On-Site SEO.<br>Product Integration and Graphic Layout (good for 50 items)<br>Payment Integration with 1 Bank Merchant<br>1 Month free website maintenance (minor revisions &amp; data uploading)<br>1 Month free Off-Site SEO To get discounts</p>
                </div>
            </div>
        </div>
    </div>
    <div data-w-id="2091289c-bf0a-9714-46dc-9ad450bf495f" class="section-17">
        <div class="div-block-12">
            <div>
                <h1 data-w-id="4607f26a-0121-1a82-a885-9e8d4f5424a3" style="-webkit-transform:translate3d(0, -57PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, -57PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, -57PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, -57PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="heading-15">CRM Development</h1>
                <p data-w-id="f9d77e34-9b77-c7df-96e3-160a53fee5c5" style="-webkit-transform:translate3d(0, -57PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, -57PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, -57PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, -57PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);opacity:0" class="paragraph-18">Your Operation to Web Application</p>
            </div>
            <div><img src="images/crm-image.png" srcset="images/crm-image-p-500.png 500w, images/crm-image-p-800.png 800w, images/crm-image-p-1080.png 1080w, images/crm-image.png 1547w" sizes="80vw" data-w-id="b2bad7fd-026d-63e0-9e41-0711c87fd98e" style="opacity:0" alt="" class="image-6"></div>
            <div class="div-block-13">
                <div class="w-layout-grid grid-5">
                    <div><img src="images/pricecrm.png" alt="" class="image-7"><a href="#" class="button-3 w-button">LET&#x27;S TALK</a></div>
                    <div>
                        <p class="paragraph-19">Development Scope</p>
                        <p class="paragraph-14">Custom PHP Base System <br>CRM Dashboard System<br>User Management System<br>Data Report Management System<br>Custom Module ready<br>2 Months Free Support</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
