@extends('frontend.layouts.master')

@section('title', 'RMJ Auto Loan Application Form')

@section('description', 'When you’re looking for a car, you’re not just looking for a vehicle that suits you, you’re looking for an investment: something that will last through the years, for yourself and your family.')

@section('content')
    <link href="{{asset('plugins/jquery-steps/demo/css/jquery.steps.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/bootstrap_trimmed.min.css')}}" rel="stylesheet" type="text/css">
    <script src="{{url('plugins/jquery-steps/lib/jquery-1.11.1.min.js')}}"></script>
    <script src="{{url('plugins/jquery-steps/build/jquery.steps.min.js')}}"></script>
    <script src="{{url('plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <style>
        .wizard > .content > .body {
            background-image: none;
        }
        @media screen and (max-width: 480px) {
            .steps > ul {
                padding-left:2px;
                padding-right:2px;
            }
            .steps > ul > li {
                display:block !important;
                width:100%;
                margin:0px;
            }
            .steps > ul > li.active {
                border-bottom:1px solid #ddd!important;
                margin: 0px;
            }
        }

        .loading:after {
            position: absolute;
            content: '';
            top: 50%;
            left: 50%;
            margin: -1.5em 0 0 -1.5em;
            width: 3em;
            height: 3em;
            -webkit-animation: spin .6s linear;
            animation: spin .6s linear;
            -webkit-animation-iteration-count: infinite;
            animation-iteration-count: infinite;
            border-radius: 7000px;
            border-color: #767676 rgba(0,0,0,.1) rgba(0,0,0,.1) rgba(0,0,0,.1);
            border-style: solid;
            border-width: .2em;
            -webkit-box-shadow: 0 0 0 1px transparent;
            box-shadow: 0 0 0 1px transparent;
            visibility: visible;
            z-index: 101;
        }
        .loading{position:relative;cursor:default;pointer-events:none}
        .loading:before{position:absolute;top:0;left:0;background:rgba(255,255,255,.8);width:100%;height:100%;z-index:100}
    </style>
    <div class="row" style="margin-top: 30px;">
        <div class="col-md-8 col-md-offset-2">
            <h1>Thank you for submitting your application.</h1>
        </div>
    </div>
@endsection
