@extends('frontend.layouts.master')

@section('data-wf-page', '5e952c81d56b94e60f145827')

@section('data-wf-site', '5e952c81d56b94c967145826')

@section('title', 'Marvill Web Development Services')

@section('description', 'The number 1 web development company in the Philippines | The art of web design | Good Concepts Best Creations.')

@section('body-class','body')

@section('content')
    <div data-w-id="74b6ff7c-242c-7a4f-3307-2f57f945ea20" class="section">
        <div data-delay="4000" data-animation="outin" data-autoplay="1" data-hide-arrows="1" data-duration="1500" data-infinite="1" class="slider w-slider">
            <div class="w-slider-mask">
                <div class="w-slide">
                    <div class="text-block">let your ideas<br>turn into reality</div>
                    <p class="paragraph">we are marvill web development<br>and we are more than ten years in the business</p><a href="#" class="button w-button">get a free quote</a></div>
            </div>
            <div class="w-slider-arrow-left"></div>
            <div class="w-slider-arrow-right">
                <div class="w-icon-slider-right"></div>
            </div>
            <div class="slide-nav w-slider-nav w-shadow"></div>
        </div>
        <div class="w-row">
            <div class="column-9 w-col w-col-2"></div>
            <div class="column-8 w-col w-col-10">
                <div data-poster-url="videos/marvill-logo-reveal-poster-00001.jpg" data-video-urls="videos/marvill-logo-reveal-transcode.mp4,videos/marvill-logo-reveal-transcode.webm" data-autoplay="true" data-loop="true" data-wf-ignore="true" class="background-video-2 w-background-video w-background-video-atom"><video autoplay="" loop="" style="background-image:url(&quot;videos/marvill-logo-reveal-poster-00001.jpg&quot;)" muted="" playsinline="" data-wf-ignore="true" data-object-fit="cover"><source src="videos/marvill-logo-reveal-transcode.mp4" data-wf-ignore="true"><source src="videos/marvill-logo-reveal-transcode.webm" data-wf-ignore="true"></video></div>
            </div>
        </div>
    </div>
    <div class="section-5">
        <div data-duration-in="300" data-duration-out="100" data-easing="linear" class="tabs mw-tabs w-tabs">
            <div class="tabs-menu w-tab-menu">
                <a data-w-tab="Tab 1" class="tab-link-tab-1 w-inline-block w-tab-link">
                    <div class="text-block-4">web development</div>
                </a>
                <a data-w-tab="Tab 2" class="tab-link-tab-2 w-inline-block w-tab-link w--current">
                    <div class="text-block-5">web crm</div>
                </a>
                <a data-w-tab="Tab 3" class="tab-link-tab-3 w-inline-block w-tab-link">
                    <div class="text-block-3">seo</div>
                </a>
                <a data-w-tab="Tab 4" class="tab-link-tab-4 w-inline-block w-tab-link">
                    <div class="text-block-6">micro controllers</div>
                </a>
            </div>
            <div class="tabs-content w-tab-content">
                <div data-w-tab="Tab 1" class="tab-pane-tab-1-2 w-tab-pane">
                    <div class="w-row">
                        <div class="w-col w-col-3 w-col-tiny-tiny-stack"></div>
                        <div class="w-col w-col-9 w-col-tiny-tiny-stack">
                            <h1 class="heading-8">WEB DEVELOPMENT</h1>
                            <div class="text-block-2">The web development industry has been a powerful tool to reach clients in all parts of the world. Marvill provides high end website development for all kinds of business. With 10 years of experience, Marvill has completed hundreds of site for all kinds of business. With design as its core strength Marvill has live by your imagination and we put them to life. Still trusted to deliver website of excellence and elegance it continues to grow in providing benchmark designs for UI/UX.<br></div>
                        </div>
                    </div>
                    <div class="div-block-14"><img src="images/gear.png" width="669" data-w-id="e876a81e-55ff-35b3-4050-00a2baf8de42" alt="" class="image-8 s1"></div>
                </div>
                <div data-w-tab="Tab 2" class="tab-pane-tab-2-2 w-tab-pane w--tab-active">
                    <div class="columns-5 w-row">
                        <div class="w-col w-col-3 w-col-small-small-stack w-col-tiny-tiny-stack"></div>
                        <div class="w-col w-col-9 w-col-small-small-stack w-col-tiny-tiny-stack">
                            <h1 class="heading-8">WEB CRM</h1>
                            <div item="content" class="text-block-2">The web has been a proven to be a powerful tool for coordination and logistics. With the team of developers from Marvill, it has customised workflows for several business with integration with numerous payment gateways as well as integration with all other platforms available for deployment. Marvill has proven to be able to handle local and international api in different platforms to work hand in hand with your custom office system. In todays age, managing your logistics has been more simpler by using all sorts of platforms.<br><br></div>
                        </div>
                    </div>
                    <div class="div-block-14"><img src="images/report.png" width="669" data-w-id="79c9fb14-6b2f-b65d-052b-a4069083dcef" style="-webkit-transform:translate3d(0, 0, 0) scale3d(0.6, 0.6, 0.6) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 0, 0) scale3d(0.6, 0.6, 0.6) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 0, 0) scale3d(0.6, 0.6, 0.6) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 0, 0) scale3d(0.6, 0.6, 0.6) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform-style:preserve-3d;opacity:1" alt="" class="image-8"></div>
                </div>
                <div data-w-tab="Tab 3" class="tab-pane-tab-3-2 w-tab-pane">
                    <div class="columns-5 w-row">
                        <div class="column-13 w-col w-col-6 w-col-small-small-stack"></div>
                        <div class="w-col w-col-6 w-col-small-small-stack">
                            <h1 class="heading-8">SEARCH ENGINE OPTIMIZATION</h1>
                            <div class="text-block-2">With the growing age for marketing, the web has been a powerful companion to reach more clients for your business. With Marvill, you will never be in risk of spending more than what you need with its conservative but effective approach in Search Engine Optimisation. Marvill understood that you want to be out there, but with proper SEO you won’t rely on fake or irrelevant traffic. Marvill continues to help its client by allocation and budgeting for paid ad service and the proper on site SEO to get the most out of your website.<br></div>
                        </div>
                    </div>
                    <div class="div-block-14"><img src="images/serarch.png" width="669" data-w-id="39183ffc-225b-c1c7-93ba-eb80d644507b" alt="" class="image-8"></div>
                </div>
                <div data-w-tab="Tab 4" class="tab-pane-tab-4-2 w-tab-pane">
                    <div class="columns-5 w-row">
                        <div class="w-col w-col-3"></div>
                        <div class="w-col w-col-9">
                            <h1 class="heading-8">MICRO CONTROLLERS</h1>
                            <div class="text-block-2">The internet of things is just around the corner. In todays technology, you might be surprised that your house can be monitored and manage over the web already, this has been possible with the use of micro controllers. Marvill with its team of hardware specialist has provided custom designs and hardware implementations to integrate with WEB CRM as well as local or offline solutions. The strength of Marvill lies in a deep understanding of what is the most effective tools accompanied by a strong network provision by CISCO accredited network developers.<br><br></div>
                        </div>
                    </div>
                    <div class="div-block-14"><img src="images/micro.png" width="669" data-w-id="8ca28add-39c9-6bc0-2796-6d8395ee7030" alt="" class="image-8"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-6">
        <div>
            <div class="columns-6 w-row">
                <div class="column-12 w-col w-col-6">
                    <div data-delay="5000" data-animation="outin" data-autoplay="1" data-duration="500" data-infinite="1" class="slider-5 w-slider">
                        <div class="mask w-slider-mask">
                            <div class="slide-6 w-slide"><img src="images/laravel.jpg" width="622" alt="" class="image-4"></div>
                            <div class="slide-7 w-slide"><img src="images/php.jpg" width="622" alt="" class="image-4"></div>
                            <div class="slide-8 w-slide"><img src="images/joomla.jpg" width="622" alt="" class="image-4"></div>
                            <div class="slide-9 w-slide"><img src="images/wp.jpg" width="622" alt="" class="image-4"></div>
                            <div class="slide-10 w-slide"><img src="images/bootstrap.jpg" width="622" alt="" class="image-4"></div>
                            <div class="slide-11 w-slide"><img src="images/arduino.jpg" width="622" alt="" class="image-4"></div>
                        </div>
                        <div class="w-slider-arrow-left"></div>
                        <div class="w-slider-arrow-right"></div>
                        <div class="w-slider-nav"></div>
                    </div>
                </div>
                <div class="w-col w-col-6">
                    <h1 class="heading-6"><strong item="title" class="bold-text">platforms + framworks </strong></h1>
                    <p class="paragraph-7">PROVIDING THE BEST SOLUTION IN ALL OUR CLIENTS</p><a href="#" class="button-2 w-button">OUR SERVICES</a></div>
            </div>
        </div>
    </div>
    <div class="section-3">
        <div class="columns-2 w-row">
            <div class="column-3 w-col w-col-5">
                <div class="div-block-5">
                    <h1 class="heading-6"><strong>awesome</strong> clients</h1>
                    <p class="paragraph-7">we are more than happy to serve</p><a href="#" class="button-2 w-button">OUR PORTFOLIO</a></div>
            </div>
            <div class="column-4 w-col w-col-7">
                <div data-delay="4000" data-animation="outin" data-autoplay="1" data-duration="500" data-infinite="1" class="slider-3 w-slider">
                    <div class="w-slider-mask">
                        <div class="w-slide"><img src="images/beauty1.png" width="500.5" srcset="images/beauty1-p-500.png 500w, images/beauty1-p-800.png 800w, images/beauty1.png 925w" sizes="(max-width: 479px) 96vw, (max-width: 767px) 97vw, 56vw" alt="" class="image-2"></div>
                        <div class="slide-3 w-slide"><img src="images/asteria.png" width="500.5" srcset="images/asteria-p-500.png 500w, images/asteria-p-800.png 800w, images/asteria.png 1001w" sizes="(max-width: 479px) 96vw, (max-width: 767px) 97vw, (max-width: 1787px) 56vw, 1001px" alt="" class="image-2"></div>
                        <div class="w-slide"><img src="images/razons.png" width="500.5" srcset="images/razons-p-500.png 500w, images/razons-p-800.png 800w, images/razons.png 1001w" sizes="(max-width: 479px) 96vw, (max-width: 767px) 97vw, (max-width: 1787px) 56vw, 1001px" alt="" class="image-2"></div>
                    </div>
                    <div class="w-slider-arrow-left"></div>
                    <div class="w-slider-arrow-right"></div>
                    <div class="w-slider-nav"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-7">
        <div class="w-layout-grid grid-6"><img src="images/googleforwork.jpg" alt=""><img src="images/cloudflare.jpg" alt=""><img src="images/mrvsol.jpg" alt=""><img src="images/grammarly.jpg" alt=""><a href="https://ph.jooble.org/jobs-web-development" target="_blank" class="w-inline-block"><img src="images/jooble_1.jpg" alt="" class="image-9"></a></div>
    </div>
    <div class="section-4">
        <div class="w-row">
            <div class="column-11 w-col w-col-5">
                <h1 class="heading-9">SEND US AN EMAIL</h1>
                <p>We will get back with you as soon as possible.</p>
                <div class="w-form">
                    <form id="contact-form" action="{{route('submit.inquiry')}}" method="POST">
                        {{csrf_field()}}
                        <div class="contact-form-grid">
                            <div id="w-node-e655e5e82a9f-ba54634e"><label for="First-Name" id="contact-first-name">First name *</label><input type="text" class="w-input" maxlength="256" name="First-Name" data-name="First Name" id="First-Name" required=""></div>
                            <div id="w-node-e655e5e82aa3-ba54634e"><label for="Last-Name" id="contact-last-name">Last name *</label><input type="text" class="w-input" maxlength="256" name="Last-Name" data-name="Last Name" id="Last-Name" required=""></div>
                            <div id="w-node-e655e5e82aa7-ba54634e"><label for="Email" id="contact-email">Email *</label><input type="email" class="w-input" maxlength="256" name="Email" data-name="Email" id="Email" required=""></div>
                            <div id="w-node-e655e5e82aab-ba54634e"><label for="Contact-Phone-Number" id="contact-phone">Phone number</label><input type="tel" class="w-input" maxlength="256" name="Contact-Phone-Number" data-name="Contact Phone Number" id="Contact-Phone-Number"></div>
                            <div id="w-node-e655e5e82aaf-ba54634e"><label for="Message" id="contact-message">Message</label><textarea data-name="Message" maxlength="5000" id="Message" name="Message" class="w-input"></textarea></div>
                        </div><input type="submit" value="Submit" data-wait="Please wait..." class="w-button"></form>
                    <div class="w-form-done">
                        <div>Thank you! Your submission has been received!</div>
                    </div>
                    <div class="w-form-fail">
                        <div>Oops! Something went wrong while submitting the form.</div>
                    </div>
                </div>
                <p class="paragraph-4">WORKING TIME<br>Monday – Friday : 09.00 – 18.00<br>Saturday : 09.00 – 12.00<br>Sunday : Close<br>‍<br>Unit 312 Tower A, The Palm Tower Condominium,<br>St. Paul Street, San Antonio Village, Makati 1203</p>
            </div>
            <div class="column-10 w-col w-col-7">
                <h1 class="heading-9">let’s get connected</h1>
                <div data-duration-in="300" data-duration-out="100" class="tabs-2 w-tabs">
                    <div class="tabs-menu-3 w-tab-menu">
                        <a data-w-tab="Tab 1" class="tab-link-tab-1-2 w-inline-block w-tab-link w--current">
                            <div class="text-block-11">INSTAGRAM</div>
                        </a>
                        <a data-w-tab="Tab 2" class="tab-link-tab-2-3 w-inline-block w-tab-link">
                            <div class="text-block-12">TWITTER</div>
                        </a>
                        <a data-w-tab="Tab 3" class="tab-link-tab-3-3 w-inline-block w-tab-link">
                            <div class="text-block-13">FACEBOOK</div>
                        </a>
                        <a data-w-tab="Tab 4" class="tab-link-tab-4-3 w-inline-block w-tab-link">
                            <div class="text-block-14">YOUTUBE</div>
                        </a>
                    </div>
                    <div class="tabs-content-2 w-tab-content">
                        <div data-w-tab="Tab 1" class="tab-pane-tab-1 w-tab-pane w--tab-active">
                            <div class="w-embed w-script">
                                <script src="https://apps.elfsight.com/p/platform.js" defer=""></script>
                                <div class="elfsight-app-c3f2123a-2c41-4168-b111-f6fd08657e32"></div>
                            </div>
                        </div>
                        <div data-w-tab="Tab 2" class="tab-pane-tab-2 w-tab-pane">
                            <div class="w-embed w-script">
                                <script src="https://apps.elfsight.com/p/platform.js" defer=""></script>
                                <div class="elfsight-app-5891757c-b49b-459b-89b5-57a3152ca835"></div>
                            </div>
                        </div>
                        <div data-w-tab="Tab 3" class="tab-pane-tab-3 w-tab-pane">
                            <div class="w-embed w-script">
                                <script src="https://apps.elfsight.com/p/platform.js" defer=""></script>
                                <div class="elfsight-app-af652da8-ed44-4cab-a517-2927a5c5dcde"></div>
                            </div>
                        </div>
                        <div data-w-tab="Tab 4" class="tab-pane-tab-4 w-tab-pane">
                            <div class="html-embed-2 w-embed w-script">
                                <script src="https://apps.elfsight.com/p/platform.js" defer=""></script>
                                <div class="elfsight-app-066a6542-2c13-418b-9d2b-1c576dd202ee"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
