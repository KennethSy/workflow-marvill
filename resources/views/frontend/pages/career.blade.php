@extends('frontend.layouts.master')

@section('data-wf-page', '5eac045946d66a767bc06bd7')

@section('data-wf-site', '5e952c81d56b94c967145826')

@section('title', 'Career #JoinMarvillTeam')

@section('description', 'Career #JoinMarvillTeam')

@section('body-class','body-3')

@section('content')
    <section id="contact-form" class="contact-form">
        <div class="w-container">
            <h2>Work with us</h2>
            <p>Join our awesome team!</p>
            <div id="formInstructions" class="small-text"><em>Fields marked with an asterisk (*) are required.</em></div>
            <div class="w-form">
                <form id="wf-form-Contact-Form" name="wf-form-Contact-Form" data-name="Contact Form">
                    <div class="contact-form-grid">
                        <div id="w-node-1e932dc6c60c-7bc06bd7"><label for="First-Name" id="contact-first-name">First name *</label><input type="text" class="w-input" maxlength="256" name="First-Name" data-name="First Name" id="First-Name" required=""></div>
                        <div id="w-node-1e932dc6c610-7bc06bd7"><label for="Last-Name" id="contact-last-name">Last name *</label><input type="text" class="w-input" maxlength="256" name="Last-Name" data-name="Last Name" id="Last-Name" required=""></div>
                        <div id="w-node-1e932dc6c614-7bc06bd7"><label for="Email" id="contact-email">Email *</label><input type="email" class="w-input" maxlength="256" name="Email" data-name="Email" id="Email" required=""></div>
                        <div id="w-node-1e932dc6c618-7bc06bd7"><label for="Contact-Phone-Number" id="contact-phone">Phone number</label><input type="tel" class="w-input" maxlength="256" name="Contact-Phone-Number" data-name="Contact Phone Number" id="Contact-Phone-Number"></div>
                        <div id="w-node-1e932dc6c61c-7bc06bd7"><label for="Message" id="contact-message">Cover Letter</label><textarea data-name="Message" maxlength="5000" id="Message" name="Message" class="w-input"></textarea></div>
                    </div><input type="submit" value="Submit" data-wait="Please wait..." class="w-button"></form>
                <div class="w-form-done">
                    <div>Thank you! Your submission has been received!</div>
                </div>
                <div class="w-form-fail">
                    <div>Oops! Something went wrong while submitting the form.</div>
                </div>
            </div>
        </div>
    </section>
    <div>
        <div class="w-layout-grid grid-7">
            <div class="w-embed w-iframe"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.6224707047813!2d121.0081883152896!3d14.5635688898261!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c97533f4050d%3A0xe1c75486d1a9e452!2sMarvill!5e0!3m2!1sen!2sph!4v1589174545376!5m2!1sen!2sph" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
            <div class="w-embed w-script">
                <script src="https://apps.elfsight.com/p/platform.js" defer=""></script>
                <div class="elfsight-app-c3f2123a-2c41-4168-b111-f6fd08657e32"></div>
            </div>
        </div>
    </div>
@endsection
