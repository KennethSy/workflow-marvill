<header id="nav" class="sticky-nav">
    <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar w-nav">
        <div>
            <a href="{{route('frontend.home')}}" aria-current="page" class="brand w-nav-brand w--current">
                <img src="{{asset('images/marvilllogo.png')}}" alt="" class="nav-logo">
            </a>
            <nav role="navigation" class="nav-menu w-nav-menu">
                <a href="{{route('frontend.about')}}" class="nav-link-4">ABOUT</a>
                <a href="#" class="nav-link-4">WORK</a>
                <a href="{{route('frontend.services')}}" class="nav-link-4">SERVICES</a>
                <a href="{{route('frontend.careers')}}" class="nav-link-4">CAREERS</a>
                <a href="#blog" class="nav-link-4">BLOGS</a>
                <a href="{{route('frontend.contactus')}}" class="nav-link-4">CONTACT US</a>
                <a href="{{url('/admin/login')}}" class="nav-link-4 worfklow">WORKFLOW</a></nav>
            <div class="w-nav-button">
                <div class="w-icon-nav-menu"></div>
            </div>
        </div>
    </div>
</header>
