<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=320, initial-scale=1" />
  <title>Cleave Confirm</title>
  <style type="text/css" media="screen">

    /* ----- Client Fixes ----- */

    /* Force Outlook to provide a "view in browser" message */
    #outlook a {
      padding: 0;
    }

    /* Force Hotmail to display emails at full width */
    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    /* Force Hotmail to display normal line spacing */
    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }


     /* Prevent WebKit and Windows mobile changing default text sizes */
    body, table, td, p, a, li, blockquote {
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
    }

    /* Remove spacing between tables in Outlook 2007 and up */
    table, td {
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    /* Allow smoother rendering of resized image in Internet Explorer */
    img {
      -ms-interpolation-mode: bicubic;
    }

     /* ----- Reset ----- */

    html,
    body,
    .body-wrap,
    .body-wrap-cell {
      margin: 0;
      padding: 0;
      background: #ffffff;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 16px;
      color: #89898D;
      text-align: left;
    }

    img {
      border: 0;
      line-height: 100%;
      outline: none;
      text-decoration: none;
    }

    table {
      border-collapse: collapse !important;
    }

    td, th {
      text-align: left;
      font-family: Arial, Helvetica, sans-serif;
      font-size: 16px;
      color: #89898D;
      line-height:1.5em;
    }

    /* ----- General ----- */

    h1, h2 {
      line-height: 1.1;
      text-align: right;
    }

    h1 {
      margin-top: 0;
      margin-bottom: 10px;
      font-size: 24px;
    }

    h2 {
      margin-top: 0;
      margin-bottom: 60px;
      font-weight: normal;
      font-size: 17px;
    }

    .outer-padding {
      padding: 50px 0;
    }

    .col-1 {
      border-right: 1px solid #D9DADA;
      width: 180px;
    }

    td.hide-for-desktop-text {
      font-size: 0;
      height: 0;
      display: none;
      color: #ffffff;
    }

    img.hide-for-desktop-image {
      font-size: 0 !important;
      line-height: 0 !important;
      width: 0 !important;
      height: 0 !important;
      display: none !important;
    }

    .body-cell {
      background-color: #ffffff;
      padding-top: 60px;
      vertical-align: top;
    }

    .body-cell-left-pad {
      padding-left: 30px;
      padding-right: 14px;
    }

    /* ----- Modules ----- */

    .brand td {
      padding-top: 25px;
    }

    .brand a {
      font-size: 16px;
      line-height: 59px;
      font-weight: bold;
    }

    .data-table th,
    .data-table td {
      width: 350px;
      padding-top: 5px;
      padding-bottom: 5px;
      padding-left: 5px;
    }

    .data-table th {
      background-color: #f9f9f9;
      color: #f8931e;
    }

    .data-table td {
      padding-bottom: 30px;
    }

    .data-table .data-table-amount {
      font-weight: bold;
      font-size: 20px;
    }


  </style>

  <style type="text/css" media="only screen and (max-width: 650px)">
    @media only screen and (max-width: 650px) {
      table[class*="w320"] {
        width: 320px !important;
      }

      td[class*="col-1"] {
        border: none;
      }

      td[class*="hide-for-mobile"] {
        font-size: 0 !important; line-height: 0 !important; width: 0 !important;
        height: 0 !important; display: none !important;
      }

      img[class*="hide-for-desktop-image"]{
        width: 176px !important;
        height: 135px !important;
        display:block !important;
        padding-left: 60px;
      }

      td[class*="hide-for-desktop-image"] {
        width: 100% !important;
        display: block !important;
        text-align: right !important;
      }

      td[class*="hide-for-desktop-text"] {
        display: block !important;
        text-align: center !important;
        font-size: 16px !important;
        height: 61px !important;
        padding-top: 30px !important;
        padding-bottom: 20px !important;
        color: #89898D !important;
      }

      td[class*="mobile-padding"] {
        padding-top: 15px;
      }

      td[class*="outer-padding"] {
        padding: 0 !important;
      }

      td[class*="body-cell-left-pad"] {
        padding-left: 20px;
        padding-right: 20px;
      }
    }

  </style>
</head>

<body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
<p>&nbsp;</p>
<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tbody>
<tr>
<td class="outer-padding" align="left" valign="top"><center>
<table class="w320" width="600" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="col-1 hide-for-mobile">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="hide-for-mobile" style="padding: 30px 0 10px 0;">&nbsp;</td>
</tr>
<tr>
<td class="hide-for-mobile" valign="top" height="150"><br /><img style="line-height: 16px;" src="https://scontent.fmnl4-1.fna.fbcdn.net/v/t1.0-9/69024054_103241867703901_3571643572983693312_o.jpg?_nc_cat=103&amp;_nc_sid=85a577&amp;_nc_eui2=AeHnoL6JOU1526CZ3fDxsM0B6y4KFt7_j8jrLgoW3v-PyNeZ0Gg9Ay1JniFX5M04Zbo&amp;_nc_ohc=n_gJCf6cRBUAX8YihHG&amp;_nc_ht=scontent.fmnl4-1.fna&amp;oh=b4c7c1f0dfbba8d9ac01a99bba208e73&amp;oe=5F3680C6" alt="logo" width="328" height="329" /></td>
</tr>
<tr>
<td class="hide-for-mobile" style="height: 180px; width: 299px;">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
<td class="col-2" valign="top">
<table style="width: 121.675%;" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="body-cell body-cell-left-pad" style="width: 100%;" valign="top" width="355" height="661">
<table style="width: 226px;" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 225.333px;">
<h1>&nbsp;</h1>
<h2><strong>{{ $date }}</strong></h2>
</td>
</tr>
<tr>
<td style="width: 225.333px;">Hello {{ $name }},<br /><br />We would like you to know that your order has been confirmed and is now being prepared.</td>
</tr>
</tbody>
</table>
<table style="width: 100%;" width="100%" cellspacing="0" cellpadding="10">
<tbody>
<tr>
<td class="hide-for-mobile" style="width: 12.3348%;" width="94">&nbsp;</td>
<td style="width: 75.3304%; padding-top: 30px; padding-bottom: 30px;" width="150">
<div style="text-align: center; background-color: #48be19;"><!-- [if mso]>
                        <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="#" style="height:35px;v-text-anchor:middle;width:150px;" arcsize="8%" stroke="f" fillcolor="#48be19">
                        <w:anchorlock/>
                        <center style="color:#ffffff;font-family:sans-serif;font-size:16px;">My Order</center>
                        </v:roundrect>
                        <![endif]--> <!-- [if !mso]><!-- --><!--<![endif]--></div>
</td>
<td class="hide-for-mobile" style="width: 12.3348%;" width="94">&nbsp;</td>
</tr>
</tbody>
</table>
<div class=" col-xs-12  cardp">&nbsp;</div>
<div class=" col-xs-12  cardp">
<h3>ORDER SUMMARY</h3>
<hr /><!--?php  $i = 0; ?--></div>
<div class=" col-xs-12  cardp">QTY ITEM</div>
<div class=" col-xs-12  cardp">@foreach (array_combine($qty , $order) as $orderqty => $orderitem )</div>
<div class=" col-xs-12  cardp">&nbsp;</div>
<div class=" col-xs-12  cardp">@if($orderqty != 0)</div>
<div class=" col-xs-12  cardp">{{ $orderqty }} x {{ $orderitem }}</div>
<div class=" col-xs-12  cardp">@endif</div>
<div class=" col-xs-12  cardp">@endforeach
<table class="table">
<tbody>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<!--?php  $i++;?--><!--?php  $i++;?--></tbody>
</table>
<br />
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td width="355">
<table class="data-table" style="height: 169px;" cellspacing="0" cellpadding="0">
<tbody>
<tr style="height: 22px;">
<th style="height: 22px; width: 351px;"><font color="#E74C3C">Delivery Address</font></th>
</tr>
<tr style="height: 70px;">
<td style="height: 70px; width: 351px;">
<p>{{ $street }}</p>
<p>Barangay {{ $barangay }}</p>
<p>{{ $city }} City</p>
<p>{{ $number }}</p>
</td>
</tr>
<tr style="height: 22px;">
<th style="height: 22px; width: 351px;">&nbsp;</th>
</tr>
<tr style="height: 55px;">
<td style="height: 55px; width: 351px;">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td class="footer" width="355"><center><img src="https://www.filepicker.io/api/file/2KMVSEJSOaxy1uHWWt1A" width="213" height="48" /></center></td>
</tr>
</tbody>
</table>
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="hide-for-desktop-text"><strong> Awesome Co </strong> <br />1337 Swuby Lane<br />Victoria, BC A1B 2C3</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</center></td>
</tr>
</tbody>
</table>
</body>
</html>
