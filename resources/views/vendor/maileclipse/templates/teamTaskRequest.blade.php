<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Zen Flat Confirm Email</title>
  <style type="text/css" media="screen">

    /* Force Hotmail to display emails at full width */
    .ExternalClass {
      display: block !important;
      width: 100%;
    }

    /* Force Hotmail to display normal line spacing */
    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }

    body,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 0;
      padding: 0;
    }

    body,
    p,
    td {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 15px;
      color: #333333;
      line-height: 1.5em;
    }

    h1 {
      font-size: 24px;
      font-weight: normal;
      line-height: 24px;
    }

    body,
    p {
      margin-bottom: 0;
      -webkit-text-size-adjust: none;
      -ms-text-size-adjust: none;
    }

    img {
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    a img {
      border: none;
    }

    .background {
      background-color: #15202b;
    }

    table.background {
      margin: 0;
      padding: 0;
      width: 100% !important;
    }

    .block-img {
      display: block;
      line-height: 0;
    }

    a {
      color: white;
      text-decoration: none;
    }

    a,
    a:link {
      color: #2A5DB0;
      text-decoration: underline;
    }

    table td {
      border-collapse: collapse;
    }

    td {
      vertical-align: top;
      text-align: left;
    }

    .wrap {
      width: 600px;
    }

    .wrap-cell {
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .header-cell,
    .body-cell,
    .footer-cell {
      padding-left: 20px;
      padding-right: 20px;
    }

    .header-cell {
      background-color: #eeeeee;
      font-size: 24px;
      color: #ffffff;
    }

    .body-cell {
      background-color: #15202b;
      padding-top: 30px;
      padding-bottom: 34px;
    }

    .footer-cell {
      background-color: #eeeeee;
      text-align: center;
      font-size: 13px;
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .card {
      width: 400px;
      margin: 0 auto;
    }

    .data-heading {
      text-align: right;
      padding: 10px;
      background-color: #15202b;
      font-weight: bold;
    }

    .data-value {
      text-align: left;
      padding: 10px;
      background-color: #15202b;
    }

    .force-full-width {
      width: 100% !important;
    }

  </style>
  <style type="text/css" media="only screen and (max-width: 600px)">
    @media only screen and (max-width: 600px) {
      body[class*="background"],
      table[class*="background"],
      td[class*="background"] {
        background: #eeeeee !important;
      }

      table[class="card"] {
        width: auto !important;
      }

      td[class="data-heading"],
      td[class="data-value"] {
        display: block !important;
      }

      td[class="data-heading"] {
        text-align: left !important;
        padding: 10px 10px 0;
      }

      table[class="wrap"] {
        width: 100% !important;
      }

      td[class="wrap-cell"] {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
      }
    }
  </style>
</head>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" bgcolor="#15202b;" class="background">
<table class="background" style="border-collapse: collapse; width: 100%; background-color: #15202b;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td class="background" align="center" valign="top" width="100%"><center>
<table class="wrap" width="600" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="wrap-cell" style="padding-top: 30px; padding-bottom: 30px;" valign="top">
<table class="force-full-width" style="border-collapse: collapse; height: 592px; background-color: #15202b; width: 560px;" cellspacing="0" cellpadding="0">
<tbody>
<tr style="height: 60px;">
<td class="header-cell" style="height: 60px; width: 560px;" valign="top" height="60">
<div><img src="{{url('/workflow-v2-logo.png')}}" alt="logo" width="150" height="59" /></div>
</td>
</tr>
<tr style="height: 488px;">
<td class="body-cell" style="height: 488px; width: 560px; background-color: #15202b;" valign="top">
<table style="height: 362px; width: 100%;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#15202b">
<tbody>
<tr style="height: 66px;">
<td style="padding-bottom: 20px; background-color: #15202b; height: 66px; width: 100%;" valign="top"><font color="#ffffff">Hi,</font><br /><br />
<p><font color="#ffffff">Please be informed that a <strong>Team Member</strong> has requested for a <strong>Task</strong>.</font></p>
</td>
</tr>
<tr style="height: 252px;">
<td style="height: 252px;">
<table width="100%" cellspacing="0" cellpadding="0" bgcolor="transparent">
<tbody>
<tr>
<td style="padding: 20px 0px; background-color: #15202b;" align="center"><center>
<table class="card" style="border-collapse: collapse; width: 100%; height: 190px;" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr style="height: 22px;">
<td style="background-color: #1da1f2; text-align: center; padding: 10px; color: white; height: 22px; width: 380px;" align="left"><strong>Task Details</strong></td>
</tr>
<tr style="height: 168px;">
<td style="border: 1px solid green; height: 168px; width: 399px;">
<table style="border-collapse: collapse; width: 100%; border: 1px solid #1da1f2; height: 217px;" width="100%" cellspacing="0" cellpadding="20">
<tbody>
<tr style="height: 217px;">
<td style="width: 100%; border-color: #1da1f2; height: 217px;">
<table style="border-collapse: collapse; width: 100%; height: 128px;" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr style="height: 21px;">
<td class="data-heading" style="height: 21px; width: 50%;" align="left" width="75"><font color="#ffffff">Requested by:</font></td>
<td class="data-value" style="height: 21px; width: 50%;"><font color="#ffffff">{{$name}}</font></td>
</tr>
<tr style="height: 21px;">
<td class="data-heading" style="height: 21px; width: 50%;" width="75"><font color="#ffffff">Company:</font></td>
<td class="data-value" style="height: 21px; width: 50%;"><font color="#ffffff">{{$company}}</font></td>
</tr>
<tr style="height: 21px;">
<td class="data-heading" style="height: 21px; width: 50%;" width="75"><font color="#ffffff">Project:</font></td>
<td class="data-value" style="height: 21px; width: 50%;"><font color="#ffffff">{{$project}}</font></td>
</tr>
<tr style="height: 21px;">
<td class="data-heading" style="height: 21px; width: 50%;" width="75"><font color="#ffffff">Details:</font></td>
<td class="data-value" style="height: 21px; width: 50%;"><font color="#ffffff">{!!$details!!}</font></td>
</tr>
<tr style="height: 44px;">
<td style="width: 100%; height: 44px; background-color: transparent;" colspan="2">
<div style="text-align: center;"><font color="#1da1f2 "><strong><a class="button-mobile" style="background-color: transaprent; border-radius: 4px; color: #1da1f2; display: inline-block; font-family: 'Cabin', Helvetica, Arial, sans-serif; font-size: 14px; font-weight: regular; line-height: 45px; text-align: center; text-decoration: none; width: 155px; -webkit-text-size-adjust: none; mso-hide: all; border: 2px solid #1da1f2;" href="{{$task_link}}">View Task</a></strong></font></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</center></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style="height: 44px;">
<td style="padding-top: 20px; background-color: #15202b; color: #fff; height: 44px;">Thank you!<br /><br /></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style="height: 44px;">
<td class="footer-cell" style="height: 44px; width: 560px; background-color: #15202b;" valign="top">
<p><font color="#ffffff">Worlflow v2</font></p>
<p><strong><font color="#ffffff">Marvill Web Development</font></strong></p>
<p><font color="#ffffff">www.marvill.com</font></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</center></td>
</tr>
</tbody>
</table>
</body>
</html>